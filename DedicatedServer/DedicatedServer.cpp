#include "DedicatedServer.h"

#include "raylib.h"
#include <exception>
#include <stdio.h>

#include "../DunkLib/ClockUtils.h"

#ifndef NO_HOT_RELOAD
#include "../DunkLib/HotReloadBinding.h"
#include "../DunkLib/HotReloadLib.hpp"
#else
#include "../DunkLib/Memory.h"
#include "../../GameplayCode/src/Game.h"

struct game_code
{
	game_update_and_render* UpdateAndRender;
	game_shutdown* Shutdown;
};
int GameUpdateAndRender(Memory* memory, int debugInstance, bool headlessMode)
{
	if (!memory->IsInitialized)
	{
		Game* game = new (memory->PermanentStorage)Game();
		game->Init(debugInstance, headlessMode);
		memory->IsInitialized = true;
	}
	Game* game = (Game*)memory->PermanentStorage;
	return game->Run();
}

void GameShutdown(Memory* memory)
{
	if (memory->IsInitialized)
	{
		Game* game = (Game*)memory->PermanentStorage;
		game->Cleanup();
	}
}
#endif

#include <string>
void GetArgumentValue(std::string argument, std::string name, int& out)
{
	std::string searchString = std::string("-") + name + std::string("=");
	size_t pos = argument.find(searchString);
	if (pos != std::string::npos)
	{
		size_t valueStartPos = pos + searchString.length();
		std::string subString = argument.substr(valueStartPos, argument.length() - valueStartPos);
		int value = stoi(subString);
		out = value;
	}
}
//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
int main(int argc, char** argv)
{
	printf("Starting dedicated server!\n");
	int debugInstance = -1;
	for (int i = 0; i < argc; i++)
	{
		std::string argument = argv[i];
		GetArgumentValue(argument, std::string("debugInstance"), debugInstance);
	}

#ifndef NO_HOT_RELOAD
	const char* sourceDLLName = "GameplayCode.dll";
	game_code gameCode = LoadGameCodeUntilValid(sourceDLLName);
#else
	game_code gameCode;
	gameCode.UpdateAndRender = GameUpdateAndRender;
	gameCode.Shutdown = GameShutdown;
#endif

#if (defined PLATFORM_WEB || defined TESTING_WEB)
	const uint32_t memoryMB = 1024;
#else
	const uint32_t memoryMB = 256;
#endif
	Memory memory = Memory(1024 * 1024 * memoryMB);

	bool runResult = 0;
	while (runResult == 0)
	{
		runResult = gameCode.UpdateAndRender(&memory, debugInstance, true);
		Delay(15);
	}
	gameCode.Shutdown(&memory);
#ifndef NO_HOT_RELOAD
	UnloadGameCode(&gameCode);
#endif

	return 0;
}
