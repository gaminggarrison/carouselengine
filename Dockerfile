FROM emscripten/emsdk:2.0.31
RUN apt-get update
RUN apt-get install -y ninja-build
RUN npm install -g browserify
RUN npm install websocket
RUN npm install wrtc
RUN npm install winston
