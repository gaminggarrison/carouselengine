#include "AllTeamState.h"
#include "VectorUtils.h"
#include "MinigameOverlayState.h"
#include "DrawUtils.h"
#include "NetUtils.h"
#include "NetworkPlayerInfo.h"

void TeamData::AddPlayer(int playerID)
{
	if (!VectorUtils::HasElement(playerID, m_playersOnTeam))
	{
		m_playersOnTeam.push_back(playerID);
	}
}

bool TeamData::HasPlayer(int playerID)
{
	return VectorUtils::HasElement(playerID, m_playersOnTeam);
}

int TeamData::GetPlayerCount()
{
	return (int)m_playersOnTeam.size();
}


TeamData& AllTeamState::AddTeam()
{
	m_teamData.push_back(TeamData());
	TeamData& team = m_teamData[(int)m_teamData.size() - 1];
	return team;
}

TeamData& AllTeamState::GetTeam(int teamID)
{
	return m_teamData[teamID];
}

int AllTeamState::GetPlayerTeamID(int playerID)
{
	for (int i = 0; i < (int)m_teamData.size(); i++)
	{
		if (m_teamData[i].HasPlayer(playerID))
		{
			return i;
		}
	}
	return -1;
}
int AllTeamState::GetTeamCount()
{
	return (int)m_teamData.size();
}

WinnerDisplay AllTeamState::GetTeamName(int teamID)
{
	if (teamID == MinigameOverlayState::WINNER_UNKNOWN)
	{
		return { "Unknown", WHITE };
	}
	else if (teamID == MinigameOverlayState::WINNER_NOBODY)
	{
		return { "Nobody", WHITE };
	}
	TeamData& teamData = GetTeam(teamID);
	if (!m_useTeamColours)
	{
		// Use player's name and colour
		int soloPlayerID = teamData.m_playersOnTeam[0];
		const char* name = NetUtils::GetPlayerInfo(soloPlayerID).m_name.c_str();
		if (TextIsEqual(name, "Client") || TextIsEqual(name, "Host"))
		{
			name = DrawUtils::GetPlayerColourName(soloPlayerID);
		}
		return { name, DrawUtils::GetPlayerColour(soloPlayerID) };
	}
	else
	{
		// Use team name and colour
		return { DrawUtils::GetPlayerColourName(teamID), DrawUtils::GetPlayerColour(teamID) };
	}
}