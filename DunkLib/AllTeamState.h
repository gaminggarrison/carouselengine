#pragma once
#include "cereal/types/static_vector.hpp"
#include "NetworkGameDefines.h"
#include "raylib.h"
#define MAX_TEAM_COUNT 16

struct TeamData
{
	chobo::static_vector<int, PLAYER_COUNT> m_playersOnTeam;
	int m_teamScore = 0;

	void AddPlayer(int playerID);
	bool HasPlayer(int playerID);
	int GetPlayerCount();

	template<class Archive>
	void serialize(Archive& archive, std::uint32_t const version)
	{
		archive(CEREAL_NVP(m_playersOnTeam), CEREAL_NVP(m_teamScore));
	}
};

typedef chobo::static_vector<TeamData, MAX_TEAM_COUNT> PlayerTeamData;

struct WinnerDisplay
{
	const char* m_name;
	Color m_colour;
};

struct AllTeamState
{
	PlayerTeamData m_teamData;
	bool m_useTeamColours = false;

	TeamData& AddTeam();
	TeamData& GetTeam(int teamID);
	int GetPlayerTeamID(int playerID);
	int GetTeamCount();

	WinnerDisplay GetTeamName(int teamID); // Some helpers

	template<class Archive>
	void serialize(Archive& archive, std::uint32_t const version)
	{
		archive(CEREAL_NVP(m_teamData), CEREAL_NVP(m_useTeamColours));
	}
};