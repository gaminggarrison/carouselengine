#include "AudioUtils.h"
#include "raylib.h"
#include <unordered_map>
#include "CameraUtils.h"
#include "MathUtils.h"
#include "TimerUtils.h"
#include "ConfigSet.h"

#include <chrono>
#include <thread>

namespace AudioUtils
{
	struct AudioState
	{
		bool m_isLoop;
		float m_volume;
		float m_lastVolume;
		int m_lastPlayedFrame;
	};
	std::vector<AudioState> s_soundStates;
	float s_effectVolume = 0.5f;
	Music m_currentMusic;
	MusicID m_currentMusicID;
	bool m_isMusicPlaying = false;
	bool m_isHeadless = false;
#ifndef PLATFORM_WEB
	thread m_audioThread;
#endif
	bool m_shuttingDown = false;

	void RunAudioThread()
	{
		while (!m_shuttingDown)
		{
			if (m_isMusicPlaying)
			{
				UpdateMusicStream(m_currentMusic);
				if (!m_currentMusic.looping)
				{
					bool isPlaying = IsMusicPlaying(m_currentMusic);
					if (!isPlaying)
					{
						m_isMusicPlaying = false;
					}
				}
			}
			this_thread::sleep_for(chrono::milliseconds(16));
		}
	}

	namespace
	{
		void JustPlaySoundNoLocation(SoundID soundID)
		{
			float volume = s_effectVolume;
			Sound& sound = soundID.GetData();
			SetSoundVolume(sound, volume);
#if defined PLATFORM_WEB || defined TESTING_WEB
			PlaySound(sound);
#else
			PlaySoundMulti(sound);
#endif
		}
		void JustPlaySound(SoundID soundID, Vector2 location)
		{
			float volume = Clamp01(CameraUtils::GetCurrentScale()) * s_effectVolume;
			Sound& sound = soundID.GetData();
			SetSoundVolume(sound, volume);
#if defined PLATFORM_WEB || defined TESTING_WEB
			PlaySound(sound);
#else
			PlaySoundMulti(sound);
#endif
		}
	}

	void Init(bool headless)
	{
		ResetTimestamps();
		m_audioRandom.Init();
		m_isHeadless = headless;

		m_shuttingDown = false;
#ifndef PLATFORM_WEB
		m_audioThread = thread(RunAudioThread);
#endif
	}
	void ResetTimestamps()
	{
		int countOfAllSounds = SoundID::GetConfigSet().GetCount();
		s_soundStates.clear();
		s_soundStates.resize(countOfAllSounds, { false, 0.0f, 0.0f, -1 });
	}

	void Shutdown()
	{
		m_shuttingDown = true;
#ifndef PLATFORM_WEB
		m_audioThread.join();
#endif
	}
	float GetPositionalVolume(Vector2 location)
	{
		Vector3 cameraPos = CameraUtils::GetAudioPosition();
		Vector3 location3d = { location.x, location.y, 0.0f };
		Vector3 toCamera = Vector3Subtract(location3d, cameraPos);
		float attenuation = Vector3SquaredLength(toCamera);
		const float scaler = 128.0f;
		return Clamp01((scaler * scaler) / attenuation);
	}
	void ResizeToFit(SoundID id)
	{
		if (s_soundStates.size() < id.m_id + 1)
		{
			s_soundStates.resize(id.m_id + 1);
		}
	}
	bool CheckSoundForFrame(SoundID soundID)
	{
		if (m_isHeadless)
		{
			return true;
		}
		if (soundID.m_id < 0)
		{
			return true;
		}
		ResizeToFit(soundID);
		int currentFrame = TimerUtils::GetCurrentFrameNumber();
		// Don't allow the sound if it's older than the last time it was played (UNLESS it's very old, in which case it's probably not a rewind but a save-game load)
		if ((currentFrame <= s_soundStates[soundID.m_id].m_lastPlayedFrame) && (currentFrame >= s_soundStates[soundID.m_id].m_lastPlayedFrame - 60))
		{
			return true;
		}
		s_soundStates[soundID.m_id].m_lastPlayedFrame = currentFrame;
		return false;
	}
	void PlaySoundNoLocation(SoundID soundID)
	{
		if (CheckSoundForFrame(soundID))
		{
			return;
		}
		JustPlaySoundNoLocation(soundID);
	}
	void PlaySoundAtLocation(SoundID soundID, Vector2 location)
	{
		if (CheckSoundForFrame(soundID))
		{
			return;
		}
		JustPlaySound(soundID, location);
	}
	// Overload that picks a random sound from the list
	void PlaySoundAtLocation(std::vector<SoundID>& soundIDs, Vector2 location)
	{
		if (m_isHeadless)
		{
			return;
		}
		int chosenIndex = m_audioRandom.GenerateInt(0, (int)soundIDs.size());
		// Since we can rewind, we need to do the rollback check using a reliable sound ID, so I'll just use the first in the list
		SoundID first = SoundID(soundIDs[0]);
		SoundID chosen = SoundID(soundIDs[chosenIndex]);

		if (CheckSoundForFrame(first))
		{
			return;
		}
		JustPlaySound(chosen, location);
	}

	

	void SetLoopingSound(SoundID soundID, float volume, Vector2 location)
	{
		if (m_isHeadless)
		{
			return;
		}
		if (soundID.m_id < 0)
		{
			return;
		}
		ResizeToFit(soundID);

		Sound& sound = soundID.GetData();
		//float positionalVolume = GetPositionalVolume(location);
		float endVolume = Clamp01(volume * CameraUtils::GetCurrentScale()) * s_effectVolume;
		if (endVolume > s_soundStates[soundID.m_id].m_volume)
		{
			s_soundStates[soundID.m_id].m_volume = endVolume;
			s_soundStates[soundID.m_id].m_isLoop = true;
		}
	}
	void ApplySoundChanges()
	{
		if (m_isHeadless)
		{
			return;
		}
		for (int i = 0; i < s_soundStates.size(); i++)
		{
			AudioState& state = s_soundStates[i];
			if (!state.m_isLoop)
			{
				continue;
			}
			Sound& sound = SoundID::GetConfigSet().Get(i);
			if (IsSoundPlaying(sound))
			{
				if (state.m_volume != state.m_lastVolume)
				{
					SetSoundVolume(sound, state.m_volume);
					state.m_lastVolume = state.m_volume;
				}
			}
			else
			{
				if (state.m_lastVolume > 0 || state.m_volume > 0)
				{
					SetSoundVolume(sound, state.m_volume);
					PlaySound(sound);
				}
				state.m_lastVolume = state.m_volume;
			}
			state.m_volume = 0.0f;
		}

#ifdef PLATFORM_WEB // Web doesn't support threads
		if (m_isMusicPlaying)
		{
			UpdateMusicStream(m_currentMusic);
			if (!m_currentMusic.looping)
			{
				bool isPlaying = IsMusicPlaying(m_currentMusic);
				if (!isPlaying)
				{
					m_isMusicPlaying = false;
				}
			}
		}
#endif
	}
	void PlayMusic(MusicID music, bool looping)
	{
		if (m_isHeadless)
		{
			return;
		}
		StopMusic();
		if (!music.IsBlank())
		{
			m_currentMusicID = music;
			m_currentMusic = music.GetData();
			m_currentMusic.looping = looping;
			m_isMusicPlaying = true;
			PlayMusicStream(m_currentMusic);
		}
	}
	void StopMusic()
	{
		if (m_isHeadless)
		{
			return;
		}
		if (m_isMusicPlaying)
		{
			StopMusicStream(m_currentMusic);
		}
		m_isMusicPlaying = false;
	}
	MusicID GetCurrentlyPlayingMusic()
	{
		if (!m_isMusicPlaying)
		{
			return MusicID::Unknown;
		}
		return m_currentMusicID;
	}
}