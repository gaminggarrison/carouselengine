#pragma once

#include "SoundID.h"
#include "MusicID.h"
struct Vector2;
#include "RandomGenerator.h"
namespace AudioUtils
{
	namespace // anonymous namespace to hide private members
	{
		RandomGenerator m_audioRandom;
		void JustPlaySound(SoundID soundID, Vector2 location);
	}

	void Init(bool headless);
	void ResetTimestamps();
	void Shutdown();
	void PlaySoundNoLocation(SoundID soundID);
	void PlaySoundAtLocation(SoundID soundID, Vector2 location);
	void PlaySoundAtLocation(std::vector<SoundID>& soundIDs, Vector2 location);
	void SetLoopingSound(SoundID soundID, float volume, Vector2 location);
	void ApplySoundChanges();

	void PlayMusic(MusicID music, bool looping = true);
	void StopMusic();
	MusicID GetCurrentlyPlayingMusic();
}