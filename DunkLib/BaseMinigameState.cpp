#include "BaseMinigameState.h"
#include "GameModeTypes.h"
#include "RandomGenerator.h"
#include "DrawUtils.h"
#include "DebugUtils.h"
#include "NetUtils.h"
#include "NetworkPlayerInfo.h"

template<typename PlayerType>
void BaseMinigameState<PlayerType>::Initialise()
{
	m_connectedIDs.resize(PLAYER_COUNT);
	for (int i = 0; i < m_connectedIDs.size(); i++)
	{
		m_connectedIDs[i] = false;
	}
	m_players.clear();
	m_gameMode = GameModeTypeID::None;
	m_allTeamState.m_teamData.clear();
	m_allTeamState.m_useTeamColours = false;
	m_introCounter = 0.0f;
	m_roundStartCounter = 0.0f;
	m_roundEndCounter = 0.0f;
	m_gameEndCounter = 0.0f;
}

template<typename PlayerType>
void BaseMinigameState<PlayerType>::SetupGame()
{
	m_introCounter = GetIntroLength();
	if (InLocalTestingMode())
	{
		m_introCounter = 0.1f;
	}
	m_roundStartCounter = 0.0f;
	m_roundEndCounter = 0.0f;
	m_roundNumber = 0;

	// Pick a game mode
	chobo::static_vector<GameModeTypeID, MAX_GAMEMODE_TYPES> validModes = GameModeTypes::GetValidGameModes(GetPlayerCount());
	if (validModes.empty())
	{
		printf("No valid gamemodes found!\n");
		return;
	}
	int chosenMode = RandomGenerator::Instance()->GenerateInt(0, (int)validModes.size());
	m_gameMode = validModes[chosenMode];

	m_allTeamState.m_teamData.clear();
	// Assign players to teams?
	GameModeType* gamemode = GameModeTypes::GetGameMode(m_gameMode);
	gamemode->AssignPlayersToTeams(m_connectedIDs, m_allTeamState);
}

template<typename PlayerType>
void BaseMinigameState<PlayerType>::AddPlayer(int playerID)
{
	m_connectedIDs[playerID] = true;

	if (m_gameMode != GameModeTypeID::None && m_allTeamState.GetPlayerTeamID(playerID) == -1) // It will be None until it's properly set up with a mode.  Adding players at the start is fine
	{
		GameModeType* gamemode = GameModeTypes::GetGameMode(m_gameMode);
		gamemode->AssignPlayerMidGame(playerID, m_allTeamState);
	}

	// Don't try spawning mid-round unless it's clear
	if (m_roundEndCounter > 0 || HasPlayerEntity(playerID) || m_players.size() == m_players.max_size())
	{
		return;
	}
	SpawnPlayerMidRound(playerID);
	//m_players.push_back(T(playerID));
}

template<typename PlayerType>
void BaseMinigameState<PlayerType>::RemovePlayer(int playerID)
{
	m_connectedIDs[playerID] = false;
	/*for (int i = (int)m_players.size() - 1; i >= 0; i--) // Don't actually remove players mid-round!
	{
		if (m_players[i].m_playerID == playerID)
		{
			m_players.erase(m_players.begin() + i);
		}
	}*/
}


template<typename PlayerType>
bool BaseMinigameState<PlayerType>::HasPlayer(int playerID)
{
	if (playerID < 0 || playerID >= m_connectedIDs.size())
	{
		return false;
	}
	return m_connectedIDs[playerID];
}

template<typename PlayerType>
bool BaseMinigameState<PlayerType>::HasPlayerEntity(int playerID)
{
	for (int i = 0; i < (int)m_players.size(); i++)
	{
		if (m_players[i].m_playerID == playerID)
		{
			return true;
		}
	}
	return false;
}

/*template<typename PlayerType>
PlayerType* BaseMinigameState<PlayerType>::GetPlayerEntity(int playerID)
{
	for (int i = 0; i < (int)m_players.size(); i++)
	{
		if (m_players[i].m_playerID == playerID)
		{
			return &players[i];
		}
	}
	return nullptr;
}*/

template<typename PlayerType>
int BaseMinigameState<PlayerType>::GetPlayerCount()
{
	int total = 0;
	for (int i = 0; i < m_connectedIDs.size(); i++)
	{
		if (m_connectedIDs[i])
		{
			total++;
		}
	}
	return total;
}

template<typename PlayerType>
Color BaseMinigameState<PlayerType>::GetPlayerColour(int playerID)
{
	if (!m_allTeamState.m_useTeamColours)
	{
		return DrawUtils::GetPlayerColour(playerID);
	}
	int playerTeam = -1;
	playerTeam = m_allTeamState.GetPlayerTeamID(playerID);

	if (playerTeam < 0)
	{
		return WHITE;
	}
	else
	{
		return DrawUtils::GetPlayerColour(playerTeam);
	}
}

template<typename PlayerType>
const char* BaseMinigameState<PlayerType>::GetPlayerName(int playerID)
{
	const char* name = NetUtils::GetPlayerInfo(playerID).m_name.c_str();
	if (TextIsEqual(name, "Client") || TextIsEqual(name, "Host"))
	{
		name = DrawUtils::GetPlayerColourName(playerID);
	}
	return name;
}

template<typename PlayerType>
int BaseMinigameState<PlayerType>::GetTeamForPlayer(int playerID)
{
	return m_allTeamState.GetPlayerTeamID(playerID);
}

template<typename PlayerType>
void BaseMinigameState<PlayerType>::BaseSetupRound()
{
	m_roundStartCounter = GetRoundStartLength();
	if (InLocalTestingMode())
	{
		m_roundStartCounter = 0.1f;
	}
	m_roundWinningTeam = WINNER_UNKNOWN;
	m_roundNumber++;
	SetupRound();
}

template<typename PlayerType>
void BaseMinigameState<PlayerType>::Update(float deltaTime, int frameNumber)
{
	if (m_introCounter > 0.0f)
	{
		m_introCounter -= deltaTime;
		if (m_introCounter <= 0.0f)
		{
			BaseSetupRound();
		}
		return;
	}
	if (m_roundStartCounter > 0.0f)
	{
		m_roundStartCounter -= deltaTime;
		return;
	}
	if (m_roundEndCounter > 0.0f)
	{
		float roundEndProportion1To0 = 1.0f - (m_roundEndCounter / GetRoundStartLength());
		roundEndProportion1To0 *= roundEndProportion1To0; // Make it slow down suddenly, then slowly smooth back to full speed to look cooler :)
		float slowMotion = Lerp(0.1f, 1.0f, roundEndProportion1To0);
		this->MinigameUpdate(deltaTime * slowMotion, frameNumber); // Slow motion after round end

		m_roundEndCounter -= deltaTime;
		if (m_roundEndCounter <= 0.0f)
		{
			// Only reset if the game isn't over!
			if (m_gameWinningTeam < 0)
			{
				BaseSetupRound();
			}
			else
			{
				m_gameEndCounter = GetGameEndLength();
			}
		}
		return;
	}
	if (m_gameWinningTeam >= 0)
	{
		m_gameEndCounter -= deltaTime;
		if (m_gameEndCounter < 0.0f)
		{
			m_gameEndCounter = 0.0f;
		}
		return;
	}

	// Normal gameplay
	this->MinigameUpdate(deltaTime, frameNumber);

	int winningTeam = CalculateWinningTeam();
	if (winningTeam != WINNER_UNKNOWN)
	{
		m_roundWinningTeam = winningTeam;
		if (m_roundWinningTeam >= 0)
		{
			TeamData& team = m_allTeamState.GetTeam(m_roundWinningTeam);
			team.m_teamScore++;

			// Check score limit
			if (team.m_teamScore >= GetGameWinningScoreLimit())
			{
				m_gameWinningTeam = m_roundWinningTeam;
			}
		}
		m_roundEndCounter = GetRoundEndLength();
	}
}

template<typename PlayerType>
void BaseMinigameState<PlayerType>::SpawnPlayerMidRound(int playerID)
{
	// Allow the implementation to control this
}

template<typename PlayerType>
int BaseMinigameState<PlayerType>::CalculateWinningTeam()
{
	// Default implementation - check for last team alive
	int teamsAlive = 0;
	int winningTeam = WINNER_UNKNOWN;
	for (int i = 0; i < m_allTeamState.GetTeamCount(); i++)
	{
		TeamData& team = m_allTeamState.GetTeam(i);
		for (int playerID : team.m_playersOnTeam)
		{
			if (HasPlayerEntity(playerID))
			{
				teamsAlive++;
				winningTeam = i;
				break;
			}
		}
	}

	if (teamsAlive > 1)
	{
		return WINNER_UNKNOWN;
	}
	else if (teamsAlive == 1)
	{
		if (InLocalTestingMode())
		{
			return WINNER_UNKNOWN;
		}
		return winningTeam;
	}
	else
	{
		return WINNER_NOBODY; // Nobody wins
	}
}

template<typename PlayerType>
Rect BaseMinigameState<PlayerType>::GetGameBounds(bool aroundZeroZero)
{
	int defaultWidth = 800;
	int defaultHeight = 450;
	if (aroundZeroZero)
	{
		return { -defaultWidth / 2, -defaultHeight / 2, defaultWidth, defaultHeight };
	}
	return { 0,0, defaultWidth, defaultHeight };
}

template<typename PlayerType>
bool BaseMinigameState<PlayerType>::InLocalTestingMode()
{
	// Skip when testing locally
	return DebugUtils::s_debugInstance == 0 && NetUtils::IsHost() && NetUtils::GetConnectedPlayerCount() == 1;
}