#pragma once
#include "CustomFrameState.h"
#include "NetworkGameDefines.h"
#include "cereal/types/static_vector.hpp"
#include "RollbackAllocator.h"
#include "MinigameOverlayState.h"
#include "Rect.h"
#include "AllTeamState.h"
#include "GameModeTypes.h"

template<typename PlayerType>
class BaseMinigameState : public CustomFrameState, public MinigameOverlayState
{
	float m_introCounter;
	float m_roundStartCounter;
	float m_roundEndCounter;
	float m_gameEndCounter;
	int m_roundWinningTeam = -1;
	int m_gameWinningTeam = -1;
	int m_roundNumber = 0;
public:
	chobo::static_vector<PlayerType, PLAYER_COUNT> m_players;
	// Keep list of connected player IDs so we can reset and rebuild the game-players list
	chobo::static_vector<bool, PLAYER_COUNT> m_connectedIDs;
	GameModeTypeID m_gameMode;
	// Store the team info
	AllTeamState m_allTeamState;

	template<class Archive>
	void serialize(Archive& archive, std::uint32_t const version)
	{
		archive(CEREAL_NVP(m_introCounter), CEREAL_NVP(m_roundStartCounter), CEREAL_NVP(m_roundEndCounter), CEREAL_NVP(m_gameEndCounter),
			CEREAL_NVP(m_roundWinningTeam), CEREAL_NVP(m_gameWinningTeam), CEREAL_NVP(m_roundNumber),
			CEREAL_NVP(m_players), CEREAL_NVP(m_connectedIDs), CEREAL_NVP(m_gameMode), CEREAL_NVP(m_allTeamState));
	}

	virtual float GetIntroCounter() { return m_introCounter; }
	virtual float GetIntroLength() { return 3.0f; }
	virtual float GetRoundStartCounter() { return m_roundStartCounter; }
	virtual float GetRoundStartLength() { return 3.0f; }
	virtual float GetRoundEndCounter() { return m_roundEndCounter; }
	virtual float GetRoundEndLength() { return 3.0f; }
	virtual float GetGameEndCounter() { return m_gameEndCounter; }
	virtual float GetGameEndLength() { return 5.0f; }
	virtual GameModeTypeID GetGamemode() { return m_gameMode; }
	virtual AllTeamState& GetTeamState() { return m_allTeamState; }
	virtual int GetRoundWinningTeam() { return m_roundWinningTeam; }
	virtual int GetGameWinningTeam() { return m_gameWinningTeam; }
	virtual int GetGameWinningScoreLimit() { return 5; }
	virtual int GetCurrentRoundNumber() { return m_roundNumber; }

	// The definitions are found in the matching .cpp file, which needs to be included into the subclass's cpp file
	// Before players are added
	virtual void Initialise();
	// After players are added
	virtual void SetupGame();
	virtual void AddPlayer(int playerID);
	virtual void RemovePlayer(int playerID);
	virtual bool HasPlayer(int playerID);
	bool HasPlayerEntity(int playerID);

	PlayerType* GetPlayerEntity(int playerID)
	{
		for (int i = 0; i < (int)m_players.size(); i++)
		{
			if (m_players[i].m_playerID == playerID)
			{
				return &m_players[i];
			}
		}
		return nullptr;
	}
	virtual int GetPlayerCount();
	virtual Color GetPlayerColour(int playerID);
	virtual const char* GetPlayerName(int playerID);
	virtual int GetTeamForPlayer(int playerID);

	virtual void Update(float deltaTime, int frameNumber);

	// These are more likely to need overriding per-minigame (though there are some default implementations)
	virtual void MinigameUpdate(float deltaTime, int frameNumber) = 0;
	virtual void SetupRound() = 0;
	virtual void SpawnPlayerMidRound(int playerID); // By default does nothing - games might want to disallow spawning until a new round starts
	virtual int CalculateWinningTeam();
	Rect GetGameBounds(bool aroundZeroZero = false);
	bool InLocalTestingMode();

private:
	void BaseSetupRound();
};
