#pragma once
#include "cereal/types/polymorphic.hpp"
#include "cereal/archives/json.hpp"
#include "cereal/archives/binary.hpp"
#include "CRCArchive.hpp"
struct Camera2D;
struct Vector2;

struct BasePlayerControlState
{
	virtual std::shared_ptr<BasePlayerControlState> Clone() = 0;
	virtual std::shared_ptr<BasePlayerControlState> CloneToHeap() = 0;

	virtual void ResetOneFrameControls() {};
	virtual void SetFromLocalInput(Camera2D& camera) = 0;
	virtual void UpdateClientSideControlPrediction() {};
	virtual void SetPredictedMousePosition(Vector2& worldMousePosition) {};

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(0);
	}
};
CEREAL_CLASS_VERSION(BasePlayerControlState, 0);
CEREAL_REGISTER_TYPE(BasePlayerControlState);

/*// https://barrgroup.com/embedded-systems/how-to/polymorphism-no-heap-memory
enum { MAX_CONTROLS_STATE_SIZE = 1024 };

struct GenericPlayerControlState
{
private:
	char buf[MAX_CONTROLS_STATE_SIZE];
public:
	BasePlayerControlState *operator->()
	{
		return (BasePlayerControlState*)buf;
	}
	BasePlayerControlState const *operator->() const
	{
		return (BasePlayerControlState const *)buf;
	}
	BasePlayerControlState *get() const // For placement new or delete
	{
		return (BasePlayerControlState*)buf;
	}

	template<class Archive>
	void save(Archive& archive, std::uint32_t const version) const
	{
		BasePlayerControlState* base = get();
		base->DoSave((cereal::BinaryOutputArchive&)archive, version);
	}
	template<class Archive>
	void load(Archive& archive, std::uint32_t const version)
	{
		BasePlayerControlState* base = get();
		base->DoLoad((cereal::BinaryInputArchive&)archive, version);
	}
};
*/