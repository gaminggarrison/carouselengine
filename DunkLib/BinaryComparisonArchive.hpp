/*! \file binary.hpp
\brief Binary input and output archives */
#ifndef CEREAL_ARCHIVES_BINARY_COMPARISON_HPP_
#define CEREAL_ARCHIVES_BINARY_COMPARISON_HPP_

#include "cereal/cereal.hpp"
#include <sstream>

namespace cereal
{
	static char reusableBuffer[1024];
	class BinaryComparisonOutputArchive : public OutputArchive<BinaryComparisonOutputArchive, AllowEmptyClassElision>, public traits::ErrorCheckArchive
	{
	public:
		//! Construct, outputting to the provided stream
		/*! @param stream The stream to output to.  Can be a stringstream, a file stream, or
		even cout! */
		BinaryComparisonOutputArchive(std::istream & stream) :
			OutputArchive<BinaryComparisonOutputArchive, AllowEmptyClassElision>(this),
			itsStream(stream)
		{ }

		~BinaryComparisonOutputArchive() CEREAL_NOEXCEPT = default;

		//! Writes size bytes of data to the output stream
		void processBinary(const void * data, std::streamsize size)
		{
			auto const writtenSize = itsStream.rdbuf()->sgetn(&(reusableBuffer[0]), size);

			if (writtenSize != size)
				throw Exception("Failed to write " + std::to_string(size) + " bytes to output stream! Wrote " + std::to_string(writtenSize));
		}
		template<class T>
		void processBinaryAndCompare(const T& existing, std::streamsize size)
		{
			const void * const data = std::addressof(existing);
			T readType = T();
			auto const readSize = itsStream.rdbuf()->sgetn(reinterpret_cast<char*>(std::addressof(readType)), size);

			if (readSize != size)
				throw Exception("Failed to read " + std::to_string(size) + " bytes from input stream! Read " + std::to_string(readSize));

			if (!m_printedDifference)
			{
				// Now compare read data with the data currently there...
				if (existing != readType)
				{
					printf("Difference found! %s:%s:%s\n", m_fieldTwoBack, m_fieldOneBack, m_currentField);
				}
			}
		}
		const char* m_currentField;
		const char* m_fieldOneBack;
		const char* m_fieldTwoBack;
	private:
		std::istream & itsStream;
		bool m_printedDifference;
	};

	// ######################################################################
	//! An input archive designed to load data saved using BinaryOutputArchive or BinaryComparisonOutputArchive
	/*  This archive does nothing to ensure that the endianness of the saved
	and loaded data is the same.  If you need to have portability over
	architectures with different endianness, use PortableBinaryComparisonOutputArchive.

	When using a binary archive and a file stream, you must use the
	std::ios::binary format flag to avoid having your data altered
	inadvertently.

	\ingroup Archives */
	//static unsigned char reusableBuffer[1024 * 1024];
	/*class BinaryComparisonInputArchive : public InputArchive<BinaryComparisonInputArchive, AllowEmptyClassElision>, public traits::ErrorCheckArchive
	{
	public:
		//! Construct, loading from the provided stream
		BinaryComparisonInputArchive(std::istream & stream) :
			InputArchive<BinaryComparisonInputArchive, AllowEmptyClassElision>(this),
			itsStream(stream)
		{ }

		~BinaryComparisonInputArchive() CEREAL_NOEXCEPT = default;

		void loadBinary(void * const data, std::streamsize size)
		{
			auto const readSize = itsStream.rdbuf()->sgetn(reinterpret_cast<char*>(data), size);

			if (readSize != size)
				throw Exception("Failed to read " + std::to_string(size) + " bytes from input stream! Read " + std::to_string(readSize));
		}
		//! Reads size bytes of data from the input stream
		template<class T>
		void loadBinaryAndCompare(T& existing, std::streamsize size)
		{
			void * const data = std::addressof(existing);
			T readType = T();
			auto const readSize = itsStream.rdbuf()->sgetn(reinterpret_cast<char*>(std::addressof(readType)), size);

			if (readSize != size)
				throw Exception("Failed to read " + std::to_string(size) + " bytes from input stream! Read " + std::to_string(readSize));

			if (!m_printedDifference)
			{
				// Now compare read data with the data currently there...
				if (existing != readType)
				{
					printf("Difference found! %s\n", m_currentField);
				}

				/*const unsigned char* pointer = reinterpret_cast<const unsigned char*>(data);
				const unsigned char* readPointer = &reusableBuffer[0];
				std::streamsize iterator = size;
				while (iterator--)
				{
					unsigned char currentValue = *(pointer++);
					unsigned char readValue = *(readPointer++);
					if (currentValue != readValue)
					{
						printf("Difference found! %s\n", m_currentField);
					}
				}
				//m_printedDifference = true;
			}
			memcpy(data, &readType, size);
		}

		const char* m_currentField;
		bool m_printedDifference = false;
	private:
		std::istream & itsStream;
	};*/

	// ######################################################################
	// Common BinaryArchive serialization functions

	//! Saving for POD types to binary
	template<class T> inline
		typename std::enable_if<std::is_arithmetic<T>::value, void>::type
		CEREAL_SAVE_FUNCTION_NAME(BinaryComparisonOutputArchive & ar, T const & t)
	{
		ar.processBinaryAndCompare(t, sizeof(t));
	}

	//! Loading for POD types from binary
	/*template<class T> inline
		typename std::enable_if<std::is_arithmetic<T>::value, void>::type
		CEREAL_LOAD_FUNCTION_NAME(BinaryComparisonInputArchive & ar, T & t)
	{
		ar.loadBinaryAndCompare<T>(t, sizeof(t));
	}*/

	//! Serializing NVP types to binary
	template<class T> inline
		void CEREAL_SAVE_FUNCTION_NAME(BinaryComparisonOutputArchive & ar, NameValuePair<T> const & t)
	{
		ar.m_fieldTwoBack = ar.m_fieldOneBack;
		ar.m_fieldOneBack = ar.m_currentField;
		ar.m_currentField = t.name;
		//ar.processBinaryAndCompare(t.value, sizeof(t.value));
		ar(t.value);
	}

	//! Serializing SizeTags to binary
	template<class T> inline
	void CEREAL_SAVE_FUNCTION_NAME(BinaryComparisonOutputArchive & ar, SizeTag<T> const & t)
	{
		//ar(t.size);
		ar.processBinaryAndCompare(t.size, sizeof(t.size));
		//ar.processBinary(std::addressof(t.size), sizeof(t.size));
	}

	//! Saving binary data
	template <class T> inline
		void CEREAL_SAVE_FUNCTION_NAME(BinaryComparisonOutputArchive & ar, BinaryData<T> const & bd)
	{
		ar.processBinary(bd.data, static_cast<std::streamsize>(bd.size));
		//ar.processBinaryAndCompare(bd.data, static_cast<std::streamsize>(bd.size));
	}
} // namespace cereal

  // register archives for polymorphic support
CEREAL_REGISTER_ARCHIVE(cereal::BinaryComparisonOutputArchive)
//CEREAL_REGISTER_ARCHIVE(cereal::BinaryComparisonInputArchive)

// tie input and output archives together
//CEREAL_SETUP_ARCHIVE_TRAITS(cereal::BinaryComparisonInputArchive, cereal::BinaryComparisonOutputArchive)

#endif // CEREAL_ARCHIVES_BINARY_COMPARISON_HPP_
