#include "BlackBoxRecording.h"

BlackBoxRecording::BlackBoxRecording()
{

}

BlackBoxRecording::~BlackBoxRecording()
{
	// When we load a recording, it actually copies over the elements directly in the 'current recording' object, so this won't get shown
	printf("Destroying recording\n"); 
}

void BlackBoxRecording::SetStartFrame(int frame)
{
	m_startFrame = frame;
}

void BlackBoxRecording::RecordEvent(int currentFrame, NetworkEventWrapper& frameEvent)
{
	BlackBoxedEvent boxedEvent = BlackBoxedEvent(currentFrame, frameEvent);
	m_events.push_back(boxedEvent);
}

int BlackBoxRecording::GetStartFrame()
{
	return m_startFrame;
}
std::vector<BlackBoxedEvent>& BlackBoxRecording::GetEvents()
{
	return m_events;
}