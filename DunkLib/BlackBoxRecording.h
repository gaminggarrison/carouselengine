#pragma once

#include "BlackBoxedEvent.h"
#include "cereal/types/vector.hpp"

class BlackBoxRecording
{
	int m_startFrame;
	std::vector<BlackBoxedEvent> m_events;
public:
	BlackBoxRecording();
	~BlackBoxRecording();
	void SetStartFrame(int frame);
	void RecordEvent(int currentFrame, NetworkEventWrapper& frameEvent);
	int GetStartFrame();
	std::vector<BlackBoxedEvent>& GetEvents();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(m_startFrame, m_events);
	}
};