#include "BlackBoxSystem.h"
#include "NetworkStateControl.h"
#include "GameLoadEvent.h"
#include "NetUtils.h"
#include "UIManager.h"
#include "FileUtils.h"
#include "DrawUtils.h"
#include "GUIUtils.h"
#include "DebugUtils.h"

BlackBoxSystem::BlackBoxSystem()
{
	m_mode = BlackBoxMode::Skipped;
}

void BlackBoxSystem::StartRecording(NetworkStateControl& state)
{
	m_mode = BlackBoxMode::Recording;
	m_currentRecording.GetEvents().clear();
	int currentFrame = state.GetHeadFrame();
	m_currentRecording.SetStartFrame(currentFrame);

	GameLoadEvent* gameLoadEvent = new GameLoadEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(), state.GetHeadFrameState());
	//state.GetHeadFrameState().CopyTo(gameLoadEvent->m_saveData);
	NetworkEventWrapper loadEvent(currentFrame, gameLoadEvent);
	m_currentRecording.RecordEvent(currentFrame, loadEvent);
}

bool BlackBoxSystem::OnEventProcessed(int currentFrame, NetworkEventWrapper& frameEvent)
{
	if (m_mode == BlackBoxMode::Recording)
	{
		m_currentRecording.RecordEvent(currentFrame, frameEvent);
		return true;
	}
	else if (m_mode == BlackBoxMode::Playing)
	{
		// Don't record events when we're in playback
		return false;
	}
	return true;
}

void BlackBoxSystem::BeginPlayback(NetworkStateControl& state)
{
	if (m_currentRecording.GetEvents().size() <= 0)
	{
		printf("ERROR!  Cannot playback black box recording with 0 events!");
		return;
	}
	if (dynamic_cast<GameLoadEvent*>(m_currentRecording.GetEvents()[0].m_event.m_event.get()) == nullptr)
	{
		printf("ERROR!  The first event of a black box recording must be a GameLoadEvent!\n");
		return;
	}
	m_mode = BlackBoxMode::Playing;
	state.AddNetworkEventDirect(m_currentRecording.GetEvents()[0].m_event);
	m_playbackPosition = 1;
}
void BlackBoxSystem::StopPlaybackAndRecording()
{
	m_mode = BlackBoxMode::Skipped;
}

void BlackBoxSystem::Update(NetworkStateControl& state)
{
	if (m_mode == BlackBoxMode::Playing)
	{
		std::vector<BlackBoxedEvent>& events = m_currentRecording.GetEvents();
		// While the next event is for the current frame, then add it
		while (m_playbackPosition < events.size() && events[m_playbackPosition].m_receivedFrame <= state.GetHeadFrame())
		{
			if (events[m_playbackPosition].m_receivedFrame < state.GetHeadFrame())
			{
				printf("Error:  Black box playback has skipped forward over an event's received time!\n");
			}
			state.AddNetworkEventDirect(events[m_playbackPosition].m_event);
			m_playbackPosition++;
		}
		if (m_playbackPosition >= events.size())
		{
			m_mode = BlackBoxMode::Skipped;
		}
	}
}

void BlackBoxSystem::SkipToFrame(int frame, NetworkStateControl& state)
{
	m_seeking = true;
	m_desiredPlaybackPosition = frame;
	// Basically, restart the playback if necessary,  and force update until we reach that point
	if (frame < state.GetHeadFrame())
	{
		BeginPlayback(state);
	}
	/*while (state.GetHeadFrame() < frame)
	{
		state.Update();
	}*/
}
void BlackBoxSystem::UpdateSeek(NetworkStateControl& state)
{
	if (m_seeking)
	{
		for (int i = 0; i < 10; i++)
		{
			if (m_desiredPlaybackPosition <= state.GetHeadFrame())
			{
				m_seeking = false;
				break;
			}
			state.Update(0.0f);
		}
	}
}

void BlackBoxSystem::DrawUI(NetworkStateControl& state)
{
	int screenWidth = UIManager::GetUIWidth();
	int screenHeight = UIManager::GetUIHeight();
	if (m_mode == BlackBoxMode::Recording)
	{
		if (DebugUtils::s_debugOn && fmodf((float)GetTime(), 1.0f) > 0.5f)
		{
			DrawUtils::DrawCircleArc({ screenWidth * 0.9f, screenHeight * 0.5f }, 8.0f, 0.0f, PI * 2.0f, 16, RED);
		}
	}
	else if (m_mode == BlackBoxMode::Playing)
	{
		if (fmodf((float)GetTime(), 1.0f) > 0.5f)
		{
			GUIUtils::DrawTextCentered("Replay", (int)(screenWidth * 0.5f), (int)(screenHeight * 0.9f) - 20, 24, RED);
		}
		int startFrame = m_currentRecording.GetStartFrame();
		std::vector<BlackBoxedEvent>& events = m_currentRecording.GetEvents();
		int endFrame = events[events.size() - 1].m_receivedFrame;

		int currentFrame = state.GetHeadFrame();
		int framesFromStart = currentFrame - startFrame;
		int recordingLength = endFrame - startFrame;
		if (recordingLength == 0)
		{
			recordingLength = 1;
		}
		float timePosition01 = (float)framesFromStart / (float)recordingLength;
		float newValue = GUIUtils::GUISliderCentered((int)(screenWidth * 0.5f), (int)(screenHeight * 0.9f), 100, "", timePosition01, 0.0f, 1.0f);
		int newFrame = (int)(newValue * recordingLength) + startFrame;
		if (newValue != timePosition01 && newFrame != currentFrame)
		{
			printf("Changing playback time to frame %d\n", newFrame);
			SkipToFrame(newFrame, state);
		}
		UpdateSeek(state);
	}
}

void BlackBoxSystem::SaveRecording(const char* filename)
{
	FileUtils::SaveObjectToBinary(filename, m_currentRecording);
}
void BlackBoxSystem::LoadRecording(const char* filename, NetworkStateControl& state)
{
	StopPlaybackAndRecording();
	bool worked = FileUtils::LoadObjectFromBinary(filename, m_currentRecording);
	if (worked)
	{
		BeginPlayback(state);
	}
}
bool BlackBoxSystem::IsPlaying()
{
	return m_mode == BlackBoxMode::Playing;
}