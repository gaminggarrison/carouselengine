#pragma once
#include "cereal/archives/binary.hpp"
#include "BlackBoxRecording.h"

enum BlackBoxMode
{
	Skipped = 0,
	Recording = 1,
	Playing = 2
};

class NetworkStateControl;
class SharedGame;

class BlackBoxSystem
{
	BlackBoxRecording m_currentRecording;
	BlackBoxMode m_mode;
	int m_playbackPosition;
	bool m_seeking = false;
	int m_desiredPlaybackPosition;

	void SkipToFrame(int frame, NetworkStateControl& state);
	void UpdateSeek(NetworkStateControl& state);
public:
	BlackBoxSystem();
	void StartRecording(NetworkStateControl& state);
	bool OnEventProcessed(int currentFrame, NetworkEventWrapper& frameEvent);
	void BeginPlayback(NetworkStateControl& state);
	void StopPlaybackAndRecording();
	void Update(NetworkStateControl& state);
	void DrawUI(NetworkStateControl& state);
	void SaveRecording(const char* filename);
	void LoadRecording(const char* filename, NetworkStateControl& state);
	bool IsPlaying();
};