#pragma once

#include "NetworkEventWrapper.h"

struct BlackBoxedEvent
{
	int m_receivedFrame;
	NetworkEventWrapper m_event;

	BlackBoxedEvent();
	BlackBoxedEvent(int receivedFrame, NetworkEventWrapper& newEvent);

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(m_receivedFrame, m_event);
	}
};