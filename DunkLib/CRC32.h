﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Copyright NetworkDLS 2010, All rights reserved
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF 
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _NSWFL_CRC32_H_
#define _NSWFL_CRC32_H_
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <sstream>
namespace Hashing {
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	class CRC32 {

	public:
		CRC32(void);
		~CRC32(void);

		void Initialize(void);

		unsigned int FullCRC(const unsigned char *sData, size_t iDataLength);
		unsigned int FullCRC(std::stringstream& ss);
		void FullCRC(const unsigned char *sData, size_t iLength, unsigned int *iOutCRC);

		void PartialCRC(unsigned int *iCRC, const unsigned char *sData, size_t iDataLength);
		void PartialCRC(unsigned int *iCRC, std::stringstream& ss);

		unsigned int iTable[256]; // CRC lookup table array.
	private:
		unsigned int Reflect(unsigned int iReflect, const char cChar);
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} //namespace::Hashing
#endif