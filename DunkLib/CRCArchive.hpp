#ifndef CEREAL_ARCHIVES_CRC_HPP_
#define CEREAL_ARCHIVES_CRC_HPP_

#include "cereal/cereal.hpp"
#include "CRCUtils.h"

namespace cereal
{
	// ######################################################################
	//! An output archive designed to generate a CRC from the data
	/*!
	\ingroup Archives */
	class CRCOutputArchive : public OutputArchive<CRCOutputArchive, AllowEmptyClassElision>, public traits::ErrorCheckArchive
	{
	public:
		CRCOutputArchive(int crcValue) :
			OutputArchive<CRCOutputArchive, AllowEmptyClassElision>(this), m_crcValue(crcValue)
		{

		}

		~CRCOutputArchive() CEREAL_NOEXCEPT = default;

		void saveCRC(const void* data, std::streamsize size)
		{
			const unsigned char* pointer = reinterpret_cast<const unsigned char*>(data);
			while (size--)
			{
				unsigned char data = *(pointer++);
				unsigned char lookup = (m_crcValue & 0xFF) ^ data;
				m_crcValue = (m_crcValue >> 8) ^ CRCUtils::GetTable()[lookup];
			}
		}
		uint32_t GetCRCValue()
		{
			return (m_crcValue ^ 0xffffffff);
		}

	private:
		uint32_t m_crcValue;
	};

	// ######################################################################
	// Common CRCArchive serialization functions

	//! Saving for POD types to CRC
	template<class T> inline
		typename std::enable_if<std::is_arithmetic<T>::value, void>::type
		CEREAL_SAVE_FUNCTION_NAME(CRCOutputArchive & ar, T const & t)
	{
		ar.saveCRC(std::addressof(t), sizeof(t));
	}

	//! Serializing NVP types to CRC
	template <class T> inline
		void CEREAL_SAVE_FUNCTION_NAME(CRCOutputArchive & ar, NameValuePair<T> const & t)
	{
		ar(t.value);
	}

	//! Serializing SizeTags to CRC
	template <class T> inline
		void CEREAL_SAVE_FUNCTION_NAME(CRCOutputArchive & ar, SizeTag<T> const & t)
	{
		ar(t.size);
	}

	//! Saving CRC data
	template <class T> inline
		void CEREAL_SAVE_FUNCTION_NAME(CRCOutputArchive & ar, BinaryData<T> const & bd)
	{
		ar.saveCRC(bd.data, static_cast<std::streamsize>(bd.size));
	}
} // namespace cereal

  // register archives for polymorphic support
CEREAL_REGISTER_ARCHIVE(cereal::CRCOutputArchive)
#endif // CEREAL_ARCHIVES_CRC_HPP_
