#include "CRCCheckEvent.h"
#include "NetworkFrame.h"
#include "NetworkStateControl.h"

CRCCheckEvent::CRCCheckEvent()
{

}

CRCCheckEvent::CRCCheckEvent(int playerID, int playerEventID, uint32_t crcValue) : m_crcValue(crcValue)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void CRCCheckEvent::RunEvent(FrameState& frameState)
{
	
}
void CRCCheckEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{
	networkStateControl.CompareWithIncomingCRC(frameNumber, m_crcValue);

	//int crcCheckFrame = networkStateControl.GetOldestHistoryFrame() + 30;
	//networkStateControl.SetFramesToCatchup(frameNumber - crcCheckFrame);
}

Color CRCCheckEvent::GetDebugColour()
{
	return LIGHTGRAY;
}

int CRCCheckEvent::GetDebugPriority()
{
	return 5;
}
bool CRCCheckEvent::DoRewindForEvent()
{
	return false;
}