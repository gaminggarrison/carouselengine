#pragma once
#include "NetworkEvent.h"

struct CRCCheckEvent : public NetworkEvent
{
	uint32_t m_crcValue;

	CRCCheckEvent();
	CRCCheckEvent(int playerID, int playerEventID, uint32_t crcValue);

	virtual void RunEvent(FrameState& frameState);
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();
	virtual bool DoRewindForEvent();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_crcValue);
	}
};

CEREAL_REGISTER_TYPE(CRCCheckEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, CRCCheckEvent);
