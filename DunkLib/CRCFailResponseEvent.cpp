#include "CRCFailResponseEvent.h"
#include "NetworkFrame.h"
#include "NetworkStateControl.h"
#include "DebugUtils.h"
#include "StateComparisonUtils.h"

CRCFailResponseEvent::CRCFailResponseEvent()
{

}

CRCFailResponseEvent::CRCFailResponseEvent(int playerID, int playerEventID, uint32_t serverCRC, uint32_t clientCRC, FrameState& frameState) : m_serverCRCValue(serverCRC), m_clientCRCValue(clientCRC), m_clientFrame(frameState)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void CRCFailResponseEvent::RunEvent(FrameState& frameState)
{

}

void CRCFailResponseEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{
	printf("Received CRCFailResponseEvent!\n");
	NetworkFrame& serverFrame = networkStateControl.GetStateForFrame(frameNumber);
	StateComparisonUtils::DumpComparison(m_clientFrame, serverFrame.m_frameState);

	//DebugUtils::s_debugPause = true;
	//DebugUtils::s_debugOn = true;
}

Color CRCFailResponseEvent::GetDebugColour()
{
	return RED;
}

int CRCFailResponseEvent::GetDebugPriority()
{
	return 20;
}
bool CRCFailResponseEvent::DoRewindForEvent()
{
	return false;
}