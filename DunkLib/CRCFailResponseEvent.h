#pragma once
#include "NetworkEvent.h"
#include "RaylibCereal.h"
#include "FrameState.h"

struct CRCFailResponseEvent : public NetworkEvent
{
	uint32_t m_serverCRCValue;
	uint32_t m_clientCRCValue;
	FrameState m_clientFrame;

	CRCFailResponseEvent();
	CRCFailResponseEvent(int playerID, int playerEventID, uint32_t serverCRC, uint32_t clientCRC, FrameState& frameState);

	virtual void RunEvent(FrameState& frameState);
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();
	virtual bool DoRewindForEvent();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_serverCRCValue, m_clientCRCValue, m_clientFrame);
	}
};

CEREAL_REGISTER_TYPE(CRCFailResponseEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, CRCFailResponseEvent);
