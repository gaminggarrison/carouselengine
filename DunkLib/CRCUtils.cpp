#include "CRCUtils.h"
#include <cstring>

/*
	Reflection is a requirement for the official CRC-32 standard.
	You can create CRCs without it, but they won't conform to the standard.
*/
unsigned int Reflect(unsigned int iReflect, const char cChar)
{
	unsigned int iValue = 0;

	// Swap bit 0 for bit 7, bit 1 For bit 6, etc....
	for (int iPos = 1; iPos < (cChar + 1); iPos++)
	{
		if (iReflect & 1)
		{
			iValue |= (1 << (cChar - iPos));
		}
		iReflect >>= 1;
	}

	return iValue;
}

bool s_initialized = false;
CRCUtils::CRCTable s_table;

/*
	This function initializes "CRC Lookup Table". You only need to call it once to
	initalize the table before using any of the other CRC32 calculation functions.
*/
void CRCUtils::Initialize(void)
{
	//0x04C11DB7 is the official polynomial used by PKZip, WinZip and Ethernet.
	unsigned int iPolynomial = 0x04C11DB7;

	memset(&s_table, 0, sizeof(s_table));

	// 256 values representing ASCII character codes.
	for (int iCodes = 0; iCodes <= 0xFF; iCodes++)
	{
		s_table[iCodes] = Reflect(iCodes, 8) << 24;

		for (int iPos = 0; iPos < 8; iPos++)
		{
			s_table[iCodes] = (s_table[iCodes] << 1)
				^ ((s_table[iCodes] & (1 << 31)) ? iPolynomial : 0);
		}

		s_table[iCodes] = Reflect(s_table[iCodes], 32);
	}
}

CRCUtils::CRCTable& CRCUtils::GetTable()
{
	return s_table;
}