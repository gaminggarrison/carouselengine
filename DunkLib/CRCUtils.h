#pragma once

namespace CRCUtils
{
	typedef unsigned int CRCTable[256];

	void Initialize();
	CRCTable& GetTable();
}