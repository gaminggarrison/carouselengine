#include "CameraUtils.h"
#include "raymath.h"
#include "MathUtils.h"
#include "UIManager.h"


Matrix s_screenToWorld;
Matrix s_worldToScreen;
float s_currentScale;
Camera2D* s_currentCamera;

void CameraUtils::UpdateCameraMatrices(Camera2D& camera)
{
	s_currentCamera = &camera;
	// Copied from https://github.com/raysan5/raylib/blob/master/src/core.c GetWorldToScreen2D

	// The camera in world-space is set by
	//   1. Move it to target
	//   2. Rotate by -rotation and scale by (1/zoom)
	//      When setting higher scale, it's more intuitive for the world to become bigger (= camera become smaller),
	//      not for the camera getting bigger, hence the invert. Same deal with rotation.
	//   3. Move it by (-offset);
	//      Offset defines target transform relative to screen, but since we're effectively "moving" screen (camera)
	//      we need to do it into opposite direction (inverse transform)

	// Having camera transform in world-space, inverse of it gives the modelview transform.
	// Since (A*B*C)' = C'*B'*A', the modelview is
	//   1. Move to offset
	//   2. Rotate and Scale
	//   3. Move by -target
	Matrix matOrigin = MatrixTranslate(-camera.target.x, -camera.target.y, 0.0f);
	Matrix matRotation = MatrixRotate({ 0.0f, 0.0f, 1.0f }, camera.rotation*DEG2RAD);
	Matrix matScale = MatrixScale(camera.zoom, camera.zoom, 1.0f);
	Matrix matTranslation = MatrixTranslate(camera.offset.x, camera.offset.y, 0.0f);

	Matrix matTransform = { 0 };
	matTransform = MatrixMultiply(MatrixMultiply(matOrigin, MatrixMultiply(matScale, matRotation)), matTranslation);

	s_worldToScreen = matTransform;
	s_screenToWorld = MatrixInvert(matTransform);

	s_currentScale = camera.zoom;
}

Vector2 CameraUtils::ScreenToWorldPos(Vector2 screenPos, bool uiSpace)
{
	//return GetScreenToWorld2D(screenPos, camera);
	if (uiSpace)
	{
		screenPos = screenPos * UIManager::GetScaleFactor();
	}
	return Vector2Transform(screenPos, s_screenToWorld);
}

Vector2 CameraUtils::WorldToScreenPos(Vector2 worldPos, bool uiSpace)
{
	//return GetWorldToScreen2D(worldPos, camera);
	Vector2 screenPos = Vector2Transform(worldPos, s_worldToScreen);
	if (uiSpace)
	{
		screenPos = screenPos * UIManager::GetSizeMultiplier();
	}
	return screenPos;
}
float CameraUtils::GetCurrentScale()
{
	return s_currentScale;
}
Vector3 CameraUtils::GetAudioPosition()
{
	const float defaultDistance = 128.0f;
	return { s_currentCamera->target.x, s_currentCamera->target.y, defaultDistance / s_currentCamera->zoom };
	//return Vector3Zero();
}
Camera2D* CameraUtils::GetCurrentCamera()
{
	return s_currentCamera;
}