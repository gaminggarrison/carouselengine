#pragma once
#include "raylib.h"

namespace CameraUtils
{
	void UpdateCameraMatrices(Camera2D& camera);
	Vector2 ScreenToWorldPos(Vector2 screenPos, bool uiSpace = false);
	Vector2 WorldToScreenPos(Vector2 worldPos, bool uiSpace = false);
	float GetCurrentScale();
	Vector3 GetAudioPosition();
	Camera2D* GetCurrentCamera();
}
