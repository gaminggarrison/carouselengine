#include "CarouselSetup.h"

CarouselSetup* CarouselSetup::s_instance = nullptr;

void CarouselSetup::Initialise(GameSpecificFunctions* gameSpecificFunctions, std::string gameFileName)
{
	m_gameFunctions = gameSpecificFunctions;
	s_gameFileName = gameFileName;
}

GameSpecificFunctions* CarouselSetup::GetGameFunctions()
{
	if (s_instance == nullptr)
	{
		return nullptr;
	}
	return s_instance->m_gameFunctions;
}