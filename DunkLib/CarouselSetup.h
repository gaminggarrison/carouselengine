#pragma once
struct GameSpecificFunctions;
#include <string>

class CarouselSetup
{
public:
	GameSpecificFunctions* m_gameFunctions = nullptr;
	std::string s_gameFileName = "";

	static CarouselSetup* s_instance;

	void Initialise(GameSpecificFunctions* gameSpecificFunctions, std::string gameFileName);

	static GameSpecificFunctions* GetGameFunctions();
};