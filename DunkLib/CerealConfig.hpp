#pragma once
#if defined(_MSC_VER) || defined(__MINGW32__) || defined(__MINGW64__)
#  if defined(core_EXPORTS)
#    define CORE_DECL __declspec(dllexport)
#  else
#    define CORE_DECL __declspec(dllimport)
#  endif
#endif