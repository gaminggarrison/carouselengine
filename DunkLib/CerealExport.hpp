#pragma once

//https://github.com/USCiLab/cereal/issues/608
#include "CerealConfig.hpp"
namespace cereal {
	namespace detail {
		// these declspecs will ensure that the polymorphic loader/saver registrations will
		// all happen against the binding maps in core.dll
		template class CORE_DECL StaticObject<InputBindingMap<BinaryInputArchive>>;
		template class CORE_DECL StaticObject<InputBindingMap<JSONInputArchive>>;
		template class CORE_DECL StaticObject<OutputBindingMap<BinaryOutputArchive>>;
		template class CORE_DECL StaticObject<OutputBindingMap<JSONOutputArchive>>;
		template class CORE_DECL StaticObject<OutputBindingMap<CRCOutputArchive>>;
		// add similar statements for other archive types as needed
		template class CORE_DECL StaticObject<Versions>;
	} // namespace detail
} // namespace cereal