#include "ColourUtils.h"

namespace ColourUtils
{
	Color Lerp(Color a, Color b, float t)
	{
		// 0 to 255 range
		float red = ((float)a.r * (1.0f - t)) + ((float)b.r * t);
		float green = ((float)a.g * (1.0f - t)) + ((float)b.g * t);
		float blue = ((float)a.b * (1.0f - t)) + ((float)b.b * t);
		float alpha = ((float)a.a * (1.0f - t)) + ((float)b.a * t);

		return { (unsigned char)red, (unsigned char)green, (unsigned char)blue, (unsigned char)alpha };
	}
	bool Equals(Color a, Color b)
	{
		return a.r == b.r && a.g == b.g && a.b == b.b && a.a == b.a;
	}
	Color FromInt32(int c)
	{
		return { (unsigned char)((c >> 24) & 0xff), (unsigned char)((c >> 16) & 0xff),(unsigned char)((c >> 8) & 0xff), (unsigned char)(c & 0xff) };
	}
}