#pragma once
#include "raylib.h"

namespace ColourUtils
{
	Color Lerp(Color a, Color b, float t);
	bool Equals(Color a, Color b);
	Color FromInt32(int c);
}