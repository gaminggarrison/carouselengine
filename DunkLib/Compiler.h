#pragma once

#ifdef _MSC_VER
#define NOINLINE __declspec(noinline)
#define DLLEXPORT __declspec(dllexport)
#else
#define NOINLINE
#define DLLEXPORT
#endif
