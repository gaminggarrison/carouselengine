#include "ConfigData.h"

#include "ConfigFunctions.h"

ConfigData* ConfigData::InstanceObject;


void ConfigData::Initialise(bool headlessMode)
{
	// Normally we'd reset and reload all the config, but here we allow it to be skipped so we can change the code while keeping the config around, allowing a new version to be serialized out
	/*if (IsKeyDown(KEY_W))
	{
		return;
	}*/
	ConfigData& instance = Instance();
	instance.m_isHeadless = headlessMode;

	instance.m_textures.Reset();
	//LoadFilesInFolder("Assets/Textures", "png", &ConfigFunctions::LoadTextureFile);
	if (!headlessMode)
	{
		Texture2D nullTexture = LoadTexture("Assets/Textures/Error.png");
		instance.m_textures.SetNullObject("Null", nullTexture);
	}

	instance.m_shaders.Reset();
	LoadFilesInFolder("Assets/Shaders", "fs", &ConfigFunctions::LoadShaderFile);
	instance.m_shaders.SetNullObject("Null", Shader());

	instance.m_sounds.Reset();
	LoadFilesInFolder("Assets/Sounds", "wav", &ConfigFunctions::LoadSoundFile);
	instance.m_sounds.SetNullObject("Null", Sound());

	instance.m_music.SetNullObject("Null", Music());

	FileUtils::LoadObjectFromJsonMinimal<EngineConfigData>("Assets/EngineConfig.json", instance.m_config);

	printf("Engine config font name: %s\n", instance.m_config.m_defaultFontName.c_str());
}
// auto drawInPlaneAt = [](float x, float y){ drawAt(x, y, 0); }; // Currying functions example https://www.fluentcpp.com/2019/05/03/curried-objects-in-cpp/

void ConfigData::UnloadTextures()
{
	if (!m_isHeadless) // In headless mode, we only load empty versions of the assets like textures
	{
		for (int i = 0; i < m_textures.GetCount(); i++)
		{
			UnloadTexture(m_textures.Get(i));
		}
	}
	m_textures.Reset();
}
void ConfigData::UnloadShaders()
{
	if (!m_isHeadless)
	{
		for (int i = 0; i < m_shaders.GetCount(); i++)
		{
			UnloadShader(m_shaders.Get(i));
		}
	}
	m_shaders.Reset();
}
void ConfigData::UnloadSounds()
{
	if (!m_isHeadless)
	{
		for (int i = 0; i < m_sounds.GetCount(); i++)
		{
			UnloadSound(m_sounds.Get(i));
		}
	}
	m_sounds.Reset();
}
void ConfigData::UnloadMusic()
{
	if (!m_isHeadless)
	{
		for (int i = 0; i < m_music.GetCount(); i++)
		{
			UnloadMusicStream(m_music.Get(i));
		}
	}
	m_music.Reset();
}

void ConfigData::UnloadAllAssets()
{
	UnloadTextures();
	UnloadShaders();
	UnloadSounds();
	UnloadMusic();
}

void ConfigData::LoadFilesInFolder(const char* folder, const char* extension, void(*ptr)(const char* filePath, const char* fileName, bool headlessMode))
{
	int fileCount = 0;
	char** filenames = GetDirectoryFiles(folder, &fileCount);
	if (fileCount <= 0)
	{
		printf("Could not find any files at folder %s\nWorking directory is:%s\n", folder, GetWorkingDirectory());
		ClearDirectoryFiles();
		return;
	}
	else
	{
		printf("Loading files in folder %s\n", folder);
	}
	std::sort(filenames, filenames + fileCount, [](const char *a, const char *b) { return strcmp(a, b) < 0; });

	for (int i = 0; i < fileCount; i++)
	{
		const char* filePath = TextFormat("%s/%s", folder, filenames[i]);
		const char* fileExtension = GetExtension(filePath);
		//printf("File %d = %s extension = %s\n", i, filenames[i], extension);
		if (fileExtension != nullptr && TextIsEqual(extension, &fileExtension[1])) // Skip the first dot raylib returns
		{
			const char* fileName = GetFileNameWithoutExt(filePath);

			(*ptr)(filePath, fileName, Instance().m_isHeadless);
		}
	}
	ClearDirectoryFiles();
}

Texture2D& ConfigData::GetTexture(const char* name)
{
	ConfigData& instance = Instance();
	int id = instance.m_textures.ConvertStringToID(name);
	return instance.m_textures.Get(id);
}