#pragma once
#include "ConfigSet.h"
#include "RaylibCereal.h"
#include "FileUtils.h"
#include <algorithm>
#include "EngineConfigData.h"
#include "CRCArchive.hpp"

class ConfigData
{
public:
	static ConfigData* InstanceObject;
	static ConfigData& Instance() { return *InstanceObject; }// static ConfigData data; return data;

	bool m_isHeadless = false;

	ConfigSet<Texture2D> m_textures;
	ConfigSet<Shader> m_shaders;
	ConfigSet<Sound> m_sounds;
	ConfigSet<Music> m_music;
	EngineConfigData m_config;

	void UnloadTextures();
	void UnloadShaders();
	void UnloadSounds();
	void UnloadMusic();

	void UnloadAllAssets();

	static void Initialise(bool headlessMode = false);

	static Texture2D& GetTexture(const char* name);

	static void LoadFilesInFolder(const char* folder, const char* extension, void(*ptr)(const char* filePath, const char* fileName, bool headlessMode));

	template<typename T>
	static void LoadJsonFilesInFolder(const char* folder, ConfigSet<T>& set)
	{
		set.Reset();
		const char* extension = "json";
		int fileCount = 0;
		char** filenames = GetDirectoryFiles(folder, &fileCount);
		std::sort(filenames, filenames + fileCount, [](const char *a, const char *b) { return strcmp(a, b) < 0; });

		// Do 2 passes - the first to setup the id->name mapping, then the second to load in the data (as we may want to resolve IDs during json load)
		for (int i = 0; i < fileCount; i++)
		{
			const char* filePath = TextFormat("%s/%s", folder, filenames[i]);
			const char* fileExtension = GetExtension(filePath);
			//printf("File %d = %s extension = %s\n", i, filenames[i], extension);
			if (fileExtension != nullptr && TextIsEqual(extension, &fileExtension[1])) // Raylib adds an initial dot now
			{
				const char* fileName = GetFileNameWithoutExt(filePath);

				T data = T();
				set.Add(fileName, data);
			}
		}
		for (int i = 0; i < fileCount; i++)
		{
			const char* filePath = TextFormat("%s/%s", folder, filenames[i]);
			const char* fileExtension = GetExtension(filePath);
			//printf("File %d = %s extension = %s\n", i, filenames[i], extension);
			if (fileExtension != nullptr && TextIsEqual(extension, &fileExtension[1])) // Raylib adds an initial dot now
			{
				const char* fileName = GetFileNameWithoutExt(filePath);
				int index = set.ConvertStringToID(fileName);
				T& data = set.Get(index);
				//ConfigFunctions::LoadJsonFile<T>(filePath, data);
				FileUtils::LoadObjectFromJsonMinimal<T>(filePath, data);
			}
		}
		set.SetNullObject("Null", T());
		ClearDirectoryFiles();
	}

	template<class T>
	static void ExportConfigSet(ConfigSet<T> set, const char* folder)
	{
		for (int i = 0; i < set.GetCount(); i++)
		{
			string stringID = set.ConvertIDToString(i);
			const char* filePath = TextFormat("%s%s%s", folder, stringID.c_str(), ".json");
			FileUtils::SaveObjectToJsonMinimal(filePath, set.Get(i));
		}
	}

	template<class T>
	static uint32_t AddConfigSetCRC(ConfigSet<T> set, uint32_t crc)
	{
		for (int i = 0; i < set.GetCount(); i++)
		{
			{
				cereal::CRCOutputArchive oarchive(crc); // Create an output archive

				oarchive(set.Get(i));
				crc = oarchive.GetCRCValue();
			}
		}
		return crc;
	}
};
