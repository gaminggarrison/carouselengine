#include "ConfigFunctions.h"
#include "RaylibCereal.h"
#include "ConfigData.h"
#include "cereal/archives/json.hpp"
#include <sstream>
#include <fstream>
#include "DrawUtils.h"
#include "MathUtils.h"

#if defined(PLATFORM_DESKTOP)
#define GLSL_VERSION            330
#else   // PLATFORM_RPI, PLATFORM_ANDROID, PLATFORM_WEB
#define GLSL_VERSION            100
#endif

using namespace std;

std::pair<Texture2D, std::string> ConfigFunctions::LoadTextureFileOnly(const char* filePath, const char* fileName, bool headlessMode)
{
	//printf("Working Directory = %s\n", GetWorkingDirectory(), filePath);
	//printf("FilePath = %s\n", filePath);
	if (!FileExists(filePath))
	{
		printf("Cannot find texture at %s!\n", filePath);
		return { Texture2D(),"" };
	}
	printf("Loading texture %s\n", filePath);
	Texture2D loaded;
	if (!headlessMode)
	{
		loaded = LoadTexture(filePath);
	}
	else
	{
		Image image = LoadImage(filePath);
		// Just get the dimensions and otherwise add it normally - the headless server might need this info at least
		loaded.width = image.width;
		loaded.height = image.height;
		UnloadImage(image);
	}

	int noMipIndex = TextFindIndex(fileName, "NOMIP");
	bool useMipMaps = true;
	if (noMipIndex >= 0)
	{
		// Remove the NOMIP section from the filename
		fileName = TextSubtext(fileName, 0, noMipIndex);
		useMipMaps = false;
	}


	int linearIndex = TextFindIndex(fileName, "LINEAR");
	bool useLinearBlend = false;
	if (linearIndex >= 0)
	{
		// Remove the LINEAR section from the filename
		fileName = TextSubtext(fileName, 0, linearIndex);
		useLinearBlend = true;
	}

	int wrapIndex = TextFindIndex(fileName, "WRAP");
	bool useTextureWrap = false;
	if (wrapIndex >= 0)
	{
		// Remove the WRAP section from the filename
		fileName = TextSubtext(fileName, 0, wrapIndex);
		useTextureWrap = true;
	}

	bool isPowerOf2 = IsPowerOf2(loaded.width) && IsPowerOf2(loaded.height);
	bool allowNonPowerOf2MipMaps = true;
#if defined PLATFORM_WEB || defined TESTING_WEB
	allowNonPowerOf2MipMaps = false;
#endif
	// For web platforms
	if (!isPowerOf2 && !allowNonPowerOf2MipMaps)
	{
		useTextureWrap = false;
		useMipMaps = false;
	}

	if (!headlessMode)
	{
		if (useMipMaps)
		{
			GenTextureMipmaps(&loaded);
		}

		if (useLinearBlend)
		{
			if (!useMipMaps)
			{
				SetTextureFilter(loaded, TEXTURE_FILTER_BILINEAR);
			}
			else
			{
				SetTextureFilter(loaded, TEXTURE_FILTER_TRILINEAR);
			}
		}
		else if (useMipMaps)
		{
			DrawUtils::SetPointFilterButLinearMipFilter(loaded);
		}
		else
		{
			SetTextureFilter(loaded, FILTER_POINT);
		}

		if (useTextureWrap)
		{
			SetTextureWrap(loaded, TEXTURE_WRAP_REPEAT);
		}
		else
		{
			SetTextureWrap(loaded, TEXTURE_WRAP_CLAMP);
		}
	}
	return { loaded, std::string(fileName) };
}

void ConfigFunctions::LoadTextureFile(const char* filePath, const char* fileName, bool headlessMode)
{
	std::pair<Texture2D, std::string> texture = LoadTextureFileOnly(filePath, fileName, headlessMode);
	if (texture.first.width > 0 && texture.first.height > 0)
	{
		ConfigData::Instance().m_textures.Add(texture.second.c_str(), texture.first);
	}
}
void ConfigFunctions::LoadShaderFile(const char* filePath, const char* fileName, bool headlessMode)
{
	Shader loaded;
	if (!headlessMode)
	{
		loaded = LoadShader(0, filePath);
		if (loaded.id == DrawUtils::GetDefaultShader().id)
		{
			printf("Shader load must have failed for %s, because it was replaced by the default shader!\n", filePath);
		}
	}
	else
	{
		loaded = Shader();
	}
	ConfigData::Instance().m_shaders.Add(fileName, loaded);
}
void ConfigFunctions::LoadSoundFile(const char* filePath, const char* fileName, bool headlessMode)
{
	Sound loaded;
	if (!headlessMode)
	{
		loaded = LoadSound(filePath);
	}
	else
	{
		loaded = Sound();
	}
	ConfigData::Instance().m_sounds.Add(fileName, loaded);
}
void ConfigFunctions::LoadMusicFile(const char* filePath, const char* fileName, bool headlessMode)
{
	Music loaded;
	if (!headlessMode)
	{
		loaded = LoadMusicStream(filePath);
	}
	else
	{
		loaded = Music();
	}
	ConfigData::Instance().m_music.Add(fileName, loaded);
}
