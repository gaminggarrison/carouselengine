#pragma once
#include "cereal/archives/json.hpp"
#include <fstream>
#include "SerializationUtils.h"
using namespace std;

template <class T>
class ConfigSet;

struct Texture;
typedef Texture Texture2D;

namespace ConfigFunctions
{
	std::pair<Texture2D, std::string> LoadTextureFileOnly(const char* filePath, const char* fileName, bool headlessMode);
	void LoadTextureFile(const char* filePath, const char* fileName, bool headlessMode);
	void LoadShaderFile(const char* filePath, const char* fileName, bool headlessMode);
	void LoadSoundFile(const char* filePath, const char* fileName, bool headlessMode);
	void LoadMusicFile(const char* filePath, const char* fileName, bool headlessMode);
}
