//#include "ConfigReference.h"
#include "ConfigReference.h"

template<class Derived, class DataType>
string ConfigReference<Derived, DataType>::IDToString(int id)
{
	ConfigSet<DataType>& set = GetConfigSet(); return set.ConvertIDToString(id);
}

template<class Derived, class DataType>
int ConfigReference<Derived, DataType>::StringToID(const char* name) { ConfigSet<DataType>& set = GetConfigSet(); return set.ConvertStringToID(name); };

template<class Derived, class DataType>
int ConfigReference<Derived, DataType>::DataCount() { return GetConfigSet().GetCount(); }

template<class Derived, class DataType>
DataType& ConfigReference<Derived, DataType>::GetData() const { return GetConfigSet().Get(m_id); }

// Explicit template instantiations?

//template<class Derived, class DataType> string ConfigReference::IDToString(int id) {  };
//template<class Derived, class DataType> int ConfigReference::StringToID(const char* name) { ConfigSet<DataType>& set = GetConfigSet(); return set.ConvertStringToID(name); };

/*ConfigReference::ConfigReference()
{

}

ConfigReference::ConfigReference(int id) : m_id(id)
{

}

bool ConfigReference::operator==(const ConfigReference& other) const
{
	return m_id == other.m_id;
}
bool ConfigReference::operator!=(const ConfigReference& other) const
{
	return !((*this) == other);
}

string ConfigReference::IDToString(int id) const
{
	return string();
}

int ConfigReference::StringToID(const char * name) const
{
	return -1;
}*/
