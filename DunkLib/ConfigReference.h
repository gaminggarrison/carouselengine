#pragma once
#include "cereal/cereal.hpp"
#include <string>
#include "cereal/types/string.hpp"
//#include "ConfigSet.h"

template <class T>
class ConfigSet;

using namespace std;

template<class Derived, class DataType>
struct ConfigReference
{
	int m_id = -1;

	static ConfigSet<DataType>& GetConfigSet() { return Derived::GetConfigSet(); }

	static string IDToString(int id);// { ConfigSet<DataType>& set = GetConfigSet(); return set.ConvertIDToString(id); };
	static int StringToID(const char* name);// { ConfigSet<DataType>& set = GetConfigSet(); return set.ConvertStringToID(name); };
	static int DataCount();// { return GetConfigSet().GetCount(); }
	DataType& GetData() const;// { return GetConfigSet().Get(m_id); }

	ConfigReference() {};
	ConfigReference(const char* name) {
		int id = GetConfigSet().ConvertStringToID(name);
		m_id = id;
	}
	ConfigReference(int id) : m_id(id) {};

	inline bool IsBlank()
	{
		return m_id == -1;
	}

	/*bool operator==(const ConfigReference& other) const;
	bool operator!=(const ConfigReference& other) const;*/
	inline bool operator==(const ConfigReference& other) const
	{
		return m_id == other.m_id;
	}
	inline bool operator!=(const ConfigReference& other) const
	{
		return !((*this) == other);
	}

	/*template<class Archive>
	void save(Archive& archive, std::uint32_t const version) const
	{
		archive(IDToString(m_id));
	}

	template<class Archive>
	void load(Archive& archive, std::uint32_t const version)
	{
		string name;
		archive(name);
		int id = StringToID(name.c_str());
		m_id = id;
	}*/
	template<class Archive>
	string save_minimal(Archive const &) const
	{
		return IDToString(m_id);
	}
	template<class Archive>
	void load_minimal(Archive const &, string const & value)
	{
		int id = StringToID(value.c_str());
		m_id = id;
	}

	//template<class T>
	//ConfigSet<T>& GetConfigSet() const { return static_cast<const Derived*>(this)->GetConfigSet(); }
};
