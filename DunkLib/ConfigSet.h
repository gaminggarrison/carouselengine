#pragma once
#include <string>
#include <vector>
#include "raylib.h"
using namespace std;

template <class T>
class ConfigSet
{
	T m_nullObject;
	string m_nullObjectName;
	vector<pair<string, T>> m_data;

public:
	void Add(const char* name, T data);
	T& Get(int id);
	T* GetPointer(int id);
	int FindByValuePointer(const T* value);
	int ConvertStringToID(const char* name);
	string ConvertIDToString(int id);
	int GetCount() { return (int)m_data.size(); }
	void SetNullObject(const char* name, T data);
	void Reset() { m_data.clear(); }
};

template<class T>
inline void ConfigSet<T>::Add(const char* name, T data)
{
	for (int i = 0; i < m_data.size(); i++)
	{
		if (TextIsEqual(m_data[i].first.c_str(), name))
		{
			// Already in the set
			printf("Asset with name \"%s\" is already loaded, so overwriting!\n", name);
			m_data[i] = { string(name), data };
			return;
		}
	}
	m_data.push_back({ string(name), data });
}

template<class T>
inline T& ConfigSet<T>::Get(int id)
{
	if (id < 0 || id >= m_data.size())
	{
		return m_nullObject;
	}
	return m_data[id].second;
}

template<class T>
inline T* ConfigSet<T>::GetPointer(int id)
{
	if (id < 0 || id >= m_data.size())
	{
		return &m_nullObject;
	}
	return &m_data[id].second;
}

template<class T>
inline int ConfigSet<T>::FindByValuePointer(const T* value)
{
	for (int i = 0; i < (int)m_data.size(); i++)
	{
		if (&m_data[i].second == value)
		{
			return i;
		}
	}
	return -1;
}

template<class T>
inline int ConfigSet<T>::ConvertStringToID(const char* name)
{
	for (int i = 0; i < m_data.size(); i++)
	{
		if (TextIsEqual(m_data[i].first.c_str(), name))
		{
			return i;
		}
	}
	if (TextIsEqual(name, m_nullObjectName.c_str()))
	{
		return -1;
	}
	printf("Asset not found: \"%s\" of type %s\n", name, typeid(T).name());
	return -1;
}
template<class T>
inline string ConfigSet<T>::ConvertIDToString(int id)
{
	if (id < 0 || id >= m_data.size())
	{
		return m_nullObjectName;
	}
	return m_data[id].first;
}

template<class T>
inline void ConfigSet<T>::SetNullObject(const char* name, T data)
{
	m_nullObjectName = string(name);
	m_nullObject = data;
}
