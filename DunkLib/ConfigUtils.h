#pragma once
#include "FileUtils.h"
#include "DebugUtils.h"
#include "FolderUtils.h"

namespace ConfigUtils
{
	template<typename T>
	void SaveObjectToJsonConfig(const char* filePath, T& object)
	{
		if (DebugUtils::s_debugInstance >= 0)
		{
			WorkingDirectorySwitcheroo2([filePath, object]() { FileUtils::SaveObjectToJson(filePath, object); });
		}
		else
		{
			FileUtils::SaveObjectToJson(filePath, object);
		}
	}
	template<typename T>
	void LoadObjectFromJsonConfig(const char* filePath, T& object)
	{
		if (DebugUtils::s_debugInstance >= 0)
		{
			FolderUtils::WorkingDirectorySwitcheroo2(&FileUtils::LoadObjectFromJson<T>, filePath, object);
		}
		else
		{
			FileUtils::LoadObjectFromJson(filePath, object);
		}
	}
}
