#pragma once

#include "CustomFrameStateWrapper.h"
namespace ControlUtils
{
	template<typename T>
	static T GetControlState(int playerID)
	{
		return CustomFrameStateWrapper::GetCurrentFrame()->GetControlState<T>(playerID);
	}
};
