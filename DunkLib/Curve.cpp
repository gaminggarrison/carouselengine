#include "Curve.h"
#include "MathUtils.h"

float Curve::Evaluate(float x)
{
	if (m_points.empty())
	{
		return 0.0f;
	}

	Vector2 first = { 0.0f, 0.0f };
	Vector2 second = { 1.0f, 0.0f };

	// Find the 2 points on either side
	for (int i = 0; i < m_points.size(); i++)
	{
		if (m_points[i].x > x)
		{
			second = m_points[i];
			break;
		}
		first = m_points[i];
	}

	// Just lerp between the 2 sides
	float fromFirst = (x - first.x);
	float range = second.x - first.x;
	if (range == 0.0f)
	{
		return first.y;
	}
	float t = fromFirst / range;
	return Lerp(first.y, second.y, t);
}