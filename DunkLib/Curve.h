#pragma once
#include "RaylibCereal.h"

struct Curve
{
	std::vector<Vector2> m_points;

	float Evaluate(float x);

	template<class Archive>
	void serialize(Archive& archive, std::uint32_t const version)
	{
		archive(CEREAL_NVP(m_points));
	}
};