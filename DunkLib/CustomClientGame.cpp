#include "CustomClientGame.h"
#include "NetworkStateControl.h"
#include "MinigameOverlayState.h"

#include "DrawUtils.h"
#include "UIManager.h"
#include "CarouselSetup.h"
#include "GameSpecificFunctions.h"
#include "MathUtils.h"
#include "GameModeTypes.h"
#include "Curve.h"
#include "NetUtils.h"
#include "NetworkPlayerInfo.h"
#include "MinigameSpecificFunctions.h"
#include "GUIUtils.h"

std::string CustomClientGame::LateUpdate(NetworkStateControl& commonGame, MetagameControl& metagame, float deltaTime)
{
	if (!NetUtils::IsHost())
	{
		return "";
	}
	CustomFrameStateWrapper wrapper = commonGame.GetHeadWrapper();
	CustomFrameState* state = wrapper.GetState<CustomFrameState>();
	MinigameOverlayState* overlayState = dynamic_cast<MinigameOverlayState*>(state);
	if (overlayState != nullptr && overlayState->GetRoundEndCounter() <= 0.0f && overlayState->GetGameEndCounter() <= 0.0f && overlayState->GetGameWinningTeam() >= 0)
	{
		return std::string("LobbyModule");
	}
	return "";
}

void CustomClientGame::DrawOverlay(NetworkStateControl& commonGame)
{
	CustomFrameStateWrapper wrapper = commonGame.GetHeadWrapper();
	CustomFrameState* state = wrapper.GetState<CustomFrameState>();
	MinigameOverlayState* overlayState = dynamic_cast<MinigameOverlayState*>(state);
	if (overlayState != nullptr)
	{
		float intro = overlayState->GetIntroCounter();
		float roundCountdown = overlayState->GetRoundStartCounter();
		float roundEndCountdown = overlayState->GetRoundEndCounter();
		float gameEndCountdown = overlayState->GetGameEndCounter();

		int baseAlpha = 64;
		int maxAlpha = 255;

		if (intro > 0.0f)
		{
			float backgroundAlpha = Clamp01(intro);
			intro = 1.0f - (intro / overlayState->GetIntroLength());
			DrawRectangle(0, 0, UIManager::GetUIWidth(), UIManager::GetUIHeight(), { 0,0,0, (unsigned char)(baseAlpha + ((maxAlpha - baseAlpha) * backgroundAlpha)) });

			// Show the game name and perhaps any details
			MinigameSpecificFunctions* minigameFunctions = dynamic_cast<MinigameSpecificFunctions*>(CarouselSetup::GetGameFunctions());

			Curve alphaCurve = { { {0.0f, 0.0f}, {0.2f, 1.0f}, {0.6f, 1.0f}, {1.0f, 0.0f}} };
			float alpha = alphaCurve.Evaluate(intro);
			Color introColour = { 255,255,255,(unsigned char)(255 * alpha) };

			const char* introText = minigameFunctions->GetGameName();
			GUIUtils::DrawTextAligned(introText, UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) - 24, 24, introColour, { 0.5f, 0.5f });
			const char* introDescription = minigameFunctions->GetGameDescription();
			GUIUtils::DrawTextAligned(introDescription, UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2), 24, introColour, { 0.5f, 0.5f });

			GameModeTypeID gameModeID = overlayState->GetGamemode();
			if (gameModeID != GameModeTypeID::None)
			{
				const char* introText2 = GameModeTypes::GetGameMode(gameModeID)->GetGameModeName();
				Curve alphaCurve2 = { { {0.2f, 0.0f}, {0.4f, 1.0f}, {0.6f, 1.0f}, {1.0f, 0.0f}} };
				float alpha2 = alphaCurve2.Evaluate(intro);
				Color introColour2 = { 255,255,255,(unsigned char)(255 * alpha2) };
				GUIUtils::DrawTextAligned(introText2, UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) + 24, 24, introColour2, { 0.5f, 0.5f });
			}
			int scoreToWin = overlayState->GetGameWinningScoreLimit();
			const char* introText3 = TextFormat("First to %d points", scoreToWin);
			Curve alphaCurve3 = { { {0.4f, 0.0f}, {0.6f, 1.0f}, {0.8f, 1.0f}, {1.0f, 0.0f}} };
			float alpha3 = alphaCurve3.Evaluate(intro);
			Color introColour3 = { 255,255,255,(unsigned char)(255 * alpha3) };
			GUIUtils::DrawTextAligned(introText3, UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) + 48, 24, introColour3, { 0.5f, 0.5f });
		}
		else if (roundCountdown > 0.0f)
		{
			Curve alphaCurve = { { {0.0f, 1.0f}, {0.9f, 1.0f}, {1.0f, 0.0f}} };
			float roundCountdownT = 1.0f - (roundCountdown / overlayState->GetRoundStartLength());
			float backgroundAlpha = alphaCurve.Evaluate(roundCountdownT);
			DrawRectangle(0, 0, UIManager::GetUIWidth(), UIManager::GetUIHeight(), { 0,0,0, (unsigned char)(baseAlpha * backgroundAlpha) });

			MinigameSpecificFunctions* minigameFunctions = dynamic_cast<MinigameSpecificFunctions*>(CarouselSetup::GetGameFunctions());
			const char* introDescription = minigameFunctions->GetGameDescription();
			GUIUtils::DrawTextAligned(introDescription, UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) - 12, 24,
				{ 255,255,255, (unsigned char)(255 * backgroundAlpha) }, { 0.5f, 0.5f });

			// Countdown to each round
			int seconds = (int)(roundCountdown + 0.999f);
			float alpha = fmodf(roundCountdown, 1.0f);
			if (seconds < 1)
			{
				seconds = 1;
			}
			const char* introText = TextFormat("%d...", seconds);
			Color introColour = { 255,255,255,(unsigned char)(255 * alpha) };
			GUIUtils::DrawTextAligned(introText, UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) + 30, 48, introColour, { 0.5f, 0.5f });

			// Maybe say if a team is about to win?
			
		}
		else if (roundEndCountdown > 0.0f)
		{
			Curve alphaCurve = { { {0.0f, 0.0f}, {0.2f, 1.0f}, {0.6f, 1.0f}, {0.9f, 1.0f}, {1.0f, 1.0f}} };
			float roundEndT = 1.0f - (roundEndCountdown / overlayState->GetRoundEndLength());

			float alpha = alphaCurve.Evaluate(roundEndT);
			DrawRectangle(0, 0, UIManager::GetUIWidth(), UIManager::GetUIHeight(), { 0,0,0, (unsigned char)(baseAlpha * alpha) });

			int winningTeamID = overlayState->GetRoundWinningTeam();
			WinnerDisplay winnerDisplay = overlayState->GetTeamState().GetTeamName(winningTeamID);
			const char* endText = TextFormat("%s wins!", winnerDisplay.m_name);
			Color endColour = winnerDisplay.m_colour;
			endColour.a = (unsigned char)(255 * alpha);
			GUIUtils::DrawTextAligned(endText, UIManager::GetUIWidth() / 2, UIManager::GetUIHeight() / 2, 24, endColour, { 0.5f, 0.5f });

			if (winningTeamID >= 0)
			{
				int scoreToWin = overlayState->GetGameWinningScoreLimit();
				AllTeamState& allTeamState = overlayState->GetTeamState();
				int score = allTeamState.GetTeam(winningTeamID).m_teamScore;
				const char* scoreText = TextFormat("%d out of %d points", score, scoreToWin);
				//Color scoreTextColour = { 255,255,255, endColour.a };
				GUIUtils::DrawTextAligned(scoreText, UIManager::GetUIWidth() / 2, UIManager::GetUIHeight() / 2 + 24, 24, endColour, { 0.5f, 0.5f });
			}
		}
		else if (gameEndCountdown > 0.0f)
		{
			Curve alphaCurve = { { {0.0f, 1.0f}, {0.2f, 1.0f}, {0.6f, 1.0f}, {1.0f, 1.0f}} }; // All on all the time? :P
			float gameEndT = 1.0f - (gameEndCountdown / overlayState->GetGameEndLength());
			float alpha = alphaCurve.Evaluate(gameEndT);
			DrawRectangle(0, 0, UIManager::GetUIWidth(), UIManager::GetUIHeight(), { 0,0,0, (unsigned char)(baseAlpha * alpha) });

			WinnerDisplay winnerDisplay = overlayState->GetTeamState().GetTeamName(overlayState->GetGameWinningTeam());
			Color endColour = winnerDisplay.m_colour;
			endColour.a = (unsigned char)(255 * alpha);

			const char* endText = TextFormat("%s reached %d points - winning the game!", winnerDisplay.m_name, overlayState->GetGameWinningScoreLimit());
			GUIUtils::DrawTextAligned(endText, UIManager::GetUIWidth() / 2, UIManager::GetUIHeight() / 2, 24, endColour, { 0.5f, 0.5f });
		}
	}
}