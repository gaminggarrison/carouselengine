#pragma once
class NetworkStateControl;
struct BasePlayerControlState;
struct MetagameControl;
struct MinigameOverlayState;
#include <string>
#include "raylib.h"

struct CustomClientGame
{
	// Can be called on the server - implementers shouldn't need to know if they're headless or not - engine functions should just work
	virtual void InitClient(NetworkStateControl& commonGame) = 0; 
	virtual void HandleInput(BasePlayerControlState& localControlState, NetworkStateControl& commonGame) = 0;

	// Has default implementation to swap back to lobby when a score limit is reached
	virtual std::string LateUpdate(NetworkStateControl& commonGame, MetagameControl& metagame, float deltaTime);
	virtual void Draw(NetworkStateControl& commonGame) = 0;

	// Has default implementation to draw introduction, countdown and score overlays
	virtual void DrawOverlay(NetworkStateControl& commonGame);
};