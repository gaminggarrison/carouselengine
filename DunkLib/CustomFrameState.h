#pragma once
#include "cereal/types/polymorphic.hpp"
#include "cereal/archives/json.hpp"
#include "cereal/archives/binary.hpp"
#include "CRCArchive.hpp"

struct Vector2;

class CustomFrameState
{
public:
	CustomFrameState() {}
	virtual ~CustomFrameState() {}

	virtual void Initialise() {};
	virtual void SetupGame() {};
	virtual void AddPlayer(int playerID) = 0;
	virtual void RemovePlayer(int playerID) = 0;
	virtual bool HasPlayer(int playerID) = 0;
	virtual void Update(float deltaTime, int frameNumber) = 0;

	// These functions are optional calls if you want to handle control prediction manually
	// Reset one frame controls is for single-shot actions
	virtual void ResetOneFrameControls() {};
	// For updating any client side control predictions (e.g. mouse) for each player (for display only)
	virtual void UpdateClientSideControlPredictions() {};
	// For setting the client's local mouse prediction (for display only)
	virtual void UpdateLocalClientPrediction(int currentPlayerID, Vector2& worldMousePosition) {};

	virtual std::shared_ptr<CustomFrameState> Clone() = 0;

	template<class Archive>
	void serialize(Archive& archive, std::uint32_t const version)
	{
		archive(0);
	}
};
CEREAL_CLASS_VERSION(CustomFrameState, 0);
CEREAL_REGISTER_TYPE(CustomFrameState);
