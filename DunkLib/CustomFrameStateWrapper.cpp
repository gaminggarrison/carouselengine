#include "CustomFrameStateWrapper.h"
#include "cereal/archives/json.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/types/memory.hpp"
#include "CRCArchive.hpp"
#include "TimerUtils.h"
#include "CarouselSetup.h"
#include "GameSpecificFunctions.h"
#include <assert.h>
#include "NetworkGameDefines.h"
#include <array>

CustomFrameStateWrapper* CustomFrameStateWrapper::s_currentFrameState = nullptr;

CustomFrameStateWrapper::CustomFrameStateWrapper()
{
	GameSpecificFunctions* gameFunctions = CarouselSetup::GetGameFunctions();
	assert(gameFunctions != nullptr);
	printf("Creating custom frame state for game %s\n", gameFunctions->GetGameName());
	std::shared_ptr<CustomFrameState> customFrameState = gameFunctions->CreateCustomGameState();
	_impl = customFrameState;

	s_currentFrameState = this;
	m_randomGenerator.Init();
	m_randomGenerator.SetAsSharedInstance();
	m_playerControlState.resize(PLAYER_COUNT);
	for (int i = 0; i < PLAYER_COUNT; i++)
	{
		m_playerControlState[i] = nullptr;
	}
	_impl->Initialise();
}

CustomFrameStateWrapper::~CustomFrameStateWrapper()
{

}

CustomFrameStateWrapper* CustomFrameStateWrapper::GetCurrentFrame()
{
	return s_currentFrameState;
}

void CustomFrameStateWrapper::Update(float deltaTime, int frameNumber)
{
	s_currentFrameState = this;
	m_randomGenerator.SetAsSharedInstance();
	TimerUtils::SetCurrentFrameNumber(frameNumber);
	assert(_impl != nullptr);
	_impl->Update(deltaTime, frameNumber);
}

void CustomFrameStateWrapper::SetControlState(BasePlayerControlState* controls, int playerID)
{
	if (playerID < 0 || playerID >= PLAYER_COUNT)
	{
		return;
	}
	int sizeNeeded = playerID + 1;
	if (m_playerControlState.size() < sizeNeeded)
	{
		m_playerControlState.resize(sizeNeeded);
	}
	if (controls == nullptr)
	{
		m_playerControlState[playerID] = nullptr;
		return;
	}
	std::shared_ptr<BasePlayerControlState> clonedControls = controls->Clone();
	m_playerControlState[playerID] = clonedControls;
}

void CustomFrameStateWrapper::ResetOneFrameControls()
{
	for (int i = 0; i < m_playerControlState.size(); i++)
	{
		if (m_playerControlState[i] != nullptr)
		{
			m_playerControlState[i]->ResetOneFrameControls();
		}
	}
	assert(_impl != nullptr);
	_impl->ResetOneFrameControls();
}
void CustomFrameStateWrapper::UpdateClientSideControlPredictions()
{
	for (int i = 0; i < m_playerControlState.size(); i++)
	{
		if (m_playerControlState[i] != nullptr)
		{
			m_playerControlState[i]->UpdateClientSideControlPrediction();
		}
	}
	assert(_impl != nullptr);
	_impl->UpdateClientSideControlPredictions();
}
void CustomFrameStateWrapper::UpdateLocalClientPrediction(int currentPlayerID, Vector2 worldMousePosition)
{
	assert(_impl != nullptr);
	if (currentPlayerID >= 0 && currentPlayerID < m_playerControlState.size())
	{
		if (m_playerControlState[currentPlayerID] != nullptr)
		{
			m_playerControlState[currentPlayerID]->SetPredictedMousePosition(worldMousePosition);
		}
	}
	_impl->UpdateLocalClientPrediction(currentPlayerID, worldMousePosition);
}

RandomGenerator& CustomFrameStateWrapper::GetRandomGenerator()
{
	return m_randomGenerator;
}

void CustomFrameStateWrapper::SetConnectedPlayers(ConnectedPlayersArray connectedPlayers)
{
	for (int i = 0; i < connectedPlayers.size(); i++)
	{
		int playerID = i;
		bool shouldExist = connectedPlayers[i];
		bool existing = _impl->HasPlayer(playerID);
		if (shouldExist && !existing)
		{
			_impl->AddPlayer(playerID);
		}
		else if (!shouldExist && existing)
		{
			_impl->RemovePlayer(playerID);
		}
	}
}