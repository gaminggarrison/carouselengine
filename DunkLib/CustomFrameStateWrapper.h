#pragma once
#include <memory>
#include "cereal/types/memory.hpp"
#include "RandomGenerator.h"
#include "raylib.h"
#include "cereal/types/polymorphic.hpp"
#include "cereal/archives/json.hpp"
#include "cereal/archives/binary.hpp"
#include "CRCArchive.hpp"
#include "CustomFrameState.h"
#include "RollbackPointer.h"
#include "cereal/types/static_vector.hpp"
#include "BasePlayerControlState.h"
#include "NetworkGameDefines.h"

typedef chobo::static_vector<RollbackPointer<BasePlayerControlState>, PLAYER_COUNT> BasePlayerControls;
typedef std::array<bool, PLAYER_COUNT> ConnectedPlayersArray;

class CustomFrameStateWrapper
{
	static CustomFrameStateWrapper* s_currentFrameState;
public:
	CustomFrameStateWrapper();
	~CustomFrameStateWrapper();

	static CustomFrameStateWrapper* GetCurrentFrame();

	void Update(float deltaTime, int frameNumber);
	void SetControlState(BasePlayerControlState* controlState, int playerID);
	void SetConnectedPlayers(ConnectedPlayersArray connectedPlayers);

	template<typename T>
	T GetControlState(int playerID)
	{
		if (playerID < 0 || playerID >= m_playerControlState.size() || m_playerControlState[playerID] == nullptr)
		{
			return T();
		}
		return *((T*)(m_playerControlState[playerID].get()));
	}
	BasePlayerControlState* GetBaseControlState(int playerID)
	{
		if (playerID < 0 || playerID >= m_playerControlState.size() || m_playerControlState[playerID] == nullptr)
		{
			return nullptr;
		}
		return m_playerControlState[playerID].get();
	}

	void ResetOneFrameControls();
	void UpdateClientSideControlPredictions();
	void UpdateLocalClientPrediction(int currentPlayerID, Vector2 worldMousePosition);
	RandomGenerator& GetRandomGenerator();

	template<typename T>
	T* GetState()
	{
		return (T*)_impl.get();
	}

	template<class Archive>
	void serialize(Archive& archive, std::uint32_t const version)
	{
		//archive(m_randomGenerator);
		archive(m_randomGenerator, _impl, m_playerControlState);
	}
private:
	RandomGenerator m_randomGenerator;
	RollbackPointer<CustomFrameState> _impl;
	//RollbackPointer<BasePlayerControlState> m_test;
	BasePlayerControls m_playerControlState;

	friend class cereal::access;
};

CEREAL_CLASS_VERSION(CustomFrameStateWrapper, 0);
