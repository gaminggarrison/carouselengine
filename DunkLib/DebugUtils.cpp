#include "DebugUtils.h"
#include "DrawUtils.h"
#include "TextureID.h"

bool DebugUtils::s_debugOn = false;
bool DebugUtils::s_debugPause = false;
int DebugUtils::s_debugInstance = -1;
std::vector<DebugLine> DebugUtils::s_debugLines = std::vector<DebugLine>();
void DebugUtils::RenderLines()
{
	Texture2D& tex = DrawUtils::GetShapeTexture();
	for (int i = 0; i < s_debugLines.size(); i++)
	{
		DebugLine& line = s_debugLines[i];
		DrawUtils::DrawSpriteArrow(tex, line.m_start, line.m_end, line.m_colour);
	}
	s_debugLines.clear();
}