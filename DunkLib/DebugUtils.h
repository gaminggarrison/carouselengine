#pragma once
#include <vector>
#include "raylib.h"

struct DebugLine
{
	Vector2 m_start;
	Vector2 m_end;
	Color m_colour;
};

class DebugUtils
{
public:
	static bool s_debugOn;
	static bool s_debugPause;

	// DebugInstance will be positive when running through the IDE (0 for first instance, 1 for second instance etc)
	static int s_debugInstance;
	static std::vector<DebugLine> s_debugLines;

	static void RenderLines();
};
