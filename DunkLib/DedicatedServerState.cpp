#include "DedicatedServerState.h"
#include "NetUtils.h"
using namespace std::placeholders;
#include "CameraUtils.h"

// Pretty sure this class never gets used because autostart game is never true.  It's all in InGameState!
void DedicatedServerState::Init()
{
	NetworkGameState::Init();
	NetUtils::StartDedicatedHost(std::bind(&NetworkGameState::OnClientConnected, this, _1), std::bind(&NetworkGameState::OnClientDisconnected, this, _1), std::bind(&NetworkGameState::OnMessageReceived, this, _1), true);
}

void DedicatedServerState::GameInit()
{

}

void DedicatedServerState::HandleInput()
{

}

void DedicatedServerState::Cleanup()
{
	NetworkGameState::Cleanup();
}


void DedicatedServerState::Update(float deltaTime)
{
	NetworkGameState::Update(deltaTime);
}



void DedicatedServerState::Draw()
{
	NetworkGameState::DrawDebug();
}
