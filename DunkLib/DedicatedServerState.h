#pragma once
#include "NetworkGameState.h"

class DedicatedServerState : public NetworkGameState
{
	virtual void Init();
	virtual void GameInit();
	virtual void HandleInput();
	virtual void Cleanup();
	virtual void Update(float deltaTime);
	virtual void Draw();
};
static_assert(sizeof(DedicatedServerState) <= MAX_GAMESTATE_SIZE, "Class is too big!");
