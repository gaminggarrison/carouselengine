#include "DrawUtils.h"
#include "raymath.h"

#include "MathUtils.h"

#include "rlgl.h"

#include <vector>
#include "UIManager.h"
#include "ConfigData.h"

#include "CameraUtils.h"
#include "TooltipUtils.h"

namespace DrawUtils
{
	Texture2D s_shapeTexture;
	Texture2D s_lineTexture;
	Texture2D s_closeButtonTexture;
	bool s_initialised = false;

	Model s_planeModel;

	void InitializeResources(bool headlessMode)
	{
		if (headlessMode)
		{
			return;
		}

		Image image = GenImageColor(1, 1, WHITE);
		s_shapeTexture = LoadTextureFromImage(image);
		UnloadImage(image);

		s_lineTexture = LoadTexture("Assets/Textures/Line.png");
		s_closeButtonTexture = LoadTexture("Assets/Textures/CloseButton.png");

		s_planeModel = LoadModelFromMesh(GenMeshPlane(1.0f, 1.0f, 1, 1));
		s_initialised = true;
	}
	bool IsInitialised()
	{
		return s_initialised;
	}

	void DrawSpriteIntoBox(Texture2D& tex, Rect box)
	{
		float aspect = (float)tex.width / (float)tex.height;
		float boxAspect = (float)box.width / (float)box.height;

		RayRectangle rayBox = box.ToRayRectangle();
		// If the aspects don't match, then shrink one dimension of the box to match the aspect
		if (aspect > boxAspect) // Too wide
		{
			rayBox.height = rayBox.height * (boxAspect / aspect);
			rayBox.y -= (rayBox.height - (float)box.height) * 0.5f;
		}
		else if (aspect < boxAspect) // Too tall
		{
			rayBox.width = rayBox.width / (boxAspect / aspect);
			rayBox.x += (rayBox.width - (float)box.width) * 0.5f;
		}
		DrawTexturePro(tex, { 0.0f, 0.0f, (float)tex.width, (float)tex.height }, rayBox, { 0.0f, 0.0f }, 0.0f, WHITE);
	}
	void DrawSprite(Texture2D& tex, Vector2 position, float rotation, Color colour)
	{
		DrawTexturePro(tex, { 0.0f, 0.0f, (float)tex.width, (float)tex.height }, { position.x, position.y, (float)tex.width, (float)tex.height }, { (float)tex.width * 0.5f, (float)tex.height * 0.5f }, -rotation * RAD2DEG, colour);
	}
	void DrawSpriteSlice(Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, int sliceNumber, float scale, Vector2 origin01)
	{
		int slices = tex.width / sliceSize;
		int currentSlice = sliceNumber % slices;
		int sliceX = sliceSize * currentSlice;
		Vector2 drawnSize = { (float)sliceSize * scale, (float)tex.height * scale };
		DrawTexturePro(tex, { (float)sliceX, 0.0f, (float)sliceSize, (float)tex.height }, { position.x, position.y, drawnSize.x, drawnSize.y }, { drawnSize.x * origin01.x, drawnSize.y * origin01.y }, -rotation * RAD2DEG, colour);
	}
	void DrawSpriteSliceFloat(Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, float animationProgress, Vector2 origin01)
	{
		int slices = tex.width / sliceSize;
		int sliceNumber = (int)((float)slices * animationProgress * 0.999f); // Make sure it doesn't go over the end when at animationProgress 1
		int currentSlice = sliceNumber % slices;
		if (currentSlice < 0)
		{
			currentSlice = slices + currentSlice;
		}
		int sliceX = sliceSize * currentSlice;
		DrawTexturePro(tex, { (float)sliceX, 0.0f, (float)sliceSize, (float)tex.height }, { position.x, position.y, (float)sliceSize, (float)tex.height }, { (float)sliceSize * origin01.x, (float)tex.height * origin01.y }, -rotation * RAD2DEG, colour);
	}
	void DrawSpriteSliceFloat01(const Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, float animationProgress, float scale)
	{
		int slices = tex.width / sliceSize;
		int sliceNumber = (int)((float)(slices - 1) * animationProgress);
		int currentSlice = sliceNumber % slices;
		int sliceX = sliceSize * currentSlice;
		Vector2 drawnSize = { (float)sliceSize * scale, (float)tex.height * scale };
		DrawTexturePro(tex, { (float)sliceX, 0.0f, (float)sliceSize, (float)tex.height }, { position.x, position.y, drawnSize.x, drawnSize.y },
			{ drawnSize.x * 0.5f, drawnSize.y * 0.5f }, -rotation * RAD2DEG, colour);
	}
	void DrawSpriteSliceFloat01Pro(const Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, float animationProgress, Vector2 scale,
		Vector2 origin01, bool horizontalFlip, bool verticalFlip)
	{
		int slices = tex.width / sliceSize;
		int sliceNumber = (int)((float)(slices - 1) * animationProgress);
		int currentSlice = sliceNumber % slices;
		int sliceX = sliceSize * currentSlice;
		Vector2 drawnSize = { (float)sliceSize * scale.x, (float)tex.height * scale.y };
		RayRectangle sourceRect = { (float)sliceX, 0.0f, (float)sliceSize, (float)tex.height };
		if (horizontalFlip)
		{
			//sourceRect.x += (float)sliceSize;
			sourceRect.width = -(float)sliceSize;
		}
		if (verticalFlip)
		{
			//sourceRect.y += (float)tex.height;
			sourceRect.height = -(float)tex.height;
		}
		RayRectangle targetRect = { position.x, position.y, drawnSize.x, drawnSize.y };
		DrawTexturePro(tex, sourceRect, targetRect, { drawnSize.x * origin01.x, drawnSize.y * origin01.y }, -rotation * RAD2DEG, colour);
	}
	void DrawSpriteSliceFloatBorder(Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, float animationProgress)
	{
		int borderSize = 2;
		int slices = tex.width / (sliceSize + borderSize);
		int sliceNumber = (int)((float)slices * animationProgress * 0.999f); // Make sure it doesn't go over the end when at animationProgress 1
		int currentSlice = sliceNumber % slices;
		int sliceX = ((sliceSize + borderSize) * currentSlice) + (borderSize / 2);
		RayRectangle sourceRect = { (float)sliceX, (float)(borderSize / 2), (float)sliceSize, (float)(tex.height - borderSize) };
		RayRectangle targetRect = { position.x, position.y, (float)sliceSize, (float)(tex.height - borderSize) };
		DrawTexturePro(tex, sourceRect, targetRect, { targetRect.width * 0.5f, targetRect.height * 0.5f }, -rotation * RAD2DEG, colour);
	}
	void DrawSprite(Texture2D& tex, Vector2 position, float rotation, Color colour, Vector2 scale)
	{
		float w = (float)tex.width * scale.x;
		float h = (float)tex.height * scale.y;
		DrawTexturePro(tex, { 0.0f, 0.0f, (float)tex.width, (float)tex.height }, { position.x, position.y, w, h }, { w * 0.5f, h * 0.5f }, -rotation * RAD2DEG, colour);
	}
	void DrawSpriteLine(const Texture2D& tex, Vector2 from, Vector2 to, Color colour)
	{
		float length = Vector2Length(Vector2Subtract(to, from));
		float rotation = Vector2Angle(from, to);
		DrawTexturePro(tex,
			{ 0.0f, 0.0f, (float)tex.width, (float)tex.height },
			{ from.x, from.y, length, (float)tex.height }, { 0.0f, (float)tex.height * 0.5f }, rotation, colour);
	}
	void DrawSpriteLineTiled(const Texture2D& tex, Vector2 from, Vector2 to, Color colour)
	{
		float length = Vector2Length(Vector2Subtract(to, from));
		float rotation = Vector2Angle(from, to);
		float repetitions = length / tex.width;
		DrawTexturePro(tex,
			{ 0.0f, 0.0f, (float)tex.width * repetitions, (float)tex.height },
			{ from.x, from.y, length, (float)tex.height }, { 0.0f, (float)tex.height * 0.5f }, rotation, colour);
	}
	void DrawSpriteLineTiledAndVerticallySliced(const Texture2D& tex, Vector2 from, Vector2 to, Color colour, int sliceSize, int sliceNumber)
	{
		float length = Vector2Length(Vector2Subtract(to, from));
		float rotation = Vector2Angle(from, to);
		float repetitions = length / tex.width;

		int slices = tex.height / sliceSize;
		int currentSlice = sliceNumber % slices;
		int sliceY = sliceSize * sliceNumber;
		DrawTexturePro(tex,
			{ 0.0f, (float)sliceY, (float)tex.width * repetitions, (float)sliceSize },
			{ from.x, from.y, length, (float)sliceSize }, { 0.0f, (float)sliceSize * 0.5f }, rotation, colour);
	}
	void DrawSpriteTiled(const Texture2D& tex, Vector2 position, float rotation, Color colour, Vector2 scale, Vector2 originProportion)
	{
		RayRectangle source = { 0.0f, 0.0f, (float)tex.width * scale.x, (float)tex.height * scale.y };
		RayRectangle dest = { position.x, position.y, (float)tex.width * scale.x, (float)tex.height * scale.y };
		Vector2 origin = { originProportion.x * (float)tex.width * scale.x, originProportion.y * (float)tex.height * scale.y };
		float angle = -rotation * RAD2DEG;
		DrawTexturePro(tex, source, dest, origin, angle, colour);
	}
	void DrawSpriteArrow(const Texture2D& tex, Vector2 from, Vector2 to, Color colour)
	{
		DrawSpriteLine(tex, from, to, colour); // Body of arrow [-]>
		// Two sides of the head -[>]
		Vector2 direction = Vector2Subtract(to, from);
		if (direction.x != 0.0f || direction.y != 0.0f)
		{
			Vector2 offsetBack = Vector2Scale(direction, -0.25f);
			Vector2 offsetAway = Vector2Scale(RotateRight90(direction), 0.25f);
			Vector2 offset1 = Vector2Add(offsetBack, offsetAway);
			Vector2 offset2 = Vector2Add(offsetBack, Vector2Negate(offsetAway));
			DrawSpriteLine(tex, Vector2Add(to, offset1), to, colour);
			DrawSpriteLine(tex, Vector2Add(to, offset2), to, colour);
		}
	}
	void DrawSpriteLineSlice(Texture2D& tex, Vector2 from, Vector2 to, Color colour, int sliceSize, int sliceNumber)
	{
		float length = Vector2Length(Vector2Subtract(to, from));
		float rotation = Vector2Angle(from, to);

		int slices = tex.width / sliceSize;
		int currentSlice = sliceNumber % slices;
		int sliceX = sliceSize * sliceNumber;
		DrawTexturePro(tex,
			{ (float)sliceX, 0.0f, (float)sliceSize, (float)tex.height },
			{ from.x, from.y, length, (float)tex.height }, { 0.0f, (float)tex.height * 0.5f }, rotation, colour);
	}
	void DrawSpriteFromOrigin(Texture2D& tex, Vector2 position, float rotation, Vector2 origin, Color colour)
	{
		DrawTexturePro(tex, { 0.0f, 0.0f, (float)tex.width, (float)tex.height }, { position.x, position.y, (float)tex.width, (float)tex.height }, { (float)tex.width * origin.x, (float)tex.height * origin.y }, -rotation * RAD2DEG, colour);
	}
	

	void DrawFullscreenTiledTexture(Texture2D& tex, Vector2 uvTiling, Vector2 uvOffset, Camera2D& camera)
	{
		float tileEdge = 1000.0f;
		Vector2 tileSize = { (float)tex.width, (float)tex.height };
		Vector2 edgeSize = Vector2Scale(tileSize, tileEdge);
		RayRectangle rectangle = { -edgeSize.x, -edgeSize.y, edgeSize.x * 2.0f, edgeSize.y * 2.0f };
		DrawTextureQuad(tex, Vector2Scale(uvTiling, tileEdge * 2.0f), uvOffset, rectangle, WHITE);
	}
	void DrawFullscreenParallaxedBackground(Texture2D& tex, float viewCircleRadius, RayRectangle cameraBounds, Camera2D& camera)
	{
		Vector2 fromBoundsCorner = Vector2Subtract(camera.target, { cameraBounds.x, cameraBounds.y });
		Vector2 interpolationValue = Vector2DivideV(fromBoundsCorner, { cameraBounds.width, cameraBounds.height });

		int smallestTexDimension = Min(tex.width, tex.height);
		float sourceRadiusPixels = (viewCircleRadius * (float)smallestTexDimension);
		//Vector2 topLeftSourceCenter = { sourceRadiusPixels, sourceRadiusPixels };
		//Vector2 bottomRightSourceCenter = { (float)tex.width - sourceRadiusPixels, (float)tex.height - sourceRadiusPixels };
		Vector2 sourceCenter = { (float)tex.width * 0.5f, (float)tex.height * 0.5f };
		Vector2 toEdgeOnSmallestDimension = { (float)smallestTexDimension * 0.5f, (float)smallestTexDimension * 0.5f };
		Vector2 topLeftSourceCenter = Vector2Add(Vector2Subtract(sourceCenter, toEdgeOnSmallestDimension), { sourceRadiusPixels, sourceRadiusPixels });
		Vector2 bottomRightSourceCenter = Vector2Subtract(Vector2Add(sourceCenter, toEdgeOnSmallestDimension), { sourceRadiusPixels, sourceRadiusPixels });

		Vector2 interpolatedSourceCenter = { Lerp(topLeftSourceCenter.x, bottomRightSourceCenter.x, interpolationValue.x), Lerp(topLeftSourceCenter.y, bottomRightSourceCenter.y, interpolationValue.y) };
		RayRectangle sourceRect = { interpolatedSourceCenter.x - sourceRadiusPixels, interpolatedSourceCenter.y - sourceRadiusPixels, sourceRadiusPixels * 2.0f, sourceRadiusPixels * 2.0f };

		Vector2 toScreenCorner = { (float)GetScreenWidth() * 0.5f, (float)GetScreenHeight() * 0.5f };
		float targetSquareHalfSide = Vector2Length(toScreenCorner);

		Vector2 screenCenter = toScreenCorner;
		RayRectangle targetRect = { screenCenter.x - targetSquareHalfSide, screenCenter.y - targetSquareHalfSide, targetSquareHalfSide * 2.0f, targetSquareHalfSide * 2.0f };

		Vector2 origin = { targetRect.width * 0.5f, targetRect.height * 0.5f };
		// Compensate for origin
		targetRect.x = targetRect.x + origin.x;
		targetRect.y = targetRect.y + origin.y;
		DrawTexturePro(tex, sourceRect, targetRect, origin, camera.rotation, WHITE);
	}
	void DrawFullscreenParallaxedBackgroundLocal(Texture2D& tex, float viewCircleRadius, RayRectangle cameraBounds, Vector2 offsetPosition, Color colour)
	{
		Vector2 fromBoundsCorner = Vector2Subtract(offsetPosition, { cameraBounds.x, cameraBounds.y });
		Vector2 interpolationValue = Vector2DivideV(fromBoundsCorner, { cameraBounds.width, cameraBounds.height });

		int smallestTexDimension = Min(tex.width, tex.height);
		float sourceRadiusPixels = (viewCircleRadius * (float)smallestTexDimension);
		//Vector2 topLeftSourceCenter = { sourceRadiusPixels, sourceRadiusPixels };
		//Vector2 bottomRightSourceCenter = { (float)tex.width - sourceRadiusPixels, (float)tex.height - sourceRadiusPixels };
		Vector2 sourceCenter = { (float)tex.width * 0.5f, (float)tex.height * 0.5f };
		Vector2 toEdgeOnSmallestDimension = { (float)smallestTexDimension * 0.5f, (float)smallestTexDimension * 0.5f };
		Vector2 topLeftSourceCenter = Vector2Add(Vector2Subtract(sourceCenter, toEdgeOnSmallestDimension), { sourceRadiusPixels, sourceRadiusPixels });
		Vector2 bottomRightSourceCenter = Vector2Subtract(Vector2Add(sourceCenter, toEdgeOnSmallestDimension), { sourceRadiusPixels, sourceRadiusPixels });

		Vector2 interpolatedSourceCenter = { Lerp(topLeftSourceCenter.x, bottomRightSourceCenter.x, interpolationValue.x), Lerp(topLeftSourceCenter.y, bottomRightSourceCenter.y, interpolationValue.y) };
		RayRectangle sourceRect = { interpolatedSourceCenter.x - sourceRadiusPixels, interpolatedSourceCenter.y - sourceRadiusPixels, sourceRadiusPixels * 2.0f, sourceRadiusPixels * 2.0f };

		Vector2 toScreenCorner = { (float)GetScreenWidth() * 0.5f, (float)GetScreenHeight() * 0.5f };
		float targetSquareHalfSide = Vector2Length(toScreenCorner);

		Vector2 screenCenter = toScreenCorner;
		RayRectangle targetRect = { screenCenter.x - targetSquareHalfSide, screenCenter.y - targetSquareHalfSide, targetSquareHalfSide * 2.0f, targetSquareHalfSide * 2.0f };

		Vector2 origin = { targetRect.width * 0.5f, targetRect.height * 0.5f };
		// Compensate for origin
		targetRect.x = targetRect.x + origin.x;
		targetRect.y = targetRect.y + origin.y;
		DrawTexturePro(tex, sourceRect, targetRect, origin, 0.0f, colour);
	}

	void CheckFullscreenRenderTexture(RenderTexture2D& rt, float scale, PixelFormat format)
	{
		int targetWidth = (int)(GetScreenWidth() * scale);
		int targetHeight = (int)(GetScreenHeight() * scale);
		CheckRenderTexture(rt, targetWidth, targetHeight, format);
	}
	bool CheckRenderTexture(RenderTexture2D& rt, int targetWidth, int targetHeight, PixelFormat format)
	{
		if (rt.id < 0 || rt.texture.width != targetWidth || rt.texture.height != targetHeight)
		{
			if (rt.id >= 0)
			{
				UnloadRenderTexture(rt);
			}
			//rt = LoadRenderTexture(targetWidth, targetHeight);
			rt = LoadRenderTextureWithFormat(targetWidth, targetHeight, format);
			//SetTextureWrap(rt.texture, WRAP_REPEAT); // WebGL only supports WRAP_CLAMP on non power of two render targets...
			printf("New Fullscreen render texture with id = %d, width = %d and height %d\n", rt.id, rt.texture.width, rt.texture.height);
			return true;
		}
		return false;
	}
	RenderTexture2D LoadRenderTextureWithFormat(int width, int height, PixelFormat format)
	{
		RenderTexture2D target = { 0 };

		target.id = rlLoadFramebuffer(width, height);   // Load an empty framebuffer

		if (target.id > 0)
		{
			rlEnableFramebuffer(target.id);

			// Create color texture (default to RGBA)
			target.texture.id = rlLoadTexture(NULL, width, height, PIXELFORMAT_UNCOMPRESSED_R8G8B8A8, 1);
			target.texture.width = width;
			target.texture.height = height;
			target.texture.format = PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;
			target.texture.mipmaps = 1;

			// Create depth renderbuffer/texture
			target.depth.id = rlLoadTextureDepth(width, height, true);
			target.depth.width = width;
			target.depth.height = height;
			target.depth.format = 19;       //DEPTH_COMPONENT_24BIT?
			target.depth.mipmaps = 1;

			// Attach color texture and depth renderbuffer/texture to FBO
			rlFramebufferAttach(target.id, target.texture.id, RL_ATTACHMENT_COLOR_CHANNEL0, RL_ATTACHMENT_TEXTURE2D, 0);
			rlFramebufferAttach(target.id, target.depth.id, RL_ATTACHMENT_DEPTH, RL_ATTACHMENT_RENDERBUFFER, 0);

			// Check if fbo is complete with attachments (valid)
			if (rlFramebufferComplete(target.id)) printf("FBO: [ID %i] Framebuffer object created successfully", target.id);

			rlDisableFramebuffer();
		}
		else printf("FBO: Framebuffer object can not be created");

		return target;
	}

	void DrawRenderTarget(Texture2D& tex, int x, int y, Color colour)
	{
		// NOTE: Render texture y-flipped
		DrawTextureRec(tex, { 0.0f, 0.0f, (float)tex.width, (float)-tex.height }, { 0.0f, 0.0f }, colour);
	}
	void DrawRenderTargetStretched(Texture2D& tex, int x, int y, int targetWidth, int targetHeight, Color colour)
	{
		DrawTexturePro(tex, { 0.0f, 0.0f, (float)tex.width, (float)-tex.height }, { (float)x, (float)y, (float)targetWidth, (float)targetHeight }, { 0.0f, 0.0f }, 0.0f, WHITE);
	}

	void DrawRenderTargetMultiTexture(Texture2D& tex, int x, int y, Color colour)
	{
		rlPushMatrix();
		rlRotatef(-90, 1, 0, 0);
		//void DrawPlane(Vector3 centerPos, Vector2 size, Color color);  //Draw a plane XZ
		//DrawPlane({ (float)x + ((float)tex.width * 0.5f), 0, (float)y + ((float)tex.height * 0.5f) }, { (float)tex.width, (float)tex.height }, RED);
		DrawModel(s_planeModel, { (float)x + ((float)tex.width * 0.5f), 0, (float)y + ((float)tex.height * 0.5f) }, 100.0f, YELLOW);
		rlPopMatrix();
	}

	void DrawCircle(Vector2 origin, float radius, int subdivisions, Color colour)
	{
		if (rlCheckRenderBatchLimit(subdivisions * 3)) rlDrawRenderBatchActive();

		rlBegin(RL_TRIANGLES);
		rlColor4ub(colour.r, colour.g, colour.b, colour.a);
		float angleStep = (PI * 2.0f) / (float)subdivisions;
		float angleA = 0.0f;
		float angleB = angleStep;
		for (int i = 0; i < subdivisions; i++)
		{
			rlVertex2f(origin.x, origin.y);
			rlVertex2f(origin.x + cosf(-angleA) * radius, origin.y + sinf(-angleA) * radius);
			rlVertex2f(origin.x + cosf(-angleB) * radius, origin.y + sinf(-angleB) * radius);
			angleA = angleB;
			angleB = angleB + angleStep;
		}
		rlEnd();
	}

	void DrawCircleArc(Vector2 origin, float radius, float angleStart, float angleEnd, int subdivisions, Color colour)
	{
		if (rlCheckRenderBatchLimit(subdivisions * 3)) rlDrawRenderBatchActive();

		rlBegin(RL_TRIANGLES);
		rlColor4ub(colour.r, colour.g, colour.b, colour.a);
		float angleStep = (angleEnd - angleStart) / (float)subdivisions;
		float angleA = angleStart;
		float angleB = angleStart + angleStep;
		for (int i = 0; i < subdivisions; i++)
		{
			rlVertex2f(origin.x, origin.y);
			rlVertex2f(origin.x + cosf(-angleA) * radius, origin.y + sinf(-angleA) * radius);
			rlVertex2f(origin.x + cosf(-angleB) * radius, origin.y + sinf(-angleB) * radius);
			angleA = angleB;
			angleB = angleB + angleStep;
		}
		rlEnd();
	}

	void DrawTriangles(Vertex2D* points, int pointCount)
	{
		if (rlCheckRenderBatchLimit(pointCount)) rlDrawRenderBatchActive();

		rlBegin(RL_TRIANGLES);
		for (int i = 0; i < pointCount; i++)
		{
			Vector2 pos = points[i].m_pos;
			Color colour = points[i].m_colour;
			rlColor4ub(colour.r, colour.g, colour.b, colour.a);
			rlVertex2f(pos.x, pos.y);
		}
		rlEnd();
	}
	void DrawTrianglesTextured(Vertex2DTex* points, int pointCount, Texture2D tex)
	{
		int triangleCount = pointCount / 3;
		int quadVertCount = triangleCount * 4;
		if (rlCheckRenderBatchLimit(quadVertCount)) rlDrawRenderBatchActive();

		rlEnableTexture(tex.id);
		rlBegin(RL_QUADS);
		for (int i = 0; i < pointCount; i++)
		{
			Vector2 pos = points[i].m_pos;
			Color colour = points[i].m_colour;
			Vector2 texCoord = points[i].m_texCoord;
			rlColor4ub(colour.r, colour.g, colour.b, colour.a);
			rlTexCoord2f(texCoord.x, texCoord.y);
			rlVertex2f(pos.x, pos.y);
			if ((i % 3) == 2) // Add the fourth quad vert, just replicating the last triangle vert
			{
				rlColor4ub(colour.r, colour.g, colour.b, colour.a);
				rlTexCoord2f(texCoord.x, texCoord.y);
				rlVertex2f(pos.x, pos.y);
			}
		}
		rlEnd();
		rlDisableTexture();
	}
	void DrawTrianglesIndexed(std::vector<Vertex2D>& vertices, std::vector<uint16_t>& indices)
	{
		//#if !defined(SUPPORT_QUADS_DRAW_MODE)
		int indexCount = (int)indices.size();
		int triangleCount = indexCount / 3;
		int quadVertCount = triangleCount * 4;
		if (rlCheckRenderBatchLimit(quadVertCount)) rlDrawRenderBatchActive();

		rlEnableTexture(s_shapeTexture.id);
		rlBegin(RL_QUADS);
		int triangleVert = 0;
		for (int i = 0; i < indexCount; i++)
		{
			uint16_t index = indices[i];
			Vertex2D& vert = vertices[index];
			Vector2 pos = vert.m_pos;
			Color colour = vert.m_colour;
			rlColor4ub(colour.r, colour.g, colour.b, colour.a);
			rlVertex2f(pos.x, pos.y);
			triangleVert++;
			if (triangleVert == 3) // Add the fourth quad vert, just replicating the last triangle vert
			{
				rlColor4ub(colour.r, colour.g, colour.b, colour.a);
				rlVertex2f(pos.x, pos.y);
				triangleVert = 0;
			}
		}
		rlEnd();
		rlDisableTexture();

		/*rlBegin(RL_TRIANGLES);
		for (int i = 0; i < indexCount; i++)
		{
			uint16_t index = indices[i];
			Vertex2D& vert = vertices[index];
			Vector2 pos = vert.m_pos;
			Color colour = vert.m_colour;
			rlColor4ub(colour.r, colour.g, colour.b, colour.a);
			rlVertex2f(pos.x, pos.y);
		}
		rlEnd();*/
		//#endif
	}
	void DrawTrianglesIndexedTextured(std::vector<Vertex2D>& vertices, std::vector<uint16_t>& indices, Texture2D& tex)
	{
		//#if !defined(SUPPORT_QUADS_DRAW_MODE)
		int indexCount = (int)indices.size();
		int triangleCount = indexCount / 3;
		int quadVertCount = triangleCount * 4;
		if (rlCheckRenderBatchLimit(quadVertCount)) rlDrawRenderBatchActive();

		rlEnableTexture(tex.id);
		rlBegin(RL_QUADS);
		int triangleVert = 0;
		for (int i = 0; i < indexCount; i++)
		{
			uint16_t index = indices[i];
			Vertex2D& vert = vertices[index];
			Vector2 pos = vert.m_pos;
			Color colour = vert.m_colour;
			rlColor4ub(colour.r, colour.g, colour.b, colour.a);
			rlVertex2f(pos.x, pos.y);
			triangleVert++;
			if (triangleVert == 3) // Add the fourth quad vert, just replicating the last triangle vert
			{
				rlColor4ub(colour.r, colour.g, colour.b, colour.a);
				rlVertex2f(pos.x, pos.y);
				triangleVert = 0;
			}
		}
		rlEnd();
		rlDisableTexture();
	}
	void DrawPolygon(Vertex2D* pointsIn, int pointCountIn)
	{
		std::vector<Vertex2D> points = std::vector<Vertex2D>();
		int pointCount = pointCountIn;
		if (pointCount < 3)
		{
			return;
		}
		points.reserve(pointCount);
		points.resize(pointCount);
		for (int i = 0; i < pointCount; i++)
		{
			points[i] = pointsIn[i];
		}
		while (true)
		{
			bool anyTrianglesMade = false;
			for (int i = 0; i < pointCount; i++)
			{
				int pPrev = ((i + pointCount) - 1) % pointCount;
				Vector2 pPrevPoint = points[pPrev].m_pos;
				int pCur = i;
				Vector2 pCurPoint = points[pCur].m_pos;
				int pNext = (i + 1) % pointCount;
				Vector2 pNextPoint = points[pNext].m_pos;

				Vector3 currentToPrev = Vector2ToVector3(Vector2Subtract(pPrevPoint, pCurPoint));
				Vector3 currentToNext = Vector2ToVector3(Vector2Subtract(pNextPoint, pCurPoint));
				Vector3 crossProduct = Vector3CrossProduct(currentToPrev, currentToNext);
				float wedgeProduct = crossProduct.z;
				if (wedgeProduct <= 0)
				{
					continue;
				}

				bool anyPointsInside = false;
				for (int j = 2; j < pointCount - 1; j++)
				{
					int pChecked = (i + j) % pointCount;
					Vector2 pCheckedPoint = points[pChecked].m_pos;
					if (IsPointInTriangle(pCheckedPoint, pPrevPoint, pCurPoint, pNextPoint))
					{
						anyPointsInside = true;
						break;
					}
				}
				if (anyPointsInside)
				{
					continue;
				}

				anyTrianglesMade = true;
				DrawTriangle(pPrevPoint, pCurPoint, pNextPoint, points[pCur].m_colour);
				points.erase(points.begin() + pCur);
				pointCount--;
			}

			if (!anyTrianglesMade)
			{
				break;
			}
		}
	}
	void DrawPolygonPoints(std::vector<Vector2>& pointsIn, Color colour)
	{
		std::vector<Vector2> points = std::vector<Vector2>();
		int pointCount = (int)pointsIn.size();
		if (pointCount < 3)
		{
			return;
		}
		points.reserve(pointCount);
		points.resize(pointCount);
		for (int i = 0; i < pointCount; i++)
		{
			points[i] = pointsIn[i];
		}
		while (true)
		{
			bool anyTrianglesMade = false;
			for (int i = 0; i < pointCount; i++)
			{
				int pPrev = ((i + pointCount) - 1) % pointCount;
				Vector2 pPrevPoint = points[pPrev];
				int pCur = i;
				Vector2 pCurPoint = points[pCur];
				int pNext = (i + 1) % pointCount;
				Vector2 pNextPoint = points[pNext];

				Vector3 currentToPrev = Vector2ToVector3(Vector2Subtract(pPrevPoint, pCurPoint));
				Vector3 currentToNext = Vector2ToVector3(Vector2Subtract(pNextPoint, pCurPoint));
				Vector3 crossProduct = Vector3CrossProduct(currentToPrev, currentToNext);
				float wedgeProduct = crossProduct.z;
				if (wedgeProduct <= 0)
				{
					continue;
				}

				bool anyPointsInside = false;
				for (int j = 2; j < pointCount - 1; j++)
				{
					int pChecked = (i + j) % pointCount;
					Vector2 pCheckedPoint = points[pChecked];
					if (IsPointInTriangle(pCheckedPoint, pPrevPoint, pCurPoint, pNextPoint))
					{
						anyPointsInside = true;
						break;
					}
				}
				if (anyPointsInside)
				{
					continue;
				}

				anyTrianglesMade = true;
				DrawTriangle(pPrevPoint, pCurPoint, pNextPoint, colour);
				points.erase(points.begin() + pCur);
				pointCount--;
			}

			if (!anyTrianglesMade)
			{
				break;
			}
		}
	}
	void GetPolygonTriangulation(std::vector<Vector2>& pointsIn, std::vector<uint16_t>& indicies)
	{
		indicies.clear();
		std::vector<std::pair<Vector2, uint16_t>> points;
		uint16_t pointCount = (uint16_t)pointsIn.size();
		if (pointCount < 3)
		{
			return;
		}
		points.reserve(pointCount);
		points.resize(pointCount);
		for (uint16_t i = 0; i < pointCount; i++)
		{
			points[i] = { pointsIn[i], i };
		}
		while (true)
		{
			bool anyTrianglesMade = false;
			for (int i = 0; i < pointCount; i++)
			{
				int pPrev = ((i + pointCount) - 1) % pointCount;
				Vector2 pPrevPoint = points[pPrev].first;
				int pCur = i;
				Vector2 pCurPoint = points[pCur].first;
				int pNext = (i + 1) % pointCount;
				Vector2 pNextPoint = points[pNext].first;

				Vector3 currentToPrev = Vector2ToVector3(Vector2Subtract(pPrevPoint, pCurPoint));
				Vector3 currentToNext = Vector2ToVector3(Vector2Subtract(pNextPoint, pCurPoint));
				Vector3 crossProduct = Vector3CrossProduct(currentToPrev, currentToNext);
				float wedgeProduct = crossProduct.z;
				if (wedgeProduct <= 0)
				{
					continue;
				}

				bool anyPointsInside = false;
				for (int j = 2; j < pointCount - 1; j++)
				{
					int pChecked = (i + j) % pointCount;
					Vector2 pCheckedPoint = points[pChecked].first;
					if (IsPointInTriangle(pCheckedPoint, pPrevPoint, pCurPoint, pNextPoint))
					{
						anyPointsInside = true;
						break;
					}
				}
				if (anyPointsInside)
				{
					continue;
				}

				anyTrianglesMade = true;

				//DrawTriangle(pPrevPoint, pCurPoint, pNextPoint, colour);
				indicies.push_back(points[pPrev].second);
				indicies.push_back(points[pCur].second);
				indicies.push_back(points[pNext].second);
				points.erase(points.begin() + pCur);
				pointCount--;
			}

			if (!anyTrianglesMade)
			{
				break;
			}
		}
	}


	Texture2D& GetShapeTexture()
	{
		return s_shapeTexture;
	}
	Texture2D& GetLineTexture()
	{
		return s_lineTexture;
	}
	Texture2D& GetCloseButtonTexture()
	{
		return s_closeButtonTexture;
	}
	Shader GetDefaultShader()
	{
		return rlGetShaderDefault();
	}

	void SetPointFilterButLinearMipFilter(Texture2D& tex)
	{
		rlTextureParameters(tex.id, RL_TEXTURE_MIN_FILTER, RL_TEXTURE_FILTER_NEAREST_MIP_LINEAR); // Try linear mip filter
		rlTextureParameters(tex.id, RL_TEXTURE_MAG_FILTER, RL_TEXTURE_FILTER_NEAREST);
	}
	void BeginScissorModeAlt(int x, int y, int width, int height)
	{
		rlDrawRenderBatchActive();

		rlEnableScissorTest();
		rlScissor(x, y, width, height);
	}

	Color s_playerColours[] = {
{ 101,2,102,255 }, // Purple
{ 253,102,5,255 }, // Orange
{48,151,7,255 }, // Green
{ 253,253,5,255 }, // Yellow
{5,49,255,255}, // Blue
{ 208,2,4,255 }, // Red
{ 4,102,100,255 }, // Teal
{ 128,128,128,255 } // Gray
	};

	const char* s_playerColourNames[] = {
		"Purple",
		"Orange",
		"Green",
		"Yellow",
		"Blue",
		"Red",
		"Teal",
		"Gray"
	};
	Color GetPlayerColour(int playerID)
	{
		if (playerID < 0 || playerID >= 8)
		{
			return WHITE;
		}
		return s_playerColours[playerID];
	}
	const char* GetPlayerColourName(int playerID)
	{
		if (playerID < 0 || playerID >= 8)
		{
			return "Unknown";
		}
		return s_playerColourNames[playerID];
	}
}
