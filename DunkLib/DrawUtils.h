#pragma once
#include "raylib.h"
#include "Vector2Int.h"
#include <vector>

#include "Rect.h"

struct Vertex2D
{
	Vector2 m_pos;
	Color m_colour;
	Vector2 m_normal;
};
struct Vertex2DTex
{
	Vector2 m_pos;
	Color m_colour;
	Vector2 m_texCoord;
};

namespace DrawUtils
{
	void InitializeResources(bool headlessMode = false);

	void DrawSpriteIntoBox(Texture2D& tex, Rect box);
	void DrawSprite(Texture2D& tex, Vector2 position, float rotation, Color colour);
	void DrawSprite(Texture2D& tex, Vector2 position, float rotation, Color colour, Vector2 scale);
	void DrawSpriteSlice(Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, int sliceNumber, float scale = 1.0f, Vector2 origin01 = { 0.5f, 0.5f });

	// Progress from 0 to 1, where last frame only appears at 1
	void DrawSpriteSliceFloat(Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, float animationProgress, Vector2 origin01 = { 0.5f, 0.5f });

	// Progress from 0 to 1 (will loop)
	void DrawSpriteSliceFloat01(const Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, float animationProgress, float scale = 1.0f);
	void DrawSpriteSliceFloat01Pro(const Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, float animationProgress, Vector2 scale = { 1.0f, 1.0f }, Vector2 origin01 = { 0.5f, 0.5f }, bool horizontalFlip = false, bool verticalFlip = false);
	void DrawSpriteSliceFloatBorder(Texture2D& tex, Vector2 position, float rotation, Color colour, int sliceSize, float animationProgress);
	void DrawSpriteLine(const Texture2D& tex, Vector2 from, Vector2 to, Color colour);
	void DrawSpriteLineTiled(const Texture2D& tex, Vector2 from, Vector2 to, Color colour);
	void DrawSpriteLineTiledAndVerticallySliced(const Texture2D& tex, Vector2 from, Vector2 to, Color colour, int sliceSize, int sliceNumber);
	void DrawSpriteTiled(const Texture2D& tex, Vector2 position, float rotation, Color colour, Vector2 scale, Vector2 originProportion);
	void DrawSpriteArrow(const Texture2D& tex, Vector2 from, Vector2 to, Color colour);
	void DrawSpriteLineSlice(Texture2D& tex, Vector2 from, Vector2 to, Color colour, int sliceSize, int sliceNumber);
	void DrawSpriteFromOrigin(Texture2D& tex, Vector2 position, float rotation, Vector2 origin, Color colour);
	
	void DrawFullscreenTiledTexture(Texture2D& tex, Vector2 uvTiling, Vector2 uvOffset, Camera2D& camera);
	void DrawFullscreenParallaxedBackground(Texture2D& tex, float viewCircleRadius, RayRectangle cameraBounds, Camera2D& camera);
	void DrawFullscreenParallaxedBackgroundLocal(Texture2D& tex, float viewCircleRadius, RayRectangle cameraBounds, Vector2 offsetPosition, Color colour);
	void CheckFullscreenRenderTexture(RenderTexture2D& rt, float scale = 1.0f, PixelFormat format = PIXELFORMAT_UNCOMPRESSED_R8G8B8A8);
	bool CheckRenderTexture(RenderTexture2D& rt, int targetWidth, int targetHeight, PixelFormat format = PIXELFORMAT_UNCOMPRESSED_R8G8B8A8);
	void DrawRenderTarget(Texture2D& tex, int x, int y, Color colour);
	void DrawRenderTargetStretched(Texture2D& tex, int x, int y, int targetWidth, int targetHeight, Color colour);
	void DrawRenderTargetMultiTexture(Texture2D& tex, int x, int y, Color colour);
	void DrawCircle(Vector2 origin, float radius, int subdivisions, Color colour);
	void DrawCircleArc(Vector2 origin, float radius, float angleStart, float angleEnd, int subdivisions, Color colour);
	// Points must be in counter-clockwise order
	void DrawTriangles(Vertex2D* points, int pointCount);
	void DrawTrianglesTextured(Vertex2DTex* points, int pointCount, Texture2D tex);
	void DrawTrianglesIndexed(std::vector<Vertex2D>& vertices, std::vector<uint16_t>& indices);
	void DrawTrianglesIndexedTextured(std::vector<Vertex2D>& vertices, std::vector<uint16_t>& indices, Texture2D& tex);
	// Points must be in counter-clockwise order
	void DrawPolygon(Vertex2D* points, int pointCount);
	void DrawPolygonPoints(std::vector<Vector2>& points, Color colour);
	void GetPolygonTriangulation(std::vector<Vector2>& points, std::vector<uint16_t>& indicies);

	RenderTexture2D LoadRenderTextureWithFormat(int width, int height, PixelFormat format);

	Texture2D& GetShapeTexture();
	Texture2D& GetLineTexture();
	Texture2D& GetCloseButtonTexture();
	Shader GetDefaultShader();
	
	void SetPointFilterButLinearMipFilter(Texture2D& tex);
	void BeginScissorModeAlt(int x, int y, int width, int height);
	Color GetPlayerColour(int playerID);
	const char* GetPlayerColourName(int playerID);
	bool IsInitialised();
}