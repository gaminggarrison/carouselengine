#pragma once

#include "cereal/cereal.hpp"
#include "RaylibCereal.h"

using namespace std;

struct EngineConfigData
{
	std::string m_rayGuiStyleName;
	std::string m_guiFontName;
	int m_guiFontSize;
	int m_guiFontAlignmentSize;
	std::string m_defaultFontName;
	int m_defaultFontSize;

	static EngineConfigData& Get();

	template<class Archive>
	void DoArchive(Archive& archive, std::uint32_t const version)
	{
		archive(CEREAL_NVP(m_rayGuiStyleName), CEREAL_NVP(m_guiFontName), CEREAL_NVP(m_guiFontSize), CEREAL_NVP(m_guiFontAlignmentSize), CEREAL_NVP(m_defaultFontName), CEREAL_NVP(m_defaultFontSize));
	}

	template<class Archive>
	void save(Archive& archive, std::uint32_t const version)
	{
		DoArchive(archive, version);
	}

	template<class Archive>
	void load(Archive& archive, std::uint32_t const version)
	{
		DoArchive(archive, version);
	}
};
CEREAL_CLASS_VERSION(EngineConfigData, 0);
