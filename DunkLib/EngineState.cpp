#include "EngineState.h"

EngineState::EngineState()
{
	
}

#include "TranslationUtils.h"
#include "DebugUtils.h"
#include "FolderUtils.h"
#include "AudioUtils.h"
#include "UIManager.h"
#include "DrawUtils.h"
#include "GUIUtils.h"
#include "CRCUtils.h"
#include "StaticStorage.h"

#include "CarouselSetup.h"
#include "GameSpecificFunctions.h"
#include "DedicatedServerState.h"
#include "ClockUtils.h"

EngineState* EngineState::s_engineState;

void EngineState::Init(int debugInstance, GameSpecificFunctions* gameFunctions, bool headlessMode, bool autoStartGame)
{
	if (!headlessMode)
	{
		SetWindowTitle(gameFunctions->GetGameName()); // Would be the first thing to fail if we're using the static version of raylib in a dynamic dll...
	}
	m_headlessMode = headlessMode;
	s_initialized = true;
	s_engineState = this;
	m_debugInstance = debugInstance;
	CarouselSetup::s_instance = &m_carouselSetup;
	m_carouselSetup.Initialise(gameFunctions, "");

	if (!headlessMode)
	{
		InitAudioDevice();
	}
	InitializeResourcesWithSwitcheroo();

	m_stateMachine.Init();
	printf("Game Init finished!\n");

	if (autoStartGame)
	{
		if (headlessMode)
		{
			printf("Pushing Dedicated Server State\n");
			StateMachine::Instance()->PushState<DedicatedServerState>();
		}
		else
		{
			gameFunctions->StartGame();
		}
	}
}

void EngineState::PrepareForReload()
{
	AudioUtils::Shutdown();
}

void EngineState::Cleanup()
{
	printf("Game Cleanup starting!\n");
	m_stateMachine.Cleanup();
	AudioUtils::Shutdown();
	//CloseAudioDevice(); // Seems to crash due to heap corruption caused by playing lots of sounds... :(
}

void EngineState::InitializeResources()
{
	bool headless = s_engineState->IsHeadless();
	SetTraceLogLevel(LOG_WARNING);
	ConfigData::Initialise(headless);

	DrawUtils::InitializeResources(headless);
	GUIUtils::InitializeResources(headless);

	TranslationUtils::Initialise();
#if (defined PLATFORM_WEB || defined TESTING_WEB)
	const size_t dynamicSpacePerFrame = 4;
#else
	const size_t dynamicSpacePerFrame = 8;
#endif
	StaticStorage::Init(1024 * 1024 * dynamicSpacePerFrame);
	AudioUtils::Init(headless);
	UIManager::Init();
	CRCUtils::Initialize();

	if (!StateMachine::Instance()->IsEmpty())
	{
		StateMachine::Instance()->GetHead().HandleReload();
	}
	else
	{
		CarouselSetup::GetGameFunctions()->InitialiseGameSpecificData();
	}
}

void EngineState::InitializeResourcesWithSwitcheroo()
{
	CarouselSetup::s_instance = &m_carouselSetup;
	DebugUtils::s_debugInstance = m_debugInstance;
	ConfigData::InstanceObject = &m_config;
	s_engineState = this;
	m_minigameLoader.Init(IsHeadless());

	m_stateMachine.Init(); // Re-set the instance pointer

	bool useLocalAssets = ShouldUseLocalAssets();
	if (useLocalAssets)
	{
		// Allow loading assets from project directory when debugging
		FolderUtils::WorkingDirectorySwitcheroo(&InitializeResources);
	}
	else
	{
		const char* workingDir = GetWorkingDirectory();
		printf("Working directory for loading is %s\n", workingDir);
		InitializeResources();
	}
}

void EngineState::HandleDebugKeys()
{
	if (IsKeyPressed(KEY_F2))
	{
		TranslationUtils::NextLanguage();
	}
	if (IsKeyPressed(KEY_ENTER) && (IsKeyDown(KEY_LEFT_ALT) || IsKeyDown(KEY_RIGHT_ALT)))
	{
		// Toggle fullscreen
		int monitorW = GetMonitorWidth(0);
		int monitorH = GetMonitorHeight(0);
		if (!IsWindowFullscreen()) // If it's GOING to toggle to fullscreen
		{
			SetWindowSize(monitorW, monitorH);
		}
		ToggleFullscreen();
		if (!IsWindowFullscreen()) // If it toggled BACK from fullscreen
		{
			int w = 800;
			int h = 450;
			SetWindowSize(w, h);
			SetWindowPosition((monitorW / 2) - (w / 2), (monitorH / 2) - (h / 2));
		}
	}
}

bool EngineState::s_initialized;

int EngineState::Run()
{
	s_engineState = this;
	if (s_initialized == false) // Static variable is lost by dll reload, so used to check if initialization is needed
	{
		printf("Reinitializing!");
		s_initialized = true;
		InitializeResourcesWithSwitcheroo();
	}

	if (!m_headlessMode)
	{
		HandleDebugKeys();
	}

	float deltaTime = GetFrameTime();
	if (m_headlessMode)
	{
		// In theory we could space out the updates from the top level, but actually we want to run the server at any given rate
		// ... and let the NetworkGameState progress time forward when neccessary
		static unsigned int s_lastTime = 0;
		unsigned int timeNow = Millisecs();
		unsigned int timeDiff = timeNow - s_lastTime;
		s_lastTime = timeNow;
		deltaTime = (float)timeDiff / 1000.0f;
	}
	m_stateMachine.Update(deltaTime);

	if (!m_headlessMode)
	{
		BeginDrawing();

		ClearBackground(BLACK);

		m_stateMachine.Draw();

		EndDrawing();
		AudioUtils::ApplySoundChanges();
	}

	if (m_stateMachine.IsEmpty())
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
bool EngineState::IsHeadless()
{
	return m_headlessMode;
}
bool EngineState::ShouldUseLocalAssets()
{
	bool useLocalAssets = DebugUtils::s_debugInstance >= 0;
	return useLocalAssets;
}