#pragma once
#include "ConfigData.h"
#include "StateMachine.h"
#include "CarouselSetup.h"
#include "MinigameLoader.h"
struct GameSpecificFunctions;

class EngineState
{
public:
	EngineState();
	void Init(int debugInstance, GameSpecificFunctions* gameFunctions, bool headlessMode = false, bool autoStartGame = true);
	void PrepareForReload();
	void Cleanup();
	static void InitializeResources();
	void InitializeResourcesWithSwitcheroo();
	int Run();
	static EngineState* s_engineState;

	bool IsHeadless();
	bool ShouldUseLocalAssets();
private:
	void HandleDebugKeys();

	static bool s_initialized;

	ConfigData m_config;
	int m_debugInstance;
	StateMachine m_stateMachine;
	bool m_headlessMode;
	CarouselSetup m_carouselSetup;
	MinigameLoader m_minigameLoader;
};