#include "FastForwardEvent.h"
#include "NetworkStateControl.h"

FastForwardEvent::FastForwardEvent()
{

}

FastForwardEvent::~FastForwardEvent()
{

}

FastForwardEvent::FastForwardEvent(int playerID, int playerEventID) : m_targetFrame(0)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}
FastForwardEvent::FastForwardEvent(int playerID, int playerEventID, int targetFrame) : m_targetFrame(targetFrame)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void FastForwardEvent::RunEvent(FrameState& frameState)
{

}
void FastForwardEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{
	int catchupFrames = m_targetFrame - networkStateControl.GetHeadFrame();
	printf("Fast forwarding from frame %d to %d (%d frames)!\n", networkStateControl.GetHeadFrame(), m_targetFrame, catchupFrames);
	networkStateControl.SetFramesToCatchup(catchupFrames);
}

Color FastForwardEvent::GetDebugColour()
{
	return DARKGREEN;
}

int FastForwardEvent::GetDebugPriority()
{
	return 10;
}

bool FastForwardEvent::DoRewindForEvent()
{
	return false; // This is a special event that should always run
}