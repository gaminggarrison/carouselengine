#pragma once
#include "NetworkEvent.h"

struct FastForwardEvent : public NetworkEvent
{
	int m_targetFrame;

	FastForwardEvent();
	virtual ~FastForwardEvent();
	FastForwardEvent(int playerID, int playerEventID);
	FastForwardEvent(int playerID, int playerEventID, int targetFrame);

	virtual void RunEvent(FrameState& frameState);
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();
	virtual bool DoRewindForEvent();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_targetFrame);
	}
};

CEREAL_REGISTER_TYPE(FastForwardEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, FastForwardEvent);
