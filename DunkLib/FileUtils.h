#pragma once

#include <fstream>
#include "cereal/archives/binary.hpp"
#include "cereal/archives/json.hpp"

namespace FileUtils
{
	template<typename T>
	void SaveObjectToBinary(const char* filename, T& object)
	{
		std::ofstream outFile(filename, std::ios::out | std::ios::binary | std::ios::trunc);
		{
			cereal::BinaryOutputArchive oarchive(outFile);
			oarchive(object);
		}
		outFile.close();
	}
	template<typename T>
	bool LoadObjectFromBinary(const char* filename, T& object)
	{
		std::ifstream inFile(filename, std::ios::in | std::ios::binary);
		bool worked = false;
		if (inFile.is_open())
		{
			cereal::BinaryInputArchive iarchive(inFile);
			iarchive(object);
			worked = true;
		}
		else
		{
			printf("Could not open binary file %s\n", filename);
		}
		inFile.close();
		return worked;
	}

	template<typename T>
	void SaveObjectToJson(const char* filePath, T& object)
	{
		std::ofstream outFile(filePath, std::ios::out | std::ios::binary | std::ios::trunc);
		{
			cereal::JSONOutputArchive oarchive(outFile);
			oarchive(object);
		}
		outFile.close();
	}
	template<typename T>
	void LoadObjectFromJson(const char* filePath, T& object)
	{
		try
		{
			std::ifstream inFile(filePath, std::ios::in | std::ios::binary);
			if (inFile.is_open())
			{
				cereal::JSONInputArchive iarchive(inFile);
				iarchive(object);
			}
			else
			{
				printf("Could not open json file %s\n", filePath);
			}
			inFile.close();
		}
		catch (std::exception e)
		{
			printf("Failed to load json %s. Error %s\n", filePath, e.what());
		}
	}
	
	template<typename T>
	bool SaveObjectToJsonMinimal(const char* filePath, T& object)
	{
		std::ofstream outFile(filePath, std::ios::out | std::ios::binary | std::ios::trunc);
		{
			cereal::JSONOutputArchive oarchive(outFile);

			uint32_t writeVersion = cereal::detail::Version<T>().version;
			oarchive(cereal::make_nvp("version", writeVersion));
			object.save(oarchive, writeVersion);
		}
		outFile.close();
		return true;
	}
	template<typename T>
	bool LoadObjectFromJsonMinimal(const char* filePath, T& target)
	{
		std::ifstream inFile(filePath, std::ios::in | std::ios::binary);
		if (inFile.is_open())
		{
			try
			{
				cereal::JSONInputArchive iarchive(inFile);
				uint32_t readVersion = 0;
				iarchive(cereal::make_nvp("version", readVersion));
				target.load(iarchive, readVersion);
			}
			catch (int e)
			{
				printf("Failed to load json from %s.  Error: %d\n", filePath, e);
				return false;
			}
			catch (std::exception e)
			{
				printf("Failed to load json from %s. Error: %s\n", filePath, e.what());
#ifndef RELEASE
				std::string message = std::string("Failed to load json from ") + filePath + ".  Error: " + e.what();
				throw std::runtime_error(message);
#endif
				return false;
			}
			catch (...)
			{
				printf("Failed to load json from %s.\n", filePath);
				return false;
			}
		}
		else
		{
			printf("File %s doesn't exist!\n", filePath);
			return false;
		}
		inFile.close();
		return true;
	}
}