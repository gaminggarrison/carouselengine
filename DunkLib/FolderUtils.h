#pragma once
#include "raylib.h"
#include <string>

#include "DebugUtils.h"

namespace FolderUtils
{
	inline std::string BeginWorkingDirectory(const char* directory)
	{
		const char* workingDir = GetWorkingDirectory(); // Allow loading assets from project directory when debugging
														// Have to copy result of GetWorkingDirectory
		std::string workingDirCopy = std::string(workingDir);

		printf("Working directory originally %s\n", workingDir);
		ChangeDirectory(directory);
		const char* newWorkingDir = GetWorkingDirectory(); // To check
		printf("Using working directory of %s\n", newWorkingDir);
		return workingDirCopy;
	}
	inline void EndWorkingDirectory(std::string originalWorkingDirectory)
	{
		ChangeDirectory(originalWorkingDirectory.c_str());
		const char* backWorkingDir = GetWorkingDirectory(); // To check
		printf("Working directory back to %s\n", backWorkingDir);
	}

	inline void WorkingDirectorySwitcheroo(void(*ptr)())
	{
		// SOLUTION_DIR defined in preprocessor definitions https://stackoverflow.com/questions/38202907/getting-projectdir-as-a-string-or-char-in-c
		std::string oldDir = BeginWorkingDirectory(SOLUTION_DIR);
		(*ptr)();
		EndWorkingDirectory(oldDir);
	}
	template<typename R, typename... Args>
	auto WorkingDirectorySwitcheroo2(R fun, Args&&... args) -> decltype(fun(std::forward<Args>(args)...))
	{
		// SOLUTION_DIR defined in preprocessor definitions https://stackoverflow.com/questions/38202907/getting-projectdir-as-a-string-or-char-in-c
		std::string oldDir = BeginWorkingDirectory(SOLUTION_DIR);
		fun(std::forward<Args>(args)...);
		EndWorkingDirectory(oldDir);
	}



	template<typename R, typename... Args>
	auto WorkingDirectorySwitcherooIfDebug(R fun, Args&&... args) -> decltype(fun(std::forward<Args>(args)...))
	{
		if (DebugUtils::s_debugInstance >= 0)
		{
			return WorkingDirectorySwitcheroo2(fun, std::forward<Args>(args)...);
		}
		else
		{
			fun(std::forward<Args>(args)...);
		}
	}
}