#include "FrameState.h"
#include "RollbackAllocator.h"
#include "PointerMath.h"
#include "CRCArchive.hpp"

void* s_currentlyLoadedFrameMemory = nullptr;

FrameState::FrameState()
{
	Init();
}

void FrameState::Init()
{
	size_t size = StaticStorage::GetStorageSize();
	m_frameMemory = malloc(size);
	printf("Allocating new frame at %p\n", m_frameMemory);
	m_memorySize = size;

	SetupCustomFrameState();
}

FrameState::FrameState(const FrameState& state)
{
	Init();
	memcpy(m_frameMemory, state.m_frameMemory, m_memorySize);
}

FrameState::~FrameState()
{
	// We don't want to destruct the stuff that's currently in shared memory,
	// so make sure to load the frame first and delete its data, then reset the reference so the shared memory will get refilled
	if (m_frameMemory != nullptr)
	{
		LoadFrameState(true);
		m_mainData = nullptr;
		printf("Freeing frame at %p\n", m_frameMemory);
		free(m_frameMemory);
		m_frameMemory = nullptr;
		ResetStaticStorageAndCachePointer();
	}
}
void FrameState::SetupCustomFrameState()
{
	ResetStaticStorageAndCachePointer();
	if (m_mainData != nullptr)
	{
		m_mainData = nullptr; // Clear any existing data
		ResetStaticStorageAndCachePointer(); // ... but make sure we're truly in a reset state
	}
	m_mainData = MakeAsT<CustomFrameStateWrapper>();
	SaveFrameState();
	ResetStaticStorageAndCachePointer();
}

void FrameState::SaveFrameState(bool skipCopy)
{
	if (!skipCopy)
	{
		memcpy(m_frameMemory, StaticStorage::GetStorage(), StaticStorage::GetStorageSize());
	}
	s_currentlyLoadedFrameMemory = m_frameMemory;
}
void FrameState::LoadFrameState(bool force) const
{
	if (force || s_currentlyLoadedFrameMemory != m_frameMemory)
	{
		void* storage = StaticStorage::GetStorage();
		size_t storageSize = StaticStorage::GetStorageSize();
		memcpy(storage, m_frameMemory, storageSize);
		s_currentlyLoadedFrameMemory = m_frameMemory;
	}
}
void FrameState::ResetStaticStorageAndCachePointer()
{
	StaticStorage::ResetStorage();
	s_currentlyLoadedFrameMemory = nullptr;
}
void FrameState::CopyTo(FrameState& other)
{
	memcpy(other.m_frameMemory, m_frameMemory, m_memorySize);
}
CustomFrameStateWrapper* FrameState::GetData() const
{
	LoadFrameState();
	return m_mainData.get();
}

uint32_t FrameState::GenerateCRC32()
{
	LoadFrameState();
	if (m_mainData == nullptr/* || m_mainData->GetPlayers().empty()*/)
	{
		printf("Cannot generate counter-CRC as there is no frame data available\n");
		// Then it's uninitialized...
		return 0;
	}

	uint32_t crc = 0xffffffff;
	{
		cereal::CRCOutputArchive oarchive(crc); // Create an output archive

		oarchive(*m_mainData.get());
		crc = oarchive.GetCRCValue();
	}
	return crc;
}
