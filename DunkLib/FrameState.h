#pragma once
#include <memory>
#include "cereal/cereal.hpp"
#include "StaticStorage.h"

#include "CustomFrameStateWrapper.h"

class FrameState
{
	void* m_frameMemory = nullptr;
	size_t m_memorySize;
	std::shared_ptr<CustomFrameStateWrapper> m_mainData;

public:
	FrameState();
	FrameState(const FrameState& state);
	~FrameState();
	void Init();
	void SetupCustomFrameState();
	void CopyTo(FrameState& other);
	void SaveFrameState(bool skipCopy = false);
	void LoadFrameState(bool force = false) const;
	CustomFrameStateWrapper* GetData() const;
	static void ResetStaticStorageAndCachePointer();

	template<class Archive>
	void save(Archive& archive, std::uint32_t const version) const
	{
		CustomFrameStateWrapper* game = GetData();
		archive(*game);
		ResetStaticStorageAndCachePointer();
	}
	template<class Archive>
	void load(Archive& archive, std::uint32_t const version)
	{
		ResetStaticStorageAndCachePointer();
		// This will corrupt any loaded frame that assumes it's on the stack
		LoadFrameState(true);
		m_mainData = nullptr;
		ResetStaticStorageAndCachePointer();
		m_mainData = MakeAsT<CustomFrameStateWrapper>();
		archive(*(m_mainData.get()));
		SaveFrameState();
		ResetStaticStorageAndCachePointer();
	}

	uint32_t GenerateCRC32();

	//FrameState& operator= (const FrameState& c);
};
