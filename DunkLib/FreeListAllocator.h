#pragma once
#include "Allocator.h" 

class FreeListAllocator : public Allocator
{
public:

	FreeListAllocator(size_t size, void* start);
	~FreeListAllocator();

	void Init(size_t size, void* start);

	void Reset();

	void* allocate(size_t size, uint8_t alignment) override;
	void deallocate(void* p) override;

private:

	struct AllocationHeader { size_t size; uint8_t adjustment; };
	struct FreeBlock { size_t size; FreeBlock* next; };
	FreeListAllocator(const FreeListAllocator&);

	//Prevent copies because it might cause errors 
	//FreeListAllocator& operator=(const FreeListAllocator&);
	//FreeBlock* _free_blocks;
	//void* m_memoryLocation;
	//size_t m_memorySize;
	FreeBlock** GetFreeBlocksHeader();
};
