#include "GUIUtils.h"
#include "TextureID.h"

#pragma warning(push, 0)
#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
#pragma warning(pop)

#include "TooltipUtils.h"
#include "DrawUtils.h"
#include "UIManager.h"
#include "MathUtils.h"
#include "CameraUtils.h"
#include "ConfigData.h"

namespace GUIUtils
{
	Font s_guiFont;
	int s_guiFontSize = 12;
	Font s_gameFont;
	int s_fontSize = 16;

	void InitializeResources(bool headlessMode)
	{
		if (headlessMode)
		{
			return;
		}
		EngineConfigData& config = ConfigData::Instance().m_config;
		printf("Loading GUI Style\n");
		const char* styleName = config.m_rayGuiStyleName.c_str();
		GuiLoadStyle(styleName);

		// TTF font : Font data and atlas are generated directly from TTF
		// NOTE: We define a font base size of x pixels tall and up-to 250 characters
		printf("Loading GUI Font\n");
		const char* guiFontName = config.m_guiFontName.c_str();
		int guiFontSize = config.m_guiFontSize;
		s_guiFont = LoadFontEx(guiFontName, guiFontSize, nullptr, 250);
		GuiSetFont(s_guiFont);
		s_guiFontSize = config.m_guiFontAlignmentSize;

		printf("Loading default game font\n");
		const char* defaultFont = ConfigData::Instance().m_config.m_defaultFontName.c_str();
		int defaultFontSize = ConfigData::Instance().m_config.m_defaultFontSize;
		s_fontSize = defaultFontSize;
		s_gameFont = LoadFontEx(defaultFont, defaultFontSize, nullptr, 250);
		SetTextureFilter(s_gameFont.texture, FILTER_POINT);
	}

	int GetGUIFontSize()
	{
		return s_guiFontSize;
	}
	void DrawIconAspectCorrectInsideRect(Texture2D& tex, Vector2Int gridSize, RayRectangle rect, Color colour, float rotation)
	{
		int pixelsPerTile = tex.height / gridSize.y;
		// the output aspect should match the input aspect
		float xScale = (float)gridSize.x / (float)gridSize.y; // less than 1 for vertical objects
		float yScale = 1.0f;
		if (xScale > 1.0f)
		{
			yScale = 1.0f / xScale;
			xScale = 1.0f;
		}
		RayRectangle sourceRect = { 0, 0, (float)(pixelsPerTile * gridSize.x), (float)(pixelsPerTile * gridSize.y) };
		RayRectangle destRect = { rect.x, rect.y, (rect.width * xScale), (rect.width * yScale) };
		// If our width or height doesn't cover the whole rect, then shift the position so the dest rect is centered
		float xGap = rect.width * (1.0f - xScale);
		float yGap = rect.height * (1.0f - yScale);
		destRect.x += xGap * 0.5f;
		destRect.y += yGap * 0.5f;

		DrawTexturePro(tex, sourceRect, destRect, { 0.0f, 0.0f }, -rotation * RAD2DEG, colour);
	}
	void GUITooltipPanel(Rect rect)
	{
		GuiPanel(rect.ToRayRectangle());
	}
	void GUIPanel(RayRectangle rect)
	{
		TooltipUtils::ClearBox({ (int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height });
		GuiPanel(rect);
	}
	void GUIPanelRect(Rect rect)
	{
		GUIPanel({ (float)rect.x, (float)rect.y, (float)rect.width, (float)rect.height });
	}
	bool GUIPanelRectWithClose(Rect rect)
	{
		GUIPanel({ (float)rect.x, (float)rect.y, (float)rect.width, (float)rect.height });
		UIManager::EndUI(); // Flush any UI (including this new panel background) to the screen, so new alpha writes won't cut behind the panel
		UIManager::StartUI();
		return GUICloseButton(rect);
	}
	bool GUICloseButton(Rect parentRect)
	{
		Texture2D& image = TextureID("CloseButton").GetData();
		RayRectangle buttonRect = { (float)(parentRect.x + parentRect.width) - 32.0f, (float)parentRect.y, 32, 32 };
		if (GUIImageButton(buttonRect, nullptr, image) || IsKeyPressed(KEY_ESCAPE))
		{
			return true;
		}
		return false;
	}
	void GUIPanelCentered(Rect rect)
	{
		rect.x -= rect.width / 2;
		rect.y -= rect.height / 2;
		GUIPanel({ (float)rect.x, (float)rect.y, (float)rect.width, (float)rect.height });
	}
	void GUILabel(RayRectangle rect, const char* text)
	{
		GuiLabel(rect, text);
	}
	void GUILabelRect(Rect rect, const char* text)
	{
		GuiLabel({ (float)rect.x, (float)rect.y, (float)rect.width, (float)rect.height }, text);
	}
	void GUILabelCentered(int x, int y, const char* text)
	{
		int height = s_guiFontSize; // Seems the font loading is borked, so just assume a height
		int width = MeasureText(text, height);

		RayRectangle rect = { (float)x - ((float)width * 0.5f), (float)y - ((float)height * 0.5f), (float)width, (float)height };
		GuiLabel(rect, text);
	}

	bool GUIButton(RayRectangle rect, const char* text)
	{
		return GuiButton(rect, text);
	}
	bool GUIButton2(int x, int y, const char* text, bool enabled, bool highlighted)
	{
		int fontSize = s_guiFontSize;
		int textWidth = MeasureText(text, fontSize);
		float boxWidth = ((float)textWidth) + 16.0f;
		float boxHeight = fontSize + 12.0f;
		RayRectangle rect = { (float)x, (float)y, boxWidth, boxHeight };
		if (enabled)
		{
			if (highlighted)
			{
				DrawRectangle(x - 2, y - 2, (int)(boxWidth + 4), (int)(boxHeight + 4), ORANGE);
			}
			bool clicked = GuiButton(rect, text);
			return clicked;
		}
		else
		{
			rect.x += 7.5f;
			GuiLabel(rect, text);
			return false;
		}
	}
	bool GUIButtonWithState(int x, int y, const char* text, bool enabled, const char* disabledRollover, HorizontalAlignMode horizontalAlign, VerticalAlignMode verticalAlign, bool highlighted, bool* isRollingOver)
	{
		int fontSize = s_guiFontSize;
		int textWidth = MeasureText(text, fontSize);
		int boxWidth = textWidth + 16;
		int boxHeight = fontSize + 12;
		Rect rect = { x, y, boxWidth, boxHeight };
		switch (horizontalAlign)
		{
		case HorizontalAlignMode::Centered:
			rect.x -= rect.width / 2;
			break;
		case HorizontalAlignMode::RightAligned:
			rect.x -= rect.width;
			break;
		default:
			break;
		}
		switch (verticalAlign)
		{
		case VerticalAlignMode::Centered:
			rect.y -= rect.height / 2;
			break;
		case VerticalAlignMode::BottomAligned:
			rect.y -= rect.height;
			break;
		default:
			break;
		}

		Vector2 mousePos = UIManager::GetUIMousePosition();
		bool mouseOver = mousePos.x >= rect.x && mousePos.x <= rect.x + rect.width && mousePos.y >= rect.y && mousePos.y <= rect.y + rect.height;
		if (mouseOver && isRollingOver != nullptr)
		{
			*isRollingOver = true;
		}

		if (enabled)
		{
			if (highlighted)
			{
				DrawRectangle(rect.x - 2, rect.y - 2, rect.width + 4, rect.height + 4, ORANGE);
			}
			return GuiButton(rect.ToRayRectangle(), text);
		}
		else
		{

			int offset = (enabled && mouseOver) ? 1 : 0;
			Color borderColour = GetColor(GuiGetStyle(BUTTON, BORDER_COLOR_DISABLED));
			Color activeButtonColour = GetColor(GuiGetStyle(BUTTON, BASE_COLOR_DISABLED));
			Color textColour = GetColor(GuiGetStyle(BUTTON, TEXT_COLOR_DISABLED));
			DrawRectangle(rect.x, rect.y, rect.width, rect.height, borderColour);
			DrawRectangle(rect.x + 1, rect.y + 1, rect.width - 2, rect.height - 2, activeButtonColour);
			if (mouseOver)
			{
				TooltipUtils::AddTooltip(std::string(disabledRollover));
			}
			DrawTextCentered(text, (int)(rect.x + rect.width * 0.5f), (int)(rect.y + rect.height * 0.5f), fontSize, textColour);
			return false;
		}
	}
	bool GUIButtonMovingX(int& x, int y, const char* text, bool enabled)
	{
		int fontSize = s_guiFontSize;
		int textWidth = MeasureText(text, fontSize);
		float boxWidth = ((float)textWidth) + 16.0f;
		float boxHeight = fontSize + 12.0f;
		RayRectangle rect = { (float)x, (float)y, boxWidth, boxHeight };
		x += (int)boxWidth;
		if (enabled)
		{
			bool clicked = GuiButton(rect, text);
			return clicked;
		}
		else
		{
			rect.x += 7.5f;
			GuiLabel(rect, text);
			return false;
		}
	}
	bool GUIButtonCentered(int x, int y, const char* text, bool enabled)
	{
		int fontSize = s_guiFontSize;
		int textWidth = MeasureText(text, fontSize);
		float boxWidth = ((float)textWidth) + 16.0f;
		float boxHeight = fontSize + 12.0f;
		RayRectangle rect = { (float)x - (boxWidth * 0.5f), (float)y - (boxHeight * 0.5f), boxWidth, boxHeight };
		if (enabled)
		{
			bool clicked = GuiButton(rect, text);
			return clicked;
		}
		else
		{
			rect.x += 7.5f;
			GuiLabel(rect, text);
			return false;
		}
	}
	bool GUIToggleCentered(int x, int y, const char* text, bool active)
	{
		int fontSize = s_guiFontSize;
		int textWidth = MeasureText(text, fontSize);
		float boxWidth = ((float)textWidth) + 16.0f;
		float boxHeight = fontSize + 12.0f;
		RayRectangle rect = { (float)x - (boxWidth * 0.5f), (float)y - (boxHeight * 0.5f), boxWidth, boxHeight };

		return GuiToggle(rect, text, active);
	}
	bool GUITextBox(RayRectangle rect, char* input, int textSize, bool editMode)
	{
		return GuiTextBox(rect, input, textSize, editMode);
	}
	bool GUIImageButton(RayRectangle rect, const char* text, Texture2D& image)
	{
		return GuiImageButtonEx(rect, text, image, { 0, 0, (float)image.height, (float)image.height });
		//return GuiImageButtonEx2(rect, text, image, { 0, 0, (float)image.height, (float)image.height }, { image.height, image.height });
	}
	bool GUIImageButton2(Rect fullRect, int border, const char* text, Texture2D& image)
	{
		Rect innerRect = { fullRect.x + border, fullRect.y + border, fullRect.width - border * 2, fullRect.height - border * 2 };
		return GuiImageButtonEx2(fullRect.ToRayRectangle(), innerRect.ToRayRectangle(), fullRect.ToRayRectangle(), text, image, { 0, 0, (float)image.height, (float)image.height });
	}
	bool GUIImageButtonAuto(Rect rect, Texture2D& image, const char* tooltip, bool enabled, bool selected, bool monochroneImageMode)
	{
		Vector2 mousePos = UIManager::GetUIMousePosition();
		bool mouseOver = rect.IsInBox(mousePos);
		int border = 2;
		int imageSize = rect.height - border * 2;
		Rect imageRect = { rect.x + border, rect.y + border, imageSize, imageSize };

		if (selected)
		{
			int outlineBorder = 2;
			DrawRectangle(rect.x - outlineBorder, rect.y - outlineBorder, rect.width + outlineBorder * 2, rect.height + outlineBorder * 2, ORANGE);
		}
		int offset = 0;
		if (enabled && mouseOver)
		{
			if (IsMouseButtonDown(0))
			{
				offset = 2;
			}
			else
			{
				offset = 1;
			}
		}
		Color borderColour = GetColor(GuiGetStyle(BUTTON, BORDER + offset * 3));
		Color activeButtonColour = GetColor(GuiGetStyle(BUTTON, BASE + offset * 3));
		Color textColour = GetColor(GuiGetStyle(BUTTON, TEXT + offset * 3));
		if (!enabled)
		{
			borderColour = GetColor(GuiGetStyle(BUTTON, BORDER_COLOR_DISABLED));
			activeButtonColour = GetColor(GuiGetStyle(BUTTON, BASE_COLOR_DISABLED));
			textColour = GetColor(GuiGetStyle(BUTTON, TEXT_COLOR_DISABLED));
		}
		DrawRectangle(rect.x, rect.y, rect.width, rect.height, borderColour);
		DrawRectangle(rect.x + 1, rect.y + 1, rect.width - 2, rect.height - 2, activeButtonColour);
		DrawTexturePro(image, { 0.0f, 0.0f, (float)image.height, (float)image.height }, imageRect.ToRayRectangle(), { 0.0f, 0.0f }, 0.0f, ((offset == 2 || monochroneImageMode) ? borderColour : WHITE));

		if (mouseOver)
		{
			TooltipUtils::AddTooltip(std::string(tooltip));
		}

		return mouseOver && enabled && IsMouseButtonReleased(0);
	}
	bool GUIImageAndTextButton(Rect fullRect, Rect imageRect, Rect textRect, const char* text, Texture2D& image, bool enabled, bool doOverride, Color textOverride, Color backgroundOverride)
	{
		//return GuiImageButtonEx2(fullRect.ToRayRectangle(), imageRect.ToRayRectangle(), textRect.ToRayRectangle(), text, image, { 0, 0, (float)image.height, (float)image.height });
		//Rect rect = Rect{ (int)buttonRect.x, (int)buttonRect.y, (int)buttonRect.width, (int)buttonRect.height };
		Vector2 mousePos = UIManager::GetUIMousePosition();
		bool mouseOver = mousePos.x >= fullRect.x && mousePos.x <= fullRect.x + fullRect.width && mousePos.y >= fullRect.y && mousePos.y <= fullRect.y + fullRect.height;

		int offset = (enabled && mouseOver) ? 1 : 0;
		Color borderColour = GetColor(GuiGetStyle(BUTTON, BORDER + offset * 3));
		Color activeButtonColour = GetColor(GuiGetStyle(BUTTON, BASE + offset * 3));
		Color textColour = GetColor(GuiGetStyle(BUTTON, TEXT + offset * 3));
		if (!enabled)
		{
			borderColour = GetColor(GuiGetStyle(BUTTON, BORDER_COLOR_DISABLED));
			activeButtonColour = GetColor(GuiGetStyle(BUTTON, BASE_COLOR_DISABLED));
			textColour = GetColor(GuiGetStyle(BUTTON, TEXT_COLOR_DISABLED));
		}
		if (doOverride)
		{
			activeButtonColour = backgroundOverride;
			textColour = textOverride;
		}
		DrawRectangle(fullRect.x, fullRect.y, fullRect.width, fullRect.height, borderColour);
		DrawRectangle(fullRect.x + 1, fullRect.y + 1, fullRect.width - 2, fullRect.height - 2, activeButtonColour);
		DrawTextCentered(text, (int)(textRect.x + textRect.width * 0.5f), (int)(textRect.y + textRect.height * 0.5f), s_guiFontSize, textColour);
		DrawTexturePro(image, { 0.0f, 0.0f, (float)image.height, (float)image.height }, imageRect.ToRayRectangle(), { 0.0f, 0.0f }, 0.0f, WHITE);
		if (mouseOver && enabled && IsMouseButtonReleased(0))
		{
			return true;
		}
		return false;
	}
	bool GUIImageAndTextButtonAuto(Rect buttonRect, const char* text, Texture2D& image, bool enabled, bool selected, bool confirmed)
	{
		int border = 2;
		int imageSize = buttonRect.height - border * 2;
		Rect imageRect = { buttonRect.x + border, buttonRect.y + border, imageSize, imageSize };
		int textOffset = border + imageSize + border;
		Rect textRect = { buttonRect.x + textOffset, buttonRect.y, buttonRect.width - textOffset, buttonRect.height };

		if (selected)
		{
			int outlineBorder = 2;
			DrawRectangle(buttonRect.x - outlineBorder, buttonRect.y - outlineBorder, buttonRect.width + outlineBorder * 2, buttonRect.height + outlineBorder * 2, ORANGE);
		}
		if (confirmed)
		{
			int outlineBorder = 1;
			DrawRectangle(buttonRect.x - outlineBorder, buttonRect.y - outlineBorder, buttonRect.width + outlineBorder * 2, buttonRect.height + outlineBorder * 2, GREEN);
		}
		bool clicked = GUIImageAndTextButton(buttonRect, imageRect, textRect, text, image, enabled, confirmed, WHITE, DARKGREEN);
		return clicked;
	}
	void GUITooltip(int x, int y, const char* text)
	{
		Vector2Int textSize = GetDefaultMeasuredText(text);

		Rect panelRect = GetTooltipRect(x, y, Vector2Int(textSize));

		GUITooltipPanel(panelRect); // Don't use DrawUtils GuiPanel because it will render tooltips!
		float textX = panelRect.x + (panelRect.width * 0.5f);
		float textY = panelRect.y + (panelRect.height * 0.5f);
		GUILabelCentered((int)textX, (int)textY, text);
	}
	void GUITooltipMultiline(int x, int y, const char* text)
	{
		TextSplitInfo splitText = GetDefaultMeasuredTextMultiline(text);
		// Draw the panel
		Rect panelRect = GetTooltipRect(x, y, splitText.m_bounds);
		GUITooltipPanel(panelRect); // Don't use DrawUtils GuiPanel because it will render tooltips!

		int spacePerLine = s_guiFontSize + 4;
		int startingYOffset = -((splitText.m_lineCount - 1) * spacePerLine) / 2;
		// Draw the text lines
		for (int i = 0; i < splitText.m_lineCount; i++)
		{
			float textX = panelRect.x + (panelRect.width * 0.5f);
			float textY = panelRect.y + (panelRect.height * 0.5f) + startingYOffset + spacePerLine * i;
			GUILabelCentered((int)textX, (int)textY, splitText.m_lines[i]);
		}
	}
	Vector2Int GetDefaultMeasuredText(const char* text)
	{
		int textHeight = s_guiFontSize; // Seems the font loading is borked, so just assume a height
		int textWidth = MeasureText(text, textHeight);
		return Vector2Int{ textWidth, textHeight };
	}
	TextSplitInfo GetDefaultMeasuredTextMultiline(const char* text)
	{
		// Split the text into lines
		int lineCount = 0;
		const char** lines = TextSplit(text, '\n', &lineCount);

		// Find the longest line
		int widestLine = 0;
		for (int i = 0; i < lineCount; i++)
		{
			const char* line = lines[i];
			Vector2Int textSize = GetDefaultMeasuredText(line);
			if (textSize.x > widestLine)
			{
				widestLine = textSize.x;
			}
		}
		Vector2Int textPanelSize = { widestLine, s_guiFontSize * lineCount + (2 * lineCount - 1) };
		return { lines, lineCount, textPanelSize };
	}
	Rect GetTooltipRect(int mouseX, int mouseY, Vector2Int minSize)
	{
		int screenWidth = UIManager::GetUIWidth();
		int screenHeight = UIManager::GetUIHeight();
		int border = 20;

		int offsetDueToMouse = (int)(20.0f * UIManager::GetSizeMultiplier());

		Rect panelRect = { mouseX + offsetDueToMouse, mouseY + offsetDueToMouse, minSize.x + 20, minSize.y + 20 };
		if (panelRect.x + panelRect.width > screenWidth - border)
		{
			panelRect.x = (mouseX - offsetDueToMouse) - panelRect.width;
		}
		if (panelRect.y + panelRect.height > screenHeight - border)
		{
			panelRect.y = (mouseY - offsetDueToMouse) - panelRect.height;
		}
		return panelRect;
	}
	float GUISlider(int x, int y, const char* text, float value, float minValue, float maxValue)
	{
		int textHeight = s_guiFontSize;
		int textWidth = MeasureText(text, textHeight);
		int sliderWidth = 50;
		float totalWidth = (float)(textWidth + sliderWidth);
		float totalHeight = 20.0f;
		RayRectangle bounds = { (float)x, (float)y, totalWidth, totalHeight };
		return GuiSlider(bounds, text, nullptr, value, minValue, maxValue);
	}
	float GUISliderCentered(int x, int y, int sliderWidth, const char* text, float value, float minValue, float maxValue)
	{
		int textHeight = s_guiFontSize;
		int textWidth = MeasureText(text, textHeight);
		float totalWidth = (float)(textWidth + sliderWidth);
		float totalHeight = 20.0f;
		RayRectangle bounds = { (float)x - (totalWidth * 0.5f), (float)y, totalWidth, totalHeight };
		return GuiSlider(bounds, text, nullptr, value, minValue, maxValue);
	}
	int GUISliderVertical(Rect scrollbarBounds, int scrollSteps, int scrollValue, bool& pressed)
	{
		GuiScrollBar(scrollbarBounds.ToRayRectangle(), scrollValue, 0, scrollSteps);

		// We handle the selection/scrolling manually to get better and consistent results
		if (IsMouseButtonDown(0))
		{
			Vector2 mousePos = UIManager::GetUIMousePosition();
			if (IsInsideRect(mousePos, scrollbarBounds.ToRayRectangle()))
			{
				pressed = true;
			}

			if (pressed)
			{
				Vector2 mouseSliderBoundsPos = WorldPositionToRectPosition(scrollbarBounds.ToRayRectangle(), mousePos);
				scrollValue = RoundToNearestInt(Clamp01(mouseSliderBoundsPos.y) * scrollSteps);
			}
		}
		else
		{
			pressed = false;
			int scrollWheel = GetMouseWheelMove();
			scrollValue = Clamp(scrollValue - scrollWheel, 0, scrollSteps);
		}
		return scrollValue;
	}
	bool GUISliderVerticalOptional(Rect bounds, int itemCount, int itemHeight, int& scrollbarPosition, bool& scrollbarPressed)
	{
		int maxVisible = (bounds.height + (itemHeight / 2)) / itemHeight;
		if (itemCount > maxVisible)
		{
			int scrollSteps = itemCount - maxVisible;
			Rect scrollbarBounds = { bounds.x, bounds.y, 16, bounds.height };
			scrollbarPosition = GUISliderVertical(scrollbarBounds, scrollSteps, scrollbarPosition, scrollbarPressed);
			return true;
		}
		return false;
	}
	bool GUIDropdown(Rect bounds, const char *text, int *active, bool editMode)
	{
		return GuiDropdownBox(bounds.ToRayRectangle(), text, active, editMode);
	}
	void GUIMessageBox(Rect bounds, const char* text)
	{
		GuiMessageBox(bounds.ToRayRectangle(), "", text, "");
	}


	int GetClosestIntegerMultiple(int desiredFontSize, int originalFontSize)
	{
		return ((desiredFontSize + (originalFontSize / 2)) / originalFontSize) * originalFontSize;
	}
	void DrawText(const char* text, int x, int y, int fontSize, Color colour)
	{
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		DrawTextEx(s_gameFont, text, { (float)x,(float)y }, (float)fontSize, 0, colour);
	}
	Vector2 MeasureRoundedSizeText(const char* text, int fontSize)
	{
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		return MeasureTextEx(font, text, (float)fontSize, 0);
	}
	int DrawTextMeasured(const char* text, int x, int y, int fontSize, Color colour)
	{
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		DrawTextEx(s_gameFont, text, { (float)x,(float)y }, (float)fontSize, 0, colour);
		return (int)MeasureTextEx(font, text, (float)fontSize, 0).x;
	}
	void DrawTextCentered(const char* text, int x, int y, int fontSize, Color colour)
	{
		float spacing = 0;
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		Vector2 textSize = MeasureTextEx(font, text, (float)fontSize, spacing);
		Vector2 pos = { (float)(x - (int)(textSize.x * 0.5f)), (float)(y - fontSize / 2) };
		DrawTextEx(s_gameFont, text, pos, (float)fontSize, spacing, colour);
	}
	void DrawTextAligned(const char* text, int x, int y, int fontSize, Color colour, Vector2 offsetProportion)
	{
		float spacing = 0;
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		Vector2 textSize = MeasureTextEx(font, text, (float)fontSize, spacing);
		Vector2 pos = { (float)(x - (int)(textSize.x * offsetProportion.x)), (float)(y - (fontSize * offsetProportion.y)) };
		DrawTextEx(s_gameFont, text, pos, (float)fontSize, spacing, colour);
	}
	void DrawTextAlignedOutlined(const char* text, int x, int y, int fontSize, Color colour, Vector2 offsetProportion, Color outlineColour)
	{
		float spacing = 0;
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		Vector2 textSize = MeasureTextEx(font, text, (float)fontSize, spacing);
		Vector2 pos = { (float)(x - (int)(textSize.x * offsetProportion.x)), (float)(y - (fontSize * offsetProportion.y)) };
		float fontSizeFloat = (float)fontSize;
		Vector2 offsets[8] = {
			{-1.0f, 1.0f},
			{0.0f, 1.0f},
			{1.0f, 1.0f},
			{1.0f, 0.0f},
			{1.0f, -1.0f},
			{0.0f, -1.0f},
			{-1.0f,-1.0f},
			{-1.0f, 0.0f}
		};

		for (int i = 0; i < 8; i++)
		{
			DrawTextEx(s_gameFont, text, pos + offsets[i], fontSizeFloat, spacing, outlineColour);
		}
		DrawTextEx(s_gameFont, text, pos, fontSizeFloat, spacing, colour);
	}
	void DrawTextCenteredV(const char* text, Vector2 pos, int fontSize, Color colour)
	{
		float spacing = 0;
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		Vector2 textSize = MeasureTextEx(font, text, (float)fontSize, spacing);
		Vector2 finalPos = Vector2Subtract(pos, { textSize.x * 0.5f, (float)(fontSize / 2) });
		DrawTextEx(s_gameFont, text, finalPos, (float)fontSize, spacing, colour);
	}
	/*void DrawTextCenteredFormatted(const char* format, char* buffer, int x, int y, int fontSize, Color colour, ...)
	{
		DrawTextCentered(buffer, x, y, fontSize, colour);
	}*/
	void DrawTextSmooth(const char* text, Vector2 position, int fontSize, Color colour)
	{
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		DrawTextEx(font, text, position, (float)fontSize, 0, colour);
	}

	void DrawTextSmoothWithCounterCameraRotation(const char* text, Vector2 position, int fontSize, Color colour)
	{
		Camera2D& camera = *CameraUtils::GetCurrentCamera();
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);

		Vector2 screenSpaceLabel = CameraUtils::WorldToScreenPos(position);
		float oldRotation = camera.rotation;
		EndMode2D();

		camera.rotation = 0.0f;
		CameraUtils::UpdateCameraMatrices(camera);

		Vector2 worldSpaceLabel = CameraUtils::ScreenToWorldPos(screenSpaceLabel);

		BeginMode2D(camera);
		DrawTextCenteredV(text, worldSpaceLabel, fontSize, colour);
		EndMode2D();

		camera.rotation = oldRotation;
		CameraUtils::UpdateCameraMatrices(camera);
		BeginMode2D(camera);
	}

	void DrawTextSmoothWithWorldRotation(const char* text, Vector2 position, int fontSize, Color colour, float rotation)
	{
		Camera2D& camera = *CameraUtils::GetCurrentCamera();
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);

		Vector2 screenSpaceLabel = CameraUtils::WorldToScreenPos(position);
		float oldRotation = camera.rotation;
		EndMode2D();

		camera.rotation = oldRotation - (rotation * RAD2DEG);
		CameraUtils::UpdateCameraMatrices(camera);

		Vector2 worldSpaceLabel = CameraUtils::ScreenToWorldPos(screenSpaceLabel);

		BeginMode2D(camera);
		DrawTextCenteredV(text, worldSpaceLabel, fontSize, colour);
		EndMode2D();

		camera.rotation = oldRotation;
		CameraUtils::UpdateCameraMatrices(camera);
		BeginMode2D(camera);
	}
	void DrawTextInRect(const char* message, Rect rect, int fontSize, bool wordWrap, Color colour)
	{
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);

		DrawTextRec(font, message, rect.ToRayRectangle(), (float)fontSize, 0.0f, wordWrap, colour);
	}
	Rect ExpandRectForText(const char* message, int fontSize, Rect rect)
	{
		int originalHeight = rect.height;
		Vector2Int wrappedSize = MeasureTextWrappedInRect(message, rect, fontSize);

		int heightChange = rect.height - originalHeight;
		rect.height = wrappedSize.y;
		rect.y -= heightChange / 2;
		return rect;
	}
	void DrawTextWithSelectionInRect(const char* message, Rect rect, int fontSize, bool wordWrap, Color colour, int selectionStart, int selectionLength, Color selectionCol, Color selectedBGCol)
	{
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);

		float floatFontSize = (float)fontSize;
		DrawTextRecEx(font, message, rect.ToRayRectangle(), floatFontSize, 0.0f, wordWrap, colour, selectionStart, selectionLength, selectionCol, selectedBGCol);
	}
	Vector2Int MeasureTextWrappedInRect(const char* text, Rect rec, int fontSize)
	{
		Font& font = s_gameFont;
		fontSize = GetClosestIntegerMultiple(fontSize, s_fontSize);
		int spacing = 0;
		bool wordWrap = true;

		// Code copied from raylib DrawTextRectEx so I can measure final height!
		int length = TextLength(text);      // Total length in bytes of the text, scanned by codepoints in loop

		int textOffsetY = 0;            // Offset between lines (on line break '\n')
		float textOffsetX = 0.0f;       // Offset X to next character to draw

		float scaleFactor = (float)fontSize / (float)font.baseSize;     // Character quad scaling factor

		// Word/character wrapping mechanism variables
		enum { MEASURE_STATE = 0, DRAW_STATE = 1 };
		int state = wordWrap ? MEASURE_STATE : DRAW_STATE;

		int startLine = -1;         // Index where to begin drawing (where a line begins)
		int endLine = -1;           // Index where to stop drawing (where a line ends)
		int lastk = -1;             // Holds last value of the character position

		for (int i = 0, k = 0; i < length; i++, k++)
		{
			// Get next codepoint from byte string and glyph index in font
			int codepointByteCount = 0;
			int codepoint = GetNextCodepoint(&text[i], &codepointByteCount);
			int index = GetGlyphIndex(font, codepoint);

			// NOTE: Normally we exit the decoding sequence as soon as a bad byte is found (and return 0x3f)
			// but we need to draw all of the bad bytes using the '?' symbol moving one byte
			if (codepoint == 0x3f) codepointByteCount = 1;
			i += (codepointByteCount - 1);

			int glyphWidth = 0;
			if (codepoint != '\n')
			{
				glyphWidth = (font.chars[index].advanceX == 0) ?
					(int)(font.recs[index].width*scaleFactor + spacing) :
					(int)(font.chars[index].advanceX*scaleFactor + spacing);
			}

			// NOTE: When wordWrap is ON we first measure how much of the text we can draw before going outside of the rec container
			// We store this info in startLine and endLine, then we change states, draw the text between those two variables
			// and change states again and again recursively until the end of the text (or until we get outside of the container).
			// When wordWrap is OFF we don't need the measure state so we go to the drawing state immediately
			// and begin drawing on the next line before we can get outside the container.
			if (state == MEASURE_STATE)
			{
				// TODO: There are multiple types of spaces in UNICODE, maybe it's a good idea to add support for more
				// Ref: http://jkorpela.fi/chars/spaces.html
				if ((codepoint == ' ') || (codepoint == '\t') || (codepoint == '\n')) endLine = i;

				if ((textOffsetX + glyphWidth + 1) >= rec.width)
				{
					endLine = (endLine < 1) ? i : endLine;
					if (i == endLine) endLine -= codepointByteCount;
					if ((startLine + codepointByteCount) == endLine) endLine = (i - codepointByteCount);

					state = !state;
				}
				else if ((i + 1) == length)
				{
					endLine = i;

					state = !state;
				}
				else if (codepoint == '\n') state = !state;

				if (state == DRAW_STATE)
				{
					textOffsetX = 0;
					i = startLine;
					glyphWidth = 0;

					// Save character position when we switch states
					int tmp = lastk;
					lastk = k - 1;
					k = tmp;
				}
			}
			else
			{
				if (codepoint == '\n')
				{
					if (!wordWrap)
					{
						textOffsetY += (int)((font.baseSize + font.baseSize / 2)*scaleFactor);
						textOffsetX = 0;
					}
				}
				else
				{
					if (!wordWrap && ((textOffsetX + glyphWidth + 1) >= rec.width))
					{
						textOffsetY += (int)((font.baseSize + font.baseSize / 2)*scaleFactor);
						textOffsetX = 0;
					}

					// When text overflows rectangle height limit, just stop drawing?  We want to measure!  So don't stop!
					//if ((textOffsetY + (int)(font.baseSize*scaleFactor)) > rec.height) break;
				}

				if (wordWrap && (i == endLine))
				{
					textOffsetY += (int)((font.baseSize + font.baseSize / 2)*scaleFactor);
					textOffsetX = 0;
					startLine = endLine;
					endLine = -1;
					glyphWidth = 0;
					k = lastk;

					state = !state;
				}
			}

			textOffsetX += glyphWidth;
		}
		return { rec.width, textOffsetY };// +(int)((font.basesize + font.basesize / 2)*scalefactor) };
	}
	bool DrawButton(RayRectangle buttonRect, const char* text, int textSize, float fillAmount, bool enabled, const char* disabledRollover)
	{
		Rect rect = Rect{ (int)buttonRect.x, (int)buttonRect.y, (int)buttonRect.width, (int)buttonRect.height };
		Vector2 mousePos = UIManager::GetUIMousePosition();
		bool mouseOver = mousePos.x >= buttonRect.x && mousePos.x <= buttonRect.x + buttonRect.width && mousePos.y >= buttonRect.y && mousePos.y <= buttonRect.y + buttonRect.height;

		int offset = (enabled && mouseOver) ? 1 : 0;
		Color borderColour = GetColor(GuiGetStyle(BUTTON, BORDER + offset * 3));
		Color activeButtonColour = GetColor(GuiGetStyle(BUTTON, BASE + offset * 3));
		Color textColour = GetColor(GuiGetStyle(BUTTON, TEXT + offset * 3));
		if (!enabled)
		{
			borderColour = GetColor(GuiGetStyle(BUTTON, BORDER_COLOR_DISABLED));
			activeButtonColour = GetColor(GuiGetStyle(BUTTON, BASE_COLOR_DISABLED));
			textColour = GetColor(GuiGetStyle(BUTTON, TEXT_COLOR_DISABLED));
		}
		DrawRectangle(rect.x, rect.y, rect.width, rect.height, borderColour);
		DrawRectangle(rect.x + 1, rect.y + 1, rect.width - 2, rect.height - 2, activeButtonColour);
		DrawRectangle(rect.x, rect.y, (int)(buttonRect.width * fillAmount), rect.height, PURPLE);
		if (mouseOver)
		{
			if (!enabled)
			{
				TooltipUtils::AddTooltip(std::string(disabledRollover));
			}
		}
		DrawTextCentered(text, (int)(buttonRect.x + buttonRect.width * 0.5f), (int)(buttonRect.y + buttonRect.height * 0.5f), textSize, textColour);
		if (mouseOver && enabled && IsMouseButtonDown(0))
		{
			return true;
		}
		return false;
	}
}