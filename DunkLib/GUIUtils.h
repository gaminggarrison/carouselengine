#pragma once

#include "raylib.h"
#include "Vector2Int.h"
#include "Rect.h"
enum class HorizontalAlignMode
{
	LeftAligned = 0,
	Centered = 1,
	RightAligned = 2
};
enum class VerticalAlignMode
{
	TopAligned = 0,
	Centered = 1,
	BottomAligned = 2
};

namespace GUIUtils
{
	struct TextSplitInfo
	{
		const char** m_lines;
		int m_lineCount;
		Vector2Int m_bounds;
	};

	extern Font s_guiFont;
	extern Font s_gameFont;
	extern int s_fontSize;
	extern int s_guiFontSize;

	void InitializeResources(bool headlessMode = false);

	int GetGUIFontSize();
	void DrawIconAspectCorrectInsideRect(Texture2D& tex, Vector2Int gridSize, RayRectangle rect, Color colour, float rotation = 0.0f);

	void GUITooltipPanel(Rect rect);
	void GUIPanel(RayRectangle rect);
	void GUIPanelRect(Rect rect);
	bool GUIPanelRectWithClose(Rect rect);
	bool GUICloseButton(Rect parentRect);
	void GUIPanelCentered(Rect rect);
	void GUILabel(RayRectangle rect, const char* text);
	void GUILabelRect(Rect rect, const char* text);
	void GUILabelCentered(int x, int y, const char* text);
	bool GUIButton(RayRectangle rect, const char* text);
	bool GUIButton2(int x, int y, const char* text, bool enabled, bool highlighted = false);
	bool GUIButtonWithState(int x, int y, const char* text, bool enabled, const char* disabledRollover, HorizontalAlignMode horizontalAlign, VerticalAlignMode verticalAlign, bool highlighted = false, bool* isRollingOver = nullptr);
	bool GUIButtonMovingX(int& x, int y, const char* text, bool enabled);
	bool GUIButtonCentered(int x, int y, const char* text, bool enabled);
	bool GUIToggleCentered(int x, int y, const char* text, bool active);
	bool GUITextBox(RayRectangle rect, char* input, int textSize, bool editMode);
	bool GUIImageButton(RayRectangle rect, const char* text, Texture2D& image);
	bool GUIImageButton2(Rect fullRect, int border, const char* text, Texture2D& image);
	bool GUIImageButtonAuto(Rect rect, Texture2D& image, const char* tooltip, bool enabled, bool selected, bool monochroneImageMode = false);
	bool GUIImageAndTextButton(Rect fullRect, Rect imageRect, Rect textRect, const char* text, Texture2D& image, bool enabled, bool doOverride, Color textOverride, Color backgroundOverride);
	bool GUIImageAndTextButtonAuto(Rect buttonRect, const char* text, Texture2D& image, bool enabled, bool selected, bool confirmed);
	void GUITooltip(int x, int y, const char* text);
	void GUITooltipMultiline(int x, int y, const char* text);
	Vector2Int GetDefaultMeasuredText(const char* text);
	TextSplitInfo GetDefaultMeasuredTextMultiline(const char* text);
	Rect GetTooltipRect(int mouseX, int mouseY, Vector2Int minSize);
	float GUISlider(int x, int y, const char* text, float value, float minValue, float maxValue);
	float GUISliderCentered(int x, int y, int sliderWidth, const char* text, float value, float minValue, float maxValue);
	int GUISliderVertical(Rect scrollbarBounds, int scrollSteps, int scrollValue, bool& pressed);
	// Returns true if a rendering offset is needed
	bool GUISliderVerticalOptional(Rect bounds, int itemCount, int itemHeight, int& scrollbarPosition, bool& pressed);
	bool GUIDropdown(Rect bounds, const char *text, int *active, bool editMode);
	void GUIMessageBox(Rect bounds, const char* text);

	int GetClosestIntegerMultiple(int desiredFontSize, int originalFontSize);
	void DrawText(const char* text, int x, int y, int fontSize, Color colour);
	Vector2 MeasureRoundedSizeText(const char* text, int fontSize);
	int DrawTextMeasured(const char* text, int x, int y, int fontSize, Color colour);
	void DrawTextCentered(const char* text, int x, int y, int fontSize, Color colour);
	void DrawTextAligned(const char* text, int x, int y, int fontSize, Color colour, Vector2 offsetProportion);
	void DrawTextAlignedOutlined(const char* text, int x, int y, int fontSize, Color colour, Vector2 offsetProportion, Color outlineColour);
	void DrawTextCenteredV(const char* text, Vector2 pos, int fontSize, Color colour);
	template <typename ... Args>
	void DrawTextCenteredFormatted(const char* format, char* buffer, int x, int y, int fontSize, Color colour, Args const & ... args) noexcept
	{
		sprintf(buffer, format, args ...);
		DrawTextCentered(buffer, x, y, fontSize, colour);
	}
	void DrawTextSmooth(const char* text, Vector2 position, int fontSize, Color colour);
	void DrawTextSmoothWithCounterCameraRotation(const char* text, Vector2 position, int fontSize, Color colour);
	void DrawTextSmoothWithWorldRotation(const char* text, Vector2 position, int fontSize, Color colour, float rotation);
	void DrawTextInRect(const char* message, Rect rect, int fontSize, bool wordWrap, Color colour);
	Rect ExpandRectForText(const char* message, int fontSize, Rect rect);
	void DrawTextWithSelectionInRect(const char* message, Rect rect, int fontSize, bool wordWrap, Color colour, int selectionStart, int selectionLength, Color selectionCol, Color selectedBGCol);
	Vector2Int MeasureTextWrappedInRect(const char* text, Rect rec, int fontSize);
	bool DrawButton(RayRectangle buttonRect, const char* text, int textSize, float fillAmount, bool enabled, const char* disabledRollover);
}