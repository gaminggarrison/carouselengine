#include "GameLoadEvent.h"
#include "SerializationUtils.h"
#include "NetworkFrame.h"
#include "NetworkStateControl.h"
#include "SerializationUtils.h"
#include "FrameState.h"

GameLoadEvent::GameLoadEvent()
{

}

GameLoadEvent::~GameLoadEvent()
{
	printf("Freeing GameLoadEvent\n");
}

GameLoadEvent::GameLoadEvent(int playerID, int playerEventID) : m_saveData()//: m_saveData(SharedGame(sharedGame))
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}
GameLoadEvent::GameLoadEvent(int playerID, int playerEventID, FrameState& frameState)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;

	// Serialize framestate data to binary buffer
	std::stringstream* stream = SerializationUtils::GetStringStream();
	stream->seekp(0, stream->beg);
	SerializationUtils::WriteToStream(*frameState.GetData());
	m_saveData = stream->str();
	stream->seekp(0, stream->beg);
	//SerializationUtils::SaveObjectToVector(m_saveData, *frameState.GetData());

	/*std::stringstream* stream = SerializationUtils::GetStringStream();
	stream->seekp(0, stream->beg);
	stream = SerializationUtils::WriteToStream<CustomFrameStateWrapper>(*frameState.GetData());

	size_t length = stream->tellp();
	m_saveData.resize(length);
	stream->seekp(0, stream->beg);
	stream*/
}

void GameLoadEvent::RunEvent(FrameState& frameState)
{
	//m_saveData.CopyTo(frameState);
	//FrameState::ResetStaticStorageAndCachePointer();

	// Deserialize binary buffer to framestate data
	std::stringstream* stream = SerializationUtils::GetStringStream();
	stream->seekp(0, stream->beg);
	stream->write(m_saveData.c_str(), m_saveData.length());
	SerializationUtils::ReadFromStream(*frameState.GetData());
	stream->seekg(0, stream->beg);

	//SerializationUtils::LoadObjectFromVector(m_saveData, *frameState.GetData());
}
void GameLoadEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{
	networkStateControl.OverrideCurrentFrame(frameNumber);
}

Color GameLoadEvent::GetDebugColour()
{
	return PURPLE;
}

int GameLoadEvent::GetDebugPriority()
{
	return 10;
}

bool GameLoadEvent::DoRewindForEvent()
{
	return false; // This is a special event that should always run (and will nuke everything else)
}