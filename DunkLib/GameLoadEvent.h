#pragma once
#include "NetworkEvent.h"
#include "RaylibCereal.h"

class FrameState;

struct GameLoadEvent : public NetworkEvent
{
	std::string m_saveData;

	GameLoadEvent();
	virtual ~GameLoadEvent();
	GameLoadEvent(int playerID, int playerEventID);
	GameLoadEvent(int playerID, int playerEventID, FrameState& frameState);

	virtual void RunEvent(FrameState& frameState);
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();
	virtual bool DoRewindForEvent();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_saveData);
	}
};

CEREAL_REGISTER_TYPE(GameLoadEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, GameLoadEvent);
