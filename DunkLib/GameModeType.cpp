#include "GameModeType.h"
#include "RandomGenerator.h"

void FreeForAllGamemode::AssignPlayersToTeams(const ConnectedPlayers& connectedIDs, AllTeamState& teamData)
{
	for (int i = 0; i < connectedIDs.size(); i++)
	{
		if (connectedIDs[i])
		{
			TeamData& newTeam = teamData.AddTeam();
			newTeam.AddPlayer(i);
		}
	}
}
void FreeForAllGamemode::AssignPlayerMidGame(const int playerID, AllTeamState& teamData)
{
	// Give them a new team
	TeamData& newTeam = teamData.AddTeam();
	newTeam.AddPlayer(playerID);
}
const char* FreeForAllGamemode::GetGameModeName()
{
	return "Free for all!";
}
bool FreeForAllGamemode::IsAppropriateForPlayerCount(int playerCount)
{
	return true;
}


void EvenTeamsGamemode::AssignPlayersToTeams(const ConnectedPlayers& connectedIDs, AllTeamState& teamData)
{
	teamData.m_useTeamColours = true;

	// Try to split them evenly
	int teamCount = 2;
	for (int i = 0; i < teamCount; i++)
	{
		teamData.AddTeam();
	}

	chobo::static_vector<int, PLAYER_COUNT> allPlayerIDs;
	for (int i = 0; i < connectedIDs.size(); i++)
	{
		if (connectedIDs[i])
		{
			allPlayerIDs.push_back(i);
		}
	}
	//Randomise the order
	RandomGenerator& random = *RandomGenerator::Instance();
	random.Shuffle(allPlayerIDs.begin(), allPlayerIDs.end());

	// Place them into the teams
	int nextTeamID = 0;
	for (int i = 0; i < allPlayerIDs.size(); i++)
	{
		TeamData& team = teamData.GetTeam(nextTeamID);
		team.AddPlayer(allPlayerIDs[i]);
		nextTeamID = (nextTeamID + 1) % teamCount;
	}
}
void EvenTeamsGamemode::AssignPlayerMidGame(const int playerID, AllTeamState& teamData)
{
	// Assign them to the smallest occupied team
	int smallestOccupiedTeamID = -1;
	int smallestOccupiedTeamSize = 1000;

	for (int i = 0; i < (int)teamData.m_teamData.size(); i++)
	{
		int teamSize = (int)teamData.m_teamData[i].m_playersOnTeam.size();
		if (teamSize < smallestOccupiedTeamSize)
		{
			smallestOccupiedTeamSize = teamSize;
			smallestOccupiedTeamID = i;
		}
	}

	if (smallestOccupiedTeamID == -1)
	{
		printf("Cannot find team to assign player %d to ", playerID);
		return;
	}
	teamData.GetTeam(smallestOccupiedTeamID).AddPlayer(playerID);
}
const char* EvenTeamsGamemode::GetGameModeName()
{
	return "Teams";
}
bool EvenTeamsGamemode::IsAppropriateForPlayerCount(int playerCount)
{
	return playerCount > 2 && (playerCount % 2) == 0;
}