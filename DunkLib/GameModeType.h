#pragma once
#include "AllTeamState.h"
typedef chobo::static_vector<bool, PLAYER_COUNT> ConnectedPlayers;

struct GameModeType
{
	virtual void AssignPlayersToTeams(const ConnectedPlayers& connectedIDs, AllTeamState& teamData) = 0;
	virtual void AssignPlayerMidGame(const int playerID, AllTeamState& teamData) = 0;
	virtual const char* GetGameModeName() = 0;
	virtual bool IsAppropriateForPlayerCount(int playerCount) = 0;
};

struct FreeForAllGamemode : GameModeType
{
	virtual void AssignPlayersToTeams(const ConnectedPlayers& connectedIDs, AllTeamState& teamData);
	virtual void AssignPlayerMidGame(const int playerID, AllTeamState& teamData);
	virtual const char* GetGameModeName();
	virtual bool IsAppropriateForPlayerCount(int playerCount);
};

struct EvenTeamsGamemode : GameModeType
{
	virtual void AssignPlayersToTeams(const ConnectedPlayers& connectedIDs, AllTeamState& teamData);
	virtual void AssignPlayerMidGame(const int playerID, AllTeamState& teamData);
	virtual const char* GetGameModeName();
	virtual bool IsAppropriateForPlayerCount(int playerCount);
};