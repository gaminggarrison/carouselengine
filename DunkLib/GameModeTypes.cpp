#include "GameModeTypes.h"

namespace GameModeTypes
{
	static_vector<GameModeType*, MAX_GAMEMODE_TYPES> m_gameModes = { nullptr, new FreeForAllGamemode(), new EvenTeamsGamemode() };


	GameModeType* GetGameMode(GameModeTypeID id)
	{
		return m_gameModes[(int)id];
	}

	static_vector<GameModeTypeID, MAX_GAMEMODE_TYPES> GetValidGameModes(int playerCount)
	{
		static_vector<GameModeTypeID, MAX_GAMEMODE_TYPES> validModes;
		for (int i = 0; i < (int)m_gameModes.size(); i++)
		{
			GameModeTypeID modeID = (GameModeTypeID)i;
			GameModeType* gameMode = GetGameMode(modeID);
			if (gameMode != nullptr && gameMode->IsAppropriateForPlayerCount(playerCount))
			{
				validModes.push_back(modeID);
			}
		}
		return validModes;
	}
}