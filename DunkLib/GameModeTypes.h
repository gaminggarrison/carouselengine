#pragma once
#include "GameModeType.h"

enum class GameModeTypeID
{
	None = 0,
	FreeForAll = 1,
	EvenTeams = 2
};

#define MAX_GAMEMODE_TYPES 8
namespace GameModeTypes
{
	GameModeType* GetGameMode(GameModeTypeID id);
	static_vector<GameModeTypeID, MAX_GAMEMODE_TYPES> GetValidGameModes(int playerCount);
};
