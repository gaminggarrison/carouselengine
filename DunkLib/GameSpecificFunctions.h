#pragma once
class CustomFrameState;
struct BasePlayerControlState;
struct CustomClientGame;
#include <memory>

// Your game will need to create a subclass that fills in the functions with the specific behaviour for your game, and pass this to the library on startup
struct GameSpecificFunctions
{
	// ---- Networking state related

	// So the library can construct your CustomFrameState subclass
	virtual std::shared_ptr<CustomFrameState> CreateCustomGameState() = 0;
	virtual std::shared_ptr<BasePlayerControlState> CreateCustomControlStateOnHeap() = 0;
	virtual std::shared_ptr<CustomClientGame> CreateCustomClientGameOnHeap() = 0;
	virtual const char* GetVersion() = 0;
	// Setup any initial world state (not including players)
	virtual void StateInit(CustomFrameState& frameState) = 0;


	// ---- General game initialisation related

	// For general initialisation at the start of the game, or when hot reloading code/assets
	virtual void InitialiseGameSpecificData() = 0;
	// Called when everything is initialised - use it to set the starting state machine state
	virtual void StartGame() = 0;
	virtual const char* GetGameName() = 0;
};
