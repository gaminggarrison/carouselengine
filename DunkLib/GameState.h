#pragma once

class GameState
{
public:
	virtual ~GameState() {}
	virtual void Init() = 0;
	virtual void Cleanup() = 0;
	virtual void Update(float deltaTime) = 0;
	virtual void Draw() = 0;
	virtual void HandleReload() {};
};

// https://barrgroup.com/embedded-systems/how-to/polymorphism-no-heap-memory
enum { MAX_GAMESTATE_SIZE = 4096 };

class GenericGameState
{
	char buf[MAX_GAMESTATE_SIZE];
public:
	GameState *operator->()
	{
		return (GameState*)buf;
	}
	GameState const *operator->() const
	{
		return (GameState const *)buf;
	}
	GameState *get() // For placement new or delete
	{
		return (GameState*)buf;
	}
};