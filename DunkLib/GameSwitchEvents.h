#pragma once
#include "cereal/types/string.hpp"

struct RequestGameSwitchEvent
{
	std::string m_gameName;

	template<class Archive>
	void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
	{
		archive(m_gameName);
	}
};

struct GameSwitchEvent
{
	std::string m_gameName;

	template<class Archive>
	void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
	{
		archive(m_gameName);
	}
};

struct GameSwitchReadyMessage
{
	std::string m_gameName;

	template<class Archive>
	void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
	{
		archive(m_gameName);
	}
};

struct DoGameSwitchEvent
{
	std::string m_gameName;

	template<class Archive>
	void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
	{
		archive(m_gameName);
	}
};

struct GameSwitchedReadyMessage
{
	std::string m_gameName;

	template<class Archive>
	void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
	{
		archive(m_gameName);
	}
};

struct GameSwitchFinishEvent
{
	std::string m_gameName;
	uint32_t m_crc;
	FrameState m_frameState;

	template<class Archive>
	void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
	{
		archive(m_gameName, m_crc, m_frameState);
	}
};