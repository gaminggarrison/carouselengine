#pragma once

#include "Memory.h"

// Need the extern "C" to make the compiler not mangle the name >_<
// DLLEXPORT is to mark the function to expose it to the outside world
extern "C" DLLEXPORT int GameUpdateAndRender(Memory* memory, int debugInstance, bool headlessMode);


typedef void game_prepare_for_reload(Memory* memory);

// Need the extern "C" to make the compiler not mangle the name >_<
// DLLEXPORT is to mark the function to expose it to the outside world
extern "C" DLLEXPORT void GamePrepareForReload(Memory* memory);



typedef void game_shutdown(Memory* memory);

// Need the extern "C" to make the compiler not mangle the name >_<
// DLLEXPORT is to mark the function to expose it to the outside world
extern "C" DLLEXPORT void GameShutdown(Memory* memory);
