#include "HotReloadBinding.h"
#include <iostream>

// It's expected that the GameplayCode will include this with a Game class defined
int GameUpdateAndRender(Memory* memory, int debugInstance, bool headlessMode)
{
	if (!memory->IsInitialized)
	{
		Game* game = new (memory->PermanentStorage)Game();
		game->Init(debugInstance, headlessMode);
		memory->IsInitialized = true;
	}
	Game* game = (Game*)memory->PermanentStorage;
	return game->Run();
}

void GamePrepareForReload(Memory* memory)
{
	if (memory->IsInitialized)
	{
		Game* game = (Game*)memory->PermanentStorage;
		game->PrepareForReload();
	}
}

void GameShutdown(Memory* memory)
{
	if (memory->IsInitialized)
	{
		Game* game = (Game*)memory->PermanentStorage;
		game->Cleanup();
	}
}
