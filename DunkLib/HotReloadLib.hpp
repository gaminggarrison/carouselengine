#pragma once
#define WIN32_LEAN_AND_MEAN
#define _AMD64_ 1
#include <libloaderapi.h>
#include <iostream>
#include <fstream>
//#include <filesystem>
#include <fileapi.h>
#include <string>
#include <direct.h>
//#include <cstdio>
using namespace std;

int GameUpdateAndRenderStub(Memory* memory, int debugInstance, bool headlessMode)
{
	return 0;
}
void GamePrepareForReloadStub(Memory* memory)
{

}
void GameShutdownStub(Memory* memory)
{

}

struct game_code
{
#ifndef NO_HOT_RELOAD
	HMODULE GameCodeDLL;
	FILETIME DllLastWriteTime;
	bool IsValid;
#endif
	game_update_and_render* UpdateAndRender;
	game_prepare_for_reload* PrepareForReload;
	game_shutdown* Shutdown;
};

void CopyFile(const char* from, const char* to)
{
	char cCurrentPath[128];

	cout << "Current Path =";
	_getcwd(cCurrentPath, sizeof(cCurrentPath)); // Get current working directory
	cout << cCurrentPath;
	cout << "\n";

	ifstream src("GameplayCode.dll", ios::binary);
	ofstream dst("GameplayCodeCopy.dll", ios::binary);
	dst << src.rdbuf();
}

inline FILETIME Win32GetLastWriteTime(const char* filename)
{
	FILETIME lastWriteTime = {};

	WIN32_FIND_DATA findData;
	HANDLE findHandle = FindFirstFileA(filename, &findData);
	if (findHandle != 0)
	{
		lastWriteTime = findData.ftLastWriteTime;
		FindClose(findHandle);
	}

	return lastWriteTime;
}

game_code LoadGameCode(const char* sourceDLLName)
{
	game_code result = {};
	result.UpdateAndRender = GameUpdateAndRenderStub;
	result.PrepareForReload = GamePrepareForReloadStub;
	result.Shutdown = GameShutdownStub;

	//const char* sourceDLLName = "GameplayCode.dll";
	const char* tempDLLName = "GameplayCodeCopy.dll";

	result.DllLastWriteTime = Win32GetLastWriteTime(sourceDLLName);


	CopyFile(sourceDLLName, tempDLLName);
	result.GameCodeDLL = LoadLibraryA(tempDLLName);
	if (result.GameCodeDLL)
	{
		result.UpdateAndRender = (game_update_and_render*)GetProcAddress(result.GameCodeDLL, "GameUpdateAndRender");
		result.PrepareForReload = (game_prepare_for_reload*)GetProcAddress(result.GameCodeDLL, "GamePrepareForReload");
		result.Shutdown = (game_shutdown*)GetProcAddress(result.GameCodeDLL, "GameShutdown");

		result.IsValid = result.UpdateAndRender != nullptr && result.Shutdown != nullptr;
		cout << "Found DLL!\n";
	}
	else
	{
		cout << "Could not find DLL :(\n";
	}

	if (!result.IsValid)
	{
		result.UpdateAndRender = GameUpdateAndRenderStub;
		result.PrepareForReload = GamePrepareForReloadStub;
		result.Shutdown = GameShutdownStub;
		cout << "Could not find UpdateAndRender!\n";
	}
	return result;
}
game_code LoadGameCodeUntilValid(const char* sourceDLLName)
{
	game_code gameCode = LoadGameCode(sourceDLLName);
	while (!gameCode.IsValid)
	{
		gameCode = LoadGameCode(sourceDLLName);
	}
	return gameCode;
}
void UnloadGameCode(game_code* gameCode)
{
	if (gameCode->GameCodeDLL)
	{
		FreeLibrary(gameCode->GameCodeDLL);
	}

	gameCode->IsValid = false;
	gameCode->UpdateAndRender = GameUpdateAndRenderStub;
	gameCode->PrepareForReload = GamePrepareForReloadStub;
	gameCode->Shutdown = GameShutdownStub;
}