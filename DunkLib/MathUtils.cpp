#include "MathUtils.h"
#include <cstdio>
#include <climits>
#include "RandomGenerator.h"

int Sign(int i)
{
	if (i < 0)
	{
		return -1;
	}
	else if (i > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int Sign(float i)
{
	if (i < 0)
	{
		return -1;
	}
	else if (i > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
int Clamp(int value, int min, int max)
{
	if (value < min)
	{
		value = min;
	}
	else if (value > max)
	{
		value = max;
	}
	return value;
}
float Clamp01(float value)
{
	if (value < 0.0f)
	{
		return 0.0f;
	}
	else if (value > 1.0f)
	{
		return 1.0f;
	}
	return value;
}

Vector2 Clamp(Vector2 value, float min, float max)
{
	value.x = Clamp(value.x, min, max);
	value.y = Clamp(value.y, min, max);
	return value;
}
Vector2 ClampInsideRect(Vector2 value, RayRectangle rect)
{
	if (value.x < rect.x)
	{
		value.x = rect.x;
	}
	else if (value.x > rect.x + rect.width)
	{
		value.x = rect.x + rect.width;
	}
	if (value.y < rect.y)
	{
		value.y = rect.y;
	}
	else if (value.y > rect.y + rect.height)
	{
		value.y = rect.y + rect.height;
	}
	return value;
}
Vector2 RectPositionToWorld(RayRectangle rect, Vector2 innerPos)
{
	Vector2 pos = Vector2Add({ rect.x, rect.y }, Vector2MultiplyV({ rect.width, rect.height }, innerPos));
	return pos;
}
Vector2 WorldPositionToRectPosition(RayRectangle rect, Vector2 worldPos)
{
	Vector2 rectPos = worldPos - Vector2{ rect.x, rect.y };
	rectPos = Vector2MultiplyV(rectPos, Vector2{ 1.0f / rect.width, 1.0f / rect.height });
	return rectPos;
}
bool IsInsideRect(Vector2 value, RayRectangle rect)
{
	return value.x >= rect.x && value.x <= rect.x + rect.width && value.y >= rect.y && value.y <= rect.y + rect.height;
}
//From https://stackoverflow.com/a/402010
bool CircleOverlapsRect(Vector2 circlePos, float circleRadius, RayRectangle rect)
{
	Vector2 rectMid = { rect.x + rect.width * 0.5f, rect.x + rect.height * 0.5f };
	Vector2 circleDistance = { fabsf(circlePos.x - rectMid.x), fabsf(circlePos.y - rectMid.y) };
	Vector2 halfRect = { rect.width * 0.5f, rect.height * 0.5f };

	if (circleDistance.x > halfRect.x + circleRadius) { return false; }
	if (circleDistance.y > halfRect.y + circleRadius) { return false; }

	if (circleDistance.x <= halfRect.x) { return true; }
	if (circleDistance.y <= halfRect.y) { return true; }

	float cornerDistSqrd = Vector2SquaredLength(circleDistance - halfRect);
	return cornerDistSqrd <= circleRadius * circleRadius;
}
RayRectangle GetRectSquishedToFitAspect(RayRectangle rect, float desiredAspect)
{
	float currentAspect = rect.width / rect.height;
	RayRectangle newRect = rect;
	if (currentAspect > desiredAspect) // Too wide
	{
		float widthBefore = newRect.width;
		newRect.width *= desiredAspect / currentAspect;
		newRect.x += (widthBefore - newRect.width) * 0.5f;
	}
	else if (currentAspect < desiredAspect) // Too tall
	{
		float heightBefore = newRect.height;
		newRect.height *= currentAspect / desiredAspect;
		newRect.y += (heightBefore - newRect.height) * 0.5f;
	}
	return newRect;
}

Vector2 VectorFromAngleDistance(float angle, float distance)
{
	return { -sinf(angle) * distance, -cosf(angle) * distance };
}

Vector2 FindLineEnd(Vector2 lineStart, Vector2 target, float maxDistance)
{
	Vector2 toTarget = Vector2Subtract(target, lineStart);
	float currentDistance = Vector2Length(toTarget);
	if (currentDistance > maxDistance)
	{
		toTarget = Vector2Scale(Vector2Normalize(toTarget), maxDistance);
	}
	return Vector2Add(lineStart, toTarget);
}
Vector2 Vector2Project(Vector2 v, Vector2 target)
{
	float dot = Vector2DotProduct(v, target);
	return Vector2Scale(target, dot / Vector2Length(target));
}
Vector2 RotateVector(Vector2 vector, float rotation)
{
	Matrix rotate = MatrixRotateZ(rotation);
	Vector2 rotated = Vector2Transform(vector, rotate);
	return rotated;
}
Vector2 RotateRight90(Vector2 vector)
{
	return { -vector.y, vector.x };
}
Vector2 RotateLeft90(Vector2 vector)
{
	return { vector.y, -vector.x };
}
Vector2Int RotateRight90Int(Vector2Int vector)
{
	return { -vector.y, vector.x };
}
Vector2Int RotateLeft90Int(Vector2Int vector)
{
	return { vector.y, -vector.x };
}
float GetDeltaWithinRange(float value, float min, float max)
{
	if (value < min)
	{
		return 0.0f;
	}
	else if (value > max)
	{
		return 1.0f;
	}
	else
	{
		float extra = value - min;
		return extra / (max - min);
	}
}
float SmootherStep(float edge0, float edge1, float x)
{
	// Scale, and clamp x to 0..1 range
	x = Clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
	// Evaluate polynomial
	return x * x * x * (x * (x * 6 - 15) + 10);
}
int Min(int a, int b)
{
	if (a < b)
	{
		return a;
	}
	else
	{
		return b;
	}
}
float Minf(float a, float b)
{
	return fminf(a, b);
}
float Maxf(float a, float b)
{
	return fmaxf(a, b);
}
float MoveTowards(float value, float target, float maxAmount)
{
	float toTarget = target - value;
	if (toTarget < -maxAmount)
	{
		toTarget = -maxAmount;
	}
	else if (toTarget > maxAmount)
	{
		toTarget = maxAmount;
	}
	return value + toTarget;
}
int MoveTowards(int value, int target, int maxAmount)
{
	int toTarget = target - value;
	if (toTarget < -maxAmount)
	{
		toTarget = -maxAmount;
	}
	else if (toTarget > maxAmount)
	{
		toTarget = maxAmount;
	}
	return value + toTarget;
}
Vector3 Vector2ToVector3(Vector2 v)
{
	return { v.x, v.y, 0.0f };
}

float sign(Vector2 p1, Vector2 p2, Vector2 p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

bool IsPointInTriangle(Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3)
{
	float d1, d2, d3;
	bool has_neg, has_pos;

	d1 = sign(pt, v1, v2);
	d2 = sign(pt, v2, v3);
	d3 = sign(pt, v3, v1);

	has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	return !(has_neg && has_pos);
}
int RoundToNearestInt(float value)
{
	if (value >= 0.0f)
	{
		return (int)(value + 0.5f);
	}
	else
	{
		return (int)(value - 0.5f); // Truncation of a negative value will move towards 0 (upwards), so we need to counteract that to get the nearest int
	}
}
Vector2 RoundToInteger(Vector2 value)
{
	return { roundf(value.x), roundf(value.y) };
}
Vector2 RoundToNearest(Vector2 value, Vector2 unit)
{
	Vector2 scaled = Vector2DivideV(value, unit);
	Vector2 rounded = RoundToInteger(scaled);
	Vector2 result = Vector2MultiplyV(rounded, unit);
	return result;
}
float Vector2CrossProduct(const Vector2& v1, const Vector2& v2) // See https://stackoverflow.com/questions/243945/calculating-a-2d-vectors-cross-product
{
	return (v1.x*v2.y) - (v1.y*v2.x);
}

unsigned char AddToChar(unsigned char value, int toAdd)
{
	int v = value;
	v += toAdd;
	if (v < 0)
	{
		v = 0;
	}
	else if (v > 255)
	{
		v = 255;
	}
	return (unsigned char)v;
}
int GetRandomInt(int min, int max)
{
	return RandomGenerator::Instance()->GenerateInt(min, max);
}
int GetRandomIntInclusive(int min, int max)
{
	return RandomGenerator::Instance()->GenerateInt(min, max + 1);
}
float GetRandomFloat(float min, float max)
{
	return RandomGenerator::Instance()->GenerateFloat(min, max);
}
float WrapAngle(float angle) // https://stackoverflow.com/questions/11980292/how-to-wrap-around-a-range
{
	float twoPI = PI * 2.0f;
	return angle - twoPI * floor(angle / twoPI);
}
float WrapAngleToNearestSide(float angle)
{
	return -PI + WrapAngle(angle + PI);
}
float GetAngleDifference(float startAngle, float targetAngle)
{
	float diff = targetAngle - startAngle;
	return WrapAngleToNearestSide(diff);
}
float AngleOfVector(Vector2 vector)
{
	// Seems that atan2 starts from East and rotates clockwise?
	return (-atan2f(vector.y, vector.x)) - PI * 0.5f;
}
float AngleOfVectorFromEast(Vector2 vector)
{
	// Seems that atan2 starts from East and rotates clockwise?
	return (-atan2f(vector.y, vector.x));
}
int FloorToInt(float value)
{
	// Floor the value https://www.codeproject.com/Tips/700780/Fast-floor-ceiling-functions
	int result = (int)value;
	if (result > value)
	{
		result--;
	}
	return result;
}
int Max(int a, int b)
{
	if (a > b)
	{
		return a;
	}
	else return b;
}
bool IsPowerOf2(int value)
{
	return value > 0 && ((value & (value - 1)) == 0);
}

//https://www.construct.net/en/blogs/ashleys-blog-2/using-lerp-delta-time-924
float LerpSmooth(float a, float b, float lerpSpeed, float deltaTime)
{
	return Lerp(a, b, 1.0f - (powf(1.0f - lerpSpeed, deltaTime)));
}

double_t LerpSmoothDouble(double_t a, double_t b, double_t lerpSpeed, double_t deltaTime)
{
	double_t t = 1.0 - (pow(1.0 - lerpSpeed, deltaTime));

	return a + ((b - a) * t);
}

// https://www.skytopia.com/project/articles/compsci/clipping.html#alg
LineSegmentResult GetLineSegmentInsideBox(Vector2 start, Vector2 end, RayRectangle rect)
{
	Vector2 min = { rect.x, rect.y };
	Vector2 max = { rect.x + rect.width, rect.y + rect.height };

	float t0 = 0.0f;
	float t1 = 1.0f;
	Vector2 delta = end - start;
	float p, q, r;

	for (int edge = 0; edge < 4; edge++) // Traverse through left, right, bottom, top edges
	{
		if (edge == 0) { p = -delta.x; q = -(min.x - start.x); }
		if (edge == 1) { p = delta.x;  q = (max.x - start.x); }
		if (edge == 2) { p = -delta.y; q = -(min.y - start.y); }
		if (edge == 3) { p = delta.y;  q = (max.y - start.y); }
		r = q / p;
		if (p == 0 && q < 0)
		{
			return { start, end, false }; // Parallal line outside
		}
		
		if (p < 0)
		{
			if (r > t1)
			{
				return { start, end, false }; // Line outside
			}
			else if (r > t0)
			{
				t0 = r; // Line is clipped!
			}
		}
		else if (p > 0)
		{
			if (r < t0)
			{
				return { start, end, false }; // Line outside
			}
			else if (r < t1)
			{
				t1 = r; // Line is clipped!
			}
		}
	}

	return { start + delta * t0, start + delta * t1, true};
}
// http://csharphelper.com/blog/2014/09/determine-where-a-line-intersects-a-circle-in-c/
std::pair<Vector2, bool> GetLineCircleIntersection(Vector2 start, Vector2 end, Vector2 circlePos, float circleRadius)
{
	Vector2 circleToStart = start - circlePos;
	float c = Vector2SquaredLength(circleToStart) - circleRadius * circleRadius;
	if (c < 0.0f)
	{
		return { start, true };
	}
	Vector2 dir = end - start;
	float a = Vector2SquaredLength(dir);
	float b = 2.0f * (dir.x * circleToStart.x + dir.y * circleToStart.y);

	float det = b * b - 4 * a * c;
	if ((a <= 0.0000001) || (det < 0))
	{
		// No real solutions
		return { end, false };
	}
	else if (det == 0)
	{
		// One solution (ie. hits at a tangent point)
		float t = -b / (2 * a);
		if (t < 0.0f || t > 1.0f) // The collision is backwards or beyond the end
		{
			return { end, false };
		}
		return { start + dir * t, true };
	}
	else
	{
		// Two solutions (we just care about the first one)
		//float t = ((-b + sqrtf(det)) / (2 * a));
		float t = ((-b - sqrtf(det)) / (2 * a));
		if (t < 0.0f || t > 1.0f) // The collision is backwards or beyond the end
		{
			return { end, false };
		}
		return { start + dir * t, true };
	}
}

Vector2 RectTopLeft(const RayRectangle& rect)
{
	return { rect.x, rect.y };
}
Vector2 RectTopRight(const RayRectangle& rect)
{
	return { rect.x + rect.width, rect.y };
}
Vector2 RectBottomLeft(const RayRectangle& rect)
{
	return { rect.x, rect.y + rect.height };
}
Vector2 RectBottomRight(const RayRectangle& rect)
{
	return { rect.x + rect.width, rect.y + rect.height };
}