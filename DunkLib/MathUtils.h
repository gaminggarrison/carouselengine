#pragma once
#include "raymath.h"
#include "Vector2Int.h"

inline float sinfX(float radians)
{
	float x = radians;
	float x2 = x * x;
	float f = 1;
	float s = 0;
	int i = 1;

	s += x / f; x *= x2; f *= ++i; f *= ++i;
	s -= x / f; x *= x2; f *= ++i; f *= ++i;
	s += x / f; x *= x2; f *= ++i; f *= ++i;
	s -= x / f; x *= x2; f *= ++i; f *= ++i;
	s += x / f; x *= x2; f *= ++i; f *= ++i;
	s -= x / f; x *= x2; f *= ++i; f *= ++i;
	s += x / f; x *= x2; f *= ++i; f *= ++i;
	s -= x / f; x *= x2; f *= ++i; f *= ++i;
	return s;
}

int Sign(int i);
int Sign(float i);
int Clamp(int value, int min, int max);
float Clamp01(float value);
Vector2 Clamp(Vector2 value, float min, float max);
Vector2 ClampInsideRect(Vector2 value, RayRectangle rect);
Vector2 RectPositionToWorld(RayRectangle rect, Vector2 innerPos);
Vector2 WorldPositionToRectPosition(RayRectangle rect, Vector2 worldPos);
bool IsInsideRect(Vector2 value, RayRectangle rect);
bool CircleOverlapsRect(Vector2 circlePos, float circleRadius, RayRectangle rect);
RayRectangle GetRectSquishedToFitAspect(RayRectangle rect, float desiredAspect);
Vector2 VectorFromAngleDistance(float angle, float distance);
Vector2 FindLineEnd(Vector2 lineStart, Vector2 target, float maxDistance);
Vector2 Vector2Project(Vector2 v, Vector2 target);
Vector2 RotateVector(Vector2 vector, float rotation);
Vector2 RotateRight90(Vector2 vector);
Vector2Int RotateRight90Int(Vector2Int vector);
Vector2Int RotateLeft90Int(Vector2Int vector);
float GetDeltaWithinRange(float value, float min, float max);
// By Ken Perlin
float SmootherStep(float value, float min, float max);

int Min(int a, int b);
float Minf(float a, float b);
float Maxf(float a, float b);
float MoveTowards(float value, float target, float maxAmount);
int MoveTowards(int value, int target, int maxAmount);
Vector3 Vector2ToVector3(Vector2 v);
bool IsPointInTriangle(Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3);
int RoundToNearestInt(float value);
Vector2 RoundToInteger(Vector2 value);
Vector2 RoundToNearest(Vector2 value, Vector2 unit);
float Vector2CrossProduct(const Vector2& v1, const Vector2& v2);
unsigned char AddToChar(unsigned char value, int toAdd);
// Not inclusive of Max
int GetRandomInt(int min, int max);
int GetRandomIntInclusive(int min, int max);

template<typename T>
T& GetRandomFromVector(std::vector<T>& v)
{
	int chosen = GetRandomInt(0, (int)v.size());
	return v[chosen];
}
float GetRandomFloat(float min, float max);

// Will keep the angle in 0 to 2*PI range
float WrapAngle(float angle);
// Will return the angle in -PI to PI range
float WrapAngleToNearestSide(float angle);
// Returns closest rotation needed in -PI to PI range
float GetAngleDifference(float startAngle, float targetAngle);
float AngleOfVector(Vector2 vector);
float AngleOfVectorFromEast(Vector2 vector);
int FloorToInt(float value);
int Max(int a, int b);
bool IsPowerOf2(int value);
float LerpSmooth(float a, float b, float lerpSpeed, float deltaTime);
double_t LerpSmoothDouble(double_t a, double_t b, double_t lerpSpeed, double_t deltaTime);

struct LineSegmentResult
{
	Vector2 m_start;
	Vector2 m_end;
	bool m_isValid;
};
LineSegmentResult GetLineSegmentInsideBox(Vector2 start, Vector2 end, RayRectangle rect);
std::pair<Vector2, bool> GetLineCircleIntersection(Vector2 start, Vector2 end, Vector2 circlePos, float circleRadius);

// Return max value for each pair of components
RMDEF Vector2 Vector2Max(Vector2 v1, Vector2 v2)
{
	Vector2 result = { 0 };

	result.x = fmaxf(v1.x, v2.x);
	result.y = fmaxf(v1.y, v2.y);

	return result;
}
RMDEF float Vector3SquaredLength(Vector3 v)
{
	return v.x * v.x + v.y * v.y + v.z * v.z;
}

RMDEF float Vector2SquaredLength(Vector2 v)
{
	float result = (v.x*v.x) + (v.y*v.y);
	return result;
}
// Calculate squared distance between two vectors
RMDEF float Vector2DistanceSqrd(Vector2 v1, Vector2 v2)
{
	float result = (v1.x - v2.x)*(v1.x - v2.x) + (v1.y - v2.y)*(v1.y - v2.y);
	return result;
}

// Transforms a Vector2 by a given Matrix
RMDEF Vector2 Vector2Transform(Vector2 v, Matrix mat)
{
	Vector2 result = { 0 };
	float x = v.x;
	float y = v.y;
	//float z = v.z;

	result.x = mat.m0*x + mat.m4*y + mat.m12;
	result.y = mat.m1*x + mat.m5*y + mat.m13;
	//result.z = mat.m2*x + mat.m6*y + mat.m10*z + mat.m14;
	return result;
}

// Multiply vector by vector
RMDEF Vector2 Vector2MultiplyV(Vector2 v1, Vector2 v2)
{
	Vector2 result = { v1.x*v2.x, v1.y*v2.y };
	return result;
}

// Divide vector by vector
RMDEF Vector2 Vector2DivideV(Vector2 v1, Vector2 v2)
{
	Vector2 result = { v1.x / v2.x, v1.y / v2.y };
	return result;
}

RMDEF bool operator== (const Vector2& first, const Vector2& second)
{
	return first.x == second.x && first.y == second.y;
}
RMDEF bool operator!= (const Vector2& first, const Vector2& second)
{
	return first.x != second.x || first.y != second.y;
}
RMDEF Vector2 operator+ (const Vector2& first, const Vector2& second)
{
	Vector2 result = { first.x + second.x, first.y + second.y };
	return result;
}
RMDEF Vector2& operator+= (Vector2& first, const Vector2& second)
{
	first.x = first.x + second.x;
	first.y = first.y + second.y;
	return first;
}
RMDEF Vector2 operator- (const Vector2& first, const Vector2& second)
{
	Vector2 result = { first.x - second.x, first.y - second.y };
	return result;
}
RMDEF Vector2 operator* (const Vector2& first, const Vector2& second)
{
	Vector2 result = { first.x * second.x, first.y * second.y };
	return result;
}
RMDEF Vector2 operator* (const Vector2& first, const float& second)
{
	Vector2 result = { first.x * second, first.y * second };
	return result;
}
RMDEF Vector2 operator/ (const Vector2& first, const Vector2& second)
{
	Vector2 result = { first.x / second.x, first.y / second.y };
	return result;
}
RMDEF Vector2 operator/ (const Vector2& first, const float& second)
{
	Vector2 result = { first.x / second, first.y / second };
	return result;
}

// Unfortunately C++ doesn't really do extension methods :(
Vector2 RectTopLeft(const RayRectangle& rect);
Vector2 RectTopRight(const RayRectangle& rect);
Vector2 RectBottomLeft(const RayRectangle& rect);
Vector2 RectBottomRight(const RayRectangle& rect);

// Calculate linear interpolation between two vectors
RMDEF Vector4 Vector4Lerp(Vector4 v1, Vector4 v2, float amount)
{
	Vector4 result = { 0 };

	result.x = v1.x + amount * (v2.x - v1.x);
	result.y = v1.y + amount * (v2.y - v1.y);
	result.z = v1.z + amount * (v2.z - v1.z);
	result.w = v1.w + amount * (v2.w - v1.w);

	return result;
}
