#pragma once
#include <stdlib.h>
#include "Compiler.h"

struct Memory
{
	bool IsInitialized;
	uint32_t PermanentStorageSize;
	void* PermanentStorage;
	Memory(uint32_t memoryBytes)
	{
		IsInitialized = false;
		PermanentStorageSize = memoryBytes;
		PermanentStorage = malloc(PermanentStorageSize);
	}
};

typedef int game_update_and_render(Memory* memory, int debugInstance, bool headlessMode);
typedef void game_shutdown(Memory* memory);
