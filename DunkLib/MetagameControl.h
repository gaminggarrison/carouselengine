#pragma once

struct MetagameControl
{
	virtual void StartGameSwitchForRoom(const char* newGameName) = 0;
};