#pragma once
#include "MinigameSpecificFunctions.h"

#if EMSCRIPTEN
#include <emscripten/emscripten.h>
namespace MINIGAME_NAMESPACE
{
	extern "C" EMSCRIPTEN_KEEPALIVE GameSpecificFunctions* Load();
}
extern "C" EMSCRIPTEN_KEEPALIVE GameSpecificFunctions* MINIGAME_NAMESPACE_LoadGeneral();
#else
#ifndef DLL_MINIGAMES
extern "C" GameSpecificFunctions* MINIGAME_NAMESPACE_LoadGeneral();
#else
#include "Memory.h"
namespace MINIGAME_NAMESPACE
{
	extern "C" DLLEXPORT GameSpecificFunctions* Load();
}
#endif
#endif
