#include "MinigameLoader.h"

#include <stdio.h>
#include "GameSpecificFunctions.h"
#include <string>

#include "raylib.h"
#include <algorithm>
#include "FileUtils.h"
#include "ConfigData.h"
#include "EngineState.h"
#include "FolderUtils.h"
#include "AudioUtils.h"

std::vector<MinigameDescription> FindModuleListFromDisk(const char* folder, const char* extension)
{
	printf("Finding module list from disk for folder %s and extension %s\n", folder, extension);
	std::vector<MinigameDescription> modules;

	int fileCount = 0;
	char** filenames = GetDirectoryFiles(folder, &fileCount);
	std::sort(filenames, filenames + fileCount, [](const char *a, const char *b) { return strcmp(a, b) < 0; });

	for (int i = 0; i < fileCount; i++)
	{
		const char* filePath = TextFormat("%s/%s", folder, filenames[i]);
		const char* fileExtension = GetExtension(filePath);
		//printf("File %d = %s extension = %s\n", i, filenames[i], extension);
		if (fileExtension != nullptr && TextIsEqual(extension, &fileExtension[1])) // Raylib adds an initial dot now
		{
			const char* fileName = GetFileNameWithoutExt(filePath);
			const char* filePathWithoutExtension = TextFormat("%s/%s", folder, fileName);
			long modificationTime = GetFileModTime(filePath);

			MinigameDescription description;
			description.m_filename = fileName;

			//std::string moduleDescriptionFilename = std::string(filePathWithoutExtension) + ".game";
			//if (FileExists(moduleDescriptionFilename.c_str()))
			//{
			/*bool success = FileUtils::LoadObjectFromJsonMinimal<MinigameDescription>(filePath, description);
			if (!success)
			{
				printf("Error deserialising MinigameDescription at path %s\n", filePath);
			}*/

			modules.push_back(description);
			//}
			//else
			//{
			//	printf("Ignoring module %s because no matching .game file found\n", fileName);
			//}
		}
	}
	ClearDirectoryFiles();
	return modules;
}

typedef GameSpecificFunctions* module_load_function();

std::string GetBaseModulesFolder()
{
	std::string modulesFolder = std::string("Modules/");

	bool loadLocal = EngineState::s_engineState->ShouldUseLocalAssets();
	if (loadLocal)
	{
		std::string baseDir = std::string(SOLUTION_DIR);
		std::string localDir = baseDir + std::string("GameplayCode/");
		modulesFolder = localDir;
	}
	else
	{
#if EMSCRIPTEN
		if (EngineState::s_engineState->IsHeadless())
		{
#ifndef NODERAWFS
			modulesFolder = std::string("working/") + modulesFolder;
#endif
		}
#endif
	}
	return modulesFolder;
}

#if EMSCRIPTEN
#include <emscripten.h>
#include <emscripten/bind.h>
#ifndef NODEFS
#include <emscripten/fetch.h>
#endif
#include <string.h>

using namespace emscripten;

#else
#ifndef DLL_MINIGAMES
#define X(x) extern "C" GameSpecificFunctions* x ## _NAMESPACE_LoadGeneral(); \

LIST_OF_MODULES
#undef X
// Which expands to examples like this, statically linking to each Minigame's Load function
//extern "C" GameSpecificFunctions* SideModule_NAMESPACE_LoadGeneral();
// I tried using separately namespaced functions (rather than all in global namespace), but emscripten had trouble finding them in linked side-modules

#define X(x) else if (strcmp(name, #x) == 0) \
{ \
	return x ## _NAMESPACE_LoadGeneral(); \
}
// Allowing us to load by string, rather than compiling to use a specific Load function
GameSpecificFunctions* LoadByString(const char* name, bool isHeadless) // This can break in the DunkLibServer project because LIST_OF_MODULES isn't defined...
{
	if (name == nullptr)
	{
		return nullptr;
	}
	LIST_OF_MODULES
	return nullptr;
}
#undef X

#define X(x) gameInfo.m_filename = std::string(#x); \
list.push_back(gameInfo);

std::vector<MinigameDescription> DoGetGamesList()
{
	std::vector<MinigameDescription> list;
	MinigameDescription gameInfo;

	LIST_OF_MODULES
	return list;
}

#endif
#endif

MinigameLoader* MinigameLoader::s_instance = nullptr;

void MinigameLoader::Init(bool headless)
{
	s_instance = this;

#if EMSCRIPTEN
#ifndef NODERAWFS
	if (headless)
	{
		// mount the current folder as a NODEFS instance
		// inside of emscripten
		EM_ASM(
			FS.mkdir("/working");
			FS.mount(NODEFS, { root: '.' }, '/working');
		);
	}
#endif
#endif
	if (headless)
	{
		// Build the game list
		printf("Building the game list\n");
		std::vector<MinigameDescription> games = GetGamesList();
		printf("Found %d games\n", (int)games.size());
		for (int i = 0; i < games.size(); i++)
		{
			printf("Game %d = %s\n", i, games[i].m_filename.c_str());
		}
	}
}
void MinigameLoader::LoadMinigameAssets()
{
	GameSpecificFunctions* minigame = MinigameLoader::s_instance->m_loadedGame;
	if (minigame != nullptr)
	{
		AudioUtils::StopMusic();
		// Unload old assets
		ConfigData::Instance().UnloadAllAssets();

		// Mount the correct folder
		std::string mountFolder = GetBaseModulesFolder() + MinigameLoader::s_instance->m_loadingFileName;
		std::string originalDir = FolderUtils::BeginWorkingDirectory(mountFolder.c_str());

		// Load in game assets
		minigame->InitialiseGameSpecificData();

		FolderUtils::EndWorkingDirectory(originalDir);
	}
}
void OnMinigameLoaded(GameSpecificFunctions* minigame)
{
	MinigameLoader::s_instance->m_loadedGame = minigame;
	if (minigame != nullptr)
	{
		printf("Minigame loaded = %s\n", minigame->GetGameName());

		MinigameLoader::s_instance->LoadMinigameAssets();

		MinigameLoader::s_instance->m_loadingState = ModuleLoadState::LOADED;
	}
	else
	{
		printf("Minigame loading failed :(\n");
		MinigameLoader::s_instance->m_loadingState = ModuleLoadState::EMPTY;
	}
}

#if EMSCRIPTEN
EM_JS(void*, loadWASM, (const char* filename, int filenameLength, const char* namespaceString, int namespaceLength, bool useFilesystem),
	{
		return Asyncify.handleAsync(async() => {
			var newLib;
			try
			{
				var moduleName = UTF8ToString(filename, filenameLength);
				var namespaceName = UTF8ToString(namespaceString, namespaceLength);
				console.log('Attempting to load dynamic module: ' + moduleName +" with namespace " + namespaceName);
				var newModule;
				if (useFilesystem)
				{
					console.log('loadDynamicLibrary with filesystem');
					newModule = await loadDynamicLibrary(moduleName, { loadAsync: true, global : true, nodelete : true, fs : FS });
					//var func = dlsym(newLib, "MINIGAME_NAMESPACE::Load");
				}
				else
				{
					console.log('loadDynamicLibrary without filesystem');
					newModule = await loadDynamicLibrary(moduleName, { loadAsync: true, global : true, nodelete : true});
				}
				if (newModule)
				{
					var namespacedLoadFunction = '_' + namespaceName + '_NAMESPACE_LoadGeneral';
					console.log('NamespacedLoadFunction = ' + namespacedLoadFunction);
					console.log('newModule = ' + newModule);
					var func = Module[namespacedLoadFunction];
					console.log('Func = ' + func);
					newLib = func();
				}
				else
				{
					console.log('newModule is not a thing');
				}
				console.log('Loaded side module');
			}
			catch (error)
			{
				console.log(error);
			}
			return newLib;
		});
	});

#include <dlfcn.h>
GameSpecificFunctions* LoadByString(const char* filename, const char* namespaceName, bool useFilesystem)
{
	std::string full = std::string(filename);
	std::string namespaceString = std::string(namespaceName);

	/*void* handle = loadWASM(full.c_str(), full.length(), useFilesystem);
	if (handle == nullptr)
	{
		printf("Error, loaded handle is null :(\n");
	}
	else
	{
		printf("Handle loaded\n");
	}*/
	/*loadWASM(full.c_str(), full.length(), useFilesystem);
	module_load_function* loadFunction = (module_load_function*)dlsym(RTLD_DEFAULT, "_LoadGeneral");
	if (loadFunction == nullptr)
	{
		printf("Side module load function not found!\n");
		return nullptr;
	}*/
	//GameSpecificFunctions* minigameInterface = loadFunction();
	GameSpecificFunctions* minigameInterface = (GameSpecificFunctions*)loadWASM(full.c_str(), full.length(),
		namespaceString.c_str(), namespaceString.length(), useFilesystem);
	return minigameInterface;
}

void LoadModule(const char* filename, const char* namespaceName, bool useFilesystem)
{
	printf("Before.  Loading module %s\n", filename);
	GameSpecificFunctions* minigameInterface = LoadByString(filename, namespaceName, useFilesystem);
	printf("After.  Starting callback...\n");
	OnMinigameLoaded(minigameInterface);
}

#ifndef NODEFS
void downloadSucceeded(emscripten_fetch_t* fetch)
{
	printf("Finished downloading %llu bytes from URL %s.\n", fetch->numBytes, fetch->url);
	//const char* filename = static_cast<const char*>(fetch->userData);
	std::string filenameCopy = MinigameLoader::s_instance->m_loadingFileName + std::string(".wasm");
	printf("Download succeeded.  Filename copy = %s\n", filenameCopy.c_str());
	// fetch->data[0] through fetch->data[fetch->numBytes-1] would be available if we used EMSCRIPTEN_FETCH_LOAD_TO_MEMORY
	emscripten_fetch_close(fetch);

	LoadModule(filenameCopy.c_str(), MinigameLoader::s_instance->m_loadingFileName.c_str(), false);
}
void downloadFailed(emscripten_fetch_t* fetch)
{
	printf("Downloading %s failed, HTTP failure status code: %d.\n", fetch->url, fetch->status);
	emscripten_fetch_close(fetch);
}


extern "C" void EMSCRIPTEN_KEEPALIVE FetchModule(const char* moduleName)
{
	std::string fileWithExtension = std::string(moduleName) + std::string(".wasm");
	const char* name = fileWithExtension.c_str();

	printf("Creating fetch for %s\n", name);
	emscripten_fetch_attr_t attr;
	emscripten_fetch_attr_init(&attr);
	strcpy(attr.requestMethod, "GET");
	attr.attributes = EMSCRIPTEN_FETCH_PERSIST_FILE;
	attr.onsuccess = downloadSucceeded;
	attr.onerror = downloadFailed;

	// Doesn't seem to actually work, as it doesn't store it's own data - so we just store the filename externally, ready to deal with the callback.
	//attr.userData = (void*)name; // Give the request the module filename, so we can load it on success

	printf("Sending fetch for %s\n", name);
	emscripten_fetch(&attr, name);
	printf("Fetch sent...\n");
}
#endif

#include <iostream>
#include <fstream>
#include <zlib.h>
#include "inflate.h"
#include "untar.h"
//#include "zip.h"
#include "NetUtils.h"

int on_extract_entry(const char* filename, void* arg)
{
	static int i = 0;
	int n = *(int*)arg;
	printf("Extracted: %s (%d of %d)\n", filename, ++i, n);

	return 0;
}

#ifndef NODEFS
void assetsDownloadSucceeded(emscripten_fetch_t* fetch)
{
	printf("Assets Finished downloading %llu bytes from URL %s.\n", fetch->numBytes, fetch->url);

	EM_ASM(
		var exists = FS.analyzePath("/Modules/").exists;
		if (!exists)
		{
			FS.mkdir("/Modules/");
		}
	);
	// A hack to avoid needing to create a directory in MEMFS
	std::string filenameCopy = std::string("Modules/") + MinigameLoader::s_instance->m_loadingFileName + std::string(".tar.gz");
	printf("Assets Download succeeded.  Filename copy = %s\n", filenameCopy.c_str());

	printf("Starting unzip!\n");
	std::string destination = std::string("/Modules/") + MinigameLoader::s_instance->m_loadingFileName;
	EM_ASM(
		var destinationFolder = UTF8ToString($0, $1);
		console.log('Attempting to create folder ' + destinationFolder);
		var exists = FS.analyzePath(destinationFolder).exists;
		if (!exists)
		{
			FS.mkdir(destinationFolder);
		}
	, destination.c_str(), destination.length());

	// We have to manually write the fetch data to MEMFS, because a fetch with EMSCRIPTEN_FETCH_PERSIST_FILE will go straight to IndexedDB where zlib can't easily access it!
	{
		std::ofstream b_stream(filenameCopy.c_str(), std::fstream::out | std::fstream::binary);
		if (b_stream)
		{
			b_stream.write(fetch->data, fetch->numBytes);
			printf("Writing zip bytes to MEMFS\n");
		}
		else
		{
			printf("Failed to write zip file to MEMFS\n");
		}
	}
	emscripten_fetch_close(fetch);


	// Do the unzipping from and into memory FS
	auto file = gzopen(filenameCopy.c_str(), "rb");
	FILE* temp = std::tmpfile();

	inflate(file, temp);

	gzclose(file);
	std::rewind(temp);

	if (temp == NULL)
	{
		printf("Temp is null!\n");
	}

	untar(temp, filenameCopy.c_str(), destination.c_str());
	std::fclose(temp);

	printf("Finished unzipping %s to destination %s\n", filenameCopy.c_str(), destination.c_str());

	FetchModule(MinigameLoader::s_instance->m_loadingFileName.c_str());
}
void assetsDownloadFailed(emscripten_fetch_t* fetch)
{
	printf("Assets Downloading %s failed, HTTP failure status code: %d.\n", fetch->url, fetch->status);
	emscripten_fetch_close(fetch);
}

extern "C" void EMSCRIPTEN_KEEPALIVE FetchModuleAssets(const char* moduleName)
{
	std::string fileWithExtension = std::string("Modules/") + std::string(moduleName) + std::string(".tar.gz");
	const char* name = fileWithExtension.c_str();

	printf("Fetching assets for %s\n", name);
	emscripten_fetch_attr_t attr;
	emscripten_fetch_attr_init(&attr);
	strcpy(attr.requestMethod, "GET");
	attr.attributes = EMSCRIPTEN_FETCH_LOAD_TO_MEMORY | EMSCRIPTEN_FETCH_REPLACE; // Try and prevent browsers caching the assets zip...
	attr.onsuccess = assetsDownloadSucceeded;
	attr.onerror = assetsDownloadFailed;
	printf("Sending assets fetch for %s\n", name);
	emscripten_fetch(&attr, name);
	printf("Assets fetch sent...\n");
}
#endif

void DoLoadMinigame(const char* filenameWithoutExtension, bool headlessMode)
{
	if (headlessMode)
	{
		std::string directory = std::string("");
#ifndef NODERAWFS
		directory = std::string("working/");
#endif
		std::string full = directory + std::string(filenameWithoutExtension) + std::string(".wasm");
		// No need to fetch the code or assets, just load the module directly
		LoadModule(full.c_str(), filenameWithoutExtension, true);
	}
	else
	{
#ifndef NODEFS
		FetchModuleAssets(filenameWithoutExtension);
#endif
	}
}

// This will only work on the nodejs server...
std::vector<MinigameDescription> DoGetGamesList()
{
	std::vector<MinigameDescription> list;

	if (NetUtils::IsClient())
	{
		list.push_back(MinigameDescription{ std::string("NASR") });
		list.push_back(MinigameDescription{ std::string("Boomshakalaka") });
		list.push_back(MinigameDescription{ std::string("Llamageddon") });
		return list;
	}

	std::string directory = std::string("");
#ifndef NODERAWFS
	directory = std::string("working/");
#endif

	return FindModuleListFromDisk(directory.c_str(), "wasm");
}

#else

#ifndef DLL_MINIGAMES
void DoLoadMinigame(const char* filename, bool headlessMode)
{
	printf("Passthrough\n");
	GameSpecificFunctions* minigameInterface = LoadByString(filename, headlessMode);
	OnMinigameLoaded(minigameInterface);
}
#else
#define WIN32_LEAN_AND_MEAN
#define _AMD64_ 1
#include <libloaderapi.h>

HMODULE s_loadedDLL = nullptr;
module_load_function* s_dllLoadFunction = nullptr;

void DoLoadMinigame(const char* filenameWithoutExtension, bool headlessMode)
{
	printf("Dynamically loading...\n");
	std::string fileWithExtension = std::string(filenameWithoutExtension) + std::string(".dll");
	const char* filename = fileWithExtension.c_str();

	if (s_loadedDLL != nullptr)
	{
		printf("Freeing existing library...\n");
		FreeLibrary(s_loadedDLL);
		s_loadedDLL = nullptr;
	}

	HMODULE newModule = LoadLibraryA(filename);
	if (newModule)
	{
		s_loadedDLL = newModule;
		s_dllLoadFunction = (module_load_function*)GetProcAddress(newModule, "Load");

		GameSpecificFunctions* minigameInterface = s_dllLoadFunction(StaticStorage::GetInstance());
		OnMinigameLoaded(minigameInterface);
	}
	else
	{
		printf("Failed to load dll %s\n", filename);
	}
}
#endif
#endif

void MinigameLoader::LoadMinigame(const char* filename, bool headlessMode)
{
	s_instance = this;
	m_loadingState = ModuleLoadState::IS_LOADING;
	m_loadingFileName = std::string(filename);
	DoLoadMinigame(filename, headlessMode);
}

std::vector<MinigameDescription> MinigameLoader::GetGamesList()
{
	std::vector<MinigameDescription> allModules = DoGetGamesList();

	std::vector<MinigameDescription> allGames;
	for (MinigameDescription info : allModules)
	{
		if (strcmp(info.m_filename.c_str(), "LobbyModule") != 0
			&& strcmp(info.m_filename.c_str(), "client") != 0
			&& strcmp(info.m_filename.c_str(), "DedicatedServer") != 0
			&& strcmp(info.m_filename.c_str(), "SideModule") != 0)
		{
			/*GameSpecificFunctions* minigameInterface = LoadByString(info.m_filename.c_str(), StaticStorage::GetInstance());
			const char* fullGameName = minigameInterface->GetGameName();
			printf("Full game name for %s is %s\n", info.m_filename.c_str(), fullGameName);*/

			allGames.push_back(info);
		}
	}
	return allGames;
}