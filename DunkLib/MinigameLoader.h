#pragma once

struct GameSpecificFunctions;
#include <vector>
#include "cereal/cereal.hpp"

enum class ModuleLoadState
{
	EMPTY = 0,
	IS_LOADING = 1,
	LOADED = 2
};

struct MinigameDescription
{
	std::string m_filename;

	template<class Archive>
	void DoSerialize(Archive& archive, std::uint32_t const version)
	{
		archive(CEREAL_NVP(m_filename));
	}

	template<class Archive>
	void save(Archive& archive, std::uint32_t const version)
	{
		DoSerialize(archive, version);
	}
	template<class Archive>
	void load(Archive& archive, std::uint32_t const version)
	{
		DoSerialize(archive, version);
	}
};

class MinigameLoader
{
public:
	void Init(bool headless);

	void LoadMinigameAssets();

	GameSpecificFunctions* m_loadedGame;
	ModuleLoadState m_loadingState = ModuleLoadState::EMPTY;
	std::string m_loadingFileName = "";
	static MinigameLoader* s_instance;
	void LoadMinigame(const char* filename, bool headlessMode);

	// Only works on the server...
	std::vector<MinigameDescription> GetGamesList();
};