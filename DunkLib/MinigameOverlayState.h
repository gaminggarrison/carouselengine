#pragma once

#include "GameModeTypes.h"
struct AllTeamState;

struct MinigameOverlayState
{
	static const int WINNER_UNKNOWN = -1;
	static const int WINNER_NOBODY = -2;

	virtual float GetIntroCounter() = 0;
	virtual float GetIntroLength() = 0;
	virtual float GetRoundStartCounter() = 0;
	virtual float GetRoundStartLength() = 0;
	virtual float GetRoundEndCounter() = 0;
	virtual float GetRoundEndLength() = 0;
	virtual float GetGameEndCounter() = 0;
	virtual float GetGameEndLength() = 0;
	virtual GameModeTypeID GetGamemode() = 0;
	virtual AllTeamState& GetTeamState() = 0;
	virtual int GetRoundWinningTeam() = 0;
	virtual int GetGameWinningTeam() = 0;
	virtual int GetGameWinningScoreLimit() = 0;
	virtual int GetCurrentRoundNumber() = 0;
};