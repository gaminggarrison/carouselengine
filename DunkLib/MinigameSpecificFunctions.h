#pragma once
#include "GameSpecificFunctions.h"

struct MinigameSpecificFunctions : GameSpecificFunctions
{
	// Don't need these for minigames, so just have some default implementations
	virtual const char* GetVersion();
	virtual const char* GetGameDescription();
	virtual void StateInit(CustomFrameState& frameState);
	virtual void StartGame();
};