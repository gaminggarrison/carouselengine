#include "MusicID.h"
#include "ConfigData.h"

MusicID::MusicID() : ConfigReference()
{

}

MusicID::MusicID(const char* name) : ConfigReference(name)
{

}

MusicID::MusicID(int id) : ConfigReference(id)
{

}

ConfigSet<Music>& MusicID::GetConfigSet()
{
	return ConfigData::Instance().m_music;
}

MusicID MusicID::Unknown = MusicID();

#include "ConfigReference.cpp"
template struct ConfigReference<MusicID, Music>;