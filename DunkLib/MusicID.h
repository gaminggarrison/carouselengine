#pragma once
#include "ConfigReference.h"

struct Music;

struct MusicID : ConfigReference<MusicID, Music>
{
	MusicID();
	MusicID(const char* name);
	MusicID(int id);
	static ConfigSet<Music>& GetConfigSet();
	static MusicID Unknown;
};
