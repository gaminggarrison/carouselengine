#define NBNET_IMPL

#if defined(_WIN32) || defined(_WIN64)
#include <winsock2.h>
#endif

static const char *log_type_strings[] = {
	"ERROR",
	"INFO",
	"DEBUG",
	"TRACE"
};
enum
{
	LOG_ERROR,
	LOG_INFO,
	LOG_DEBUG,
	LOG_TRACE
};
#include <stdio.h>
//#include <stdlib.h>
#include <stdarg.h>

int s_logLevel = LOG_ERROR;

// Basic logging function
void Log(int type, const char *fmt, ...)
{
	if (type > s_logLevel)
	{
		return;
	}
	va_list args;

	va_start(args, fmt);

	printf("[%s] ", log_type_strings[type]);
	vprintf(fmt, args);
	printf("\n");

	va_end(args);
}

#define NBN_LogInfo(...) Log(LOG_INFO,  __VA_ARGS__)
#define NBN_LogError(...) Log(LOG_ERROR, __VA_ARGS__)
#define NBN_LogDebug(...) Log(LOG_DEBUG, __VA_ARGS__)
#define NBN_LogTrace(...) Log(LOG_TRACE, __VA_ARGS__)

#define NBN_Allocator malloc // nbnet allocation function
#define NBN_Deallocator free // nbnet deallocation function
#define NBN_Reallocator realloc

void SetLogLevel(int level) { s_logLevel = level; }

#include "nbnet.h"
#if defined(PLATFORM_WEB)
	#include "net_drivers/webrtc.h"
#else
#include "net_drivers/udp.h"
#endif