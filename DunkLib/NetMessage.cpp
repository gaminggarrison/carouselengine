#include "NetMessage.h"

size_t NetMessage::GetDataSize()
{
	return m_messageLength;
}
char* NetMessage::GetRawMessage()
{
	return m_message;
}
void NetMessage::SetFromPacket(char* data, size_t length)
{
	m_message = data;
	m_messageLength = length;
	m_type = m_message[0];
	m_sendingPlayer = m_message[1];
	m_recipient = m_message[2];
}
void NetMessage::ReadIntoBuffer(void* data)
{
	std::stringstream* stream = SerializationUtils::GetStringStream();
	stream->seekg(0);
	stream->read((char*)data, m_messageLength);
	stream->seekg(0);
}
char NetMessage::GetMessageType()
{
	return m_type;
}
char NetMessage::GetSendingPlayer()
{
	return m_sendingPlayer;
}
void NetMessage::SetIntendedRecipient(char recipient)
{
	m_recipient = recipient;
}
char NetMessage::GetIntendedRecipient()
{
	return m_recipient;
}