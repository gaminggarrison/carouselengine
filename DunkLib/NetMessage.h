#pragma once
#include <string>
#include <sstream>
#include "SerializationUtils.h"

class NetMessage
{
	char m_type;
	char m_sendingPlayer;
	char m_recipient;

	char* m_message; // Only for input
	size_t m_messageLength;
public:
	template<class T>
	void SetMessage(char type, char sendingPlayer, char recipient, T& object)
	{
		m_type = type;
		m_sendingPlayer = sendingPlayer;
		m_recipient = recipient;
		std::stringstream* stream = SerializationUtils::GetStringStream();
		stream->seekp(0, stream->beg);
		stream->put(m_type);
		stream->put(m_sendingPlayer);
		stream->put(m_recipient);
		stream = SerializationUtils::WriteToStream<T>(object);

		size_t length = stream->tellp();
		m_messageLength = length;
		stream->seekp(0, stream->beg);
	}

	NetMessage() {};

	template<class T>
	NetMessage(char type, char sendingPlayer, char recipient, T& object)
	{
		SetMessage<T>(type, sendingPlayer, recipient, object);
	}
	//void* GetData();
	size_t GetDataSize();
	void ReadIntoBuffer(void* data);
	void SetFromPacket(char* data, size_t length);
	char GetMessageType();
	char GetSendingPlayer();
	void SetIntendedRecipient(char recipient);
	char GetIntendedRecipient();
	char* GetRawMessage();
	template<class T> void GetMessageObject(T& target);
};

template<class T>
void NetMessage::GetMessageObject(T& target)
{
	std::stringstream* stream = SerializationUtils::GetStringStream();
	stream->seekp(0, stream->beg);
	stream->write(m_message, m_messageLength);
	stream->seekp(0, stream->beg);

	// Don't want to read from the beginning, as it will have the message type id and sendingPlayerID
	stream->seekg(3, stream->beg);
	SerializationUtils::ReadFromStream(target);
	stream->seekg(0, stream->beg);
}
