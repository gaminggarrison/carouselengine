#pragma once
#include "NetworkGameDefines.h"
#include "NetworkPlayerInfoSet.h"
#include "NetMessage.h"

namespace NetUtils
{
	enum class ConnectionMode
	{
		DISCONNECTED = 0,
		IS_SERVER = 1,
		IS_CLIENT = 2,
		IS_RELAY = 3
	};
	struct NetSession
	{
		ConnectionMode m_mode = ConnectionMode::DISCONNECTED;

		int simulatedLagMilliseconds = 0;
		float sessionTimer = 0;

		void* m_playerPeers[PLAYER_COUNT] = { nullptr };
		PlayerInfoSet m_playerInfo;

		std::function<void(PlayerInfo* playerInfo)> OnNewConnection;
		std::function<void(PlayerInfo* playerInfo)> OnDisconnection;
		std::function<void(NetMessage& message)> OnNewMessage;

		const char* m_buildVersion = "Unknown";
		int m_playerID = -3; // Uninitialised
		std::string m_playerName;
		std::string m_ipAddress;
		bool m_doesVersionDifferFromServer = false;
		NetMessage m_lastMessage;

		uint8_t m_messageBuffer[255000]; // This is the max NBNet can support ATM

		void Init();
	};
}