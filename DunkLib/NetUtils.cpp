/*#if defined(__EMSCRIPTEN__)
	#define NO_ENET
#else
	#define NO_NBNET
#endif*/
#define NO_ENET

#ifndef NO_NBNET
// Do we use HTTPS? https://github.com/nathhB/nbnet/blob/master/net_drivers/webrtc.h#L60
//#define NBN_USE_HTTPS

#include "NBNetShared.h"
#include "NBNetUtils.hpp"
#endif

#include "NetUtils.h"
#include <stdio.h>
#include "SerializationUtils.h"
#include "NetworkPlayerInfoSet.h"
#include <array>

#define PROTOCOL_NAME "carousel"
#define PROTOCOL_PORT 42042

// Through HTTPS, the non-root server will use nginx to route websocket connections onto 443 to the actual port
// The client needs to connect to 443
#ifdef NBN_USE_HTTPS
	#define CLIENT_PORT 443
#else
	#define CLIENT_PORT PROTOCOL_PORT
#endif

#include "NetSession.h"

namespace NetUtils
{
	NetSession* s_data;

	bool StartServer();
	bool StartRelay();
	double DoGetPingToServer();

	void PrepareMessage()
	{

	}

	void GeneralInit()
	{
		s_data = new NetSession();
		s_data->Init();
	}

	bool StartHost(std::function<void(PlayerInfo* playerInfo)> onNewConnectionCallback, std::function<void(PlayerInfo* playerInfo)> onDisconnectionCallback, std::function<void(NetMessage& message)> onNewMessageCallback, std::string playerName)
	{
		s_data->OnNewMessage = onNewMessageCallback;
		s_data->OnNewConnection = onNewConnectionCallback;
		s_data->OnDisconnection = onDisconnectionCallback;

		bool success = StartServer();
		if (!success)
		{
			return false;
		}

		s_data->m_playerInfo[0].m_id = 0; // Player 0 will be the server
		s_data->m_playerInfo[0].m_state = ConnectionState::Connected;
		s_data->m_playerInfo[0].m_name = playerName;
		s_data->m_playerName = playerName;
		s_data->m_playerID = 0;
		s_data->m_doesVersionDifferFromServer = false;
		return true;
	}
	bool StartDedicatedHost(std::function<void(PlayerInfo* playerInfo)> onNewConnectionCallback, std::function<void(PlayerInfo* playerInfo)> onDisconnectionCallback, std::function<void(NetMessage& message)> onNewMessageCallback, bool relay)
	{
		s_data->OnNewMessage = onNewMessageCallback;
		s_data->OnNewConnection = onNewConnectionCallback;
		s_data->OnDisconnection = onDisconnectionCallback;

		bool success;
		if (relay)
		{
			success = StartRelay();
		}
		else
		{
			success = StartServer();
		}
		if (!success)
		{
			return false;
		}
		s_data->m_playerID = -2;
		s_data->m_doesVersionDifferFromServer = false;
		return true;
	}

	std::array<bool, PLAYER_COUNT> GetConnectedPlayers()
	{
		std::array<bool, PLAYER_COUNT> connectedPlayers;

		// Always assume only the first slot is filled when we're offline
		if (!IsConnected())
		{
			for (int i = 0; i < PLAYER_COUNT; i++)
			{
				connectedPlayers[i] = (i == 0);
			}
		}
		else
		{
			for (int i = 0; i < PLAYER_COUNT; i++)
			{
				connectedPlayers[i] = s_data->m_playerInfo[i].m_state == ConnectionState::Connected;
			}
		}

		return connectedPlayers;
	}
	bool IsDedicatedHost()
	{
		return (IsHost() || IsRelay()) && s_data->m_playerID < 0;
	}
	int GetConnectedPlayerCount()
	{
		if (!IsConnected())
		{
			return 0;
		}
		int count = 0;
		for (int i = 0; i < PLAYER_COUNT; i++)
		{
			if (s_data->m_playerInfo[i].m_state != ConnectionState::Disconnected)
			{
				count++;
			}
		}
		return count;
	}

	char GetLocalPlayerID()
	{
		return s_data->m_playerID;
	}
	
	int GetMaxPlayers()
	{
		return PLAYER_COUNT;
	}
	bool IsReadyForInput()
	{
		if (IsClient())
		{
			bool isReady = s_data->m_playerID >= 0 && s_data->m_playerID < PLAYER_COUNT && s_data->m_playerInfo[s_data->m_playerID].m_state == ConnectionState::Connected;
			return isReady;
		}
		else
		{
			return !IsDedicatedHost();
		}
	}
	const PlayerInfo& GetPlayerInfo(int playerID)
	{
		return s_data->m_playerInfo[playerID];
	}
	bool IsVersionDifferentFromServer()
	{
		return s_data->m_doesVersionDifferFromServer;
	}
	void SetSimulatedLag(int milliseconds)
	{
		s_data->simulatedLagMilliseconds = milliseconds;
		if (s_data->simulatedLagMilliseconds < 0)
		{
			s_data->simulatedLagMilliseconds = 0;
		}
	}
	int GetSimulatedLag()
	{
		return s_data->simulatedLagMilliseconds;
	}
	int GetPingToServer()
	{
		if (IsMessageServer() || !IsConnected())
		{
			return 0;
		}
		else
		{
			double ping = DoGetPingToServer();
			return (int)(1000 * ping);
		}
	}
	void SendPlayerInfoSetUpdate()
	{
		NetMessage listUpdateMessage;
		PlayerInfoSet infoCopy = s_data->m_playerInfo;
		listUpdateMessage.SetMessage(-3, -1, -1, infoCopy);
		if (infoCopy[0].m_state != ConnectionState::Disconnected)
		{
			printf("Sending playing info set with player 0 being %s\n", infoCopy[0].m_name.c_str());
		}
		else
		{
			printf("Somehow the playerInfoSet is empty?\n");
		}
		SendPacketToAll(listUpdateMessage);
	}
	void SendSessionStartMessage()
	{
		NetMessage sessionMessage;
		SessionStartMessage sessionData;
		sessionData.m_sessionName = "New Session";
		sessionMessage.SetMessage(-5, s_data->m_playerID, 0, sessionData); // Use negative numbers for system messages
		SendPacketToPlayer(sessionMessage, 0);
	}
	void AddPlayer(void* peer)
	{
		int playerID = -1;
		for (int i = 0; i < PLAYER_COUNT; i++)
		{
			if (s_data->m_playerInfo[i].m_state == ConnectionState::Disconnected)
			{
				s_data->m_playerPeers[i] = peer;
				s_data->m_playerInfo[i].m_id = i;
				s_data->m_playerInfo[i].m_state = ConnectionState::ConnectedPrehandshake;
				playerID = i;
				break;
			}
		}
		if (playerID >= 0)
		{
			NetMessage idInfoMessage;
			ClientHandshakeStart idPacketData;
			idPacketData.m_newPlayerID = playerID;
			idPacketData.m_serverVersion = s_data->m_buildVersion;
			idInfoMessage.SetMessage(-1, 0, playerID, idPacketData); // Use negative numbers for system messages
			SendPacketToPlayer(idInfoMessage, playerID);

			SendPlayerInfoSetUpdate();
		}
		else
		{
			printf("Could not find free player slot!");
		}
	}
	void RemovePlayer(void* peer)
	{
		int playerID = -1;
		for (int i = 0; i < PLAYER_COUNT; i++)
		{
			if (s_data->m_playerPeers[i] == peer)
			{
				playerID = i;
				break;
			}
		}
		if (playerID < 0)
		{
			printf("Player disconnected but cannot find their playerID!\n");
			return;
		}
		s_data->m_playerPeers[playerID] = nullptr; // Null the peer early, because the data could get corrupted anyway (we don't want it being accessed)

		const PlayerInfo& playerInfo = GetPlayerInfo(playerID);
		printf("PlayerID %d named %s disconnected.\n", playerID, playerInfo.m_name.c_str());

		bool wasKnownToGameLogic = s_data->m_playerInfo[playerID].m_state == ConnectionState::Connected;
		s_data->m_playerInfo[playerID].m_state = ConnectionState::Disconnected;

		if (wasKnownToGameLogic)
		{
			s_data->OnDisconnection(&s_data->m_playerInfo[playerID]);
		}

		SendPlayerInfoSetUpdate();
	}
	void DoSetName(int playerID, const char* name)
	{
		if (playerID < 0 || playerID > PLAYER_COUNT)
		{
			printf("Cannot set name of player %d to %s\n", playerID, name);
			return;
		}
		s_data->m_playerInfo[playerID].m_name = std::string(name);

		SendPlayerInfoSetUpdate();
	}
	void SetNewName(const char* name)
	{
		if (name == nullptr || strlen(name) <= 0)
		{
			printf("Ignore new empty name\n");
			return;
		}
		int playerID = GetLocalPlayerID();

		if (IsMessageServer())
		{
			DoSetName(playerID, name);
		}
		else
		{
			PlayerNameChangeRequest nameChangeRequest;
			nameChangeRequest.m_newPlayerName = std::string(name);
			SendNetMessage(-4, -2, nameChangeRequest);
		}
	}
	// Forward declare this, since the implementation will be library specific
	void HandleServerMessageForwarding(NetMessage& message);

	void HandleNewMessage(NetMessage& message)
	{
		// Message types < 0 are system messages, and don't need forwarding
		// Recipient -2 is for the server only, -1 is for all, 0-n is for player
		if (message.GetMessageType() >= 0 && IsMessageServer() && (IsDedicatedHost() || message.GetIntendedRecipient() != s_data->m_playerID) && message.GetIntendedRecipient() > -2)
		{
			HandleServerMessageForwarding(message);
			// Don't handle the message if we only forwarded it to another player
			if (message.GetIntendedRecipient() >= 0 && message.GetIntendedRecipient() != s_data->m_playerID)
			{
				return;
			}
		}

		if (message.GetMessageType() == -1) // Telling us what playerID we have (handshake start)
		{
			ClientHandshakeStart idPacketData;
			message.GetMessageObject(idPacketData);
			printf("Server says my playerID is %d\n", idPacketData.m_newPlayerID);
			s_data->m_playerID = idPacketData.m_newPlayerID;
			std::string myVersion = std::string(s_data->m_buildVersion);
			int versionDifference = myVersion.compare(idPacketData.m_serverVersion);
			if (versionDifference != 0)
			{
				printf("Version differs from server!\n");
				// Maybe disconnect and show an error
				s_data->m_doesVersionDifferFromServer = true;
				Disconnect();
			}
			else
			{
				NetMessage responseMessage;
				ClientHandshakeResponse responseData;
				responseData.m_playerName = s_data->m_playerName;
				responseMessage.SetMessage(-2, s_data->m_playerID, 0, responseData); // Use negative numbers for system messages
				SendPacketToPlayer(responseMessage, 0); // Since we're the client, ID 0 here will just send to the message server
			}
		}
		else if (message.GetMessageType() == -2) // Client Handshake response
		{
			ClientHandshakeResponse responseData;
			message.GetMessageObject(responseData);
			std::string newPlayerName = responseData.m_playerName;
			int playerID = message.GetSendingPlayer();
			if (playerID >= 0 && playerID < PLAYER_COUNT)
			{
				s_data->m_playerInfo[playerID].m_state = ConnectionState::Connected;
				s_data->m_playerInfo[playerID].m_name = newPlayerName;
			}
			printf("Received client handshake response from playerID %d!  Welcome %s\n", playerID, newPlayerName.c_str());
			SendPlayerInfoSetUpdate();
			s_data->OnNewConnection(&s_data->m_playerInfo[playerID]); // Tell the game logic about the player when the handshake is over
		}
		else if (message.GetMessageType() == -3) // Giving us the set of currently connected players
		{
			PlayerInfoSet infoSetData;
			message.GetMessageObject(infoSetData);
			s_data->m_playerInfo = infoSetData;
			printf("Received player info set from the server!\n");
			if (s_data->m_playerID >= 0 && s_data->m_playerID < PLAYER_COUNT)
			{
				printf("Received playerInfoSet.  My id = %d, my connection state = %d, and I'm called %s\n",
					s_data->m_playerID,
					s_data->m_playerInfo[s_data->m_playerID].m_state,
					s_data->m_playerInfo[s_data->m_playerID].m_name.c_str());
			}
		}
		else if (message.GetMessageType() == -4) // Player requesting a name change
		{
			PlayerNameChangeRequest nameChangeRequest;
			message.GetMessageObject(nameChangeRequest);

			int playerID = (int)message.GetSendingPlayer();
			DoSetName(playerID, nameChangeRequest.m_newPlayerName.c_str());
		}
		else
		{
			s_data->OnNewMessage(message);
		}
	}
}

#ifndef NO_NBNET
namespace NetUtils
{
	bool Initialize(const char* buildVersion)
	{
		GeneralInit();
		s_data->m_buildVersion = buildVersion;
		SetLogLevel(LOG_ERROR);
		return true;
	}
	NetSession* GetSessionData()
	{
		return s_data;
	}
	void SetSessionData(NetSession* session)
	{
		s_data = session;
	}
	void Cleanup()
	{
		Disconnect();
	}
	
	bool IsHost()
	{
		return s_data->m_mode == ConnectionMode::IS_SERVER || s_data->m_playerID == 0;
	}
	bool IsSessionLeader()
	{
		return (s_data->m_mode == ConnectionMode::IS_SERVER || s_data->m_mode == ConnectionMode::IS_CLIENT) && s_data->m_playerID == 0;
	}
	bool IsMessageServer()
	{
		return s_data->m_mode == ConnectionMode::IS_SERVER || s_data->m_mode == ConnectionMode::IS_RELAY;
	}
	bool IsClient()
	{
		return s_data->m_mode == ConnectionMode::IS_CLIENT;
	}
	bool IsConnected()
	{
		return s_data->m_mode != ConnectionMode::DISCONNECTED;
	}
	bool IsRelay()
	{
		return s_data->m_mode == ConnectionMode::IS_RELAY;
	}
	bool StartServer()
	{
		printf("Starting NBNet server\n");
		NBN_GameServer_Init(PROTOCOL_NAME, PROTOCOL_PORT);
		NBN_LogDebug("Before Start!");
		int result = NBN_GameServer_Start();
		bool success = result >= 0;
		NBN_LogDebug("NBN_GameServer_Start result = %d, success = %s", result, result >= 0 ? "true" : "false");
		if (success)
		{
			s_data->m_mode = ConnectionMode::IS_SERVER;
		}
		else
		{
			NBN_GameServer_Deinit();
			s_data->m_mode = ConnectionMode::DISCONNECTED;
		}
		return success;
	}
	bool StartRelay()
	{
		printf("Starting NBNet server as relay\n");
		NBN_GameServer_Init(PROTOCOL_NAME, PROTOCOL_PORT);
		NBN_LogDebug("Before Start!");
		int result = NBN_GameServer_Start();
		bool success = result >= 0;
		NBN_LogDebug("NBN_GameServer_Start result = %d, success = %s", result, result >= 0 ? "true" : "false");
		if (success)
		{
			s_data->m_mode = ConnectionMode::IS_RELAY;
		}
		else
		{
			NBN_GameServer_Deinit();
			s_data->m_mode = ConnectionMode::DISCONNECTED;
		}
		return success;
	}

	bool StartClient(const char* ipAddress, std::function<void(NetMessage& message)> onNewMessageCallback, std::string playerName)
	{
		s_data->OnNewMessage = onNewMessageCallback;
		s_data->m_playerName = playerName;
		s_data->m_ipAddress = std::string(ipAddress);

		s_data->m_doesVersionDifferFromServer = false;

		NBN_GameClient_Init(PROTOCOL_NAME, s_data->m_ipAddress.c_str(), CLIENT_PORT);

		bool success = NBN_GameClient_Start() >= 0;
		if (success)
		{
			s_data->m_mode = ConnectionMode::IS_CLIENT;
		}
		else
		{
			NBN_GameClient_Deinit();
			s_data->m_mode = ConnectionMode::DISCONNECTED;
		}
		return success;
	}

	bool Disconnect()
	{
		if (IsConnected())
		{
			s_data->m_playerID = -3;
			if (IsMessageServer())
			{
				NBN_GameServer_Stop();
				NBN_GameServer_Deinit();
			}
			else if (IsClient())
			{
				NBN_GameClient_Stop();
				NBN_GameClient_Deinit();
			}
			return true;
		}
		return false;
	}

	void HandleNewConnection()
	{
		if (GetConnectedPlayerCount() < GetMaxPlayers())
		{
			NBN_Connection* connection = NBN_GameServer_GetIncomingConnection();
			NBN_GameServer_AcceptIncomingConnection();

			printf("A new client connected!\n");

			AddPlayer(connection);
		}
		else
		{
			NBN_GameServer_RejectIncomingConnectionWithCode(SERVER_FULL_CODE);
		}
	}
	void HandleClientDisconnection()
	{
		NBN_Connection* client = NBN_GameServer_GetDisconnectedClient();
		if (client == nullptr)
		{
			printf("HandleClientDisconnection has a disconnecting client who is already null!\n");
		}
		RemovePlayer(client);
	}

	void Poll(float deltaTime)
	{
		if (!IsConnected())
		{
			return;
		}
		if (IsMessageServer())
		{
			NBN_GameServer_AddTime((double)deltaTime);

			int _event;
			while ((_event = NBN_GameServer_Poll()) != NBN_NO_EVENT)
			{
				NBN_LogInfo("Event of type %d", _event);
				if (_event < 0)
				{
					Log(LOG_ERROR, "Something went wrong");
					break;
				}

				switch (_event)
				{
				case NBN_NEW_CONNECTION:
					HandleNewConnection();
					break;
				case NBN_CLIENT_DISCONNECTED:
					HandleClientDisconnection();
					break;
				case NBN_CLIENT_MESSAGE_RECEIVED:
					NBN_MessageInfo messageInfo = NBN_GameServer_GetMessageInfo();
					assert(messageInfo.type == NBN_BYTE_ARRAY_MESSAGE_TYPE);

					NBN_ByteArrayMessage* msg = (NBN_ByteArrayMessage*)messageInfo.data;
					s_data->m_lastMessage.SetFromPacket((char*)msg->bytes, msg->length);
					HandleNewMessage(s_data->m_lastMessage);
					NBN_ByteArrayMessage_Destroy(msg);
					break;
				}
			}

			if (NBN_GameServer_SendPackets() < 0)
			{
				Log(LOG_ERROR, "Server failed to send packets!");
			}
		}
		else if (IsClient())
		{
			NBN_GameClient_AddTime((double)deltaTime);
			int _event;
			while ((_event = NBN_GameClient_Poll()) != NBN_NO_EVENT)
			{
				if (_event < 0)
				{
					Log(LOG_ERROR, "An error occurred while polling client events.  Exit");
					break;
				}

				switch (_event)
				{
				case NBN_CONNECTED:
					printf("Connected!\n");
					break;

				case NBN_DISCONNECTED:
					printf("Disconnected!\n");
					s_data->m_mode = ConnectionMode::DISCONNECTED;
					break;

				case NBN_MESSAGE_RECEIVED:
					NBN_MessageInfo messageInfo = NBN_GameClient_GetMessageInfo();
					assert(messageInfo.type == NBN_BYTE_ARRAY_MESSAGE_TYPE);

					NBN_ByteArrayMessage* msg = (NBN_ByteArrayMessage*)messageInfo.data;
					s_data->m_lastMessage.SetFromPacket((char*)msg->bytes, msg->length);
					HandleNewMessage(s_data->m_lastMessage);
					NBN_ByteArrayMessage_Destroy(msg);
					break;
				}
			}

			if (NBN_GameClient_SendPackets() < 0)
			{
				Log(LOG_ERROR, "Client failed to send packets!");
			}
		}

		s_data->sessionTimer += deltaTime;
	}

	void SendPacketToAll(NetMessage& message)
	{
		if (IsClient())
		{
			message.ReadIntoBuffer(s_data->m_messageBuffer);
			NBN_OutgoingMessage* outgoing = NBN_GameClient_CreateByteArrayMessage(s_data->m_messageBuffer, (unsigned int)message.GetDataSize());
			assert(outgoing);
			int result = NBN_GameClient_SendReliableMessage(outgoing);
			if (result < 0)
			{
				Log(LOG_ERROR, "Failed to send broadcast packet of size %d", message.GetDataSize());
			}
		}
		else if (IsHost() || IsRelay())
		{
			bool willSendSomething = false;
			for (int i = 0; i < PLAYER_COUNT; i++)
			{
				if (i != s_data->m_playerID && s_data->m_playerInfo[i].m_state != ConnectionState::Disconnected && s_data->m_playerPeers[i] != nullptr)
				{
					NBN_Connection* peer = (NBN_Connection*)s_data->m_playerPeers[i];
					if (!peer->is_closed)
					{
						willSendSomething = true;
						break;
					}
				}
			}
			if (willSendSomething)
			{
				message.ReadIntoBuffer(s_data->m_messageBuffer);
				NBN_OutgoingMessage* outgoing = NBN_GameServer_CreateByteArrayMessage(s_data->m_messageBuffer, (unsigned int)message.GetDataSize());
				assert(outgoing);

				for (int i = 0; i < PLAYER_COUNT; i++)
				{
					if (i != s_data->m_playerID && s_data->m_playerInfo[i].m_state != ConnectionState::Disconnected && s_data->m_playerPeers[i] != nullptr)
					{
						NBN_Connection* peer = (NBN_Connection*)s_data->m_playerPeers[i];
						if (!peer->is_closed)
						{
							int result = NBN_GameServer_SendReliableMessageTo(peer, outgoing);
							if (result < 0)
							{
								Log(LOG_ERROR, "Could not send packet of size %d to playerID %d", message.GetDataSize(), i);
							}
						}
					}
				}
			}

			/*int result = NBN_GameServer_BroadcastReliableMessage(outgoing);
			if (result < 0)
			{
				Log(LOG_ERROR, "Could not send broadcast packet of size %d", message.GetDataSize());
			}*/
		}
	}
	void SendPacketToPlayer(NetMessage& message, int playerID)
	{
		if (playerID == -2 && IsMessageServer())
		{
			printf("We don't want to send the message to the host when we're the host!\n");
			return;
		}
		if (playerID == -1)
		{
			SendPacketToAll(message);
			return;
		}
		if (IsClient())
		{
			message.ReadIntoBuffer(s_data->m_messageBuffer);
			unsigned int messageSize = message.GetDataSize();
			NBN_OutgoingMessage* outgoing = NBN_GameClient_CreateByteArrayMessage(s_data->m_messageBuffer, messageSize);
			assert(outgoing);
			int result = NBN_GameClient_SendReliableMessage(outgoing);
			if (result < 0)
			{
				Log(LOG_ERROR, "Failed to send packet of size %d to player %d", message.GetDataSize(), playerID);
			}
		}
		else if (IsMessageServer())
		{
			NBN_Connection* playerPeer = ((NBN_Connection*)s_data->m_playerPeers[playerID]);
			if (playerPeer == nullptr || playerPeer->is_closed)
			{
				Log(LOG_INFO, "Could not send packet of size %d to playerID %d because their connection is closed", message.GetDataSize(), playerID);
				return;
			}
			message.ReadIntoBuffer(s_data->m_messageBuffer);
			unsigned int messageSize = message.GetDataSize();
			NBN_OutgoingMessage* outgoing = NBN_GameServer_CreateByteArrayMessage(s_data->m_messageBuffer, messageSize);
			assert(outgoing);
			int result = NBN_GameServer_SendReliableMessageTo((NBN_Connection*)s_data->m_playerPeers[playerID], outgoing);
			if (result < 0)
			{
				Log(LOG_ERROR, "Could not send packet of size %d to playerID %d", message.GetDataSize(), playerID);
			}
		}
	}
	void HandleServerMessageForwarding(NetMessage& message)
	{
		int intended = message.GetIntendedRecipient();
		// Message forwarding
		// Bounce message
		if (intended == -1) // For everyone
		{
			bool willSendSomething = false;
			for (int i = 0; i < PLAYER_COUNT; i++)
			{
				if (i != s_data->m_playerID && i != message.GetSendingPlayer() && s_data->m_playerInfo[i].m_state != ConnectionState::Disconnected && s_data->m_playerPeers[i] != nullptr)
				{
					NBN_Connection* peer = (NBN_Connection*)s_data->m_playerPeers[i];
					if (!peer->is_closed)
					{
						willSendSomething = true;
						break;
					}
				}
			}
			if (willSendSomething)
			{
				NBN_OutgoingMessage* outgoing = NBN_GameServer_CreateByteArrayMessage((uint8_t*)message.GetRawMessage(), (unsigned int)message.GetDataSize());
				assert(outgoing);

				for (int i = 0; i < PLAYER_COUNT; i++)
				{
					if (i != s_data->m_playerID && i != message.GetSendingPlayer() && s_data->m_playerInfo[i].m_state != ConnectionState::Disconnected && s_data->m_playerPeers[i] != nullptr)
					{
						NBN_Connection* peer = (NBN_Connection*)s_data->m_playerPeers[i];
						if (!peer->is_closed)
						{
							NBN_GameServer_SendReliableMessageTo(peer, outgoing);
						}
					}
				}
			}
		}
		else // For specific peer
		{
			NBN_Connection* peer = (NBN_Connection*)s_data->m_playerPeers[intended];
			if (peer == nullptr)
			{
				printf("Failed to find peer with id %d\n", intended);
				return;
			}
			NBN_OutgoingMessage* outgoing = NBN_GameServer_CreateByteArrayMessage((uint8_t*)message.GetRawMessage(), (unsigned int)message.GetDataSize());
			assert(outgoing);
			int result = NBN_GameServer_SendReliableMessageTo(peer, outgoing);
			if (result < 0)
			{
				Log(LOG_ERROR, "Could not forward packet of size %d to playerID %d", message.GetDataSize(), intended);
			}
		}
	}

	double DoGetPingToServer()
	{
		return NBN_GameClient_GetStats().ping;
	}
}

#elif !defined(NO_ENET)
#include <enet/enet.h>
//#include "StunClient.h"

namespace NetUtils
{
	struct StoredEvent
	{
		ENetPacket* packet;
		float m_time;
		int m_targetPlayer;
	};

	ENetHost* server;
	ENetHost* client;
	//ENetHost* toStun;

	// For simulating fake lag
	std::vector<StoredEvent> m_storedEvents = std::vector<StoredEvent>();

	ENetHost* GetENetHost()
	{
		ENetHost* host;
		if (server != nullptr)
		{
			host = server;
		}
		else
		{
			host = client;
		}
		return host;
	}

	bool Initialize(const char* buildVersion)
	{
		GeneralInit();
		m_buildVersion = buildVersion;
		return enet_initialize() == 0;
	}
	void Cleanup()
	{
		Disconnect();
		enet_deinitialize();
	}
	
	bool StartServer()
	{
		/*StunClient::SetStunAddressAndPort("stun.l.google.com", 19302);
		std::pair<std::string, uint16_t> endpointInfo = StunClient::FindStunEndpointHost(1234);
		printf("STUN endpoint info for host %s:%d\n", endpointInfo.first.c_str(), endpointInfo.second);*/

		ENetAddress address;

		// Bind to localhost
		address.host = ENET_HOST_ANY;
		address.port = PROTOCOL_PORT;
		server = enet_host_create(&address, PLAYER_COUNT, 2, 0, 0);

		if (server == nullptr)
		{
			fprintf(stderr, "An error occurred while trying to create an ENet server host.\n");
			server = nullptr;
			return false;
		}

		//ConnectToSTUNServer("stun.l.google.com", 19302);

		return true;
	}
	bool StartClient(const char* ipAddress, std::function<void(NetMessage& message)> onNewMessageCallback, std::string playerName)
	{
		OnNewMessage = onNewMessageCallback;
		m_playerName = playerName;
		m_ipAddress = ipAddress;

		client = enet_host_create(NULL, 1, 2, 0, 0);
		if (client == nullptr)
		{
			fprintf(stderr, "An error occurred while trying to create an ENet client host.\n");
			return false;
		}
		ENetAddress address;

		// Attempt to resolve to target
		int result = enet_address_set_host(&address, m_ipAddress.c_str());
		if (result != 0)
		{
			fprintf(stderr, "Error resolving ipaddress %s.\n", m_ipAddress.c_str());
			client = nullptr;
			return false;
		}
		address.port = PROTOCOL_PORT;

		ENetPeer* peer = enet_host_connect(client, &address, 2, 0);
		if (peer == nullptr)
		{
			fprintf(stderr, "No available peers for connecting to %s.\n", m_ipAddress.c_str());
			client = nullptr;
			return false;
		}

		ENetEvent event;
		// Wait some time for the connection attempt to succeed
		if (enet_host_service(client, &event, 1000) > 0 && event.type == ENET_EVENT_TYPE_CONNECT)
		{
			printf("Connection to %s:%u succeeded!\n", m_ipAddress.c_str(), address.port);
		}
		else
		{
			// Either the timeout is up or a disconnect event was receieved.  Reset the peer in the event we timed out without any events.
			enet_peer_reset(peer);
			printf("Connection to %s:%u failed :(\n", m_ipAddress.c_str(), address.port);
			client = nullptr;
			return false;
		}
		m_doesVersionDifferFromServer = false;
		return true;
	}

	bool Disconnect()
	{
		if (IsConnected())
		{
			if (server != nullptr)
			{
				enet_host_destroy(server);
				server = nullptr;
			}
			if (client != nullptr)
			{
				enet_peer_disconnect_now(&client->peers[0], 0);
				enet_host_destroy(client);
				client = nullptr;
			}
			m_playerID = 0;
			return true;
		}
		return false;
	}
	bool IsHost()
	{
		return server != nullptr;
	}
	bool IsClient()
	{
		return client != nullptr;
	}
	bool IsConnected()
	{
		ENetHost* host = GetENetHost();
		if (host == nullptr)
		{
			return false;
		}
		return true;
	}

	
	void HandleServerMessageForwarding(NetMessage& message)
	{
		int intended = message.GetIntendedRecipient();
		// Message forwarding
		// Bounce message
		if (intended == -1) // For everyone
		{
			bool sentSomething = false;
			for (int i = 1; i < PLAYER_COUNT; i++)
			{
				if (i != message.GetSendingPlayer() && m_playerInfo[i].m_state != ConnectionState::Disconnected && m_playerPeers[i] != nullptr)
				{
					ENetPacket* packet = enet_packet_create(message.GetRawMessage(), message.GetDataSize(), ENET_PACKET_FLAG_RELIABLE);

					ENetPeer* peer = (ENetPeer*)m_playerPeers[i];

					if (peer == nullptr)
					{
						printf("Failed to find peer with id %d\n", i);
					}
					else
					{
						enet_peer_send(peer, 0, packet);
						sentSomething = true;
					}
				}
			}
			if (sentSomething)
			{
				enet_host_flush(server);
			}
		}
		else // For specific peer
		{
			ENetPeer* peer = (ENetPeer*)m_playerPeers[intended];
			if (peer == nullptr)
			{
				printf("Failed to find peer with id %d\n", intended);
				return;
			}
			ENetPacket* packet = enet_packet_create(message.GetRawMessage(), message.GetDataSize(), ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(peer, 0, packet);
			enet_host_flush(server);
		}
	}

	
	void HandleNewConnection(ENetEvent& event)
	{
		printf("A new client connected from %x:%u.\n",
			event.peer->address.host,
			event.peer->address.port);

		AddPlayer(event.peer);
	}
	void HandleDisconnection(ENetEvent& event)
	{
		if (IsHost())
		{
			RemovePlayer(event.peer);
		}
		// Reset the peer's client information
		event.peer->data = NULL;
	}

	void HandlePacketToAll(ENetPacket* packet)
	{
		ENetHost* host = GetENetHost();
		enet_host_broadcast(host, 0, packet);
		enet_host_flush(host);
	}
	void SendPacketToAll(NetMessage& message)
	{
		ENetHost* host = GetENetHost();
		if (host == nullptr)
		{
			//printf("Trying to send a packet when host is null\n");
			return;
		}
		//printf("Broadcasting packet!\n");

		assert(message.GetIntendedRecipient() == -1);

		ENetPacket* packet = enet_packet_create(nullptr, message.GetDataSize(), ENET_PACKET_FLAG_RELIABLE);
		message.ReadIntoBuffer(packet->data);
		if (simulatedLagMilliseconds > 0)
		{
			m_storedEvents.push_back({ packet, sessionTimer, -1 });
		}
		else
		{
			HandlePacketToAll(packet);
		}
	}
	void HandlePacketToPlayer(ENetPacket* packet, int playerID)
	{
		ENetHost* host = GetENetHost();
		if (host == nullptr)
		{
			printf("Host is null in HandlePacketToPlayer %d", playerID);
			return;
		}
		if (playerID == -1)
		{
			HandlePacketToAll(packet);
			return;
		}
		if (IsClient())
		{
			// Send it to the host who will bounce it to the specific player
			enet_host_broadcast(host, 0, packet);
			enet_host_flush(host);
		}
		else
		{
			ENetPeer* peer = (ENetPeer*)m_playerPeers[playerID];

			if (peer == nullptr)
			{
				printf("Failed to find peer with id %d\n", playerID);
				return;
			}
			enet_peer_send(peer, 0, packet);
			enet_host_flush(host);
		}
	}
	void SendPacketToPlayer(NetMessage& message, int playerID)
	{
		// If we're on the host, we just send to that peer directly
		// otherwise might need to bounce...
		ENetHost* host = GetENetHost();
		if (host == nullptr)
		{
			//printf("Trying to send a packet when host is null\n");
			return;
		}
		//printf("Sending packet to %d\n", playerID);
		assert(message.GetIntendedRecipient() == playerID);

		ENetPacket* packet = enet_packet_create(nullptr, message.GetDataSize(), ENET_PACKET_FLAG_RELIABLE);
		message.ReadIntoBuffer(packet->data);

		if (simulatedLagMilliseconds > 0)
		{
			m_storedEvents.push_back({ packet, sessionTimer, playerID });
		}
		else
		{
			HandlePacketToPlayer(packet, playerID);
		}
	}

	void SendStoredEvents()
	{
		for (int i = (int)m_storedEvents.size() - 1; i >= 0; i--)
		{
			StoredEvent& packet = m_storedEvents[i];
			if (sessionTimer > packet.m_time + (simulatedLagMilliseconds * 0.001f))
			{
				HandlePacketToPlayer(packet.packet, packet.m_targetPlayer);
				m_storedEvents.erase(m_storedEvents.begin() + i);
			}
		}
	}
	void Poll(float deltaTime)
	{
		ENetHost* host = GetENetHost();
		if (host == nullptr)
		{
			return;
		}

		ENetEvent event;

		while (enet_host_service(host, &event, 0) > 0)
		{
			switch (event.type)
			{
			case ENET_EVENT_TYPE_CONNECT:
				HandleNewConnection(event);
				break;

			case ENET_EVENT_TYPE_RECEIVE:
				m_lastMessage.SetFromPacket((char*)event.packet->data, event.packet->dataLength);
				HandleNewMessage(m_lastMessage);
				// Clean up the packet now that we're done using it
				//enet_packet_destroy(event.packet);
				break;

			case ENET_EVENT_TYPE_DISCONNECT:
				HandleDisconnection(event);
				break;

			default:
				printf("We got an event of type %u\n", event.type);
				break;
			}
		}
		sessionTimer += deltaTime;
		SendStoredEvents();
	}
	double DoGetPingToServer()
	{
		return 0;
	}
}
#else
namespace NetUtils
{
	bool Initialize(const char* buildVersion)
	{
		GeneralInit();
		m_buildVersion = buildVersion;
		return true;
	}
	void Cleanup()
	{

	}
	bool StartServer()
	{
		return true;
	}
	bool IsHost()
	{
		return true;
	}
	bool IsClient()
	{
		return false;
	}
	bool IsConnected()
	{
		return true;
	}

	bool StartClient(const char* ipAddress, std::function<void(NetMessage& message)> onNewMessageCallback, std::string playerName)
	{
		OnNewMessage = onNewMessageCallback;
		m_playerName = playerName;
		m_ipAddress = ipAddress;

		m_doesVersionDifferFromServer = false;
		return true;
	}

	bool Disconnect()
	{
		if (IsConnected())
		{
			m_playerID = 0;
			return true;
		}
		return false;
	}

	void Poll(float deltaTime)
	{

	}

	void SendPacketToAll(NetMessage& message)
	{

	}
	void SendPacketToPlayer(NetMessage& message, int playerID)
	{

	}
}
#endif