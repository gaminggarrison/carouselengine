#pragma once
#include <functional>
#include "NetMessage.h"
#include "NetworkGameDefines.h"
#include <array>
#include "NetMessage.h"

namespace NetUtils
{
	struct NetSession;

	constexpr int TO_HOST = -2;
	constexpr int TO_ALL = -1;
	struct PlayerInfo;

	// Returns true on success
	bool Initialize(const char* buildVersion);
	NetSession* GetSessionData(); // For handling code reload
	void SetSessionData(NetSession* session); // For handling code reload

	void Cleanup();

	// Returns true on success
	bool StartHost(std::function<void(PlayerInfo* playerInfo)> onNewConnectionCallback, std::function<void(PlayerInfo* playerInfo)> onDisconnectionCallback, std::function<void(NetMessage& message)> onNewMessageCallback, std::string playerName);
	bool StartDedicatedHost(std::function<void(PlayerInfo* playerInfo)> onNewConnectionCallback, std::function<void(PlayerInfo* playerInfo)> onDisconnectionCallback, std::function<void(NetMessage& message)> onNewMessageCallback, bool relay);
	// Blocks until connected.  Returns true on success.
	bool StartClient(const char* ipAddress, std::function<void(NetMessage& message)> onNewMessageCallback, std::string playerName);
	bool Disconnect();

	void Poll(float deltaTime);

	void SetNewName(const char* name);
	int GetMaxPlayers();
	std::array<bool, PLAYER_COUNT> GetConnectedPlayers();
	int GetConnectedPlayerCount();
	const PlayerInfo& GetPlayerInfo(int player);
	bool IsReadyForInput();
	bool IsHost();
	bool IsSessionLeader();
	bool IsMessageServer();
	bool IsDedicatedHost();
	bool IsClient();
	bool IsConnected();
	bool IsRelay();
	bool IsVersionDifferentFromServer();
	char GetLocalPlayerID();
	void SetSimulatedLag(int milliseconds);
	int GetSimulatedLag();
	int GetPingToServer();

	void SendPacketToAll(NetMessage& message);
	void SendPacketToPlayer(NetMessage& message, int playerID);
	template<class T>
	void SendNetMessage(char type, char recipient, T& object)
	{
		NetMessage m = NetMessage(type, NetUtils::GetLocalPlayerID(), recipient, object);
		SendPacketToPlayer(m, recipient);
	}
}
