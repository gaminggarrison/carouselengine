#include "NetworkEvent.h"
#include "NetworkStateControl.h"
#include "NetUtils.h"

int NetworkEvent::s_nextEventID = 0;

void NetworkEvent::InitDefault()
{
	m_playerID = NetUtils::GetLocalPlayerID();
	m_playerEventID = NetworkEvent::GetNextEventID();
}

NetworkEvent::~NetworkEvent()
{

}

int NetworkEvent::GetNextEventID()
{
	return s_nextEventID++;
}
Color NetworkEvent::GetDebugColour()
{
	return Color{ 255,0,0,255 };
}
int NetworkEvent::GetDebugPriority()
{
	return 10;
}
bool NetworkEvent::DoRewindForEvent()
{
	return true;
}
void NetworkEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{

}