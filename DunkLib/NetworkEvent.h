#pragma once

#include "cereal/types/polymorphic.hpp"
#include "cereal/archives/binary.hpp"
#include "raylib.h"

class NetworkFrame;
class NetworkStateControl;
class FrameState;

struct NetworkEvent
{
	void InitDefault();
	virtual ~NetworkEvent();
	virtual void RunEvent(FrameState& frame) = 0;
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();
	virtual bool DoRewindForEvent();
	int m_playerID = -1;
	int m_playerEventID = -1;

	static int s_nextEventID;
	static int GetNextEventID();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(m_playerID, m_playerEventID);
	}
};
