#pragma once
#include "NetworkEvent.h"
#include <memory>

struct NetworkEventWrapper
{
	int m_frame;
	std::shared_ptr<NetworkEvent> m_event;

	NetworkEventWrapper();
	NetworkEventWrapper(int frame, NetworkEvent* newEvent);

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version) // Can be used in the replay system, so let's keep a version
	{
		archive(m_frame, m_event);
	}
};
