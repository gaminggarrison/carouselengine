#include "NetworkFrame.h"
#include "StaticStorage.h"

#include "NetUtils.h"
#include "CameraUtils.h"

NetworkFrame::NetworkFrame() : m_frameState()
{

}

void NetworkFrame::AddNetworkEvent(std::shared_ptr<NetworkEvent> newEvent)
{
	m_networkEvents.push_back(newEvent);
}
void NetworkFrame::ApplyEvents(FrameState& frameState, bool updateLocalClientPrediction)
{
	CustomFrameStateWrapper* game = m_frameState.GetData();
	game->ResetOneFrameControls();
	for (int i = 0; i < m_networkEvents.size(); i++)
	{
		m_networkEvents[i]->RunEvent(frameState);
	}

	game->UpdateClientSideControlPredictions();
	if (updateLocalClientPrediction)
	{
		Vector2 worldMousePos = CameraUtils::ScreenToWorldPos(GetMousePosition(), false);
		game->UpdateLocalClientPrediction(NetUtils::GetLocalPlayerID(), worldMousePos);
	}
}
void NetworkFrame::ClearPlayerEvent(int playerID, int playerEventID)
{
	for (int i = (int)m_networkEvents.size() - 1; i >= 0; i--)
	{
		if (m_networkEvents[i]->m_playerID == playerID && m_networkEvents[i]->m_playerEventID == playerEventID)
		{
			m_networkEvents.erase(m_networkEvents.begin() + i);
		}
	}
}
void NetworkFrame::Clear()
{
	m_networkEvents.clear();
}

std::vector<std::shared_ptr<NetworkEvent>>& NetworkFrame::GetEvents()
{
	return m_networkEvents;
}
