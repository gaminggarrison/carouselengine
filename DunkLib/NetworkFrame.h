#pragma once
#include <vector>
#include <memory>
#include "NetworkEvent.h"
#include "FrameState.h"

class NetworkFrame
{
	std::vector<std::shared_ptr<NetworkEvent>> m_networkEvents = std::vector<std::shared_ptr<NetworkEvent>>();
public:
	FrameState m_frameState;
	NetworkFrame();
	void AddNetworkEvent(std::shared_ptr<NetworkEvent> newEvent);

	// Applies all the events of this frame to it's state (ready for Update)
	void ApplyEvents(FrameState& frameState, bool updateLocalClientPrediction);
	void Clear();
	void ClearPlayerEvent(int playerID, int playerEventID);
	std::vector<std::shared_ptr<NetworkEvent>>& GetEvents();
};
