#include "NetworkGameState.h"
#include "NetUtils.h"
#include "NetworkPlayerInfo.h"

#include <functional>
#include "DebugUtils.h"
#include "CRCCheckEvent.h"
#include "PlayerJoinedEvent.h"
#include "PlayerLeftEvent.h"
#include "PlayerControlCancelEvent.h"
#include "CarouselSetup.h"
#include "GameSpecificFunctions.h"
#include "EngineState.h"
#include "GameSwitchEvents.h"
#include "MinigameLoader.h"
#include "CustomFrameStateWrapper.h"
#include "ClockUtils.h"
#include "VectorUtils.h"
#include "CustomClientGame.h"
#include "DrawUtils.h"
#include "GUIUtils.h"
#include "UIManager.h"

using namespace std::placeholders;

NetworkGameState::~NetworkGameState()
{

}

void NetworkGameState::Init()
{
	m_localPlayerControl = CarouselSetup::GetGameFunctions()->CreateCustomControlStateOnHeap();
	if (m_runSimulation)
	{
		m_commonGame.Init(false);
		SetupHeadFrame();
	}

	const char* buildVersion = CarouselSetup::GetGameFunctions()->GetVersion();
	bool initialised = NetUtils::Initialize(buildVersion);
	m_netSession = NetUtils::GetSessionData();

	if (initialised)
	{
		// Just always record a replay?
		if (m_runSimulation)
		{
			m_commonGame.GetBlackBoxSystem().StartRecording(m_commonGame);
		}
		cout << "Network initialized\n";
		if (!EngineState::s_engineState->IsHeadless() && DebugUtils::s_debugInstance == 0)
		{
			cout << "Starting host!\n";
			NetUtils::StartHost(std::bind(&NetworkGameState::OnClientConnected, this, _1), std::bind(&NetworkGameState::OnClientDisconnected, this, _1), std::bind(&NetworkGameState::OnMessageReceived, this, _1), "Host");
		}
	}
}
void NetworkGameState::HandleReload()
{
	NetUtils::SetSessionData(m_netSession);
	if (m_customClientGame != nullptr)
	{
		MinigameLoader::s_instance->LoadMinigameAssets();
	}
}
void NetworkGameState::SetupHeadFrame()
{
	// Make sure we load the first frame, otherwise we could accidentily save some garbage as a frame!
	m_commonGame.GetHeadFrameState().LoadFrameState();
	m_commonGame.GetHeadFrameState().GetData()->GetRandomGenerator().SetAsSharedInstance();
	// Either our child class or the carousel setup functions can configure the starting frame state
	GameInit();
	CarouselSetup::GetGameFunctions()->StateInit(*m_commonGame.GetHeadWrapper().GetState<CustomFrameState>());

	CustomFrameStateWrapper& headWrapper = m_commonGame.GetHeadWrapper();
	CustomFrameState* headState = headWrapper.GetState<CustomFrameState>();
	if (!EngineState::s_engineState->IsHeadless())
	{
		headState->AddPlayer(0);
	}
	headState->SetupGame();
	m_commonGame.GetHeadFrameState().SaveFrameState(); // Save the result from the game initialisation
}
void NetworkGameState::Cleanup()
{
	NetUtils::Cleanup();
}
void NetworkGameState::Update(float deltaTime)
{
	NetUtils::Poll(deltaTime);
	if (!m_runSimulation)
	{
		return;
	}
	if (UpdateGameSwitching())
	{
		//m_commonGame.GetSyncedClock().Update(deltaTime, m_commonGame);
		return;
	}
	if (DebugUtils::s_debugInstance > 0 && !NetUtils::IsConnected() && !m_hasTriedConnecting) // Keep trying to connect
	{
		bool connected = NetUtils::StartClient("127.0.0.1", std::bind(&NetworkGameState::OnMessageReceived, this, _1), "Client");
		if (connected)
		{
			m_hasTriedConnecting = true;
		}
	}

	// Update client controls
	if (!NetUtils::IsDedicatedHost() && m_commonGame.IsReadyForInput())
	{
		HandleInput();
		if (m_customClientGame != nullptr)
		{
			BasePlayerControlState& localControlState = *m_localPlayerControl.get();
			m_customClientGame->HandleInput(localControlState, m_commonGame);
		}
	}

	// Then update game
	m_commonGame.Update(deltaTime);
	HandleCRCChecks(deltaTime);
	if (m_customClientGame != nullptr)
	{
		std::string newGameName = m_customClientGame->LateUpdate(m_commonGame, *this, deltaTime);
		if (!newGameName.empty())
		{
			StartGameSwitchForRoom(newGameName.c_str());
		}
	}
}
void NetworkGameState::Draw()
{
	if (m_customClientGame != nullptr && m_gameSwitchingState == GameSwitchingState::NOT_SWITCHING)
	{
		m_customClientGame->Draw(m_commonGame);
		m_customClientGame->DrawOverlay(m_commonGame);
	}
	DrawDebug();

	if (!DrawUtils::IsInitialised())
	{
		return;
	}
	if (m_gameSwitchingState == GameSwitchingState::WAITING_FOR_READY)
	{
		GUIUtils::DrawText("Waiting for ready messages", GetScreenWidth() / 2, GetScreenHeight() / 2, 24, WHITE);
	}
	else if (m_gameSwitchingState == GameSwitchingState::LOADING)
	{
		float timeInSecond = fmodf((float)GetTime(), 1.0f);
		const char* text = "Loading.";
		if (timeInSecond > 0.33)
		{
			text = "Loading..";
		}
		else if (timeInSecond > 0.66)
		{
			text = "Loading...";
		}
		GUIUtils::DrawText(text, GetScreenWidth() / 2, GetScreenHeight() / 2, 24, WHITE);
	}
}
void NetworkGameState::DrawDebug()
{
	UIManager::StartUI();
	if (DebugUtils::s_debugOn)
	{
		m_commonGame.DrawDebugUI();
	}
	m_commonGame.DrawWarnings();
	UIManager::EndUI();
}

void NetworkGameState::HandleCRCChecks(float deltaTime)
{
	if ((!DebugUtils::s_debugPause) && NetUtils::IsHost())
	{
		m_crcCheckCounter += deltaTime;
		if (m_crcCheckCounter >= m_crcCheckGap)
		{
			m_crcCheckCounter = 0.0f;
			NetworkStateControl& commonGame = m_commonGame;
			int frameToCRC = (commonGame.GetNewestHistoryFrame() + commonGame.GetOldestHistoryFrame()) / 2;
			uint32_t crc = commonGame.GenerateCRCForFrame(frameToCRC);
			if (crc != 0)
			{
				CRCCheckEvent* crcEvent = new CRCCheckEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(), crc);
				commonGame.SendNetworkEventOnly(crcEvent, frameToCRC);
			}
		}
	}
}

void NetworkGameState::OnClientConnected(NetUtils::PlayerInfo* playerInfo)
{
	printf("Client connected with id %d\n", playerInfo->m_id);

	if (NetUtils::IsRelay())
	{
		// Maybe we want to do game changes through the Relay so they can direct players to the correct minigame
		CarouselSetup::s_instance->s_gameFileName = "LobbyModule";
	}
	if (!CarouselSetup::s_instance->s_gameFileName.empty())
	{
		// Load them into the current game first, then send them the history
		NetMessage newMessage;
		GameSwitchEvent switchEvent;
		switchEvent.m_gameName = CarouselSetup::s_instance->s_gameFileName;
		newMessage.SetMessage(3, NetUtils::GetLocalPlayerID(), playerInfo->m_id, switchEvent);
		NetUtils::SendPacketToPlayer(newMessage, playerInfo->m_id);
	}
	else
	{
		// Just add them directly
		OnPlayerReadyToBeAdded(playerInfo->m_id);
	}
	
}
void NetworkGameState::OnClientDisconnected(NetUtils::PlayerInfo* playerInfo)
{
	printf("Client disconnected with id %d\n", playerInfo->m_id);
	NetworkEventWrapper wrapper = NetworkEventWrapper(m_commonGame.GetHeadFrame() + 2, new PlayerLeftEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(), playerInfo->m_id));
	NetMessage message;
	message.SetMessage<NetworkEventWrapper>(2, NetUtils::GetLocalPlayerID(), -1, wrapper);
	if (m_runSimulation)
	{
		m_commonGame.AddNetworkEvent(wrapper);
	}
	NetUtils::SendPacketToAll(message);
}
void NetworkGameState::OnPlayerReadyToBeAdded(int playerID)
{
	if (m_runSimulation)
	{
		m_commonGame.SendCompleteHistoryToPlayer(playerID);
	}

	// Schedule the player joining event (that adds any game-specific player entities)
	NetworkEventWrapper wrapper = NetworkEventWrapper(m_commonGame.GetHeadFrame() + 2, new PlayerJoinedEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(), playerID));
	NetMessage newPlayer;
	newPlayer.SetMessage<NetworkEventWrapper>(2, NetUtils::GetLocalPlayerID(), NetUtils::TO_ALL, wrapper);
	if (m_runSimulation)
	{
		m_commonGame.AddNetworkEvent(wrapper);
	}
	NetUtils::SendPacketToAll(newPlayer);
}
void NetworkGameState::OnWrappedNetworkEvent(NetworkEventWrapper& netEvent)
{
	if (!m_runSimulation)
	{
		return;
	}
	if (m_gameSwitchingState != GameSwitchingState::NOT_SWITCHING)
	{
		m_switchingGameBuffer.push_back(netEvent);
		return;
	}
	if (netEvent.m_event == nullptr)
	{
		printf("NetEvent on frame %d is null!\n", netEvent.m_frame);
		return;
	}
	NetworkStateControl& netState = m_commonGame;
	if (netEvent.m_frame <= netState.GetOldestHistoryFrame() && netEvent.m_event->DoRewindForEvent())
	{
		NetworkEvent* toCancel = netEvent.m_event.get();
		if (toCancel != nullptr)
		{
			if (!NetUtils::IsHost()) // Only the server should be able to cancel events
			{
				return;
			}
			cout << "Trying to cancel event from player " << toCancel->m_playerID << "\n";
			NetworkEventWrapper cancelEvent(netEvent.m_frame, new PlayerControlCancelEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(),
				toCancel->m_playerID, toCancel->m_playerEventID, netState.GetHeadFrame()));
			NetMessage message;
			message.SetMessage(2, NetUtils::GetLocalPlayerID(), toCancel->m_playerID, cancelEvent);
			NetUtils::SendPacketToPlayer(message, message.GetSendingPlayer());
			return;
		}
	}
	netState.AddNetworkEvent(netEvent);
}
bool NetworkGameState::UpdateGameSwitching()
{
	switch (m_gameSwitchingState)
	{
		case GameSwitchingState::NOT_SWITCHING:
			return false;
		case GameSwitchingState::LOADING:
		{
			if (MinigameLoader::s_instance->m_loadingState == ModuleLoadState::EMPTY)
			{
				printf("!!!New game could not be loaded!!! Disconnecting! :/\n");
				NetUtils::Disconnect();
				m_gameSwitchingState = GameSwitchingState::NOT_SWITCHING;
			}
			if (MinigameLoader::s_instance->m_loadingState == ModuleLoadState::LOADED)
			{
				printf("New Game is loaded, sending ready message\n");
				SwitchGame(MinigameLoader::s_instance->m_loadedGame);

				if (!NetUtils::IsDedicatedHost())
				{
					m_switchingReadyPlayerIDs.push_back(NetUtils::GetLocalPlayerID());
				}

				GameSwitchReadyMessage readyMessage{ m_switchingGameFileName };
				NetUtils::SendNetMessage(4, NetUtils::TO_ALL, readyMessage);

				m_gameSwitchingState = GameSwitchingState::WAITING_FOR_READY;
			}
			break;
		}
		case GameSwitchingState::WAITING_FOR_READY:
		{
			if (NetUtils::IsHost())
			{
				ConnectedPlayersArray players = NetUtils::GetConnectedPlayers();
				bool allReady = true;
				for (int i = 0; i < players.size(); i++)
				{
					if (players[i] && !VectorUtils::HasElement(i, m_switchingReadyPlayerIDs))
					{
						allReady = false;
						break;
					}
				}
				if (allReady)
				{
					printf("All players are switched!  Resuming game...\n");
					uint32_t newCrc = m_commonGame.GetHeadFrameState().GenerateCRC32();
					GameSwitchFinishEvent finishSwitchEvent{ m_switchingGameFileName, newCrc, m_commonGame.GetHeadFrameState() };
					NetUtils::SendNetMessage(5, NetUtils::TO_ALL, finishSwitchEvent);

					m_gameSwitchingState = GameSwitchingState::NOT_SWITCHING;
				}
			}
			break;
		}
	}
	return true;
}
void NetworkGameState::OnMessageReceived(NetMessage& message)
{
	if (!m_runSimulation)
	{
		return;
	}
	if (message.GetMessageType() == 2)
	{
		NetworkEventWrapper netEvent;
		message.GetMessageObject(netEvent);
		OnWrappedNetworkEvent(netEvent);
	}
	else if (message.GetMessageType() == 3)
	{
		GameSwitchEvent switchEvent;
		message.GetMessageObject(switchEvent);
		printf("Received player switch event from server!\n");
		NetworkGameState::BeginGameSwitch(switchEvent.m_gameName.c_str());
	}
	else if (message.GetMessageType() == 4)
	{
		if (NetUtils::IsHost() && m_gameSwitchingState == GameSwitchingState::NOT_SWITCHING)
		{
			// If we're not switching, then the ready message will be from a newly joining player
			// We tell them to complete the game load, and then join them into the game
			int newPlayerID = message.GetSendingPlayer();
			GameSwitchFinishEvent finishSwitchEvent{ CarouselSetup::s_instance->s_gameFileName, m_commonGame.GetHeadFrameState().GenerateCRC32(), m_commonGame.GetHeadFrameState() };
			NetUtils::SendNetMessage(5, newPlayerID, finishSwitchEvent);
			OnPlayerReadyToBeAdded(newPlayerID);
		}
		else
		{
			GameSwitchReadyMessage readyMessage;
			message.GetMessageObject(readyMessage);
			printf("Received ready message\n");
			// Store which players are ready
			m_switchingReadyPlayerIDs.push_back((int)message.GetSendingPlayer());
		}
	}
	else if (message.GetMessageType() == 5)
	{
		GameSwitchFinishEvent finishSwitchEvent;
		message.GetMessageObject(finishSwitchEvent);
		printf("Received GameSwitchFinishEvent\n");

		m_commonGame.Init();
		finishSwitchEvent.m_frameState.CopyTo(m_commonGame.GetHeadFrameState());
		FrameState::ResetStaticStorageAndCachePointer();

		uint32_t localCRC = m_commonGame.GetHeadFrameState().GenerateCRC32();
		if (localCRC != finishSwitchEvent.m_crc)
		{
			printf("There is a crc difference as restarting game!\n");
		}
		m_gameSwitchingState = GameSwitchingState::NOT_SWITCHING;
	}
}
NetworkStateControl& NetworkGameState::GetCommonGame()
{
	return m_commonGame;
}

void NetworkGameState::StartGameSwitchForRoom(const char* newGameName)
{
	if (strcmp(m_switchingGameFileName.c_str(), newGameName) == 0)
	{
		printf("Already running that game!  Cannot switch to %s\n", newGameName);
		return;
	}
	printf("Sending load game message to %s\n", newGameName);

	NetMessage newMessage;
	GameSwitchEvent switchEvent;
	switchEvent.m_gameName = newGameName;
	newMessage.SetMessage(3, NetUtils::GetLocalPlayerID(), -1, switchEvent);
	NetUtils::SendPacketToAll(newMessage);

	BeginGameSwitch(newGameName);
}

void NetworkGameState::BeginGameSwitch(const char* gameName)
{
	if (m_gameSwitchingState != GameSwitchingState::NOT_SWITCHING)
	{
		printf("Already switching game!  Cannot switch to %s\n", gameName);
		return;
	}
	printf("Beginning game switch to %s\n", gameName);
	m_gameSwitchingState = GameSwitchingState::LOADING;

	m_switchingGameFileName = std::string(gameName);
	m_switchingGameBuffer.clear();

	m_switchingReadyPlayerIDs.clear();

	if (MinigameLoader::s_instance != nullptr)
	{
		MinigameLoader::s_instance->LoadMinigame(m_switchingGameFileName.c_str(), NetUtils::IsDedicatedHost());
	}
}

const char* NetworkGameState::GetCurrentlyLoadedGameFileName()
{
	return m_switchingGameFileName.c_str();
}

void NetworkGameState::SwitchGame(GameSpecificFunctions* newGame)
{
	printf("Attempting to switch loaded game called %s\n", newGame->GetGameName());
	m_customClientGame = nullptr; // Free anything first

	CarouselSetup::s_instance->Initialise(newGame, m_switchingGameFileName);

	m_localPlayerControl = CarouselSetup::GetGameFunctions()->CreateCustomControlStateOnHeap();
	m_commonGame.OverrideCurrentFrame(0);

	m_commonGame.ClearAllFramesAndEvents();

	SetupHeadFrame();
	CustomFrameStateWrapper& headWrapper = m_commonGame.GetHeadWrapper();
	headWrapper.SetConnectedPlayers(NetUtils::GetConnectedPlayers());
	headWrapper.GetState<CustomFrameState>()->SetupGame();
	m_commonGame.GetHeadFrameState().SaveFrameState(); // Save the result from the game initialisation

	
	NetworkStateControl& netState = m_commonGame;
	for (NetworkEventWrapper& netEvent : m_switchingGameBuffer)
	{
		if (netEvent.m_frame <= netState.GetOldestHistoryFrame() && netEvent.m_event->DoRewindForEvent())
		{
			continue;
		}
		netState.AddNetworkEvent(netEvent);
	}

	m_switchingGameBuffer.clear();

	InitInnerGame();
}
void NetworkGameState::InitInnerGame()
{
	GameSpecificFunctions* newGame = CarouselSetup::GetGameFunctions();
	m_customClientGame = newGame->CreateCustomClientGameOnHeap();
	if (m_customClientGame != nullptr)
	{
		m_customClientGame->InitClient(m_commonGame);
	}
}