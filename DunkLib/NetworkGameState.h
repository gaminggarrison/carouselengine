#pragma once
#include "GameState.h"
#include "BasePlayerControlState.h"
#include "NetworkStateControl.h"
#include "MetagameControl.h"

class NetMessage;
struct GameSpecificFunctions;
struct CustomClientGame;
#include "NetSession.h"

enum class GameSwitchingState
{
	NOT_SWITCHING = 0,
	LOADING = 1,
	WAITING_FOR_READY = 2
};

namespace NetUtils
{
	struct PlayerInfo;
	char GetLocalPlayerID();
}


class NetworkGameState : GameState, MetagameControl
{
public:
	virtual ~NetworkGameState();
	virtual void Init();
	virtual void HandleReload();
	virtual void Cleanup();
	virtual void Update(float deltaTime);
	virtual void Draw();
	virtual void DrawDebug();

	virtual void GameInit() = 0;
	virtual void HandleInput() = 0;
	NetworkStateControl& GetCommonGame();
	
	template<typename T>
	T& GetLocalControlState() { return (T&)*((T*)m_localPlayerControl.get()); }
	template<typename T>
	T GetLocalHeadControlState() { return m_commonGame.GetHeadWrapper().GetControlState<T>(NetUtils::GetLocalPlayerID()); }

	void HandleCRCChecks(float deltaTime);

	virtual void OnClientConnected(NetUtils::PlayerInfo* playerInfo);
	virtual void OnClientDisconnected(NetUtils::PlayerInfo* playerInfo);
	virtual void OnMessageReceived(NetMessage& message);


	NetworkStateControl m_commonGame;

	virtual void StartGameSwitchForRoom(const char* newGameName);
	void BeginGameSwitch(const char* gameName);
	const char* GetCurrentlyLoadedGameFileName();
	void InitInnerGame();
private:
	void OnPlayerReadyToBeAdded(int playerID);
	bool UpdateGameSwitching();
	void OnWrappedNetworkEvent(NetworkEventWrapper& netEvent);

	void SetupHeadFrame();
	void SwitchGame(GameSpecificFunctions* newGame);

	bool m_hasTriedConnecting = false;
	float m_crcCheckGap = 1.0f;
	float m_crcCheckCounter = 0.0f;
	std::shared_ptr<BasePlayerControlState> m_localPlayerControl;

	GameSwitchingState m_gameSwitchingState = GameSwitchingState::NOT_SWITCHING;
	std::string m_switchingGameFileName = "";
	std::vector<NetworkEventWrapper> m_switchingGameBuffer;
	std::vector<int> m_switchingReadyPlayerIDs;
	std::shared_ptr<CustomClientGame> m_customClientGame = nullptr;
	NetUtils::NetSession* m_netSession;
	
protected:
	bool m_runSimulation = true;
};
static_assert(sizeof(NetworkGameState) <= MAX_GAMESTATE_SIZE, "Class is too big!");