#pragma once
#include "cereal/cereal.hpp"

namespace NetUtils
{
	enum class ConnectionState
	{
		Disconnected = 0,
		ConnectedPrehandshake = 1,
		Connected = 2
	};
	struct PlayerInfo
	{
		int m_id = -1;
		ConnectionState m_state = ConnectionState::Disconnected;
		std::string m_name;

		PlayerInfo& operator=(const PlayerInfo& other)
		{
			printf("Invoking PlayerInfo copy assignment!\n");
			m_id = other.m_id;
			m_state = other.m_state;
			m_name = other.m_name;
			return *this;
		}

		template<class Archive>
		void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
		{
			archive(m_id, m_state, m_name);
		}
	};
}