#pragma once
#include "NetworkPlayerInfo.h"
#include "cereal/types/static_vector.hpp"
#include "NetworkGameDefines.h"
#include "cereal/types/string.hpp"

namespace NetUtils
{
	struct PlayerInfoSet
	{
		static_vector<PlayerInfo, PLAYER_COUNT> m_playerInfo;

		template<class Archive>
		void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
		{
			archive(m_playerInfo);
		}

		PlayerInfo& operator[](int);
	};

	struct ClientHandshakeStart
	{
		int m_newPlayerID;
		std::string m_serverVersion;

		template<class Archive>
		void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
		{
			archive(m_newPlayerID, m_serverVersion);
		}
	};
	struct ClientHandshakeResponse
	{
		std::string m_playerName;

		template<class Archive>
		void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
		{
			archive(m_playerName);
		}
	};
	struct SessionStartMessage
	{
		std::string m_sessionName;

		template<class Archive>
		void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
		{
			archive(m_sessionName);
		}
	};
	struct PlayerNameChangeRequest
	{
		std::string m_newPlayerName;

		template<class Archive>
		void serialize(Archive& archive) // This is never saved to disk, so shouldn't need a version
		{
			archive(m_newPlayerName);
		}
	};
}
