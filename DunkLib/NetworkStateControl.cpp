#include "NetworkStateControl.h"
#include "MathUtils.h"
#include "NetUtils.h"
#include "DebugUtils.h"
#include "StaticStorage.h"
#include "CRCFailResponseEvent.h"
#include "UIManager.h"
#include "GUIUtils.h"
#include "CustomFrameStateWrapper.h"

#include "StateComparisonUtils.h"
#include "GameLoadEvent.h"
#include "FastForwardEvent.h"
#include "RollbackHorizonEvent.h"
#include <array>
#include "PlayerControlsEvent.h"

using namespace std;

NetworkStateControl::NetworkStateControl()
{

}

void NetworkStateControl::Init(bool offlineMode)
{
	//size_t dynamicSpacePerFrame = 1024 * 1024 * 16;
	//StaticStorage::Init(dynamicSpacePerFrame);
	for (int i = 0; i < m_frameBuffer; i++)
	{
		m_frames[i] = new NetworkFrame();
	}
	m_offlineMode = offlineMode;
	m_latestFramesForPlayers.clear();
	m_latestFramesForPlayers.resize(PLAYER_COUNT, 8);
	m_rollbackHorizonFrame = 0;
	m_syncedClock.SetInternalTime(0);
}
void NetworkStateControl::ClearAllFramesAndEvents()
{
	for (int i = 0; i < m_frameBuffer; i++)
	{
		m_frames[i]->Clear();
		m_frames[i]->m_frameState.SetupCustomFrameState();
	}
	m_newEvents.clear();
}

bool NetworkStateControl::HandleDebugControl(float deltaTime)
{
	if (DebugUtils::s_debugInstance >= 0)
	{
		bool skipRollback = IsKeyDown(KEY_SEMICOLON);
		if (skipRollback)
		{
			NetworkFrame& frame = *m_frames[GetHeadFrame() % m_frameBuffer];
			frame.m_frameState.GetData()->Update(deltaTime, GetHeadFrame());
			return true;
		}
		if (IsKeyDown(KEY_KP_ADD))
		{
			NetUtils::SetSimulatedLag(NetUtils::GetSimulatedLag() + 1);
		}
		else if (IsKeyDown(KEY_KP_SUBTRACT))
		{
			NetUtils::SetSimulatedLag(NetUtils::GetSimulatedLag() - 1);
		}

		if (IsKeyPressed(KEY_HOME))
		{
			GetBlackBoxSystem().BeginPlayback(*this);
		}
		if (IsKeyPressed(KEY_PAGE_UP))
		{
			GetBlackBoxSystem().LoadRecording("Recording.rec", *this);
		}
#ifndef EMSCRIPTEN
		if (IsFileDropped())
		{
			int dropCount = 0;
			char** droppedFiles = GetDroppedFiles(&dropCount);

			const char* droppedFile = droppedFiles[0];
			if (IsFileExtension(droppedFile, ".rec"))
			{
				GetBlackBoxSystem().LoadRecording(droppedFile, *this);
			}

			ClearDroppedFiles();
		}
#endif

		if (IsKeyPressed(KEY_PAGE_DOWN))
		{
			GetBlackBoxSystem().SaveRecording("Recording.rec");
		}
	}
	if (IsKeyPressed(KEY_F10))
	{
		DebugUtils::s_debugOn = !DebugUtils::s_debugOn;
	}
	if (IsKeyPressed(KEY_PAUSE))
	{
		DebugUtils::s_debugPause = !DebugUtils::s_debugPause;
	}

	if (DebugUtils::s_debugPause)
	{
		if (IsKeyPressed(KEY_RIGHT))
		{
			m_debugScrubOffset++;
		}
		else if (IsKeyPressed(KEY_LEFT))
		{
			m_debugScrubOffset--;
		}
		else if (IsKeyDown(KEY_LEFT_CONTROL) && IsKeyPressed(KEY_SPACE))
		{
			// Do a test update on the current scrub frame
			int frameNumber = (m_currentFrame + m_debugScrubOffset);
			int frameID = frameNumber % m_frameBuffer;
			NetworkFrame& frame = *m_frames[frameID];
			NetworkFrame* nextFrame = new NetworkFrame();
			nextFrame->m_frameState = frame.m_frameState;

			frame.ApplyEvents(nextFrame->m_frameState, false);
			CustomFrameStateWrapper* sharedGame = nextFrame->m_frameState.GetData();
			sharedGame->Update(deltaTime, frameNumber);
		}
		return true;
	}
	else
	{
		m_debugScrubOffset = 0;
	}
	return false;
}

int NetworkStateControl::ProcessNewEvents()
{
	m_processedEvents.clear();
	int oldestNewEvent = m_currentFrame;
	for (int i = (int)m_newEvents.size() - 1; i >= 0; i--)
	{
		NetworkEventWrapper newEvent = m_newEvents[i];
		if (newEvent.m_frame < GetOldestHistoryFrame())
		{
			if (newEvent.m_event->DoRewindForEvent())
			{
				m_hasReceivedAnEventBeforeHistory = true;
				NetworkEvent* errorEvent = newEvent.m_event.get();
				m_eventReceivedBeforeHistory = std::string(typeid(*errorEvent).name());
				printf("Rewind-worthy event of type %s is on frame %d but oldest history frame is %d!\n", m_eventReceivedBeforeHistory.c_str(), newEvent.m_frame, GetOldestHistoryFrame());
			}
			m_newEvents.erase(m_newEvents.begin() + i);
		}
		else if (newEvent.m_frame <= GetNewestHistoryFrame())
		{
			int frameID = newEvent.m_frame % m_frameBuffer;
			m_frames[frameID]->AddNetworkEvent(newEvent.m_event);
			m_processedEvents.push_back(newEvent);
			if (newEvent.m_frame < oldestNewEvent && newEvent.m_event->DoRewindForEvent())
			{
				oldestNewEvent = newEvent.m_frame;
			}
			m_newEvents.erase(m_newEvents.begin() + i);
		}
	}
	return oldestNewEvent;
}

void NetworkStateControl::Update(float realDeltaTime)
{
	if (NetUtils::IsDedicatedHost() && NetUtils::GetConnectedPlayerCount() <= 0) // Don't update the game when there are no players
	{
		return;
	}

	m_syncedClock.Update(realDeltaTime, *this);

	float deltaTime = 1.0f / 60.0f;
	
	if (HandleDebugControl(deltaTime))
	{
		return;
	}
	m_blackBoxSystem.Update(*this);

	int oldestNewEvent = ProcessNewEvents();
	if (m_offlineMode && oldestNewEvent < m_currentFrame)
	{
		printf("We can't rewind in offline mode!  We haven't saved any history!\n");
	}
	m_oldestNewFrame = oldestNewEvent;

	int frameNumber = m_oldestNewFrame;

	int toSimulate = m_syncedClock.GetFramesToSimulate(m_currentFrame, *this);

	if (m_framesToCatchUp > 0)
	{
		toSimulate = m_framesToCatchUp;
		// Update the timer to account for the catchup
		m_syncedClock.SetInternalTime(m_syncedClock.GetInternalTime() + m_syncedClock.FrameToClock(toSimulate));
	}

	int newFrame = Max(frameNumber, m_currentFrame + toSimulate);
	assert(newFrame >= m_currentFrame);
	for (; frameNumber < newFrame; frameNumber++)
	{
		NetworkFrame& frame = *m_frames[frameNumber % m_frameBuffer];
		NetworkFrame& nextFrame = *m_frames[(frameNumber + 1) % m_frameBuffer];
		if (frameNumber + 1 > m_currentFrame)
		{
			nextFrame.Clear();
			// Add any queued events
			for (int i = (int)m_newEvents.size() - 1; i >= 0; i--)
			{
				NetworkEventWrapper newEvent = m_newEvents[i];
				if (newEvent.m_frame == frameNumber + 1)
				{
					nextFrame.AddNetworkEvent(newEvent.m_event);
					m_processedEvents.push_back(newEvent);
					m_newEvents.erase(m_newEvents.begin() + i);
				}
			}
		}

		frame.ApplyEvents(frame.m_frameState, !m_blackBoxSystem.IsPlaying());
		CustomFrameStateWrapper* sharedGame = frame.m_frameState.GetData();
		sharedGame->Update(deltaTime, frameNumber);
		nextFrame.m_frameState.SaveFrameState(m_offlineMode);
	}

	m_framesToCatchUp = 0;
	m_currentFrame = newFrame;
	m_oldestNewFrame++;

	if (m_doSelfConsistencyChecking && m_currentFrame > 10 && !DebugUtils::s_debugPause && !m_blackBoxSystem.IsPlaying() && DebugUtils::s_debugInstance >= 0)
	{
		RunSelfConsistencyCheck();
	}

	if (NetUtils::IsHost())
	{
		// Figure out who is the furthest client back, and allow everyone to go one buffer's worth of frames ahead
		int oldestPlayerFrame = m_currentFrame;
		std::array<bool, PLAYER_COUNT> connectedPlayers = NetUtils::GetConnectedPlayers();
		for (int i = 0; i < PLAYER_COUNT; i++)
		{
			if (connectedPlayers[i])
			{
				int newestFrameForPlayer = m_latestFramesForPlayers[i];
				if (newestFrameForPlayer < oldestPlayerFrame)
				{
					oldestPlayerFrame = newestFrameForPlayer;
				}
			}
		}

		int maxRollbackFrame = m_rollbackHorizonFrame - m_frameBuffer;
		if (oldestPlayerFrame < maxRollbackFrame)
		{
			printf("Uh oh, we have a player at frame %d when the max rollback frame is %d\n", oldestPlayerFrame, maxRollbackFrame);
		}
		else
		{
			int newRollbackHorizon = (oldestPlayerFrame + m_frameBuffer) - 1;
			AddNetworkEventAndSend(new RollbackHorizonEvent(newRollbackHorizon));
		}
	}
}

CustomFrameStateWrapper& NetworkStateControl::GetHeadWrapper()
{
	return *m_frames[(m_currentFrame + m_debugScrubOffset) % m_frameBuffer]->m_frameState.GetData();
}
FrameState& NetworkStateControl::GetHeadFrameState()
{
	return m_frames[(m_currentFrame + m_debugScrubOffset) % m_frameBuffer]->m_frameState;
}

int NetworkStateControl::GetHeadFrame()
{
	return m_currentFrame;
}

int NetworkStateControl::GetOldestHistoryFrame()
{
	return Max((m_currentFrame - m_frameBuffer) + 1, m_lastLoadedFrame);
}
int NetworkStateControl::GetNewestHistoryFrame()
{
	return m_currentFrame;
}

NetworkFrame& NetworkStateControl::GetStateForFrame(int frameNumber)
{
	if (frameNumber < GetOldestHistoryFrame() || frameNumber > GetNewestHistoryFrame())
	{
		printf("Getting incorrect state for frame %d, as it's out of known range between %d and %d\n", frameNumber, GetOldestHistoryFrame(), GetNewestHistoryFrame());
	}
	return *m_frames[(frameNumber + m_frameBuffer) % m_frameBuffer];
}

void NetworkStateControl::AddNetworkEventDirect(NetworkEventWrapper& netevent)
{
	SetLastKnownPlayerFrame(netevent.m_event->m_playerID, netevent.m_frame);
	m_newEvents.push_back(netevent);

	netevent.m_event->OnEventAdded(*this, netevent.m_frame);
}

void NetworkStateControl::SetLastKnownPlayerFrame(int playerID, int frame)
{
	if (playerID < 0)
	{
		// Probably just the server creating commands
		return;
	}
	if (playerID < 0 || playerID >= m_latestFramesForPlayers.size())
	{
		printf("Cannot SetLastKnownPlayerFrame for playerID %d\n", playerID);
		return;
	}
	m_latestFramesForPlayers[playerID] = frame;
}

void NetworkStateControl::AddNetworkEvent(NetworkEventWrapper& netevent)
{
	bool useEvent = m_blackBoxSystem.OnEventProcessed(m_currentFrame, netevent);

	if (useEvent)
	{
		AddNetworkEventDirect(netevent);
	}
}

void NetworkStateControl::OverrideCurrentFrame(int frameNumber, bool clearEvents)
{
	m_currentFrame = frameNumber;
	m_lastLoadedFrame = frameNumber;
	m_oldestNewFrame = frameNumber;
	m_syncedClock.SetInternalTime(m_syncedClock.FrameToClock(frameNumber));
	m_rollbackHorizonFrame = frameNumber;
	if (clearEvents)
	{
		for (int i = 0; i < m_frameBuffer; i++)
		{
			m_frames[i]->Clear();
		}
	}
}
void NetworkStateControl::SetFramesToCatchup(int catchupFrames)
{
	if (catchupFrames < 0)
	{
		catchupFrames = 0;
	}
	m_framesToCatchUp = catchupFrames;
}

uint32_t NetworkStateControl::GenerateCRCForFrame(int frameNumber)
{
	if (frameNumber < GetOldestHistoryFrame() || frameNumber > GetNewestHistoryFrame())
	{
		printf("Cannot generate CRC for frame %d, as it's out of range between %d and %d\n", frameNumber, GetOldestHistoryFrame(), GetNewestHistoryFrame());
		return 0;
	}
	NetworkFrame& frame = GetStateForFrame(frameNumber);
	return frame.m_frameState.GenerateCRC32();
}
void NetworkStateControl::CompareWithIncomingCRC(int frameNumber, uint32_t crc)
{
	if (frameNumber <= GetOldestHistoryFrame())
	{
		printf("Ignoring incoming CRC because it's before my time :)\n");
		return;
	}
	if (frameNumber >= GetNewestHistoryFrame())
	{
		printf("Ignoring incoming CRC because it's from the future :o\n");
		return;
	}
	uint32_t myCRC = GenerateCRCForFrame(frameNumber);
	if (myCRC != 0) // 0 means we are unable to generate a CRC for this frame
	{
		if (myCRC != crc)
		{
			bool wasOutOfSync = m_currentlyOutOfSync;
			m_currentlyOutOfSync = true;

			if (!wasOutOfSync)
			{
				if (!m_blackBoxSystem.IsPlaying())
				{
					char filename[50];
					sprintf(filename, "OutOfSyncFrame%d.rec", frameNumber);
					m_blackBoxSystem.SaveRecording(filename);
				}

				// Send response
				printf("Sending CRCFailResponseEvent to server\n");
				SendNetworkEventOnlyToPlayer(new CRCFailResponseEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(), crc, myCRC, GetStateForFrame(frameNumber).m_frameState), frameNumber, NetUtils::TO_HOST);
			}
		}
		else
		{
			m_currentlyOutOfSync = false;
		}
	}
}

bool NetworkStateControl::IsCurrentlyOutOfSync()
{
	return m_currentlyOutOfSync;
}

void NetworkStateControl::AddNetworkEventAndSend(NetworkEvent* netevent)
{
	AddNetworkEventAndSend(netevent, 0);
}
void NetworkStateControl::AddNetworkEventAndSend(NetworkEvent* netevent, int frameOffset)
{
	NetworkEventWrapper wrapper = NetworkEventWrapper(GetHeadFrame() + frameOffset, netevent);
	AddNetworkEvent(wrapper);

	NetMessage message;
	message.SetMessage(2, NetUtils::GetLocalPlayerID(), -1, wrapper);
	NetUtils::SendPacketToAll(message);
}
void NetworkStateControl::SendNetworkEventOnly(NetworkEvent* netevent, int frameNumber)
{
	NetworkEventWrapper wrapper = NetworkEventWrapper(frameNumber, netevent);

	NetMessage message;
	message.SetMessage(2, NetUtils::GetLocalPlayerID(), -1, wrapper);
	NetUtils::SendPacketToAll(message);
}

void NetworkStateControl::SendNetworkEventOnlyToPlayer(NetworkEvent* netevent, int frameNumber, int playerID)
{
	NetworkEventWrapper wrapper = NetworkEventWrapper(frameNumber, netevent);

	NetMessage message;
	message.SetMessage(2, NetUtils::GetLocalPlayerID(), playerID, wrapper);
	NetUtils::SendPacketToPlayer(message, playerID);
}

void NetworkStateControl::SendCompleteHistoryToPlayer(int playerID)
{
	// Send oldest frame and all the events that we know about, to give the client a complete history
	int frameToSend = GetOldestHistoryFrame();
	FrameState& frameState = GetStateForFrame(frameToSend).m_frameState;
	NetworkEventWrapper loadEvent(frameToSend, new GameLoadEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(), frameState));

	NetMessage message;
	message.SetMessage(2, NetUtils::GetLocalPlayerID(), playerID, loadEvent);
	NetUtils::SendPacketToPlayer(message, playerID);

	for (int i = frameToSend; i < GetNewestHistoryFrame(); i++)
	{
		NetworkFrame& frame = GetStateForFrame(i);
		vector<shared_ptr<NetworkEvent>>& events = frame.GetEvents();
		for (shared_ptr<NetworkEvent>& e : events)
		{
			SendNetworkEventOnlyToPlayer(e.get(), i, playerID);
		}
	}
	for (NetworkEventWrapper& newEvent : m_newEvents)
	{
		NetMessage newMessage;
		newMessage.SetMessage(2, NetUtils::GetLocalPlayerID(), playerID, newEvent);
		NetUtils::SendPacketToPlayer(newMessage, playerID);
	}
	// Make them catch up
	SendNetworkEventOnlyToPlayer(new FastForwardEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(), GetNewestHistoryFrame()), GetNewestHistoryFrame(), playerID);
}

BlackBoxSystem& NetworkStateControl::GetBlackBoxSystem()
{
	return m_blackBoxSystem;
}

void NetworkStateControl::DrawDebugUI()
{
	int x = 20;
	int y = UIManager::GetUIHeight() - 50;

	int frame = GetHeadFrame();
	char str[100];

	if (m_debugScrubOffset == 0)
	{
		sprintf(str, "Frame: %d", frame);
	}
	else
	{
		if (m_debugScrubOffset > 0)
		{
			sprintf(str, "Frame: %d +%d", frame, m_debugScrubOffset);
		}
		else
		{
			sprintf(str, "Frame: %d %d", frame, m_debugScrubOffset);
		}
	}
	GUIUtils::DrawText(str, x, y, 18, ORANGE);


	int rewind = frame - m_oldestNewFrame;
	sprintf(str, "Rewind: %d", rewind);
	GUIUtils::DrawText(str, x + 150, y, 18, ORANGE);

	double_t averageDiffFromServer = m_syncedClock.GetAverageDiffFromServer();
	int frameDiff = m_syncedClock.ClockToFrame(averageDiffFromServer);
	sprintf(str, "Diff from server: %d", frameDiff);
	GUIUtils::DrawText(str, x + 300, y, 18, ORANGE);

	Vector2 mousePos = GetMousePosition();
	bool ignoreOwnEvents = false;// !m_blackBoxSystem.IsPlaying();
	y += 20;
	int boxSize = 8;
	int tailFrame = GetOldestHistoryFrame();
	for (int i = 0; i < m_frameBuffer * 2; i++)
	{
		int boxX = x + boxSize * i;
		Rect boxRect = { boxX, y, boxSize, boxSize };
		int actualFrame = tailFrame + i;
		bool hasNewEvent = false;
		NetworkEvent* eventOnFrame = nullptr;
		for (NetworkEventWrapper& e : m_processedEvents)
		{
			if (ignoreOwnEvents && e.m_event->m_playerID == NetUtils::GetLocalPlayerID())
			{
				continue;
			}
			if (e.m_frame == actualFrame)
			{
				hasNewEvent = true;
				eventOnFrame = e.m_event.get();
				break;
			}
		}
		bool hasQueuedEvent = false;
		for (NetworkEventWrapper& e : m_newEvents)
		{
			if (ignoreOwnEvents && e.m_event->m_playerID == NetUtils::GetLocalPlayerID())
			{
				continue;
			}
			if (e.m_frame == actualFrame)
			{
				hasQueuedEvent = true;
				eventOnFrame = e.m_event.get();
				break;
			}
		}
		Color colour = RED;
		bool hasEvents = false;
		if (hasQueuedEvent)
		{
			colour = BLUE;
			hasEvents = true;
		}
		else if (hasNewEvent)
		{
			colour = ORANGE;
			hasEvents = true;
		}
		else if (actualFrame <= m_currentFrame)
		{
			int bufferPosition = (actualFrame + m_frameBuffer) % m_frameBuffer;
			NetworkFrame& frame = *m_frames[bufferPosition];
			if (frame.GetEvents().size() > 0)
			{
				int topPriority = -1;
				colour = GREEN;
				for (std::shared_ptr<NetworkEvent>& event : frame.GetEvents())
				{
					if (event == nullptr)
					{
						colour = RED;
						topPriority = 1000;
						hasEvents = true;
						continue;
					}
					if (ignoreOwnEvents && event->m_playerID == NetUtils::GetLocalPlayerID())
					{
						continue;
					}
					int eventPriority = event->GetDebugPriority();
					if (eventPriority > topPriority)
					{
						colour = event->GetDebugColour();
						eventOnFrame = event.get();
						topPriority = eventPriority;
						hasEvents = true;
					}
				}
			}
		}
		if (hasEvents)
		{
			DrawRectangle(boxX, y, boxSize, boxSize, colour);
		}
		DrawRectangleLines(boxX, y, boxSize, boxSize, WHITE);
		if (eventOnFrame != nullptr && boxRect.IsInBox(mousePos))
		{
			std::string eventName = std::string(typeid(*eventOnFrame).name());
			GUIUtils::DrawTextCenteredFormatted("Event = %s from player %d", str, GetScreenWidth() / 2, y + boxSize + 8, 12, RED, eventName.c_str(), eventOnFrame->m_playerID);
		}
	}
	int visibleFrame = m_currentFrame + m_debugScrubOffset;
	int loopPosition = ((visibleFrame - tailFrame) + m_frameBuffer) % m_frameBuffer;
	int boxX = x + boxSize * loopPosition;
	DrawRectangleLines(boxX, y, boxSize, boxSize, YELLOW);

	// Draw memory usage
	size_t totalMemory = StaticStorage::GetAllocator()->getSize();
	size_t usedMemory = StaticStorage::GetAllocator()->getUsedMemory();
	size_t numAllocations = StaticStorage::GetAllocator()->getNumAllocations();
	x = UIManager::GetUIWidth() / 2;
	y = UIManager::GetUIHeight() - 100;
	GUIUtils::DrawTextCenteredFormatted("Total memory: %d, Used: %d, Allocations: %d", str, x, y, 12, RED, totalMemory, usedMemory, numAllocations);
	float memoryUsagePercent = ((float)usedMemory * 100.0f) / (float)totalMemory;
	y += 20;
	GUIUtils::DrawTextCenteredFormatted("Memory usage: %f%%", str, x, y, 12, RED, memoryUsagePercent); // Could use %.2f to make it 2 decimal places, but it's debug so what the hey
	y += 20;

	// Draw simulated lag
	int lagMilliseconds = NetUtils::GetSimulatedLag();
	if (lagMilliseconds != 0)
	{
		GUIUtils::DrawTextCenteredFormatted("Simulated lag: %dms", str, x, y, 12, RED, lagMilliseconds);
	}
}
void NetworkStateControl::DrawWarnings()
{
	if (IsCurrentlyOutOfSync())
	{
		GUIUtils::DrawTextCentered("Out of sync :(", UIManager::GetUIWidth() / 2, UIManager::GetUIHeight() / 2, 24, RED);
	}
	if (NetUtils::IsClient() && !DoesClockMatchServer())
	{
		GUIUtils::DrawTextCentered("Catching up to server...", UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) - 48, 24, BLUE);
	}
	bool frameMatchesClock = DoesFramePositionMatchClock();
	if (!frameMatchesClock)
	{
		char buffer[100];
		int clockDiff = m_syncedClock.ClockToFrame(m_syncedClock.GetInternalTime()) - m_currentFrame;
		GUIUtils::DrawTextCenteredFormatted("Catching up with clock... Clock is %d frames ahead of sim", buffer, UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) - 24, 24, BLUE, clockDiff);
	}
	if (m_hasReceivedAnEventBeforeHistory)
	{
		char buffer[100];
		GUIUtils::DrawTextCenteredFormatted("Has received an event before local history! %s", buffer, UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) + 30, 12, RED, m_eventReceivedBeforeHistory.c_str());
	}
	if (m_hasFailedSelfConsistency)
	{
		GUIUtils::DrawText("Failed self consistency check :(", UIManager::GetUIWidth() / 2, (UIManager::GetUIHeight() / 2) + 30, 24, RED);
	}
	size_t totalMemory = StaticStorage::GetAllocator()->getSize();
	size_t usedMemory = StaticStorage::GetAllocator()->getUsedMemory();
	float memoryUsagePercent = ((float)usedMemory * 100.0f) / (float)totalMemory;
	if (memoryUsagePercent > 90.0f)
	{
		GUIUtils::DrawText("Low memory :-O", UIManager::GetUIWidth() / 2, UIManager::GetUIHeight() - 150, 24, RED);
	}

	m_blackBoxSystem.DrawUI(*this);
}

void NetworkStateControl::RunSelfConsistencyCheck()
{
	// Idea is to get current head CRC, then try rolling back + recalculating to see if we get the same result
	int headFrameNumber = GetHeadFrame();
	NetworkFrame& headFrame = GetStateForFrame(headFrameNumber);
	uint32_t originalHeadCRC = headFrame.m_frameState.GenerateCRC32();

	int frameNumber = m_currentFrame;
	NetworkFrame& previousFrame = GetStateForFrame(headFrameNumber - 1);

	float deltaTime = 1.0f / 60.0f;
	previousFrame.ApplyEvents(previousFrame.m_frameState, true);
	CustomFrameStateWrapper* sharedGame = previousFrame.m_frameState.GetData();
	sharedGame->Update(deltaTime, frameNumber - 1);

	uint32_t newCRC = previousFrame.m_frameState.GenerateCRC32();
	if (newCRC != originalHeadCRC)
	{
		m_hasFailedSelfConsistency = true;
		FrameState& originalFrameState = GetStateForFrame(headFrameNumber).m_frameState;
		StateComparisonUtils::DumpComparison(previousFrame.m_frameState, originalFrameState);
		DebugUtils::s_debugPause = true;
	}
	else
	{
		m_hasFailedSelfConsistency = false;
	}
	FrameState::ResetStaticStorageAndCachePointer();
}
void NetworkStateControl::SetSelfConsistencyChecking(bool on)
{
	m_doSelfConsistencyChecking = on;
}
SyncedClock& NetworkStateControl::GetSyncedClock()
{
	return m_syncedClock;
}
bool NetworkStateControl::IsReadyForInput()
{
	bool isReady = NetUtils::IsReadyForInput();
	/*if (!isReady)
	{
		printf("NetUtils not ready\n");
	}*/
	isReady &= DoesClockMatchServer();
	/*if (!isReady)
	{
		printf("Clock drift not ready\n");
	}*/
	isReady &= !m_currentlyOutOfSync;
	/*if (!isReady)
	{
		printf("Out of Sync not ready\n");
	}*/

	isReady &= DoesFramePositionMatchClock();

	// Let it build up enough history before allowing inputs
	if (GetOldestHistoryFrame() > (GetNewestHistoryFrame() - m_frameBuffer) + 1)
	{
		isReady = false;
	}


	std::vector<NetworkEventWrapper>& events = m_newEvents;
	for (NetworkEventWrapper& e : events)
	{
		if (e.m_event->m_playerID == NetUtils::GetLocalPlayerID() && dynamic_cast<PlayerControlsEvent*>(e.m_event.get()) != nullptr)
		{
			isReady = false;
			break;
		}
	}

	/*if (!isReady)
	{
		printf("Clock not ready\n");
	}*/
	return isReady;
}
bool NetworkStateControl::DoesFramePositionMatchClock()
{
	int clockTime = m_syncedClock.ClockToFrame(m_syncedClock.GetInternalTime());
	return abs(clockTime - m_currentFrame) < 10;
}
bool NetworkStateControl::DoesClockMatchServer()
{
	return fabs(m_syncedClock.GetAverageDiffFromServer()) < 0.25;
}
void NetworkStateControl::SetRollbackHorizon(int horizonFrame)
{
	m_rollbackHorizonFrame = horizonFrame;
}
int NetworkStateControl::GetRollbackHorizon()
{
	return m_rollbackHorizonFrame;
}