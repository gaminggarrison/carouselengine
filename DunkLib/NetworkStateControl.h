#pragma once
class SharedGame;
#include "NetworkFrame.h"
#include "NetworkEventWrapper.h"
#include "BlackBoxSystem.h"
#include "SyncedClock.h"

class NetworkStateControl
{
	int m_frameBuffer = 60;
	NetworkFrame* m_frames[60];
	int m_currentFrame = 0;
	int m_lastLoadedFrame = 0;
	int m_framesToCatchUp = 0;
	int m_oldestNewFrame = 0;
	std::vector<NetworkEventWrapper> m_newEvents = std::vector<NetworkEventWrapper>();
	std::vector<NetworkEventWrapper> m_processedEvents = std::vector<NetworkEventWrapper>();
	int m_debugScrubOffset = 0;
	bool m_currentlyOutOfSync = false;
	bool m_hasReceivedAnEventBeforeHistory = false;
	std::string m_eventReceivedBeforeHistory;
	bool m_hasFailedSelfConsistency = false;
	BlackBoxSystem m_blackBoxSystem;
	bool m_offlineMode = false;
	bool m_doSelfConsistencyChecking = false;
	SyncedClock m_syncedClock;
	int m_rollbackHorizonFrame = 0; // We don't want to simulate so far ahead that actions coming in could be before our history buffer
	std::vector<int> m_latestFramesForPlayers; // Store where each player is in time

	// Return true if we want to skip the normal updating
	bool HandleDebugControl(float deltaTime);

	// Returns the oldest new event that we need to rollback to
	int ProcessNewEvents();
	void AddNetworkEventDirect(NetworkEventWrapper& netevent);
	void SetLastKnownPlayerFrame(int playerID, int frame);
public:
	NetworkStateControl();
	void Init(bool offlineMode = false);
	void ClearAllFramesAndEvents();
	CustomFrameStateWrapper& GetHeadWrapper();

	template<typename T>
	T& GetHead() { return (T&)*GetHeadWrapper().GetState<T>(); }

	template<typename T>
	T& GetPastState(int framesBack) {
		int pastFrame = GetHeadFrame() - framesBack;
		if (pastFrame < GetOldestHistoryFrame())
		{
			pastFrame = GetOldestHistoryFrame();
		}
		return (T&)*GetStateForFrame(pastFrame).m_frameState.GetData()->GetState<T>();
	}

	CustomFrameStateWrapper& GetPastStateWrapper(int framesBack) {
		int pastFrame = GetHeadFrame() - framesBack;
		if (pastFrame < GetOldestHistoryFrame())
		{
			pastFrame = GetOldestHistoryFrame();
		}
		return *GetStateForFrame(pastFrame).m_frameState.GetData();
	}

	FrameState& GetHeadFrameState();
	NetworkFrame& GetStateForFrame(int frameNumber);
	void Update(float realDeltaTime);
	void RunSelfConsistencyCheck();
	int GetHeadFrame();
	int GetOldestHistoryFrame();
	int GetNewestHistoryFrame();
	uint32_t GenerateCRCForFrame(int frameNumber);
	void CompareWithIncomingCRC(int frameNumber, uint32_t crc);
	void OverrideCurrentFrame(int frameNumber, bool clearEvents = true); // Will probably have to reset history

	// Not currently used
	void SetFramesToCatchup(int catchupFrames);
	void AddNetworkEvent(NetworkEventWrapper& netevent);
	void AddNetworkEventAndSend(NetworkEvent* netevent);
	void AddNetworkEventAndSend(NetworkEvent* netevent, int frameOffset);
	void SendNetworkEventOnly(NetworkEvent* netevent, int frameNumber);
	void SendNetworkEventOnlyToPlayer(NetworkEvent* netevent, int frameNumber, int playerID);
	void SendCompleteHistoryToPlayer(int playerID);
	bool IsCurrentlyOutOfSync();
	BlackBoxSystem& GetBlackBoxSystem();
	void SetSelfConsistencyChecking(bool on);
	SyncedClock& GetSyncedClock();
	bool IsReadyForInput();
	bool DoesFramePositionMatchClock();
	bool DoesClockMatchServer();
	void SetRollbackHorizon(int horizonFrame);
	int GetRollbackHorizon();

	void DrawDebugUI();
	void DrawWarnings();

	friend class BlackBoxSystem;
};
