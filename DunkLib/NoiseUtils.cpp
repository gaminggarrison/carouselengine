#include "NoiseUtils.h"
#include "noise1234.h"
#include "MathUtils.h"

Image NoiseUtils::GeneratePerlinNoise(int width, int height)
{
	Image perlinImage = GenImageColor(width, height, WHITE);
	Color* pixels = (Color*)perlinImage.data;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			float value = 0.0f;
			//float value = stb_perlin_fbm_noise3((float)x / 64.0f, (float)y / 64.0f, 0.0f, 2.0f, 0.5f, 6);
			float amplitude = 0.25f;
			float frequency = 1.0f / 32.0f;
			for (int o = 0; o < 6; o++)
			{
				// X and y always wraps at 255 so, our wavelength can't be bigger than that
				float nudge = pnoise2((float)x * frequency, (float)y * frequency, (int)(width * frequency), (int)(height*frequency));
				value += nudge * amplitude;
				amplitude *= 0.5;
				frequency *= 2;
			}
			//float value = stb_perlin_noise3_wrap_nonpow2((float)x * 32.0f, (float)y * 32.0f, 0.0f, 128, 128, 1, 128);
			value = (value + 1.1f) * 0.4f;
			value = powf(value, 8) * 128.0f;// value * value * value *value * 16.0f;
			value = Clamp(value, 0.0f, 1.0f);
			unsigned char var8 = (unsigned char)(value * 255);
			Color colour = { var8, var8, var8, 255 };
			pixels[x + y * width] = colour;
		}
	}
	return perlinImage;
}