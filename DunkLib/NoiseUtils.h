#pragma once
#include "raylib.h"

namespace NoiseUtils
{
	Image GeneratePerlinNoise(int width, int height);
}