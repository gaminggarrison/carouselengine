#include "PingResponseEvent.h"
#include "NetworkFrame.h"
#include "NetworkStateControl.h"

PingResponseEvent::PingResponseEvent()
{

}

PingResponseEvent::PingResponseEvent(int playerID, int playerEventID, double_t originalSendTime, double_t serverTime) : m_originalSendTime(originalSendTime), m_serverTime(serverTime)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void PingResponseEvent::RunEvent(FrameState& frameState)
{

}
void PingResponseEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{
	networkStateControl.GetSyncedClock().OnPingResponse(m_originalSendTime, m_serverTime);
}

Color PingResponseEvent::GetDebugColour()
{
	return YELLOW;
}

int PingResponseEvent::GetDebugPriority()
{
	return 5;
}
bool PingResponseEvent::DoRewindForEvent()
{
	return false;
}