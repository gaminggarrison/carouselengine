#pragma once
#include "NetworkEvent.h"

struct PingResponseEvent : public NetworkEvent
{
	double_t m_originalSendTime;
	double_t m_serverTime;

	PingResponseEvent();
	PingResponseEvent(int playerID, int playerEventID, double_t originalSendTime, double_t serverTime);

	virtual void RunEvent(FrameState& frameState);
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();
	virtual bool DoRewindForEvent();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_originalSendTime, m_serverTime);
	}
};

CEREAL_REGISTER_TYPE(PingResponseEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, PingResponseEvent);
