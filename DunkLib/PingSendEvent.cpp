#include "PingSendEvent.h"
#include "NetworkFrame.h"
#include "NetworkStateControl.h"
#include "PingResponseEvent.h"

PingSendEvent::PingSendEvent()
{

}

PingSendEvent::PingSendEvent(int playerID, int playerEventID, double_t clientTime) : m_clientTime(clientTime)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void PingSendEvent::RunEvent(FrameState& frameState)
{

}
void PingSendEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{
	double_t serverTime = networkStateControl.GetSyncedClock().GetInternalTime();
	networkStateControl.SendNetworkEventOnlyToPlayer(new PingResponseEvent(-2, m_playerEventID, m_clientTime, serverTime), frameNumber, m_playerID);
}

Color PingSendEvent::GetDebugColour()
{
	return YELLOW;
}

int PingSendEvent::GetDebugPriority()
{
	return 5;
}
bool PingSendEvent::DoRewindForEvent()
{
	return false;
}