#pragma once
#include "NetworkEvent.h"

struct PingSendEvent : public NetworkEvent
{
	double_t m_clientTime;

	PingSendEvent();
	PingSendEvent(int playerID, int playerEventID, double_t clientTime);

	virtual void RunEvent(FrameState& frameState);
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();
	virtual bool DoRewindForEvent();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_clientTime);
	}
};

CEREAL_REGISTER_TYPE(PingSendEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, PingSendEvent);
