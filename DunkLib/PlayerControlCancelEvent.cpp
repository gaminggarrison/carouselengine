#include "PlayerControlCancelEvent.h"
#include "NetworkFrame.h"
#include "NetworkStateControl.h"

PlayerControlCancelEvent::PlayerControlCancelEvent()
{

}

PlayerControlCancelEvent::PlayerControlCancelEvent(int playerID, int playerEventID, int cancelPlayer, int cancelEvent, int actualFrame)
	: m_cancelPlayer(cancelPlayer), m_cancelEvent(cancelEvent), m_actualFrame(actualFrame)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void PlayerControlCancelEvent::RunEvent(FrameState& frameState)
{
	// Find the control state from last frame and override current frame

	// Remove any PlayerControlsEvents from the player for this frame
	//sharedGame.SetControlState(m_playerID);
}
void PlayerControlCancelEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{
	NetworkFrame& frame = networkStateControl.GetStateForFrame(frameNumber);
	frame.ClearPlayerEvent(m_cancelPlayer, m_cancelEvent);

	//networkStateControl.SetFramesToCatchup(m_actualFrame - networkStateControl.GetHeadFrame());
}

Color PlayerControlCancelEvent::GetDebugColour()
{
	return RED;
}
int PlayerControlCancelEvent::GetDebugPriority()
{
	return 20;
}
