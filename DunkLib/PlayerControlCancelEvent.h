#pragma once
#include "NetworkEvent.h"
#include "RaylibCereal.h"

struct PlayerControlCancelEvent : public NetworkEvent
{
	int m_cancelPlayer;
	int m_cancelEvent;
	int m_actualFrame;
	PlayerControlCancelEvent();
	PlayerControlCancelEvent(int playerID, int playerEventID, int cancelPlayer, int cancelEvent, int actualFrame);

	virtual void RunEvent(FrameState& frameState);
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_cancelPlayer, m_cancelEvent, m_actualFrame);
	}
};

CEREAL_REGISTER_TYPE(PlayerControlCancelEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, PlayerControlCancelEvent);
