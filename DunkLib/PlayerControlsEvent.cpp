#include "PlayerControlsEvent.h"
#include "NetworkFrame.h"
#include "CustomFrameStateWrapper.h"

PlayerControlsEvent::PlayerControlsEvent()
{

}

PlayerControlsEvent::PlayerControlsEvent(int playerID, int playerEventID, BasePlayerControlState& controlState) : m_state(controlState.CloneToHeap())
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void PlayerControlsEvent::RunEvent(FrameState& frameState)
{
	frameState.GetData()->SetControlState(m_state.get(), m_playerID);
}

Color PlayerControlsEvent::GetDebugColour()
{
	return GREEN;
}
int PlayerControlsEvent::GetDebugPriority()
{
	return 100;
}
