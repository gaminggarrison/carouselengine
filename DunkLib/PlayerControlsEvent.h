#pragma once
#include "NetworkEvent.h"
#include "RaylibCereal.h"
#include "BasePlayerControlState.h"

struct PlayerControlsEvent : public NetworkEvent
{
	std::shared_ptr<BasePlayerControlState> m_state;

	PlayerControlsEvent();
	PlayerControlsEvent(int playerID, int playerEventID, BasePlayerControlState& controlState);

	virtual void RunEvent(FrameState& frameState);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_state);
	}
};

CEREAL_REGISTER_TYPE(PlayerControlsEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, PlayerControlsEvent);