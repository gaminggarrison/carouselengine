#include "PlayerJoinedEvent.h"
#include "NetworkFrame.h"
#include "CustomFrameState.h"

PlayerJoinedEvent::PlayerJoinedEvent()
{

}

PlayerJoinedEvent::PlayerJoinedEvent(int playerID, int playerEventID, int joiningPlayer) : m_joiningPlayer(joiningPlayer)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void PlayerJoinedEvent::RunEvent(FrameState& frameState)
{
	frameState.GetData()->GetState<CustomFrameState>()->AddPlayer(m_joiningPlayer);
}

Color PlayerJoinedEvent::GetDebugColour()
{
	return RED;
}

int PlayerJoinedEvent::GetDebugPriority()
{
	return 1;
}