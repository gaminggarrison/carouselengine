#pragma once
#include "NetworkEvent.h"
#include "RaylibCereal.h"

struct PlayerJoinedEvent : public NetworkEvent
{
	int m_joiningPlayer;

	PlayerJoinedEvent();
	PlayerJoinedEvent(int playerID, int playerEventID, int joiningPlayer);

	virtual void RunEvent(FrameState& frameState);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_joiningPlayer);
	}
};

CEREAL_REGISTER_TYPE(PlayerJoinedEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, PlayerJoinedEvent);
