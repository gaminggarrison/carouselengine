#include "PlayerLeftEvent.h"
#include "NetworkFrame.h"
#include "CustomFrameState.h"

PlayerLeftEvent::PlayerLeftEvent()
{

}

PlayerLeftEvent::PlayerLeftEvent(int playerID, int playerEventID, int leavingPlayer) : m_leavingPlayer(leavingPlayer)
{
	m_playerID = playerID;
	m_playerEventID = playerEventID;
}

void PlayerLeftEvent::RunEvent(FrameState& frameState)
{
	frameState.GetData()->GetState<CustomFrameState>()->RemovePlayer(m_leavingPlayer);
}

Color PlayerLeftEvent::GetDebugColour()
{
	return DARKGRAY;
}

int PlayerLeftEvent::GetDebugPriority()
{
	return 2;
}
