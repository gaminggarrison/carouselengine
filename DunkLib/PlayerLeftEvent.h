#pragma once
#include "NetworkEvent.h"
#include "RaylibCereal.h"

struct PlayerLeftEvent : public NetworkEvent
{
	int m_leavingPlayer;

	PlayerLeftEvent();
	PlayerLeftEvent(int playerID, int playerEventID, int leavingPlayer);

	virtual void RunEvent(FrameState& frameState);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_leavingPlayer);
	}
};

CEREAL_REGISTER_TYPE(PlayerLeftEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, PlayerLeftEvent);
