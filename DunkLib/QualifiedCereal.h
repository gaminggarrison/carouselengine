#pragma once
//#define FULLNAME(className, ...) MINIGAME_NAMESPACE::className

// We need this indirection so the namespace define gets expanded correctly
#define QUALIFIED_CLASS_VERSION1(className, version) CEREAL_CLASS_VERSION(className, version)
#define QUALIFIED_CLASS_VERSION(className, version) QUALIFIED_CLASS_VERSION1(MINIGAME_NAMESPACE::className, version)

#define QUALIFIED_REGISTER_TYPE1(className) CEREAL_REGISTER_TYPE(className)
#define QUALIFIED_REGISTER_TYPE(className) QUALIFIED_REGISTER_TYPE1(MINIGAME_NAMESPACE::className)

#define QUALIFIED_REGISTER_POLYMORPHIC_RELATION1(baseClass, className) CEREAL_REGISTER_POLYMORPHIC_RELATION(baseClass, className)
#define QUALIFIED_REGISTER_POLYMORPHIC_RELATION(baseClass, className) QUALIFIED_REGISTER_POLYMORPHIC_RELATION1(baseClass, MINIGAME_NAMESPACE::className)