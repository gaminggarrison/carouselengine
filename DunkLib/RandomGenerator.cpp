#include "RandomGenerator.h"
#include <time.h>

/* The state word must be initialized to non-zero */
uint32_t xorshift32(struct xorshift32_state *state)
{
	/* Algorithm "xor" from p. 4 of Marsaglia, "Xorshift RNGs" */
	uint32_t x = state->a;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
	return state->a = x;
}

void RandomGenerator::Init()
{
	m_state.a = (uint32_t)time(nullptr);
}
constexpr uint32_t MAX_VALUE = 0xffffffff;
float RandomGenerator::GenerateFloat01()
{
	uint32_t nextVal = xorshift32(&m_state);
	return static_cast<float>(nextVal) / static_cast<float>(MAX_VALUE);
}
int RandomGenerator::GenerateInt(int min, int max)
{
	float val01 = GenerateFloat01();
	return (int)(min + (val01 * (max - min)));
}
float RandomGenerator::GenerateFloat(float min, float max)
{
	float val01 = GenerateFloat01();
	return min + (val01 * (max - min));
}

RandomGenerator* RandomGenerator::s_instance = nullptr;

void RandomGenerator::SetAsSharedInstance()
{
	s_instance = this;
}
RandomGenerator* RandomGenerator::Instance()
{
	return s_instance;
}
