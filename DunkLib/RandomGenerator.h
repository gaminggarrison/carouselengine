#pragma once
#include <stdint.h>
#include "cereal/cereal.hpp"

struct xorshift32_state {
	uint32_t a;
};

class RandomGenerator
{
	xorshift32_state m_state;

	static RandomGenerator* s_instance;

public:
	void Init();
	float GenerateFloat01();
	// Not inclusive of Max
	int GenerateInt(int min, int max);
	float GenerateFloat(float min, float max);

	template<class RandomAccessIterator>
	void Shuffle(RandomAccessIterator first, RandomAccessIterator last)
	{
		for (auto i = (last - first) - 1; i > 0; --i)
		{
			int randomPos = GenerateInt(0, (int)i);
			std::swap(first[i], first[randomPos]);
		}
	}

	void SetAsSharedInstance();
	static RandomGenerator* Instance();

	template<class Archive>
	void serialize(Archive& archive, std::uint32_t const version)
	{
		archive(CEREAL_NVP(m_state.a));
	}
};