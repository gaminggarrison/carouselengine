#pragma once

#include "raylib.h"
#include "cereal/cereal.hpp"

template<class Archive>
void serialize(Archive& archive, Vector2& v)
{
	archive(cereal::make_nvp("x", v.x), cereal::make_nvp("y", v.y));
}

template<class Archive>
void serialize(Archive& archive, Vector3& v)
{
	archive(cereal::make_nvp("x", v.x), cereal::make_nvp("y", v.y), cereal::make_nvp("z", v.z));
}

template<class Archive>
void serialize(Archive& archive, Color& c)
{
	archive(CEREAL_NVP(c.r), CEREAL_NVP(c.g), CEREAL_NVP(c.b), CEREAL_NVP(c.a));
}

template<class Archive>
void serialize(Archive& archive, RayRectangle& r)
{
	archive(r.x, r.y, r.width, r.height);
}