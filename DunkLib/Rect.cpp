#include "Rect.h"

bool Rect::IsInBox(Vector2 point)
{
	return point.x >= (float)x && point.y >= (float)y && point.x <= (float)(x + width) && point.y <= (float)(y + height);
}
bool Rect::DoesOverlap(Rect other)
{
	bool hasSeparation = (x + width < other.x) || (y + height < other.y) || (other.x + other.width < x) || (other.y + other.height < y);
	return !hasSeparation;
}
