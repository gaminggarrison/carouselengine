#pragma once
#include "Vector2Int.h"

struct Rect
{
	int x;
	int y;
	int width;
	int height;
	bool IsInBox(Vector2 point);
	bool DoesOverlap(Rect other);
	Vector2Int Min() { return { x, y }; }
	Vector2Int Max() { return { x + width, y + height }; }
	Vector2Int Mid() { return { x + width / 2, y + height / 2 }; }
	Vector2 MidV() { return { (float)x + (float)width * 0.5f, (float)y + (float)height * 0.5f }; }
	RayRectangle ToRayRectangle() { return { (float)x, (float)y, (float)width, (float)height }; }
	Vector2 BoxToWorld(Vector2 insideBox) { return { x + width * insideBox.x, y + height * insideBox.y }; }
	Rect AddBorder(int border) { return { x - border, y - border, width + border + border, height + border + border }; }
	Rect Translate(Vector2Int offset) { return { x + offset.x, y + offset.y, width, height }; }
	Rect CenterOnPoint(Vector2Int point) { return { point.x - (width / 2), point.y - (height / 2), width, height }; }
	Vector2 RestrictToRect(Vector2 point)
	{
		if (point.x < (float)x) point.x = (float)x;
		if (point.x > (float)(x + width)) point.x = (float)(x + width);
		if (point.y < (float)y) point.y = (float)y;
		if (point.y > (float)(y + height)) point.y = (float)(y + height);
		return point;
	}

	// EdgeX and EdgeY can be 0 to 1 for min or max edge on x and y axis
	Rect GetEdgeRect(float edgeX, float edgeY, int newWidth, int newHeight){
		return { (x + (int)(width * edgeX)) - (int)((1.0f - edgeX) * newWidth), (y + (int)(height * edgeY)) - (int)((1.0f - edgeY) * newHeight), width, height };
	}
};
