#pragma once
#include <stdlib.h> // size_t, malloc, free
#include <new> // bad_alloc, bad_array_new_length
#include <utility> // std::forward

#include "StaticStorage.h"
#include <stdio.h>

template <class T> struct RollbackAllocator {
	typedef T value_type;
	RollbackAllocator() noexcept { } // default ctor not required
	template <class U> RollbackAllocator(const RollbackAllocator<U>&) noexcept { }
	template <class U> bool operator==(
		const RollbackAllocator<U>&) const noexcept {
		return true;
	}
	template <class U> bool operator!=(
		const RollbackAllocator<U>&) const noexcept {
		return false;
	}

	T* allocate(const size_t n) const {
		if (n == 0) { return nullptr; }
		if (n > static_cast<size_t>(-1) / sizeof(T)) {
			throw std::bad_array_new_length();
		}
		void* const pv = StaticStorage::Alloc(n * sizeof(T));
		if (!pv)
		{
			size_t totalMemory = StaticStorage::GetAllocator()->getSize();
			size_t usedMemory = StaticStorage::GetAllocator()->getUsedMemory();
			size_t numAllocations = StaticStorage::GetAllocator()->getNumAllocations();
			int memoryUsagePercent = (int)(((float)usedMemory * 100.0f) / (float)totalMemory);
			printf("Bad alloc!  Total memory: %zu, Used: %zu, Allocations: %zu, Usage: %d%%\n", totalMemory, usedMemory, numAllocations, memoryUsagePercent);
			throw std::bad_alloc();
		}
		return static_cast<T*>(pv);
	}
	void deallocate(T* const p, size_t) const noexcept {
		StaticStorage::Dealloc(p);
	}
};

template<class T, class... Args>
std::shared_ptr<T> MakeAsT(Args&&... args)
{
	return std::allocate_shared<T, RollbackAllocator<T>>(RollbackAllocator<T>(), std::forward<Args>(args)...);
}
