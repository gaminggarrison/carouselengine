#include "RollbackHorizonEvent.h"
#include "NetworkFrame.h"
#include "NetworkStateControl.h"

RollbackHorizonEvent::RollbackHorizonEvent()
{

}

RollbackHorizonEvent::RollbackHorizonEvent(int horizonFrame) : m_horizonFrame(horizonFrame)
{
	NetworkEvent::InitDefault();
}

void RollbackHorizonEvent::RunEvent(FrameState& frameState)
{

}
void RollbackHorizonEvent::OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber)
{
	networkStateControl.SetRollbackHorizon(m_horizonFrame);
}

Color RollbackHorizonEvent::GetDebugColour()
{
	return DARKGREEN;
}

int RollbackHorizonEvent::GetDebugPriority()
{
	return 1;
}
bool RollbackHorizonEvent::DoRewindForEvent()
{
	return false;
}