#pragma once
#include "NetworkEvent.h"

struct RollbackHorizonEvent : public NetworkEvent
{
	int m_horizonFrame;
	RollbackHorizonEvent();
	RollbackHorizonEvent(int horizonFrame);

	virtual void RunEvent(FrameState& frameState);
	virtual void OnEventAdded(NetworkStateControl& networkStateControl, int frameNumber);
	virtual Color GetDebugColour();
	virtual int GetDebugPriority();
	virtual bool DoRewindForEvent();

	template<class Archive>
	void serialize(Archive& archive, const uint32_t version)
	{
		archive(cereal::base_class<NetworkEvent>(this), m_horizonFrame);
	}
};

CEREAL_REGISTER_TYPE(RollbackHorizonEvent);
CEREAL_REGISTER_POLYMORPHIC_RELATION(NetworkEvent, RollbackHorizonEvent);
