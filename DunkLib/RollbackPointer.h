#pragma once
#include "RollbackAllocator.h"

// Will handle deserialisation of the pointer into the rollback storage.
// Use the base type for the pointer, and construct with the derived type.
// Your types will need a Clone() function
template<typename T>
struct RollbackPointer
{
	std::shared_ptr<T> m_value;

	RollbackPointer() {}
	~RollbackPointer()
	{
		//printf("Destroying rollback pointer!\n");
		m_value = nullptr;
	}

	template<typename S, class... Args>
	S& Construct(Args&&... args)
	{
		std::shared_ptr<S> newS = MakeAsT<S>(std::forward<Args>(args)...);
		m_value = newS;
		return *(newS.get());
	}

	RollbackPointer<T>& operator=(const std::shared_ptr<T>& t)
	{
		if (m_value.get() != nullptr && m_value.use_count() == 0)
		{
			printf("Uh oh?");
		}
		m_value = t;
		return *this;
	}
	/*RollbackPointer<T>& operator=(const RollbackPointer<T>& t)
	{
		//m_value = t.m_value;
		swap(t);
		return *this;
	}*/

	T* operator->()
	{
		return m_value.get();
	}
	T* get()
	{
		return m_value.get();
	}

	// https://www.informit.com/articles/article.aspx?p=31529&seqNum=7 - it makes a point that this implicit conversion is dangerous, so might just skip it
	/*operator T*() // User-defined implicit conversion to T*
	{
		return m_value.get();
	}
	operator void*() // Implicit conversion to void* to make a delete call intentially ambigious, so disallowing it
	{
		return m_value.get();
	}*/

	bool operator==(const T* rhs) const
	{
		return m_value.get() == rhs;
	}
	bool operator!=(const T* rhs) const
	{
		return !operator==(rhs);
	}
	bool operator==(const std::shared_ptr<T> rhs) const
	{
		return m_value == rhs;
	}
	bool operator!=(const std::shared_ptr<T> rhs) const
	{
		return !operator==(rhs);
	}
	bool operator==(const RollbackPointer<T> rhs) const
	{
		return m_value == rhs.m_value;
	}
	bool operator!=(const RollbackPointer<T> rhs) const
	{
		return !operator==(rhs);
	}

	template<class Archive>
	void save(Archive& archive) const
	{
		archive(m_value);
	}

	template<class Archive>
	void load(Archive& archive)
	{
		archive(m_value);
		if (m_value != nullptr)
		{
			m_value = (std::shared_ptr<T>)m_value->Clone();
		}
	}
};
