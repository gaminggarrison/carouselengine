#pragma once
#include <string>
#include "RollbackAllocator.h"
#include "cereal/cereal.hpp"

typedef std::basic_string<char, std::char_traits<char>, RollbackAllocator<char>> RollbackableString;

struct RollbackString
{
	RollbackableString m_data;

	RollbackString(const char* s = nullptr)
	{
		Set(s);
	}

	RollbackString(std::string s)
	{
		Set(s.c_str());
	}

	void Set(const char* s)
	{
		if (s == nullptr)
		{
			m_data.clear();
		}
		else
		{
			m_data = RollbackableString(s);
		}
	}

	operator const char*()
	{
		return m_data.c_str();
	}

	const char* c_str() const
	{
		return m_data.c_str();
	}
	bool empty() const
	{
		return m_data.empty();
	}

	template<class Archive>
	std::string save_minimal(Archive const &) const
	{
		return std::string(m_data.c_str());
	}
	template<class Archive>
	void load_minimal(Archive const &, std::string const & value)
	{
		Set(value.c_str());
	}
};


/*
namespace cereal
{
	//! Serialization for basic_string types, if binary data is supported
	template <class Archive, class CharT, class Traits,
	cereal::traits::DisableIf<traits::is_text_archive<Archive>::value> = traits::sfinae>
		void CEREAL_SAVE_FUNCTION_NAME(Archive& ar, const RollbackString& str)
	{
		// Save number of chars + the data
		ar(make_size_tag(static_cast<size_type>(str.size())));
		ar(binary_data(str.data(), str.size() * sizeof(CharT)));
	}

	template <class Archive, class CharT, class Traits,
		cereal::traits::EnableIf<traits::is_text_archive<Archive>::value> = traits::sfinae>
		void CEREAL_SAVE_FUNCTION_NAME(Archive & ar, const RollbackString& str)
	{
		ar(make_size_tag(static_cast<size_type>(str.size())));
		ar(binary_data(str.data(), str.size() * sizeof(CharT)));
	}


	//! Serialization for basic_string types, if binary data is supported
	template <class Archive, class CharT, class Traits,
		cereal::traits::DisableIf<traits::is_text_archive<Archive>::value> = traits::sfinae>
		void CEREAL_LOAD_FUNCTION_NAME(Archive& ar, RollbackString& str)
	{
		// Load number of chars + the data
		size_t size = 0;
		ar(size);
		str = RollbackString(size, "");
		ar(binary_data(str.data(), size * sizeof(CharT)));
	}

	template <class Archive, class CharT, class Traits,
		cereal::traits::EnableIf<traits::is_text_archive<Archive>::value> = traits::sfinae>
		void CEREAL_LOAD_FUNCTION_NAME(Archive & ar, RollbackString& str)
	{
		// Load number of chars + the data
		size_t size = 0;
		ar(size);
		str = RollbackString(size, "");
		ar(binary_data(str.data(), size * sizeof(CharT)));
	}
}*/