#pragma once
#include <sstream>
#include "cereal/archives/binary.hpp"
#include <streambuf>
#include <iostream>

using namespace std;

class SerializationUtils
{
	static stringstream ss;

public:
	static stringstream* GetStringStream();
	template<class T>
	static inline std::stringstream* WriteToStream(T& object)
	{
		{
			cereal::BinaryOutputArchive oarchive(ss);
			oarchive(object);
		}
		return &ss;
	}

	template<class T>
	static inline void ReadFromStream(T& object)
	{
		{
			cereal::BinaryInputArchive iarchive(ss); // Create an input archive

			iarchive(object);
		}
		ss.seekg(0, ss.beg);
	}

	// See https://stackoverflow.com/questions/33726072/how-to-serialize-a-json-object-without-enclosing-it-in-a-sub-object-using-cereal
	template< typename Class, typename Archive,
		typename std::enable_if< cereal::traits::has_member_serialize<Class, Archive>::value>::type* = nullptr>
		inline static void serializeHelper(Class& cl, Archive& ar)
	{
		cl.serialize(ar);
	}

	template< typename Class, typename Archive,
		typename std::enable_if< cereal::traits::has_member_save<Class, Archive>::value>::type* = nullptr>
		inline static void serializeHelper(Class& cl, Archive& ar)
	{
		cl.save(ar);
	}

	// More version could follow for remaining serialization types (external, versioned...)

	template< typename Class, typename Archive,
		typename std::enable_if< cereal::traits::has_member_serialize<Class, Archive>::value>::type* = nullptr>
		inline static void deserializeHelper(Class& cl, Archive& ar)
	{
		cl.serialize(ar);
	}

	template< typename Class, typename Archive,
		typename std::enable_if< cereal::traits::has_member_load<Class, Archive>::value>::type* = nullptr>
		inline static void deserializeHelper(Class& cl, Archive& ar)
	{
		cl.load(ar);
	}

	// More version could follow for remaining deserialization types (external, versioned...)
};
