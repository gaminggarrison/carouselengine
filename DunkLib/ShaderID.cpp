#include "ShaderID.h"
#include "ConfigData.h"

ShaderID::ShaderID() : ConfigReference()
{

}

ShaderID::ShaderID(const char* name) : ConfigReference(name)
{

}

ShaderID::ShaderID(int id) : ConfigReference(id)
{

}

ConfigSet<Shader>& ShaderID::GetConfigSet()
{
	return ConfigData::Instance().m_shaders;
}

ShaderID ShaderID::Unknown = ShaderID();

#include "ConfigReference.cpp"
template struct ConfigReference<ShaderID, Shader>;