#pragma once
#include "ConfigReference.h"

struct Shader;

struct ShaderID : ConfigReference<ShaderID, Shader>
{
	ShaderID();
	ShaderID(const char* name);
	ShaderID(int id);
	static ConfigSet<Shader>& GetConfigSet();
	static ShaderID Unknown;
};
