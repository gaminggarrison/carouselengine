#include "SimpleControlState.h"
#include "raylib.h"
#include "raymath.h"
#include "CameraUtils.h"
#include "RollbackAllocator.h"

void SimpleControlState::SetFromLocalInput(Camera2D& camera)
{
	m_left = IsKeyDown(KEY_A) || IsKeyDown(KEY_LEFT);
	m_right = IsKeyDown(KEY_D) || IsKeyDown(KEY_RIGHT);
	m_up = IsKeyDown(KEY_W) || IsKeyDown(KEY_UP);
	m_down = IsKeyDown(KEY_S) || IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_SPACE);

	m_mouseWorldPosition = CameraUtils::ScreenToWorldPos(GetMousePosition());
	m_lastKnownMouseWorldPosition = m_mouseWorldPosition;

	m_mouse0 = IsMouseButtonDown(0);
	m_mouse0Pressed = IsMouseButtonPressed(0);
	m_mouse1 = IsMouseButtonDown(1);
	m_mouse1Pressed = IsMouseButtonPressed(1);
}

void SimpleControlState::ResetOneFrameControls()
{
	m_mouse0Pressed = false;
	m_mouse1Pressed = false;
}
void SimpleControlState::UpdateClientSideControlPrediction()
{
	m_lastKnownMouseWorldPosition = m_mouseWorldPosition;
}
void SimpleControlState::SetPredictedMousePosition(Vector2& worldMousePosition)
{
	m_lastKnownMouseWorldPosition = worldMousePosition;
}
bool SimpleControlState::NeedsSending(const SimpleControlState& last)
{
	// By default, we always send if a mouse button is down (because the mouse position will probably be important!)
	// Ideally we only need to send the mouse position on a frame where it's important (like firing a bullet)
	// This function could be extended to check the game state (or to not use the mouse :))
	return m_left != last.m_left || m_right != last.m_right || m_up != last.m_up || m_down != last.m_down
		|| m_mouse0 || m_mouse0 != last.m_mouse0 || m_mouse0Pressed || m_mouse1 || m_mouse1 != last.m_mouse1 || m_mouse1Pressed;
}
std::shared_ptr<BasePlayerControlState> SimpleControlState::Clone()
{
	return MakeAsT<SimpleControlState>(*this);
}
std::shared_ptr<BasePlayerControlState> SimpleControlState::CloneToHeap()
{
	return std::make_shared<SimpleControlState>(*this);
}