#pragma once

#include "BasePlayerControlState.h"
#include "RaylibCereal.h"

struct SimpleControlState : public BasePlayerControlState
{
public:
	bool m_up = false;
	bool m_down = false;
	bool m_left = false;
	bool m_right = false;
	Vector2 m_mouseWorldPosition;

	// Don't rely on this - it will not necessarily be in sync across clients.
	// Designed to be used for visual 'aiming' that doesn't affect the game (i.e. a gun that pivots visually but only does something when the mouse is down)
	Vector2 m_lastKnownMouseWorldPosition;
	bool m_mouse0 = false;
	bool m_mouse0Pressed = false;
	bool m_mouse1 = false;
	bool m_mouse1Pressed = false;

	template<class Archive>
	void serialize(Archive& archive, std::uint32_t const version)
	{
		archive(m_up, m_down, m_left, m_right, m_mouseWorldPosition, m_mouse0, m_mouse0Pressed, m_mouse1, m_mouse1Pressed);
	}

	virtual std::shared_ptr<BasePlayerControlState> Clone();
	virtual std::shared_ptr<BasePlayerControlState> CloneToHeap();

	virtual void ResetOneFrameControls();
	virtual void UpdateClientSideControlPrediction();
	virtual void SetFromLocalInput(Camera2D& camera);
	virtual void SetPredictedMousePosition(Vector2& worldMousePosition);

	bool NeedsSending(const SimpleControlState& last);
};
CEREAL_CLASS_VERSION(SimpleControlState, 2);
CEREAL_REGISTER_TYPE(SimpleControlState);
CEREAL_REGISTER_POLYMORPHIC_RELATION(BasePlayerControlState, SimpleControlState);