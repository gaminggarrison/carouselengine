#include "SoundID.h"
#include "ConfigData.h"

SoundID::SoundID() : ConfigReference()
{

}

SoundID::SoundID(const char* name) : ConfigReference(name)
{

}

SoundID::SoundID(int id) : ConfigReference(id)
{

}

ConfigSet<Sound>& SoundID::GetConfigSet()
{
	return ConfigData::Instance().m_sounds;
}

SoundID SoundID::Unknown = SoundID();

#include "ConfigReference.cpp"
template struct ConfigReference<SoundID, Sound>;