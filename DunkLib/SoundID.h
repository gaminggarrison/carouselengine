#pragma once
#include "ConfigReference.h"

struct Sound;

struct SoundID : ConfigReference<SoundID, Sound>
{
	SoundID();
	SoundID(const char* name);
	SoundID(int id);
	static ConfigSet<Sound>& GetConfigSet();
	static SoundID Unknown;
};
