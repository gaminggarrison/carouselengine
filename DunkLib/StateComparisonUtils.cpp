#include "StateComparisonUtils.h"
#include <sstream>
#include "cereal/archives/json.hpp"
#include "FrameState.h"
#include <fstream>
#include "CustomFrameStateWrapper.h"

std::stringstream s_stateComparisonStreamA;
std::stringstream s_stateComparisonStreamB;

#if EMSCRIPTEN
#ifdef NODERAWFS
#define CWD ""
#else
#define CWD "/working/"
#endif
#else
#define CWD ""
#endif

void StateComparisonUtils::DumpComparison(FrameState& clientFrame, FrameState& serverFrame)
{
	s_stateComparisonStreamA.seekp(0, s_stateComparisonStreamA.beg);
	{
		cereal::JSONOutputArchive oarchive(s_stateComparisonStreamA); // Create an output archive
		CustomFrameStateWrapper* clientSharedGame = clientFrame.GetData();
		oarchive(*clientSharedGame);
	}
	s_stateComparisonStreamA.seekp(0, s_stateComparisonStreamA.beg);

	// Now load from the stringstream against the server frame
	s_stateComparisonStreamB.seekg(0, s_stateComparisonStreamB.beg);
	{
		cereal::JSONOutputArchive oarchive(s_stateComparisonStreamB);
		CustomFrameStateWrapper* serverSharedGame = serverFrame.GetData();
		oarchive(*serverSharedGame);
	}
	s_stateComparisonStreamB.seekg(0, s_stateComparisonStreamB.beg);

	// Output to files for manual comparison
	std::ofstream outFileA(CWD "ClientFrame.json", std::ofstream::out);
	outFileA << s_stateComparisonStreamA.rdbuf();
	outFileA.close();

	std::ofstream outFileB(CWD "ServerFrame.json", std::ofstream::out);
	outFileB << s_stateComparisonStreamB.rdbuf();
	outFileB.close();

	// Now we compare the text streams!
	//std::vector<std::string> linesA;
	//std::vector<std::string> linesB;

	/*char comparisonA[512];
	char comparisonB[512];

	std::vector<std::string> differences;
	std::streamsize nA;
	std::streamsize nB;
	do
	{
		nA = s_stateComparisonStreamA.rdbuf()->sgetn(comparisonA, sizeof(comparisonA));
		nB = s_stateComparisonStreamB.rdbuf()->sgetn(comparisonB, sizeof(comparisonB));

		std::streamsize max = nA > nB ? nA : nB;
		std::streamsize min = nA < nB ? nA : nB;
		for (int i = 0; i < min; i++)
		{
			if (comparisonA[i] != comparisonB[i])
			{
				differences.emplace_back(comparisonA, comparisonA + nA);
				differences.emplace_back(comparisonB, comparisonB + nB);
				break;
			}
		}
	} while (differences.empty() && nA > 0 && nB > 0);

	for (int i = 0; i < differences.size(); i++)
	{
		printf("Difference found: %s\n", differences[i].c_str());
	}*/
}