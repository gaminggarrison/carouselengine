#pragma once

class FrameState;

namespace StateComparisonUtils
{
	void DumpComparison(FrameState& clientFrame, FrameState& serverFrame);
}