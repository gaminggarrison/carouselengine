#include "StateMachine.h"
#include "GameState.h"
#include <stdio.h>

void StateMachine::Init()
{
	//m_stack.clear();
	s_instance = this;
}

void StateMachine::Cleanup()
{
	for (int i = (int)m_stack.size() - 1; i >= 0; i--)
	{
		m_stack[i].get()->Cleanup();
	}
	m_stack.clear();
}

void StateMachine::PopState()
{
	if (m_stack.empty())
	{
		printf("Woops, the game state stack is empty!\n");
		return;
	}
	GetHead().Cleanup();
	m_stack.pop_back();
}
bool StateMachine::IsEmpty()
{
	return m_stack.empty();
}
GameState& StateMachine::GetHead()
{
	return *m_stack[m_stack.size() - 1].get();
}
void StateMachine::Update(float deltaTime)
{
	GetHead().Update(deltaTime);
}
void StateMachine::Draw()
{
	GetHead().Draw();
}
StateMachine* StateMachine::s_instance = nullptr;
StateMachine* StateMachine::Instance()
{
	return s_instance;
}