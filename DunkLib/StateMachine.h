#pragma once
#include "static_vector.hpp"

#include "GameState.h"

class StateMachine
{
	chobo::static_vector<GenericGameState, 8> m_stack;
	static StateMachine* s_instance;

public:
	static StateMachine* Instance();
	void Init();
	void Cleanup();

	template<class T> T& PushState();
	void PopState();
	bool IsEmpty();
	GameState& GetHead();
	void Update(float deltaTime);
	void Draw();
};

template<class T>
inline T& StateMachine::PushState()
{
	m_stack.push_back(GenericGameState());
	GenericGameState& head = m_stack[m_stack.size() - 1];
	new(head.get()) T;
	head->Init();
	return *((T*)head.get());
}
