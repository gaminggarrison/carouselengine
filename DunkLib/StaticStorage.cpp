#include "StaticStorage.h"
#include <malloc.h>
#include <stdio.h>
#include <string.h> // For memcpy
#include "PointerMath.h"

StaticStorage* StaticStorage::s_storage;

StaticStorage::StaticStorage(size_t size) : m_reusableBuffer(malloc(size)),/* m_freeListAllocator(size, m_reusableBuffer), */m_bufferSize(size)
{
	size_t usableSize = size - sizeof(FreeListAllocator);
	void* usableMemoryLocation = pointer_math::add(m_reusableBuffer, sizeof(FreeListAllocator));

	memset(m_reusableBuffer, 0, size); // Clear the static storage to 0
	FreeListAllocator example(usableSize, usableMemoryLocation);
	memcpy(m_reusableBuffer, (void*)&example, sizeof(FreeListAllocator));

	m_freeListAllocator = ((FreeListAllocator*)m_reusableBuffer);
	printf("Allocating StaticStorage memory!\n");
}

void StaticStorage::Init(size_t size)
{
	s_storage = new StaticStorage(size);
}

/*void StaticStorage::SetInstance(StaticStorage* storage)
{
	s_storage = storage;
}
StaticStorage* StaticStorage::GetInstance()
{
	return s_storage;
}*/

void* StaticStorage::Alloc(size_t size)
{
	return s_storage->m_freeListAllocator->allocate(size, 4);
}
void StaticStorage::Dealloc(void* pointer)
{
	return s_storage->m_freeListAllocator->deallocate(pointer);
}
void* StaticStorage::GetStorage()
{
	return s_storage->m_reusableBuffer;
}
size_t StaticStorage::GetStorageSize()
{
	return s_storage->m_bufferSize;
}
void StaticStorage::ResetStorage()
{
	s_storage->m_freeListAllocator->Reset();
}
const FreeListAllocator* StaticStorage::GetAllocator()
{
	return s_storage->m_freeListAllocator;
}
