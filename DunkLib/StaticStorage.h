#pragma once
#include "FreeListAllocator.h"
#include <memory>

class StaticStorage
{
	void* m_reusableBuffer;
	size_t m_bufferSize;
	FreeListAllocator* m_freeListAllocator;

	StaticStorage(size_t size);

	static StaticStorage* s_storage;

public:
	static void Init(size_t size);
	//static void SetInstance(StaticStorage* storage);
	//static StaticStorage* GetInstance();
	static void* Alloc(size_t size);
	static void Dealloc(void* pointer);
	static void* GetStorage();
	static size_t GetStorageSize();
	static void ResetStorage();
	static const FreeListAllocator* GetAllocator();
};