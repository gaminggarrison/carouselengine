#include "SyncedClock.h"
#include "NetworkEventWrapper.h"
#include "NetworkStateControl.h"
#include "PingSendEvent.h"
#include "NetUtils.h"
#include "MathUtils.h"
#include "DebugUtils.h"

void SyncedClock::Update(float deltaTime, NetworkStateControl& stateControl)
{
	if (DebugUtils::s_debugPause)
	{
		return;
	}
	// When playing solo, cap out at a deltatime of 1 second, in case the player paused the game (debugging or moving the window)
	if (deltaTime > 1.0f && (!NetUtils::IsConnected() || (NetUtils::IsHost() && !NetUtils::IsDedicatedHost() && NetUtils::GetConnectedPlayerCount() <= 1)))
	{
		deltaTime = 1.0f;
	}
	m_internalTimer += deltaTime;

	if (NetUtils::IsClient())
	{
		int milliseconds = (int)(deltaTime * 1000);
		m_pingTimer += milliseconds;
		if (m_pingTimer > 100)
		{
			m_pingTimer -= 100;
			stateControl.AddNetworkEventAndSend(new PingSendEvent(NetUtils::GetLocalPlayerID(), NetworkEvent::GetNextEventID(), m_internalTimer));
		}
	}

	if (m_averageDiffFromServer != 0)
	{
		double_t change = -m_averageDiffFromServer;
		double_t minChange = 32.0 / 1000.0; // 32ms min offset before we start adjusting (2 60fps frames worth)
		if (abs(change) < minChange)
		{
			return;
		}

		double_t target = 0.0;
		double_t result = LerpSmoothDouble(m_averageDiffFromServer, target, 0.75, deltaTime);
		change = result - m_averageDiffFromServer;

		m_averageDiffFromServer += change;

		m_internalTimer += change;
	}
}

void SyncedClock::OnPingResponse(double_t originalSendTime, double_t serverTime)
{
	double_t currentTime = GetInternalTime();
	double_t roundTripTime = (currentTime - originalSendTime);

	m_averageRoundTripTime = (m_averageRoundTripTime * 0.8) + (roundTripTime * 0.2);
	//m_averageRoundTripTime = ((m_averageRoundTripTime * 0.5f) + (roundTripTime * 100)) / 1000;
	double_t timeDiff = currentTime - serverTime;
	double_t diffFromServer = timeDiff - (roundTripTime * 0.5);

	m_averageDiffFromServer = (m_averageDiffFromServer * 0.5) + (diffFromServer * 0.5);
	//m_averageDiffFromServer = ((m_averageDiffFromServer * 900) + (diffFromServer * 100)) / 1000;
	/*printf("Ping response!  Diff = %.2f, RTT = %.2f (Avg: %.2f), diff from server = %.2f (Avg: %.2f)\n",
		timeDiff * 1000, roundTripTime * 1000, m_averageRoundTripTime * 1000, diffFromServer * 1000, m_averageDiffFromServer * 1000);*/
}
void SyncedClock::SetInternalTime(double_t time)
{
	m_internalTimer = time;
	m_averageDiffFromServer = 0.0;
}
double_t SyncedClock::GetInternalTime()
{
	return m_internalTimer;
}

int SyncedClock::GetFramesToSimulate(int currentFrame, NetworkStateControl& stateControl)
{
	int framesToSimulate = 1;
	double_t nextFrameTime = FrameToClock(currentFrame + 1);
	double_t frameTime = 1.0 / 60.0;

	int framesBehind = (int)((m_internalTimer - nextFrameTime) / frameTime);
	if (framesBehind > 60) // If we're more than a second behind, just gun it!  (likely to happen if tabbed away in web)
	{
		framesToSimulate = 60;
	}
	else if (nextFrameTime < (m_internalTimer - (frameTime * 2)))
	{
		framesToSimulate = 2;
	}
	else if (nextFrameTime > m_internalTimer + (frameTime * 2)) // Stop if too far ahead
	{
		framesToSimulate = 0;
	}

	assert(currentFrame <= stateControl.GetRollbackHorizon());

	if (currentFrame + framesToSimulate > stateControl.GetRollbackHorizon())
	{
		framesToSimulate = stateControl.GetRollbackHorizon() - currentFrame;
	}

	return framesToSimulate;
}

int SyncedClock::ClockToFrame(double_t time)
{
	return RoundToNearestInt((float)(time * 60.0));
}
double_t SyncedClock::FrameToClock(int frame)
{
	return (double)(frame) / 60.0;
}
double_t SyncedClock::GetAverageDiffFromServer()
{
	return m_averageDiffFromServer;
}