#pragma once
#include <cstdint>

// So I don't want to include a windows SDK dependency just for this, should hopefully be 64 bit :o)
typedef double double_t;
//#include <corecrt_math.h> 

class NetworkStateControl;

class SyncedClock
{
	double_t m_internalTimer = 0;
	uint32_t m_pingTimer = 0;
	double_t m_averageDiffFromServer = 0.0;
	double_t m_averageRoundTripTime = 0.0;
public:
	void Update(float deltaTime, NetworkStateControl& stateControl);
	void SetInternalTime(double_t time);
	double_t GetInternalTime();
	void OnPingResponse(double_t originalSendTime, double_t serverTime);
	int ClockToFrame(double_t time);
	int GetFramesToSimulate(int currentFrame, NetworkStateControl& stateControl);
	double_t FrameToClock(int frame);
	double_t GetAverageDiffFromServer();
};