#include "TextureID.h"
#include "ConfigData.h"

TextureID::TextureID() : ConfigReference()
{

}

TextureID::TextureID(const char* name) : ConfigReference(name)
{

}

TextureID::TextureID(int id) : ConfigReference(id)
{

}

ConfigSet<Texture2D>& TextureID::GetConfigSet()
{
	return ConfigData::Instance().m_textures;
}

TextureID TextureID::Unknown = TextureID();

#include "ConfigReference.cpp"
template struct ConfigReference<TextureID, Texture2D>;