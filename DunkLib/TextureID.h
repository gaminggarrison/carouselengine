#pragma once
#include "ConfigReference.h"

#include "raylib.h"

struct TextureID : ConfigReference<TextureID, Texture2D>
{
	TextureID();
	TextureID(const char* name);
	TextureID(int id);
	static ConfigSet<Texture2D>& GetConfigSet();
	static TextureID Unknown;
};
