#include "TimerUtils.h"

namespace TimerUtils
{
	int s_currentFrameNumber = 0;

	int GetCurrentFrameNumber()
	{
		return s_currentFrameNumber;
	}

	void SetCurrentFrameNumber(int frame)
	{
		s_currentFrameNumber = frame;
	}
}