#pragma once

namespace TimerUtils
{
	int GetCurrentFrameNumber();

	// Set internally by the Carousel system
	void SetCurrentFrameNumber(int frame);
}