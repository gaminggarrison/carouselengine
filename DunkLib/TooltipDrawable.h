#pragma once

class TooltipDrawable
{
public:
	virtual void DrawTooltip(int x, int y) const = 0;
};
