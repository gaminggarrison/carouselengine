#include "TooltipUtils.h"
#include "UIManager.h"
#include "GUIUtils.h"

const TooltipDrawable* TooltipUtils::s_toDraw = nullptr;
std::string TooltipUtils::s_toDrawMessage = "";
Rect TooltipUtils::s_toDrawBox = { 0,0,0,0 };

void TooltipUtils::TooltipBox(Rect rect, const TooltipDrawable& object)
{
	Vector2 mousePos = UIManager::GetUIMousePosition();
	if (rect.IsInBox(mousePos))
	{
		s_toDraw = std::addressof(object);
		s_toDrawMessage = "";
		s_toDrawBox = rect;
	}
}
void TooltipUtils::TooltipBox(Rect rect, std::string message)
{
	Vector2 mousePos = UIManager::GetUIMousePosition();
	if (rect.IsInBox(mousePos))
	{
		s_toDraw = nullptr;
		s_toDrawMessage = message;
		s_toDrawBox = rect;
	}
}
void TooltipUtils::AddTooltip(std::string message)
{
	s_toDraw = nullptr;
	s_toDrawMessage = message;

	Vector2 mousePos = UIManager::GetUIMousePosition();
	// Create a fake box so other logic can potentially clear it
	s_toDrawBox = { (int)(mousePos.x - 4), (int)(mousePos.y - 4), 8, 8 };
}
void TooltipUtils::AddTooltip(const TooltipDrawable& object)
{
	s_toDraw = std::addressof(object);
	s_toDrawMessage = "";

	Vector2 mousePos = UIManager::GetUIMousePosition();
	// Create a fake box so other logic can potentially clear it
	s_toDrawBox = { (int)(mousePos.x - 4), (int)(mousePos.y - 4), 8, 8 };
}
void TooltipUtils::ClearBox(Rect rect)
{
	if (s_toDraw != nullptr && rect.DoesOverlap(s_toDrawBox))
	{
		s_toDraw = nullptr;
		s_toDrawMessage = "";
	}
}
void TooltipUtils::DrawTooltips()
{
	if (s_toDraw != nullptr)
	{
		Vector2 mousePos = UIManager::GetUIMousePosition();
		s_toDraw->DrawTooltip((int)mousePos.x, (int)mousePos.y);
	}
	else if (!s_toDrawMessage.empty())
	{
		Vector2 mousePos = UIManager::GetUIMousePosition();
		GUIUtils::GUITooltip((int)mousePos.x, (int)mousePos.y, s_toDrawMessage.c_str());
	}
	s_toDraw = nullptr;
	s_toDrawMessage = "";
}