#pragma once
#include "TooltipDrawable.h"
#include <vector>
#include <string>
#include "Rect.h"

using namespace std;

class TooltipUtils
{
	const static TooltipDrawable* s_toDraw;
	static std::string s_toDrawMessage;
	static Rect s_toDrawBox;
public:
	static void TooltipBox(Rect rect, const TooltipDrawable& object);
	static void TooltipBox(Rect rect, std::string message);
	static void AddTooltip(const TooltipDrawable& object);
	static void AddTooltip(std::string message);
	static void ClearBox(Rect rect);
	static void DrawTooltips();
};