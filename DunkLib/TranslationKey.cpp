#include "TranslationKey.h"
#include "TranslationUtils.h"

const char* TranslationKey::GetValue()
{
	return TranslationUtils::GetString(m_key.c_str());
}
bool TranslationKey::IsEmpty()
{
	return m_key.empty();
}