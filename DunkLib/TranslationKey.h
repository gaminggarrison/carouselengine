#pragma once
#include <string>
using namespace std;

struct TranslationKey
{
	string m_key;

	template<class Archive>
	string save_minimal(Archive const &) const
	{
		return m_key;
	}
	template<class Archive>
	void load_minimal(Archive const &, string const & value)
	{
		m_key = value;
	}

	const char* GetValue();
	bool IsEmpty();
};