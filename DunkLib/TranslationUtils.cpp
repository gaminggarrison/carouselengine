#include "TranslationUtils.h"
#include "raylib.h"
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

#include "DebugUtils.h"
#include "FolderUtils.h"
using namespace std;

int TranslationUtils::s_currentLanguageID = 1;
int TranslationUtils::s_languageCount = 0;
const char* TranslationUtils::s_languageFilePath = "Assets/Translation.csv";
unordered_map<string, vector<string>> TranslationUtils::s_translationData = unordered_map<string, vector<string>>();

std::string ReplaceAll(std::string str, const std::string& from, const std::string& to)
{
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos)
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length();
	}
	return str;
}

void TranslationUtils::Initialise()
{
	s_translationData.clear();
	if (FileExists(s_languageFilePath))
	{
		printf("Translation file found!\n");
		char* text = LoadText(s_languageFilePath);
		string data = string(text);

		printf("File is %d long\n", (int)data.size());
		istringstream iss(data);
		vector<string> lines;
		string line;
		while (getline(iss, line, '\n'))
		{
			lines.push_back(line);
		}

		std::string escapedNewline = std::string("\\n");
		std::string newLine = std::string("\n");

		for (int i = 0; i < lines.size(); i++)
		{
			string& lineText = lines[i];
			istringstream lineStream(lineText);

			vector<string> lineData;
			string word;
			while (getline(lineStream, word, ','))
			{
				word = ReplaceAll(word, escapedNewline, newLine);
				lineData.push_back(word);
			}
			if (s_languageCount == 0)
			{
				s_languageCount = (int)lineData.size();
			}
			s_translationData[lineData[0]] = lineData;
		}
	}
	else
	{
		printf("Translation file not found :(\n");
	}
}
bool TranslationUtils::HasKey(const char* key)
{
	auto location = s_translationData.find(string(key));
	return location != s_translationData.end();
}
const char* TranslationUtils::GetString(const char* key)
{
	auto location = s_translationData.find(string(key));
	if (location != s_translationData.end())
	{
		vector<string>& strings = location->second;
		if (s_currentLanguageID >= strings.size())
		{
			printf("We have no translation for key %s in this language\n", key);
			return key;
		}
		return strings[s_currentLanguageID].c_str();
	}
	else
	{
		printf("Key %s not found in translation map\n", key);
		return key;
	}
}
void TranslationUtils::NextLanguage()
{
	s_currentLanguageID = (s_currentLanguageID + 1) % s_languageCount;
}

bool TranslationUtils::AddKeyToFileIfNotExisting(const char* key)
{
	return AddKeyToFileIfNotExisting(key, "");
}

bool TranslationUtils::AddKeyToFileIfNotExisting(const char* key, const char* englishString)
{
	if (DebugUtils::s_debugInstance >= 0)
	{
		FolderUtils::WorkingDirectorySwitcheroo2(&Initialise);
	}
	else
	{
		Initialise();
	}

	key = TextToUpper(key);
	std::string keyAsString = std::string(key);
	std::replace(keyAsString.begin(), keyAsString.end(), ' ', '_');
	key = keyAsString.c_str();

	if (HasKey(key))
	{
		return false;
	}

	if (DebugUtils::s_debugInstance >= 0)
	{
		FolderUtils::WorkingDirectorySwitcheroo2([key, englishString]() { ActuallyAddKeyToFile(key, englishString); });
	}
	else
	{
		ActuallyAddKeyToFile(key, englishString);
	}

	return true;
}

void TranslationUtils::ActuallyAddKeyToFile(const char* key, const char* englishString)
{
	char* text = LoadText(s_languageFilePath);
	string data = string(text);
	string lineData = "\n" + std::string(key) + "," + englishString;
	for (int i = 2; i < s_languageCount; i++)
	{
		lineData = lineData + "," + englishString;
	}
	data = data + lineData;
	SaveFileText(s_languageFilePath, (char*)data.c_str());

	TranslationUtils::Initialise();
}