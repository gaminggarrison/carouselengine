#pragma once
#include <vector>
#include <unordered_map>

using namespace std;

class TranslationUtils
{
	static int s_currentLanguageID;
	static int s_languageCount;
	static unordered_map<string, vector<string>> s_translationData;

	static const char* s_languageFilePath;

	static void ActuallyAddKeyToFile(const char* key, const char* englishString);
public:
	static void Initialise();
	static bool HasKey(const char* key);
	static const char* GetString(const char* key);
	static void NextLanguage();
	static bool AddKeyToFileIfNotExisting(const char* key); // Adds a key line directly to the CSV if the key isn't already there
	static bool AddKeyToFileIfNotExisting(const char* key, const char* englishString); // Adds a key line directly to the CSV if the key isn't already there
};
