#include "UIManager.h"
#include "DrawUtils.h"

float UIManager::s_uiSize;
int UIManager::s_lastWindowWidth;
int UIManager::s_lastWindowHeight;
RenderTexture2D UIManager::s_uiRenderTexture;
bool UIManager::s_inUIMode;

void UIManager::Init()
{
	SetUISize(1.0f);
	s_lastWindowWidth = 0;
	s_lastWindowHeight = 0;
	s_inUIMode = false;
}

void UIManager::SetUISize(float uiSize)
{
	s_uiSize = uiSize;
}
void UIManager::HandleResizing()
{
	bool hasWindowChanged = GetScreenWidth() != s_lastWindowWidth || GetScreenHeight() != s_lastWindowHeight;
	if (hasWindowChanged)
	{
		s_lastWindowWidth = GetScreenWidth();
		s_lastWindowHeight = GetScreenHeight();
		printf("Window width = %d, Height = %d\n", s_lastWindowWidth, s_lastWindowHeight);

		// Check UI RT size to reference resolution, if it's more than double, then bump up the scale
		float referenceWidth = 800;
		float referenceHeight = 450;
		float scaledW = GetScreenWidth() * s_uiSize;
		float scaledH = GetScreenHeight() * s_uiSize;
		if (scaledW > referenceWidth * 2.0f || scaledH > referenceHeight * 2.0f)
		{
			SetScaleFactor(fminf(GetScaleFactor() * 2.0f, 8.0f));
		}
		else if (scaledW < referenceWidth * 0.75 || scaledH < referenceHeight * 0.75f)
		{
			SetScaleFactor(fmaxf(GetScaleFactor() * 0.5f, 1.0f));
		}
	}
}
void UIManager::StartUI()
{
#if PLATFORM_WEB // SetMouseScale doesn't seem to work on WebGL so.... no UI scaling in Web?
	s_lastWindowWidth = GetScreenWidth();
	s_lastWindowHeight = GetScreenHeight();
	return;
#endif
	if (s_inUIMode)
	{
		printf("We're already in UI mode...\n");
		return;
	}
	HandleResizing();

	DrawUtils::CheckFullscreenRenderTexture(s_uiRenderTexture, s_uiSize);
	BeginTextureMode(s_uiRenderTexture);
	ClearBackground({ 0, 0, 0, 0 });

	// Yeaaaah the reason we use mouse scale rather than handle scaling ourselves, is because raygui will call GetMousePosition itself...
	// However, SetMouseScale doesn't seem to work on WebGL so.... no UI scaling in Web?
	SetMouseScale(s_uiSize, s_uiSize);
	s_inUIMode = true;
}
void UIManager::EndUI()
{
#if PLATFORM_WEB
	s_lastWindowWidth = GetScreenWidth();
	s_lastWindowHeight = GetScreenHeight();
	return;
#endif
	if (!s_inUIMode)
	{
		printf("We might have quit early from UI mode...\n");
		return;
	}
	EndTextureMode();
	DrawUtils::DrawRenderTargetStretched(s_uiRenderTexture.texture, 0, 0,
		(int)(s_uiRenderTexture.texture.width / s_uiSize),
		(int)(s_uiRenderTexture.texture.height / s_uiSize), WHITE);

	SetMouseScale(1.0f, 1.0f);
	s_inUIMode = false;
}

void UIManager::SetScaleFactor(float scaleFactor)
{
	SetUISize(1.0f / scaleFactor);
}
float UIManager::GetScaleFactor()
{
	return 1.0f / s_uiSize;
}
float UIManager::GetSizeMultiplier()
{
	return s_uiSize;
}

int UIManager::GetUIWidth()
{
	// GetScreenWidth/Height when in a render texture will return the render texture size since raylib 3.5+ or something?
	// Just use the last known window sizes?
	return (int)(s_lastWindowWidth * s_uiSize);
}
int UIManager::GetUIHeight()
{
	return (int)(s_lastWindowHeight * s_uiSize);
}
Vector2 UIManager::GetUIMousePosition()
{
	return GetMousePosition();
	//return Vector2Scale(GetMousePosition(), s_uiSize);
}
void UIManager::BeginUIScissor(int x, int y, int width, int height)
{
	DrawUtils::BeginScissorModeAlt(x, GetUIHeight() - (y + height), width, height);
}
void UIManager::EndUIScissor()
{
	EndScissorMode();
}