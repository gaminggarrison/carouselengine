#pragma once
#include "raylib.h"

class UIManager
{
	static float s_uiSize;
	static int s_lastWindowWidth;
	static int s_lastWindowHeight;
	static RenderTexture2D s_uiRenderTexture;
	static bool s_inUIMode;

	static void SetUISize(float uiSize);
	static void HandleResizing();
public:
	static void Init();
	static void StartUI();
	static void EndUI();
	static void SetScaleFactor(float scaleFactor);
	static float GetScaleFactor();
	static float GetSizeMultiplier();
	static int GetUIWidth();
	static int GetUIHeight();
	static Vector2 GetUIMousePosition();
	static void BeginUIScissor(int x, int y, int width, int height);
	static void EndUIScissor();
};