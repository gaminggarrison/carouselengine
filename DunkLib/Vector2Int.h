#pragma once

#include "raymath.h"
#include <math.h>
#include "cereal/cereal.hpp"

typedef struct Vector2Int {
	int x;
	int y;
	Vector2Int()
	{
		x = 0;
		y = 0;
	}
	Vector2Int(Vector2 v)
	{
		// Floor the values https://www.codeproject.com/Tips/700780/Fast-floor-ceiling-functions as we're finding the grid square that contains the Vector2
		x = (int)v.x;
		if (x > v.x)
		{
			x--;
		}
		y = (int)v.y;
		if (y > v.y)
		{
			y--;
		}
	}
	Vector2Int(int x_in, int y_in)
	{
		x = x_in;
		y = y_in;
	}
	Vector2 ToVector2()
	{
		return { (float)x, (float)y };
	}
	bool IsZero()
	{
		return x == 0 && y == 0;
	}
	template<class Archive>
	void serialize(Archive& archive)
	{
		archive(CEREAL_NVP(x), CEREAL_NVP(y));
	}
} Vector2Int;
RMDEF bool operator== (const Vector2Int& first, const Vector2Int& second)
{
	return first.x == second.x && first.y == second.y;
}
RMDEF bool operator!= (const Vector2Int& first, const Vector2Int& second)
{
	return first.x != second.x || first.y != second.y;
}


#if defined(__TINYC__)
#define RMDEF static inline // plain inline not supported by tinycc (See issue #435)
#else
#define RMDEF inline        // Functions may be inlined or external definition used
#endif

RMDEF Vector2Int Vector2IntSign(Vector2Int v)
{
	int signX = 0;
	if (v.x < 0)
	{
		signX = -1;
	}
	else if (v.x > 0)
	{
		signX = 1;
	}
	int signY = 0;
	if (v.y < 0)
	{
		signY = -1;
	}
	else if (v.y > 0)
	{
		signY = 1;
	}
	return { signX, signY };
}

RMDEF bool Vector2IntEquals(Vector2Int v1, Vector2Int v2)
{
	return v1.x == v2.x && v1.y == v2.y;
}

// Vector with components value 0.0f
RMDEF Vector2Int Vector2IntZero(void)
{
	Vector2Int result = { 0, 0 };
	return result;
}

// Vector with components value 1.0f
RMDEF Vector2Int Vector2IntOne(void)
{
	Vector2Int result = { 1, 1 };
	return result;
}

// Add two vectors (v1 + v2)
RMDEF Vector2Int Vector2IntAdd(Vector2Int v1, Vector2Int v2)
{
	Vector2Int result = { v1.x + v2.x, v1.y + v2.y };
	return result;
}

// Subtract two vectors (v1 - v2)
RMDEF Vector2Int Vector2IntSubtract(Vector2Int v1, Vector2Int v2)
{
	Vector2Int result = { v1.x - v2.x, v1.y - v2.y };
	return result;
}

// Calculate vector length
RMDEF int Vector2IntManhattenDistance(Vector2Int v)
{
	int result = abs(v.x) + abs(v.y);
	return result;
}

// Calculate distance between two vectors
RMDEF int Vector2IntManhattenDistance(Vector2Int v1, Vector2Int v2)
{
	int result = abs(v1.x - v2.x) + abs(v1.y - v2.y);
	return result;
}

// Scale vector (multiply by value)
RMDEF Vector2Int Vector2IntScale(Vector2Int v, int scale)
{
	Vector2Int result = { v.x*scale, v.y*scale };
	return result;
}

// Multiply vector by vector
RMDEF Vector2Int Vector2IntMultiplyV(Vector2Int v1, Vector2Int v2)
{
	Vector2Int result = { v1.x*v2.x, v1.y*v2.y };
	return result;
}

// Negate vector
RMDEF Vector2Int Vector2IntNegate(Vector2Int v)
{
	Vector2Int result = { -v.x, -v.y };
	return result;
}

// Divide vector by a float value
RMDEF Vector2Int Vector2IntDivide(Vector2Int v, int div)
{
	Vector2Int result = { v.x / div, v.y / div };
	return result;
}

// Divide vector by vector
RMDEF Vector2Int Vector2IntDivideV(Vector2Int v1, Vector2Int v2)
{
	Vector2Int result = { v1.x / v2.x, v1.y / v2.y };
	return result;
}
RMDEF Vector2Int operator+ (const Vector2Int& first, const Vector2Int& second)
{
	return Vector2Int(first.x + second.x, first.y + second.y);
}
RMDEF Vector2Int operator- (const Vector2Int& first, const Vector2Int& second)
{
	return Vector2Int(first.x - second.x, first.y - second.y);
}
RMDEF Vector2Int operator* (const Vector2Int& first, const Vector2Int& second)
{
	return Vector2Int(first.x * second.x, first.y * second.y);
}
RMDEF Vector2Int operator/ (const Vector2Int& first, const Vector2Int& second)
{
	return Vector2Int(first.x / second.x, first.y / second.y);
}