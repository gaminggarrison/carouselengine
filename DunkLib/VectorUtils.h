#pragma once

namespace VectorUtils
{
	template<typename T, typename V>
	bool HasElement(T element, V& vector)
	{
		for (T& t : vector)
		{
			if (t == element)
			{
				return true;
			}
		}
		return false;
	}

	// Const version
	template<typename T, typename V>
	bool HasElement(T element, const V& vector)
	{
		for (const T& t : vector)
		{
			if (t == element)
			{
				return true;
			}
		}
		return false;
	}

	// Pointer version
	template<typename T, typename V>
	bool HasElementPointer(T* element, V& vector)
	{
		for (T* t : vector)
		{
			if (t == element)
			{
				return true;
			}
		}
		return false;
	}

	template<typename T, typename V>
	bool AddElementIfNotContained(T element, V& vector)
	{
		bool isThere = false;
		for (T& t : vector)
		{
			if (t == element)
			{
				isThere = true;
				break;
			}
		}
		if (!isThere)
		{
			vector.push_back(element);
			return true;
		}
		return false;
	}

	template<typename T, typename V>
	int GetIndex(T element, V& vector)
	{
		for (int i = 0; i < vector.size(); i++)
		{
			if (vector[i] == element)
			{
				return i;
			}
		}
		return -1;
	}

	template<typename T, typename V>
	int GetIndexPointer(T* element, V& vector)
	{
		for (int i = 0; i < vector.size(); i++)
		{
			if (&vector[i] == element)
			{
				return i;
			}
		}
		return -1;
	}
}