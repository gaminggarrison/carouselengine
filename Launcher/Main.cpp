//#define TESTING_WEB // For testing

//------------------------------------------------------------------------------------
// Global Variables Declaration
//------------------------------------------------------------------------------------
static const int screenWidth = 900;
static const int screenHeight = 800;

#if defined PLATFORM_WEB || defined TESTING_WEB
#include "raylib.h"
#include <exception>
#include "../../GameplayCode/src/Game.h"

#if defined(PLATFORM_WEB)
    #include <emscripten/emscripten.h>
	#include <stdio.h>
	#include <iostream>
	#include <emscripten/bind.h>
	#include <emscripten/val.h>
#endif

//------------------------------------------------------------------------------------
// Module Functions Declaration (local)
//------------------------------------------------------------------------------------
static void InitGame(void);         // Initialize game
static void UpdateGame(void);       // Update game (one frame)
static void DrawGame(void);         // Draw game (one frame)
static void UnloadGame(void);       // Unload game
static void UpdateDrawFrame(void);  // Update and Draw (one frame)

Game* s_game;

//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
int main(void)
{
    // Initialization (Note windowTitle is unused on Android)
    //---------------------------------------------------------
	SetConfigFlags(FLAG_WINDOW_RESIZABLE | FLAG_MSAA_4X_HINT | FLAG_WINDOW_ALWAYS_RUN);
    InitWindow(screenWidth, screenHeight, "Call SetWindowTitle to change");

	InitGame();
	

#if defined(PLATFORM_WEB)
    emscripten_set_main_loop(UpdateDrawFrame, 60, 1);
#else
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
	
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update and Draw
        //----------------------------------------------------------------------------------
		UpdateDrawFrame();
		emscripten_sleep(1);
        //----------------------------------------------------------------------------------
    }
#endif
    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadGame();         // Unload loaded data (textures, sounds, models...)
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}

//------------------------------------------------------------------------------------
// Module Functions Definitions (local)
//------------------------------------------------------------------------------------

// Initialize game variables
void InitGame(void)
{
	//s_game = nullptr;
	
	s_game = new Game();
	s_game->Init(-1);
	printf("Game being null == %d\n", (s_game == nullptr) ? 1 : 0);
}

// Update game (one frame)
void UpdateGame(void)
{
	if (IsKeyPressed(KEY_SPACE))
	{
		throw new std::invalid_argument("test error");
	}
}

// Draw game (one frame)
void DrawGame(void)
{
    BeginDrawing();

        ClearBackground(RAYWHITE);
		DrawText("Something", 100, 100, 24, BLACK);
        
    EndDrawing();
}

// Unload game variables
void UnloadGame(void)
{
	printf("Calling Unload Game!\n");
    // TODO: Unload all dynamic loaded data (textures, sounds, models...)
	if (s_game != nullptr)
	{
		s_game->Cleanup();
		delete s_game;
		s_game = nullptr;
	}
}

// Update and Draw (one frame)
void UpdateDrawFrame(void)
{
	if (s_game != nullptr)
	{
		int result = s_game->Run();
		if (result != 0)
		{
			UnloadGame();
			CloseWindow();
		}
	}
	else
	{
		UpdateGame();
		DrawGame();
	}
}
#else
#include "Main.h"

#include "raylib.h"
#include <chrono> // For sleeping to let the first one load in if running multiple instances
#include <thread>
using namespace std;

#ifndef NO_HOT_RELOAD
#include "../DunkLib/HotReloadBinding.h"
#include "../DunkLib/HotReloadLib.hpp"
#endif
#include <vector>

void GetArgumentValue(std::string argument, std::string name, int& out)
{
	std::string searchString = std::string("-") + name + std::string("=");
	size_t pos = argument.find(searchString);
	if (pos != std::string::npos)
	{
		size_t valueStartPos = pos + searchString.length();
		std::string subString = argument.substr(valueStartPos, argument.length() - valueStartPos);
		int value = stoi(subString);
		out = value;
	}
}

int main(int argc, char** argv)
{
	Main main = Main();

	std::vector<std::string> arguments;
	int debugInstance = -1;
	for (int i = 0; i < argc; i++)
	{
		std::string argument = argv[i];
		arguments.push_back(argument);

		GetArgumentValue(argument, std::string("debugInstance"), debugInstance);
	}


	if (debugInstance >= 0)
	{
		// Give time for the first instance to load in so we don't contend for the dll
		std::this_thread::sleep_for(std::chrono::milliseconds(1000 * debugInstance));
	}

#ifndef NO_HOT_RELOAD
	const char* sourceDLLName = "GameplayCode.dll";
	game_code gameCode = LoadGameCodeUntilValid(sourceDLLName);
#else
	game_code gameCode;
	gameCode.UpdateAndRender = GameUpdateAndRender;
	gameCode.Shutdown = GameShutdown;
#endif

	Memory memory = Memory(1024 * 1024 * 1024);

	SetConfigFlags(FLAG_WINDOW_RESIZABLE | FLAG_MSAA_4X_HINT | FLAG_WINDOW_ALWAYS_RUN);
	InitWindow(screenWidth, screenHeight, "Call SetWindowTitle to change");
	Image icon = LoadImage("Assets/Icon.png");
	SetWindowIcon(icon);
	if (debugInstance >= 0 && (debugInstance % 2) == 1)
	{
		SetWindowPosition(2000, 160);
	}
	SetTargetFPS(60);
	SetExitKey(0);

	bool runResult = 0;
	while (runResult == 0 && !WindowShouldClose())
	{
#ifndef NO_HOT_RELOAD
		// TODO:
		// If new file detected, copy it and try loading it until it works :o)
		if (IsKeyPressed(KEY_F1) && debugInstance >= 0)
		{
			//FILETIME newDLLWriteTime = Win32GetLastWriteTime(sourceDLLName);
			//if (CompareFileTime(&newDLLWriteTime, &gameCode.DllLastWriteTime) != 0)
			{
				system("cls");
				cout << "Reloading!\n";
				if (gameCode.GameCodeDLL)
				{
					gameCode.PrepareForReload(&memory);
				}
				UnloadGameCode(&gameCode);
				gameCode = LoadGameCode(sourceDLLName);
			}
		}
#endif
		runResult = gameCode.UpdateAndRender(&memory, debugInstance, false);
	}
	gameCode.Shutdown(&memory);
	CloseWindow(); // Shiii - raylib has name collisions with windows.h!  Maybe ignore this for now?  Otherwise have to rebuild raylib with different names...

	//cout << "___________________________\nPress enter to quit";
	//cin.get();

#ifndef NO_HOT_RELOAD
	UnloadGameCode(&gameCode);
#endif
	return 0;
}
#endif