#apt-get update
#apt-get install -y ninja-build
#echo "Changing directory to $1"
#cd "$1"
rm -rf Build/CMakeCache.txt Build/CMakeFiles/
#cmake -H. -S CarouselEngine -Bbuild -DPLATFORM=Web -GNinja "-DCMAKE_EXE_LINKER_FLAGS=-s USE_GLFW=3" -DGameName="$1" -DCMAKE_BUILD_TYPE=Release #-DCMAKE_TOOLCHAIN_FILE=emscripten.cmake -DCMAKE_VERBOSE_MAKEFILE=ON
#cmake --build build
emcmake cmake -S CarouselEngine -Bbuild -DPLATFORM=Web -DCMAKE_BUILD_TYPE=Release -DGameName="$1" -GNinja # -DCMAKE_VERBOSE_MAKEFILE=ON
cmake --build build
cd Build/Release/bin
gzip -c index.wasm > index.wasm.gz
gzip -c index.js > index.js.gz
gzip -c nbnet_bundle.js > nbnet_bundle.js.gz
#cd build
#make