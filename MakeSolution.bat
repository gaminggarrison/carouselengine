setlocal
echo "Making Solution for %1"
REM Should be called with the name of the game folder
del CMakeSolution\CMakeCache.txt
del /f /s /q CMakeSolution\CMakeFiles\ 1>nul
rmdir /s /q CMakeSolution\CMakeFiles\
cmake -H. -S CarouselEngine -BCMakeSolution -G "Visual Studio 15 Win64" -DGameName="%1"
REM cmake -H. -BCMakeSolution -G "Visual Studio 15" -DGameName="%1"
pause