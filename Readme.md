# Carousel

Carousel is a library for making multiplayer PC and/or Web games in C++.
It allows a multiplayer game to be made without worrying about network logic - it uses a highly optimised determinstic rollback system to copy and recompute predicted frame states, only needing to send player actions.
Mid-game join can be supported by implementing simple Cereal functions for custom data types.

This library is indended to be used as a git submodule directly below a main project folder, and has scripts that can be called from the parent directory for compilation tasks.

Gameplay code lives in the Parent/GameplayCode directory.

Build scripts can generate Visual Studio 2017 projects and webassembly builds using Emscripten.

## Dependencies:
- **CMake** if you want to generate Visual Studio solution files (they will generate inside a CMakeSolution folder in the parent directory)
- Code dependencies (Raylib, ENet, NBNet) are included as nested submodules.  Serialization is done using a built-in copy of the Cereal library.

## Extra dependencies for web builds:
- **Docker** (with the CarouselEngine docker file mounted) to run the emscripten build process (so you don't need to install emscripten and dependencies directly)
- **Node.js** to run the dedicated web-game server and http web server that serves up the client for local testing

## Deployment:
Web builds will be created in a Parent/build/Release/bin folder.  Windows builds can be created in Visual Studio.  Mac or Linux would currently need some CMake adjustment.

## License:
This library (ZLib license), and all dependencies can be used for commercial use.

Raylib: ZLib
NBNet: Zlib
ENet: http://enet.bespin.org/License.html
Cereal: BSD http://opensource.org/licenses/BSD-3-Clause