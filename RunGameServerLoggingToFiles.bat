echo "Running Game Server %1 in mode: %2 - logging to server.log and server_error.log"
REM npm will load the package.json, and run whatever target is called server
cd Build/%2/bin
REM call npm run server
call npm run server > server.log 2> server_error.log
REM call npm run --prefix Build/Release/bin server
pause